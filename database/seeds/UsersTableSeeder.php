<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        DB::table('users')->insert([
            'nombre'     => "ADMIN",
            'apellido'     => "ADMIN",
            'dni'     => "20362122",
            'email'    => 'admin@admin.com',
            'telefono'    => '1234567890',
            'clave'    => '12345',
            'password' => bcrypt('123456'),
            'cargo_id' => 1, //se supone que 1 debe ser administrador de sistemas
            'rol_id' => 1 //se supone que 1 debe ser super admin
        ]);
        DB::table('users')->insert([
            'nombre'     => "USUARIO",
            'apellido'     => "USUARIO",
            'dni'     => "19980219",
            'email'    => 'usuario@usuario.com',
            'telefono'    => '1234567890',
            'clave'    => '12345',
            'password' => bcrypt('123456'),
            'cargo_id' => 2, //se supone que 1 debe ser administrador de sistemas
            'rol_id' => 2 //se supone que 2 debe ser usuario
        ]);
        DB::table('users')->insert([
            'nombre'     => "VISITANTE",
            'apellido'     => "VISITANTE",
            'dni'     => "20991521",
            'email'    => 'visitante@visitante.com',
            'telefono'    => '1234567890',
            'clave'    => '12345',
            'password' => bcrypt('123456'),
            'cargo_id' => 3, //se supone que 1 debe ser administrador de sistemas
            'rol_id' => 3 //se supone que 3 debe ser visitante
        ]);
    }
}
