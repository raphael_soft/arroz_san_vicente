<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('currencies')->insert([
            'name'     => "US Dollars",
            'symbol'    => '$',
            'code' => 'USD'
        ]);
        DB::table('currencies')->insert([
            'name'     => "Pesos Ecuatorianos",
            'symbol'    => '$',
            'code' => 'PEC'
        ]);
        
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Unidades",
            'simbolo'    => 'Und'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Kilogramos",
            'simbolo'    => 'Kg'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Gramos",
            'simbolo'    => 'Gr'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Toneladas",
            'simbolo'    => 'Tn'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Toneladas Metricas",
            'simbolo'    => 'TM'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Galones",
            'simbolo'    => 'Gal'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Barriles",
            'simbolo'    => 'Bar'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Litros",
            'simbolo'    => 'Lt'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Mililitros",
            'simbolo'    => 'Ml'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Onzas",
            'simbolo'    => 'Oz'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Miligramos",
            'simbolo'    => 'Mlg'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Centimetros cubicos",
            'simbolo'    => 'Cm3'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Pies",
            'simbolo'    => 'Ft'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Pulgadas",
            'simbolo'    => 'Pulg'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Metros",
            'simbolo'    => 'Mt'
        ]);
        DB::table('tipo_mediciones')->insert([
            'nombre'     => "Centimetros",
            'simbolo'    => 'Cm'
        ]);
        DB::table('ivas')->insert([
            'nombre'     => "IVA_1",
            'valor'    => 12.00
        ]);
        DB::table('ivas')->insert([
            'nombre'     => "IVA_2",
            'valor'    => 14.00
        ]);
        DB::table('ivas')->insert([
            'nombre'     => "IVA_3",
            'valor'    => 9.00
        ]);
        DB::table('sistema')->insert([
            'nombre'     => "Asociacion de Ganaderos Balzar",
            'razon_social'     => "Asociacion de Ganaderos Balzar",
            'direccion'    => 'Direccion',
            'telefono1'    => '123456',
            'email1'    => 'ejemplo@ejemplo.com',
            'nombre_representante'    => 'Nombre Representante',
            'cedula_representante'    => '5464673536',
            'telefono_representante'    => '1232434',
            'ruc'    => '007867785667-2',
            'currencies_id' => 1
        ]);
        DB::table('clientes_ventas')->insert([
            'razon_social'     => "Cliente Generico",
            'direccion'    => 'ECUADOR',
            'telefono'    => '123456',
            'email'    => 'cliente@ejemplo.com',
            'cedula_ruc'    => '5464673536'
        ]);
        DB::table('rol')->insert([
            'id'                => 1,
            'nombre'       => "ADMINISTRADOR DE SISTEMAS",
            'responsabilidades' => "Administrar los sistemas de software",
            'estado'            => 1,
            'ver'   => 1,
            'registrar'   => 1,
            'editar'      => 1,
            'borrar'      => 1,
            'superuser'   => 1
        ]);
        DB::table('rol')->insert([
            'id'                => 2,
            'nombre'       => "USUARIO",
            'responsabilidades' => "",
            'estado'            => 1,
            'ver'   => 1,
            'registrar'   => 1,
            'editar'      => 1
            
        ]);
        DB::table('rol')->insert([
            'id'                => 3,
            'nombre'       => "VISITANTE",
            'responsabilidades' => "",
            'estado'            => 1,
            'ver'   => 1
            
        ]);

        DB::table('cargo')->insert([
            'id'                => 1,
            'nombre'       => "SUPERVISOR",
            'responsabilidades' => "Se encarga de velar porque los procesos se lleven a cabo adecuadamente",
            'estado'            => 1
        ]);
        DB::table('cargo')->insert([
            'id'                => 2,
            'nombre'       => "EMPLEADO NIVEL 1",
            'responsabilidades' => "Ejecuta tareas del tipo administrativas, con alguna limitante respecto al supervisor.",
            'estado'            => 1
        ]);
        DB::table('cargo')->insert([
            'id'                => 3,
            'nombre'       => "EMPLEADO NIVEL 2",
            'responsabilidades' => "Ejecuta tareas diferentes a las administrativas y de mas bajo nivel",
            'estado'            => 1
        ]);
        $this->call(UsersTableSeeder::class);
    }
}
