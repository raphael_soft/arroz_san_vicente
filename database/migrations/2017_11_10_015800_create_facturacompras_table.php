<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateFacturacomprasTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('facturacompras', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            $table->integer('socio_id')->unsigned();

            $table->string('n_orden',50)->unique();

            $table->string('n_factura',50)->nullable(false);

            $table->integer('tipo')->unsigned()->nullable(false);

            $table->double('subtotal')->nullable(false);

            $table->double('monto_iva')->nullable(false);

            $table->double('total')->nullable(false);

            $table->char('status', 1)->default('1');

            $table->date('fecha_facturacion');

            $table->timestamps();

            $table->unique(['n_factura','tipo']);

            $table->foreign('socio_id')->references('id')->on('socios')->onDelete('cascade');

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('facturacompras');

    }

}

