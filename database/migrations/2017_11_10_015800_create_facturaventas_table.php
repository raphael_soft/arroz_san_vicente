<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateFacturaventasTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('facturaventas', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('cliente_id')->unsigned();

            $table->string('n_orden',50)->unique();

            //$table->integer('tipo')->nullable();

            $table->double('subtotal')->nullable();

            $table->double('monto_iva')->nullable();

            $table->double('total')->nullable();

            $table->char('status', 1)->default('1');

            $table->date('fecha_facturacion');

            $table->timestamps();



            $table->foreign('cliente_id')->references('id')->on('clientes_ventas')->onDelete('restrict');

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('facturaventas');

    }

}

