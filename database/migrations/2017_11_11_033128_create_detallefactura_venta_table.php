<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateDetalleFacturaVentaTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('detalle_factura_venta', function (Blueprint $table) {

            $table->integer('factura_id')->unsigned()->nullable(false);

            $table->integer('producto_id')->unsigned()->nullable(false);

            $table->integer('impuesto_id')->unsigned()->nullable(false);

            $table->integer('cantidad')->nullable(false);
            $table->double('precio')->nullable(false);

            $table->timestamps();

            $table->primary(['factura_id','producto_id']);
            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('restrict');
            

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('detallefacturas');

    }

}

