<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateClientesTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('clientes_ventas', function (Blueprint $table) {

            $table->increments('id');

            $table->string('razon_social',100)->nullable(false);

            $table->string('email',50)->nullable(true)->default(null);

            $table->string('telefono',20)->nullable(true)->default(null);
            $table->string('direccion',200)->nullable(true)->default(null);
            $table->string('cedula_ruc',15)->nullable(true)->default(null);
            
            
            $table->unique('cedula_ruc');
            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('socios');

    }

}

