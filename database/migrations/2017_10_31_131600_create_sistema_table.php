<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateSistemaTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('sistema', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('currencies_id')->nullable(false)->unsigned();
            $table->string('nombre',100)->nullable(false)->default('');
            $table->string('razon_social',100)->nullable(false)->default('');
            $table->string('email',50)->nullable(true)->default('');
            $table->string('email2',50)->nullable(true)->default('');
            $table->string('email1',50)->nullable(true)->default('');
            $table->string('twiter',50)->nullable(true)->default('');
            $table->string('web',300)->nullable(true)->default('');
            $table->string('facebook',100)->nullable(true)->default('');
            $table->string('logo1',300)->nullable(true)->default('');
            $table->string('logo2',300)->nullable(true)->default('');
            $table->string('direccion_fiscal',400)->nullable(true)->default('');
            $table->string('telefono',20)->nullable(true)->default('');
            $table->string('telefono1',20)->nullable(true)->default('');
            $table->string('telefono2',20)->nullable(true)->default('');
            $table->string('direccion',400)->nullable(true)->default('');
            $table->string('membrete',400)->nullable(true)->default('');
            $table->string('telefono_representante',20)->nullable(true)->default('');
            $table->string('nombre_representante',100)->nullable(true)->default('');
            $table->string('direccion_representante',400)->nullable(true)->default('');
            $table->string('cedula_representante',15)->nullable(true)->default('');
            $table->string('ruc',20)->nullable(true)->default('');
            $table->boolean('show_nombre')->default(1)->nullable(false);
            $table->boolean('show_membrete')->default(1)->nullable(false);
            $table->boolean('show_razon_social')->default(1)->nullable(false);
            $table->boolean('show_email1')->default(1)->nullable(false);
            $table->boolean('show_email2')->default(0)->nullable(false);
            $table->boolean('show_twiter')->default(0)->nullable(false);
            $table->boolean('show_facebook')->default(0)->nullable(false);
            $table->boolean('show_telefono1')->default(1)->nullable(false);
            $table->boolean('show_telefono2')->default(0)->nullable(false);
            $table->boolean('show_web')->default(0)->nullable(false);
            $table->boolean('show_logo1')->default(0)->nullable(false);
            $table->boolean('show_logo2')->default(0)->nullable(false);
            $table->boolean('show_logo1_repo')->default(0)->nullable(false);
            $table->boolean('show_logo2_repo')->default(0)->nullable(false);
            $table->boolean('show_ruc')->default(1)->nullable(false);
            $table->boolean('show_direccion_fiscal')->default(1)->nullable(false);
            $table->integer('last_facturacompra_producciones')->default(0)->nullable(false);
            $table->integer('next_facturacompra_producciones')->default(1)->nullable(false);

            $table->foreign('currencies_id')->references('id')->on('currencies')->onDelete('restrict');
            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('sistema');

    }

}

