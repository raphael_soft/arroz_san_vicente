<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100)->unique();
            $table->string('symbol',10)->default('');
            $table->char('precision', 3)->default('2');
            $table->char('thousand_separator', 255)->default('.');
            $table->char('decimal_separator', 255)->default(',');
            $table->string('code', 10)->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('currencies');
    }
}
