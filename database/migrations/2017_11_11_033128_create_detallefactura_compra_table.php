<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateDetalleFacturaCompraTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('detalle_factura_compra', function (Blueprint $table) {

            $table->string('n_factura',50);

            $table->integer('producto_id')->unsigned();

            $table->integer('impuesto_id')->unsigned();

            $table->integer('cantidad');
            $table->double('precio');

            $table->timestamps();

            $table->primary(['n_factura','producto_id']);
            $table->foreign('n_factura')->references('n_factura')->on('facturacompras')->onDelete('cascade');
            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('restrict');
            

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('detalle_factura_compra');

    }

}

