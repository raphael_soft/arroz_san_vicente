<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100)->nullable(false);
            $table->integer('dni')->unique();
            $table->timestamp('fecha_registro')->useCurrent();
            $table->string('fecha_nacimiento', 20)->default(null)->nullable();
            $table->string('fecha_ingreso', 20)->default(null)->nullable();
            $table->string('apellido', 50)->default(null)->nullable();
            $table->string('direccion', 100)->default(null)->nullable();
            $table->string('ciudad', 50)->default(null)->nullable();
            $table->string('telefono',20)->default(null)->nullable();
            $table->binary('avatar', 800)->default(null)->nullable();
            $table->integer('rol_id')->unsigned()->nullable();
            $table->integer('cargo_id')->unsigned()->nullable();
            $table->string('email', 30)->unique()->default(null)->nullable();
            $table->char('status', 1)->default('1');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('rol_id')->references('id')->on('rol')->onDelete('restrict');
            $table->foreign('cargo_id')->references('id')->on('cargo')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
