<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable(false)->default('');
            $table->string('codigo',50);
            $table->double('precio_venta')->default(0)->nullable(true);
            $table->double('costo_produccion')->default(0)->nullable(true);;
            $table->integer('categoria_id')->unsigned()->nullable(false);
            $table->integer('subcategoria_id')->unsigned()->default(0);
            $table->integer('medicion_id')->unsigned()->default(0);
            $table->integer('iva_id')->unsigned()->default(0);
            $table->char('status', 1)->default('1');
            $table->timestamps();

            //$table->unique(['nombre','categoria_id','subcategoria_id'],'product_cat_subcat');
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->foreign('subcategoria_id')->references('id')->on('subcategorias');
            $table->foreign('medicion_id')->references('id')->on('tipo_mediciones');
            $table->foreign('iva_id')->references('id')->on('ivas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
