<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateSociosTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('socios', function (Blueprint $table) {

            $table->increments('id');

            $table->string('nombre',100)->nullable(false);

            $table->string('email',50)->nullable(true)->default(null);

            $table->string('telefono',20)->nullable(true)->default(null);
            $table->string('direccion',200)->nullable(true)->default(null);
            $table->string('telefono_representante',20)->nullable(true)->default(null);
            $table->string('email_representante',100)->nullable(true)->default(null);
            $table->string('nombre_representante',100)->nullable(true)->default(null);
            $table->string('direccion_representante',200)->nullable(true)->default(null);
            $table->string('cedula_representante',15)->nullable(true)->default(null);
            $table->string('ruc',20)->nullable(true)->default(null);

            $table->string('codigo',50)->nullable(true)->default(null)->unique();

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('socios');

    }

}

