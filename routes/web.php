<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/email_test', function () {
    return view('emails.test');
});
Route::get('/error-acceso', function () {
    return view('auth.error');
});
// home
Route::get('/', 'PanelController@Home')->middleware('auth');
Route::get('/home', 'PanelController@Home')->middleware('auth');
Route::get('/login', 'LoginController@inicio')->name('login');
Route::post('/login', 'LoginController@authenticate');
// inicio de sesion

Route::get('/inicio', 'LoginController@inicio');
Route::post('/inicio', 'LoginController@authenticate');
Route::get('/salir', 'LoginController@logout');

//imagenes subidas
Route::get('/storage/{filename}', function ($filename) 
{
   $path = storage_path('/app/public/' . $filename);
   	//dd($path);
   if (!File::exists($path)) {
      abort(404);
   }

   $file = File::get($path);
   $pos = strpos($filename, ".");
   $type = substr($filename, $pos+1);
   $response = Response::make($file, 200);
   $response->header("Content-Type", 'image/'.$type);

   return $response;
});

// INVENTARIO PRODUCCION
Route::get('/inventario/{tipo}', 'InventarioController@index')->middleware('auth','ver')->name('inventario');


// CONFIGURACION
Route::group(['middleware' => 'auth','middleware' => 'superuser'], function() {

	Route::prefix('configuracion')->group(function () {

		// IVA
	    Route::get('/iva', 'IvaController@index')->middleware('ver');

			Route::prefix('iva')->group(function () {
				// crear
				Route::get('/create', 'IvaController@create')->middleware('registrar');
				// guardar
				Route::post('/create', 'IvaController@store')->middleware('registrar');
				// mostrar
				Route::get('/show/{id}', 'IvaController@show')->middleware('ver');
				// edit
				Route::get('/edit/{id}', 'IvaController@edit')->middleware('editar');
				// update
				Route::post('/edit/{id}', 'IvaController@update')->middleware('editar');
				// destroy
				Route::post('/destroy/{id}', 'IvaController@destroy')->middleware('borrar');
				Route::get('/destroy/{id}', 'IvaController@destroy')->middleware('borrar');
			});
		// cargos
	    Route::get('/cargos', 'CargosController@index')->middleware('ver');

	    	Route::prefix('cargos')->group(function () {
				// crear
				Route::get('/create', 'CargosController@create')->middleware('registrar');
				// guardar
				Route::post('/create', 'CargosController@store')->middleware('registrar');
				// mostrar
				Route::get('/show/{id?}', 'CargosController@show')->middleware('ver');
				// edit
				Route::get('/edit/{id?}', 'CargosController@edit')->middleware('editar');
				// update
				Route::post('/edit/{id?}', 'CargosController@update')->middleware('editar');
				// destroy
				Route::get('/destroy/{id?}', 'CargosController@destroy')->middleware('borrar');
				Route::post('/destroy/{id?}', 'CargosController@destroy')->middleware('borrar');
			});	

	    // ROLES
	    Route::get('/roles', 'RolController@index')->middleware('ver');

	    	Route::prefix('roles')->group(function () {
				// crear
				Route::get('/create', 'RolController@create')->middleware('registrar');
				// guardar
				Route::post('/create', 'RolController@store')->middleware('registrar');
				// mostrar
				Route::get('/show/{id?}', 'RolController@show')->middleware('ver');
				// edit
				Route::get('/edit/{id?}', 'RolController@edit')->middleware('editar');
				// update
				Route::post('/edit/{id?}', 'RolController@update')->middleware('editar');
				// destroy
				Route::get('/destroy/{id?}', 'RolController@destroy')->middleware('borrar');
				Route::post('/destroy/{id?}', 'RolController@destroy')->middleware('borrar');
			});

	    // USUARIOS
	    Route::get('/usuario', 'UsuarioController@index')->middleware('ver');

	    	Route::prefix('usuario')->group(function () {
				// crear
				Route::get('/create', 'UsuarioController@create')->middleware('registrar')->name('create_usuario');
				// guardar
				Route::post('/create', 'UsuarioController@store')->middleware('registrar')->name('store_usuario');
				// mostrar
				Route::get('/show/{id}', 'UsuarioController@show')->middleware('ver')->name('show_usuario');
				// edit
				Route::get('/edit/{id}', 'UsuarioController@edit')->middleware('editar')->name('edit_usuario');
				// update
				Route::post('/edit/{id}', 'UsuarioController@update')->middleware('editar')->name('update_usuario');
				// destroy
				Route::get('/destroy/{id}', 'UsuarioController@destroy')->middleware('borrar')->name('confirm_destroy_usuario');
				Route::post('/destroy/{id}', 'UsuarioController@destroy')->middleware('borrar')->name('destroy_usuario');
			});

	    // PERMISOS
	    Route::get('/permisos', 'PermisoController@index');

	    // CATEGORIAS
	    Route::get('/categorias', 'CategoriaController@index')->middleware('ver');

	    	Route::prefix('categorias')->group(function () {
				// crear
				Route::get('/create', 'CategoriaController@create')->middleware('registrar');
				// guardar
				Route::post('/create', 'CategoriaController@store')->middleware('registrar');
				// mostrar
				Route::get('/show/{id?}', 'CategoriaController@show')->middleware('ver');
				// edit
				Route::get('/edit/{id?}', 'CategoriaController@edit')->middleware('editar');
				// update
				Route::post('/edit/{id?}', 'CategoriaController@update')->middleware('editar');
				// destroy
				Route::get('/destroy/{id?}', 'CategoriaController@destroy')->middleware('borrar');
				Route::post('/destroy/{id?}', 'CategoriaController@destroy')->middleware('borrar');
			});

		// SUBCATEGORIAS
	    Route::get('/subcategorias', 'SubCategoriaController@index')->middleware('ver');

	    	Route::prefix('subcategorias')->group(function () {
				// crear
				Route::get('/create', 'SubCategoriaController@create')->middleware('registrar');;
				// guardar
				Route::post('/create', 'SubCategoriaController@store')->middleware('registrar');
				// mostrar
				Route::get('/show/{id?}', 'SubCategoriaController@show')->middleware('ver');
				// edit
				Route::get('/edit/{id?}', 'SubCategoriaController@edit')->middleware('editar');
				// update
				Route::post('/edit/{id?}', 'SubCategoriaController@update')->middleware('editar');
				// destroy
				Route::get('/destroy/{id?}', 'SubCategoriaController@destroy')->middleware('borrar');
				Route::post('/destroy/{id?}', 'SubCategoriaController@destroy')->middleware('borrar');
			});	

	    // TIPO DE MEDICION
	    Route::get('/mediciones', 'TipoMedicionController@index')->middleware('ver');

	    	Route::prefix('mediciones')->group(function () {
				// crear
				Route::get('/create', 'TipoMedicionController@create')->middleware('registrar');
				// guardar
				Route::post('/create', 'TipoMedicionController@store')->middleware('registrar');
				// mostrar
				Route::get('/show/{id?}', 'TipoMedicionController@show')->middleware('ver');
				// edit
				Route::get('/edit/{id?}', 'TipoMedicionController@edit')->middleware('editar');
				// update
				Route::post('/edit/{id?}', 'TipoMedicionController@update')->middleware('editar');
				// destroy
				Route::post('/destroy/{id}', 'TipoMedicionController@destroy')->middleware('borrar');
				Route::get('/destroy/{id}', 'TipoMedicionController@destroy')->middleware('borrar');
			});

	    
		//MODULO FACTURAS 
		
		Route::get('/facturas', 'FacturasController@index')->middleware('auth','ver');

		Route::post('/facturas', 'FacturasController@store')->middleware('auth','registrar');

		Route::get('/sistema', 'SistemaController@index')->middleware('auth','ver');

		Route::post('/sistema', 'SistemaController@store')->middleware('auth','registrar');

		Route::get('/clientes', 'ClienteController@index')->middleware('auth','ver');

	    	Route::prefix('clientes')->group(function () {
				// crear
				Route::get('/create', 'ClienteController@create')->middleware('registrar');
				// guardar
				Route::post('/create', 'ClienteController@store')->middleware('registrar');
				// mostrar
				Route::get('/show/{id}', 'ClienteController@show')->middleware('ver');
				// edit
				Route::get('/edit/{id}', 'ClienteController@edit')->middleware('editar');
				// update
				Route::post('/edit/{id}', 'ClienteController@update')->middleware('editar');
				// destroy
				Route::post('/destroy/{id}', 'ClienteController@destroy')->middleware('borrar');
				Route::get('/destroy/{id}', 'ClienteController@destroy')->middleware('borrar');
			});
	
	});
});
//cancelar y aprobar

Route::group(['middleware' => 'auth', 'middleware' => 'ver', 'middleware' => 'web'], function() {
	Route::prefix('revisiones')->group(function(){
		Route::get('/index', 'RevisionesController@index')->name('revisiones');
		Route::get('/show/{id}', 'RevisionesController@show')->name('show_revision');
		Route::get('/create', 'RevisionesController@create')->name('create_revision');
		Route::post('/create', 'RevisionesController@store')->name('store_revision');
		Route::get('/edit/{id}', 'RevisionesController@edit')->name('edit_revision');
		Route::post('/edit/{id}', 'RevisionesController@update')->name('update_revision');
		Route::get('/destroy/{id}', 'RevisionesController@destroy')->name('destroy_revision');
	});
	Route::prefix('produccion')->group(function () {
		Route::get('/terrenos', 'ProduccionController@terrenos')->name('terrenos');
		Route::get('/terrenos/create', 'ProduccionController@create_terreno')->name('create_terreno');
		Route::post('/terrenos/create', 'ProduccionController@store_terreno')->name('store_terreno');
		Route::get('/terrenos/{id}', 'ProduccionController@show_terreno')->name('show_terreno');
		Route::post('/terrenos/{id}', 'ProduccionController@show_terreno');
		Route::get('/terrenos/edit/{id}', 'ProduccionController@edit_terreno')->name('edit_terreno');
		Route::post('/terrenos/edit/{id}', 'ProduccionController@update_terreno')->name("update_terreno");
		Route::get('/terrenos/destroy/{id}', 'ProduccionController@destroy_terreno')->name('destroy_terreno');

		Route::get('/insumos', 'ProduccionController@insumos')->name('insumos');
		Route::get('/insumos/asignar', 'ProduccionController@asignar_insumo')->name('asignar_insumo');
		Route::post('/insumos/asignar', 'ProduccionController@store_insumo')->name('store_insumo');
		Route::get('/insumos/{id}', 'ProduccionController@show_insumo')->name('show_insumo');
		Route::get('/insumos/edit/{id}', 'ProduccionController@edit_insumo')->name('edit_insumo');
		Route::post('/insumos/edit/{id}', 'ProduccionController@update_insumo')->name("update_insumo");
		Route::get('/insumos/destroy/{id}', 'ProduccionController@destroy_insumo')->name('destroy_insumo');		

		Route::get('/hectareas', 'ProduccionController@hectareas')->name('hectareas');
		Route::get('/hectareas/create', 'ProduccionController@create_hectarea')->name('create_hectarea');
		Route::post('/hectareas/create', 'ProduccionController@store_hectarea')->name('store_hectarea');
		Route::get('/hectareas/{id}', 'ProduccionController@show_hectarea')->name('show_hectarea');
		Route::get('/hectareas/edit/{id}', 'ProduccionController@edit_hectarea')->name('edit_hectarea');
		Route::post('/hectareas/edit/{id}', 'ProduccionController@update_hectarea')->name("update_edit_hectarea");
		Route::get('/hectareas/destroy/{id}', 'ProduccionController@destroy_hectarea')->name('destroy_hectarea');
		 
		Route::get('/tipos_cosechas', 'ProduccionController@tipos_cosechas')->name('tipos_cosechas');
		Route::get('/tipos_cosechas/create', 'ProduccionController@create_tipo_cosecha')->name('create_tipo_cosecha');
		Route::post('/tipos_cosechas/create', 'ProduccionController@store_tipo_cosecha')->name('store_tipo_cosecha');
		Route::get('/tipos_cosechas/{id}', 'ProduccionController@show_tipo_cosecha')->name('show_tipo_cosecha');
		Route::get('/tipos_cosechas/edit/{id}', 'ProduccionController@edit_tipo_cosecha')->name('edit_tipo_cosecha');
		Route::post('/tipos_cosechas/edit/{id}', 'ProduccionController@update_tipo_cosecha')->name("update_tipo_cosecha");
		Route::get('/tipos_cosechas/destroy/{id}', 'ProduccionController@destroy_tipo_cosecha')->name('destroy_tipo_cosecha');

		Route::get('/cosechas', 'ProduccionController@cosechas')->name('cosechas');
		Route::get('/cosechas/create', 'ProduccionController@create_cosecha')->name('create_cosecha');
		Route::post('/cosechas/create', 'ProduccionController@store_cosecha')->name('store_cosecha');
		Route::get('/cosechas/{id}', 'ProduccionController@show_cosecha')->name('show_cosecha');
		Route::get('/cosechas/edit/{id}', 'ProduccionController@edit_cosecha')->name('edit_cosecha');
		Route::post('/cosechas/edit/{id}', 'ProduccionController@update_cosecha')->name("update_cosecha");
		Route::get('/cosechas/destroy/{id}', 'ProduccionController@destroy_cosecha')->name('destroy_cosecha');

		Route::get('/calificaciones', 'ProduccionController@calificaciones')->name('calificaciones');
		Route::get('/calificaciones/create', 'ProduccionController@create_calificacion')->name('create_calificacion');
		Route::post('/calificaciones/create', 'ProduccionController@store_calificacion')->name('store_calificacion');
		Route::get('/calificaciones/{id}', 'ProduccionController@show_calificacion')->name('show_calificacion');
		Route::get('/calificaciones/edit/{id}', 'ProduccionController@edit_calificacion')->name('edit_calificacion');
		Route::post('/calificaciones/edit/{id}', 'ProduccionController@update_calificacion')->name("update_calificacion");
		Route::get('/calificaciones/destroy/{id}', 'ProduccionController@destroy_calificacion')->name('destroy_calificacion');

		Route::get('/estadisticas/cosechas', 'ProduccionController@estadistica_cosechas')->name('estadistica_cosechas');
		Route::post('/estadisticas/cosechas', 'ProduccionController@estadistica_cosechas')->name('estadistica_cosechas');
		Route::get('/estadisticas/calificaciones', 'ProduccionController@estadistica_calificaciones')->name('estadistica_calificaciones');
		Route::post('/estadisticas/calificaciones', 'ProduccionController@estadistica_calificaciones')->name('estadistica_calificaciones');
	});
});

// FACTURAS DE VENTAS
Route::get('/facturas_ventas', 'FacturaventaController@index')->middleware('auth','ver');
Route::get('/ventas_por_cobrar', 'FacturaventaController@index_por_cobrar')->middleware('auth','ver');
Route::get('/ventas_abonos', 'FacturaventaController@index_abonos')->middleware('auth','ver');

Route::group(['middleware' => 'auth', 'middleware' => 'ver'], function() {
	Route::prefix('estadisticas')->group(function () {
		Route::get('/ingreso', 'EstadisticaController@ingreso');
		Route::post('/ingreso', 'EstadisticaController@ingreso');
		// 
		Route::get('/egreso', 'EstadisticaController@egreso');
		Route::post('/egreso', 'EstadisticaController@egreso');
		// 
		Route::get('/produccion', 'EstadisticaController@produccion');
		Route::post('/produccion', 'EstadisticaController@get_produccion');
	});
});

//MODULO SOCIO

	//INDEX
	Route::get('/socios', 'SocioController@index')->middleware('auth','ver');

	//CREATE
	Route::get('/create_socio', 'SocioController@create')->middleware('auth','registrar');
	Route::post('/create_socio', 'SocioController@store')->middleware('auth','registrar');
		
	//EDIT
	Route::get('/edit_socio/{id}', 'SocioController@edit')->middleware('auth','editar');
	Route::post('/edit_socio/{id}', 'SocioController@update')->middleware('auth','editar');
		
	// SHOW
	Route::get('/show_socio/{id}', 'SocioController@show')->middleware('auth','ver');

	// DELETE
	Route::get('/destroy_socio/{id}', 'SocioController@destroy')->middleware('auth','borrar');


//MODULO PRODUCTOS

	//ROUTES PRODUCTOS
	
	//INDEX
	
	Route::get('/conf_productos/{tipo?}', 'ProductoController@index')->middleware('auth','ver')->name('productos');

	//SHOW
	Route::get('/show_producto/{id}', 'ProductoController@show')->middleware('auth','ver');

	//EDIT
	Route::get('/edit_producto/{id}', 'ProductoController@edit')->middleware('auth','editar');
	Route::post('/edit_producto/{id}', 'ProductoController@update')->middleware('auth','editar');

	//CREATE GET
	Route::get('/create_producto', 'ProductoController@create')->middleware('auth','registrar');
	Route::post('/create_producto', 'ProductoController@store')->middleware('auth','registrar');

	//DESTROY
	Route::get('/destroys/{id}', 'ProductoController@destroys')->middleware('auth','borrar');


//MODULO FACTURACION COMPRAS

	//INDEX
	Route::get('/facturas_compras/{tipo?}/{from?}/{to?}', 'FacturaCompraController@index')->middleware('auth','ver')->name('compras');
	//Route::post('/facturas_compras/', 'FacturaCompraController@index')->middleware('auth','ver');
	//Route::get('/facturas_compras_productos', 'FacturacompraController@index_productos')->middleware('auth');
	Route::get('imprimir-ventas/{fein?}/{fefi?}', ['as' => 'imprimir-ventas','uses' => 'EstadisticaController@imprimirVentas'])->middleware('auth');
	Route::get('imprimir-compras/{fein?}/{fefi?}/{tipo?}', ['as' => 'imprimir-compras','uses' => 'EstadisticaController@imprimirCompras'])->middleware('auth');
	//SHOW
	Route::get('/show_facturacompra/{id}', 'FacturaCompraController@show')->middleware('auth','ver');
	Route::get('/show_abonos_facturaventa/{id}', 'FacturaventaController@show_abonos')->middleware('auth','ver');
	Route::get('/show_abono/{id}', 'FacturaventaController@show_abono')->middleware('auth','ver');
	Route::get('/show_facturacompra/{id}', 'FacturaCompraController@show')->middleware('auth','ver');
	Route::get('/show_facturaventa/{id}', 'FacturaventaController@show')->middleware('auth','ver');
	Route::get('/imprimir_facturaventa/{id}', 'FacturaventaController@imprimir')->middleware('auth','ver');
	Route::get('/imprimir_abonos_facturaventa/{id}', 'FacturaventaController@imprimirAbonos')->middleware('auth','ver');
	Route::get('/imprimir_abono/{id}', 'FacturaventaController@imprimirAbono')->middleware('auth','ver');
	Route::get('/imprimir_facturacompra/{id}', 'FacturaCompraController@imprimir')->middleware('auth','ver');
	//EDIT
	Route::get('/edit_facturacompra/{id}', 'FacturaCompraController@edit')->middleware('auth','editar');
	Route::post('/edit_facturacompra/{id}', 'FacturaCompraController@update')->middleware('auth','editar');

	//CREATE GET
	Route::get('/create_facturacompra/{tipo}', 'FacturaCompraController@create')->name('create_facturacompra')->middleware('auth','registrar');
	Route::post('/create_facturacompra/{tipo}', 'FacturaCompraController@invoice')->middleware('auth','ver');
	Route::post('/finish_facturacompra/', 'FacturaCompraController@store')->middleware('auth','registrar');
	Route::get('/aprobar_factura/{id}', 'FacturaCompraController@aprobar')->middleware('auth','registrar');
	Route::get('/cancelar_factura/{id}', 'FacturaCompraController@cancelar')->middleware('auth','registrar');
	//CREATE GET
	Route::get('/create_facturaventa', 'FacturaventaController@create')->name('create_facturaventa')->middleware('auth','registrar');
	Route::post('/create_facturaventa', 'FacturaventaController@invoice')->middleware('auth','ver');
	Route::get('/create_abono', 'FacturaventaController@create_abono')->middleware('auth','registrar');
	Route::post('/create_abono', 'FacturaventaController@store_abono')->middleware('auth','registrar');
	Route::post('/finish_facturaventa/', 'FacturaventaController@store')->middleware('auth','registrar');
	Route::get('/anular_facturaventa/{id}', 'FacturaventaController@anular')->middleware('auth','superuser');
	
//MODULO ESTADISTICAS COMPRAS
		
	Route::get('/estadistica_compra/{from?}/{to?}/{tipo?}', 'EstadisticaController@compras')->middleware('auth','ver');
	Route::post('/estadistica_compra', 'EstadisticaController@get_compras')->middleware('auth','ver');

	Route::get('/estadistica_compra_por_productos/{from?}/{to?}', 'EstadisticaController@get_estadisticasCompras')->middleware('auth','ver');
	Route::post('/estadistica_compra_por_productos/', 'EstadisticaController@get_estadisticasCompras')->middleware('auth','ver');

	Route::get('/estadistica_compra_por_tipo_prod/{from?}/{to?}', 'EstadisticaController@estadisticasCompras2')->middleware('auth','ver');
	Route::post('/estadistica_compra_por_tipo_prod/', 'EstadisticaController@get_estadisticasCompras2')->middleware('auth','ver');

	Route::get('/estadistica_compra_por_cat_subcat/{from?}/{to?}', 'EstadisticaController@estadisticasCompras3')->middleware('auth','ver');
	Route::post('/estadistica_compra_por_cat_subcat/', 'EstadisticaController@get_estadisticasCompras3')->middleware('auth','ver');

	Route::get('/estadistica_compra_por_cat_subcat2/{from?}/{to?}', 'EstadisticaController@get_estadisticasCompras4')->middleware('auth','ver');
	Route::post('/estadistica_compra_por_cat_subcat2/', 'EstadisticaController@get_estadisticasCompras4')->middleware('auth','ver');

	Route::get('/estadistica_compra_subtotales_diarios/{from?}/{to?}', 'EstadisticaController@get_estadisticasCompras5')->middleware('auth','ver');
	Route::post('/estadistica_compra_subtotales_diarios/', 'EstadisticaController@get_estadisticasCompras5')->middleware('auth','ver');

	Route::get('/estadistica_compra_plain/{from?}/{to?}', 'EstadisticaController@estadisticasCompras6')->middleware('auth','ver');
	Route::post('/estadistica_compra_plain/', 'EstadisticaController@get_estadisticasCompras6')->middleware('auth','ver');

	Route::get('/estadistica_compra_por_socios/{from?}/{to?}', 'EstadisticaController@get_estadisticasCompras7')->middleware('auth','ver');
	Route::post('/estadistica_compra_por_socios/', 'EstadisticaController@get_estadisticasCompras7')->middleware('auth','ver');
//MODULO ESTADISTICAS VENTAS
		
	Route::get('/estadisticas_ventas/{from?}/{to?}', 'EstadisticaController@ventas')->middleware('auth','ver');
	Route::post('/estadisticas_ventas', 'EstadisticaController@get_ventas')->middleware('auth','ver');

	Route::get('/estadistica_venta_por_productos1/{from?}/{to?}', 'EstadisticaController@estadisticasVentas')->middleware('auth','ver');
	Route::post('/estadistica_venta_por_productos1/', 'EstadisticaController@get_estadisticasVentas')->middleware('auth','ver');

	Route::get('/estadistica_venta_por_productos2/{from?}/{to?}', 'EstadisticaController@estadisticasVentas2')->middleware('auth','ver');
	Route::post('/estadistica_venta_por_productos2/', 'EstadisticaController@get_estadisticasVentas2')->middleware('auth','ver');

	Route::get('/estadistica_venta_por_cat/{from?}/{to?}', 'EstadisticaController@estadisticasVentas3')->middleware('auth','ver');
	Route::post('/estadistica_venta_por_cat/', 'EstadisticaController@get_estadisticasVentas3')->middleware('auth','ver');

	Route::get('/estadistica_venta_por_cat_subcat/{from?}/{to?}', 'EstadisticaController@estadisticasVentas4')->middleware('auth','ver');
	Route::post('/estadistica_venta_por_cat_subcat/', 'EstadisticaController@get_estadisticasVentas4')->middleware('auth','ver');

	Route::get('/estadistica_venta_productos3/{from?}/{to?}', 'EstadisticaController@estadisticasVentas5')->middleware('auth','ver');
	Route::post('/estadistica_venta_productos3/', 'EstadisticaController@get_estadisticasVentas5')->middleware('auth','ver');

	Route::get('/estadistica_venta_subtotales/{from?}/{to?}', 'EstadisticaController@estadisticasVentas6')->middleware('auth','ver');
	Route::post('/estadistica_venta_subtotales/', 'EstadisticaController@get_estadisticasVentas6')->middleware('auth','ver');

	Route::get('/estadistica_venta_plain/{from?}/{to?}', 'EstadisticaController@estadisticasVentas7')->middleware('auth','ver');
	Route::post('/estadistica_venta_plain/', 'EstadisticaController@get_estadisticasVentas7')->middleware('auth','ver');

	Route::get('/estadistica_venta_global_cat/', 'EstadisticaController@estadisticasVentas8')->middleware('auth','ver');
	Route::post('/estadistica_venta_global_cat/', 'EstadisticaController@getEstadisticasVentas8')->name('filtrar_estadisticas_ventas_globales')->middleware('auth','ver');



///////////////////////////////////////  ajax   /////////////////////////////////////////////////////////
Route::post('cargar-abonos-facturaventa', ['as' => 'cargar-abonos-facturaventa', 'uses' => 'FacturaventaController@cargarAbonosAjax']);			
Route::post('registrar-abono', ['as' => 'registrar-abono', 'uses' => 'FacturaventaController@registrarAbonoAjax']);		
Route::post('registrar-modo-pago', ['as' => 'registrar-modo-pago', 'uses' => 'FacturaventaController@registrarModoPagoAjax']);	
Route::post('registrar-precio', ['as' => 'registrar-precio', 'uses' => 'ProductoController@registrarPrecioAjax']);
Route::post('registrar-impuesto', ['as' => 'registrar-impuesto', 'uses' => 'IvaController@registrarImpuestoAjax']);
Route::post('registrar-producto', ['as' => 'registrar-producto', 'uses' => 'ProductoController@registrarProductoAjax']);
Route::post('registrar-categoria', ['as' => 'registrar-categoria', 'uses' => 'CategoriaController@registrarCategoriaAjax']);
Route::post('registrar-subcategoria', ['as' => 'registrar-subcategoria', 'uses' => 'SubCategoriaController@registrarSubCategoriaAjax']);
Route::post('registrar-cliente', ['as' => 'registrar-cliente', 'uses' => 'ClienteController@registrarClienteAjax']);
Route::post('registrar-proveedor', ['as' => 'registrar-proveedor', 'uses' => 'SocioController@registrarProveedorAjax']);
Route::post('registrar-medicion', ['as' => 'registrar-medicion', 'uses' => 'TipoMedicionController@registrarMedicionAjax']);
Route::post('cargar-costos', ['as' => 'cargar-costos', 'uses' => 'ProductoController@cargarCostosAjax']);
Route::post('cargar-precios', ['as' => 'cargar-precios', 'uses' => 'ProductoController@cargarPreciosAjax']);
Route::post('cargar-datos-socio', ['as' => 'cargar-datos-socio', 'uses' => 'SocioController@cargarDatosAjax']);
Route::post('cargar-categorias', ['as' => 'cargar-categorias', 'uses' => 'CategoriaController@cargarCategoriasAjax']);
Route::post('cargar-subcategorias', ['as' => 'cargar-subcategorias', 'uses' => 'SubCategoriaController@cargarSubCategoriasAjax']);
Route::post('cargar-proveedores', ['as' => 'cargar-proveedores', 'uses' => 'SocioController@cargarProveedoresAjax']);
Route::post('reset-factura-counter', ['as' => 'reset-factura-counter', 'uses' => 'FacturasController@resetCounterAjax']);
Route::post('reset-factura-produccion-counter', ['as' => 'reset-factura-produccion-counter', 'uses' => 'FacturasController@resetCounterProduccionAjax']);
Route::post('verificar-existencia', ['as' => 'verificar-existencia', 'uses' => 'InventarioController@verificarExistenciaAjax']);
Route::post('buscar_n_factura', ['as' => 'buscar_n_factura', 'uses' => 'FacturaCompraController@verificarExistenciaAjax']);
