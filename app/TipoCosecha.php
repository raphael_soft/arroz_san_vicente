<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCosecha extends Model
{
    //
    protected $table = "tipos_cosechas";

    protected $fillable = ['id', 'nombre'];

}
