<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturaventas extends Model
{
    protected $table = "facturaventas";

    protected $fillable = ['id', 'cliente_id', 'n_orden','fecha_facturacion','hora_facturacion','subtotal','monto_iva','total','modo_pago_id'];

     public function cliente() {
        return $this->belongsTo('App\ClienteVentas','cliente_id');
    }

    public function detalles(){
        return $this->hasMany('App\Detallefacturaventa','factura_id');
    }

	public function modo_pago(){
        return $this->belongsTo('App\ModoPago');
    }  

    public function abonos(){
       return $this->hasMany('App\Abono'); 
    }  

}
