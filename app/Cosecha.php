<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cosecha extends Model
{
    protected $table = "cosechas";

    protected $fillable = ['id', 'terreno_id', 'tipo_id', 'producto_id', 'precio_venta', 'nombre', 'peso','fecha'];

    public function terreno()
    {
		return $this->belongsTo('App\Terreno');
    }

    public function tipo()
    {
		return $this->belongsTo('App\TipoCosecha');
    }

    public function producto()
    {
		return $this->belongsTo('App\Producto');
    }

    public function calificaciones()
    {
		return $this->hasMany('App\Calificacion');
    }
}
