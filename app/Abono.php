<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abono extends Model
{
    //
    protected $table = "abono";

    protected $fillable = ['id', 'monto', 'fecha', 'facturaventas_id'];

    public function facturaventas()
    {
        return $this->belongsTo('App\Facturaventas');
    }

}
