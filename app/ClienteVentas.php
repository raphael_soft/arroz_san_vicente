<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteVentas extends Model
{
    protected $table = "clientes_ventas";

    protected $fillable = ['id','razon_social','email','telefono','cedula_ruc','direccion'];

    public function facturas_venta() {
        return $this->hasMany('App\Facturaventas','cliente_id','id');
    }
}
