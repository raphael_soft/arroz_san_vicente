<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configproduccion extends Model
{
    protected $table = "configproduccion";


    protected $fillable = ['id', 'nombre', 'tipo', 'numero_factura','contador'];
}
