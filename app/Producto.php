<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class Producto extends Model

{

    protected $table = "productos";


    protected $fillable = ['id', 'nombre', 'codigo', 'status','precio_venta','costo_produccion', 'categoria_id', 'subcategoria_id', 'medicion_id', 'iva_id'];

    public function precios(){
        return $this->hasMany('App\Precio');
    }
    public function categoria(){
        return $this->belongsTo('App\Categoria');
    }

    public function iva(){
        return $this->belongsTo('App\Iva');
    }

    public function subcategoria(){
        return $this->belongsTo('App\SubCategoria','subcategoria_id','id');
    }

    public function detalle_factura_compra() {

        return $this->hasMany('App\Detallefacturacompra');

    }

    public function detalle_factura_venta() {

        return $this->hasMany('App\Detallefacturaventa');

    }

    public function medicion(){
        return $this->belongsTo('App\TipoMedicion','medicion_id');
    }

    public function inventario() {

        return $this->hasOne('App\Inventario');

    }

    

}

