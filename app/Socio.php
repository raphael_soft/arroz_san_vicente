<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Socio extends Model
{
    protected $table = "socios";

    protected $fillable = ['id','nombre','email','telefono','telefono_representante','direccion','codigo','nombre_representante','ruc','cedula_representante','email_representante','direccion_representante'];


    public function producto() {
        return $this->hasMany('App\Producto');
    }

    public function facturas_compra() {
        return $this->hasMany('App\Facturacompras');
    }
}
