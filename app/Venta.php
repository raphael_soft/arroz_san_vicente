<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    //

    protected $table = "ventas";

    protected $fillable = ['id', 'socio_id', 'codigo_factura', 'orden_factura', 'fecha', 'iva_id', 'subt_otal', 'total_iva', 'total', 'status'];

    public function socio() {
        return $this->belongsTo('App\Socio');
    }

    public function iva() {
        return $this->belongsTo('App\Iva');
    }

    public function venta_item() {
        return $this->belongsTo('App\VentaItem');
    }

}
