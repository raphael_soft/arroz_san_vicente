<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    //
    protected $table = "currencies";

    protected $fillable = ['id', 'name', 'symbol', 'precision','thousand_separator','decimal_separator','code'];

    public function sistema()
    {
        return $this->hasMany('App\Sistema');
    }

    
}
