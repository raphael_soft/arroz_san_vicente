<?php
namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth; 
use Closure;


class CheckBorrarPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   public function handle($request, Closure $next)
    {
        if (Auth::check() and !Auth::user()->rol->borrar) {
            return redirect('error-acceso');

        }else{
        	$request->flash();
            return $next($request);
        }
    }
}