<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProduccionFormRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            'socio'           => 'required|numeric',
            'res'             => 'required|numeric',
            'tipo_produccion' => 'required|numeric',
            'categoria'       => 'required|numeric',
            'tipo_medicion'   => 'required|numeric',
            'cantidad'        => 'required',
        ];
    }
}
