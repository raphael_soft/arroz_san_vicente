<?php



namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Socio;

use App\Iva;

use App\Categoria;

use App\SubCategoria;

use App\TipoMedicion;

use App\Facturacompras;

use App\Detallefacturacompra;

use App\Inventario;

use App\Producto;

use App\Movimiento;

use App\Sistema;


class FacturaCompraController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */


     public function index($tipo = "",Request $request)

    {
        switch($tipo){
        case "producciones": $t = 0; break;
        case "productos" : $t = 1; break;
        default: return;
        }
        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $request->has("from") != "" ? $request->from : $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $request->has("from") != "" ? $request->from : $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $request->has("to") != "" ? $request->to : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $ventas = array();
        $aux = strtotime($fromDate);
        $fromDate = date('Y-m-d',$aux);
        $aux = strtotime($toDate);
        $toDate = date('Y-m-d',$aux);
        $factura = Facturacompras::where('tipo','=', $t)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es produccion y 1 es productos

        return view('factura_compra.index', compact('factura','tipo','fromDate','toDate'));

    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create(Request $request, $tipo)
    {
        $n_factura = null;

        switch($tipo){
            case "producciones": $n_factura = Sistema::first()->next_facturacompra_producciones; break;
            case "productos" : break;
            default: return;
        }

    	$socios = Socio::all();
        $productos = Producto::where('status','=',1)->get();
    	$impuestos   = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        $categorias   = Categoria::where('status','=',1)->get();
        $subcategorias   = SubCategoria::where('status','=',1)->get();
        $mediciones   = TipoMedicion::where('status','=',1)->get();
        
    	//ORDEN ID
    	$order_id = uniqid();

    	//FECHA ACTUAL
    	$fecha = date("Y-m-d");

    	return view('factura_compra.create', compact('order_id','fecha','n_factura','socios','impuestos','productos','categorias','subcategorias','mediciones','tipo'));
    }

    public function ImprimirPdf($id){

        $factura   = Facturacompras::find($id);

        $detalles  = Detallefacturacompra::where('n_factura', '=', $factura->n_factura)->get();

        $socio     = Socio::where('id', '=', $factura->socio_id)->get();

    	return view('factura_compra.imprimir', compact('factura','detalles','socio'));

    }

    public function store(Request $request)
    {
    	DB::beginTransaction();

        $factura = new Facturacompras($request->factura);

        $existe = Facturacompras::where('n_factura','=',$factura->n_factura)->where('tipo','=',$factura->tipo)->where('n_orden','=',$factura->n_orden)->get()->count();
        
        if($existe > 0)
           return response()->json(["resp" => "existe"]);

        $factura->save();

        $calculo_subtotal = 0;
        $monto_iva = 0;
        
        try{

            if(isset($request->productosFactura) and !empty($request->productosFactura)){
            
                foreach ($request->productosFactura as $key => $value) {

                    $detalleFacturacompra =  new Detallefacturacompra($value);

                    //MOVIMIENTO
                    /*$movimiento                    = new Movimiento;

                    $movimiento->producto_id       = $detalleFactura->id;

                    $movimiento->cantidad          = $detalleFactura->cantidad;

                    $movimiento->tipo              = "COMPRA";

                    $movimiento->save();*/

                  	$iva_valor = isset($detalleFacturacompra->impuesto) ? $detalleFacturacompra->impuesto->valor : 0;

                    $temp1  = ($detalleFacturacompra->precio * $detalleFacturacompra->cantidad);//SUBTOTAL

                    $temp2  = ($temp1 * $iva_valor/100);

                    $monto_iva += $temp2;
                    $calculo_subtotal += $temp1;

                    $detalleFacturacompra->save();
                }
            }else{
            	DB::rollBack();
            	return response()->json(["resp" => "Error al registrar los items de la factura"]);	
            }
                
            $factura->subtotal    =  $calculo_subtotal;

            $factura->monto_iva   =  $monto_iva;

            $factura->total       =  $monto_iva + $calculo_subtotal;

            $factura->update();

            $sistema = Sistema::first();

            $sistema->last_facturacompra_producciones = $factura->n_factura; // guardamos el nuevo numero de factura generado
            $sistema->next_facturacompra_producciones = $factura->n_factura + 1; // guardamos el nuevo numero de factura siguiente
            $sistema->update();
            DB::commit();
            return response()->json(["resp" => "ok"]);
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["resp" => "Error: " . $e->getMessage()]);
        }

    }

    public function invoice(Request $request, $tipo)
    {
        $factura                     = new Facturacompras();
        $factura->socio_id           = $request['socio'];
        $factura->status = 1;//0 es cancelado, 1 es pendiente, y 2 es aprobado 

        if($tipo == "producciones"){// si la factura de compra es de produccion, entonces el numero de factura debe generarse automaticamente
            $factura->tipo = 0; //0 es de produccion y 1 normal
        }

        if($tipo == "productos"){//si la factura de compra es de tipo normal, entonces el numero de factura es ingresado por el usuario
            $factura->tipo = 1; //0 es de produccion y 1 normal
        }

        if($request->has('n_factura'))
            $factura->n_factura = $request->n_factura;
        //CODIGO DE FACTURA PARA EL SIGUIENTE REGISTRO
         //si el request proviene del registro de factura de compras de productos, n_factura no existira y hay que generarlo pero al registrar la factura, no en este momento
            
        $factura->n_orden            = $request['orden_id'];

        $factura->fecha_facturacion   = date("Y-m-d");
        $listaimpuestosval         = $request['listaimpuestosval'];
  
        //MONTO DE IVA

        //$impuesto_general             = Iva::find($request['impuesto_general']);

        $productosFactura = array();
        $calculo_subtotal = 0;
        $monto_iva = 0;
        if(isset($request['listaproductos'])){
            foreach ($request['listaproductos'] as $key => $value) {
                $iva_producto = null;
                
                if($request['listaimpuestos'][$key] != null and $request['listaimpuestos'][$key] != "null" and $request['listaimpuestos'][$key] != ""){
                    
                    $iva_producto = Iva::find($request['listaimpuestos'][$key]);

                }
                $temp1 = ($request['listaprecios'][$key] * $request['listacantidades'][$key]);//SUBTOTAL

                if($iva_producto != null)
                    $temp2  = ($temp1 * $iva_producto->valor / 100);//impuesto
                else
                    $temp2 = 0;

                $calculo_subtotal += $temp1;
                $monto_iva += $temp2;


                $detalleFactura                    = new Detallefacturacompra;

                $detalleFactura->producto_id       = $value;

                $detalleFactura->n_factura         = $factura->n_factura;

                $detalleFactura->impuesto_id         = $request['listaimpuestos'][$key];

                $detalleFactura->cantidad          = $request['listacantidades'][$key];

                $detalleFactura->precio          = $request['listaprecios'][$key];

                array_push($productosFactura,$detalleFactura);

            }

            $factura->subtotal    =  $calculo_subtotal;

            $factura->monto_iva   =  $monto_iva;

            $factura->total       =  $monto_iva + $calculo_subtotal;

            $request->flash();//guardo momentaneamente en la sesion todo lo que esta en el request
            $vista = "confirmacion";
            $impuestos   = Iva::where('status','=',1)->orderBy('valor','asc')->get();
            return view("factura_compra.invoice",compact('factura','productosFactura','vista','tipo','impuestos','request'));
        }

        $request->flash();
        return redirect("create_facturacompra/{$tipo}")->with('error', 'Error al hacer la compra. Intente nuevamente');

    }

    public function show($id)
    {
        $factura   = Facturacompras::find($id);
        $tipo = null;    
        if($factura->tipo == 0)
            $tipo = "producciones";
        if($factura->tipo == 1)
            $tipo = "productos";    
        $impuestos   = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        $productosFactura  = Detallefacturacompra::where('n_factura', '=', $factura->n_factura)->get();

        $vista = 'final';
        
    	return view('factura_compra.invoice', compact('factura','productosFactura','vista','tipo','impuestos'));

    }

    public function imprimir($id)

    {

        $factura   = Facturacompras::find($id);
        $tipo = null;    
        if($factura->tipo == 0)
            $tipo = "producciones";
        if($factura->tipo == 1)
            $tipo = "productos";    

        $productosFactura  = Detallefacturacompra::where('n_factura', '=', $factura->n_factura)->get();
        $impuestos   = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        $vista = 'final';
        return view('factura_compra.invoice_print', compact('factura','productosFactura','vista','tipo','impuestos'));

    }

    public function aprobar($id){ //aqui se deben ingresar los productos a inventario
        $factura = Facturacompras::find($id);
        $factura->status = 2; // 2 es aprobado, 1 es pendiente y 0 cancelado
        $factura->update();
        $tip = $factura->tipo;
        if($tip == 0)
            $tipo = "producciones";
        if($tip == 1)
            $tipo = "productos";

        $productosFactura  = Detallefacturacompra::where('n_factura', '=', $factura->n_factura)->get();

        foreach($productosFactura as $key => $p){//agregamos sus cantidades al inventario de produccion

         $cant_existente = Inventario::where('producto_id','=',$p->producto_id)->where('tipo','=',$tip)->sum('cantidad');
         $cant_total = ($cant_existente + $p->cantidad);
         $producto_inventario = Inventario::updateOrCreate(
                ['producto_id' => $p->producto_id, 'tipo' => $tip],

                ['cantidad' => $cant_total]
            );
         
        }        

        $vista = 'final';
        $impuestos   = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        return view('factura_compra.invoice', compact('factura','productosFactura','vista','tipo','impuestos'));
    }

    public function cancelar($id){
        $factura = Facturacompras::find($id);
        $factura->status = 0; // 2 es aprobado, 1 es pendiente y 0 cancelado
        $factura->update();
        $vista = 'final';
        $tipo = $factura->tipo;
        if($tipo == 0)
            $tipo = "producciones";
        if($tipo == 1)
            $tipo = "productos";
        $impuestos   = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        $productosFactura  = Detallefacturacompra::where('n_factura', '=', Facturacompras::find($id)->n_factura)->get();
        return view('factura_compra.invoice', compact('factura','productosFactura','vista','tipo','impuestos'));
    }

    public function verificarExistenciaAjax(Request $request){

        if($request->ajax()){
            
            $n_factura = isset($request->n_factura) ? $request->n_factura : null;
            $tipo = isset($request->tipo) ? $request->tipo : null;


            if($n_factura == '' || $n_factura == null){
                return response()->json(["resp" => "error", "msg" => "El numero de factura es invalido."]);
            }
            if($tipo == '' || $tipo == null){
                return response()->json(["resp" => "error", "msg" => "Error con el tipo de factura al consultar en la BD."]);
            }

            if($tipo == "producciones")
                $tipo = 0;

            if($tipo == "productos")
                $tipo = 1;


            $existencia = Facturacompras::where('n_factura','=',$n_factura)->where('tipo','=',$tipo)->get();

            if($existencia->count() == 0 || $existencia == null ){
                return response()->json(["resp" => "no", "msg" => "Numero de factura no existe."]);   
            }
            
            if($existencia->count() > 0 || $existencia != null ){
                return response()->json(["resp" => "si", "msg" => "Numero de factura si existe."]);   
            }

        }
    }
}

