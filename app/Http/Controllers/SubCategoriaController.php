<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\SubCategoria;
use App\Http\Requests\CategoriaFormRequest;
use Illuminate\Http\Request;

class SubCategoriaController extends Controller {
    
    public function index() {
        //
        $subcategorias = SubCategoria::all();
        return view('configuracion.subcategoria.index', compact('subcategorias'));
    }

    public function create() {
        //
        $categorias = Categoria::all();
        return view('configuracion.subcategoria.create', compact('categorias'));
    }
        
    public function store(Request $request) {
        //
        // dd($request;
        // dd($request->all());

        $nombre = strtoupper($request->get('nombre'));
        $categoria_padre = $request->get('categoria_padre');
        $descripcion = $request->get('descripcion');

        $existe = SubCategoria::where('nombre','=',$nombre)->where('categoria_id','=',$categoria_padre)->get()->count();
        if($existe > 0){
            $request->flash();
            return redirect('/configuracion/subcategorias/create')->with('error', 'Esta subcategoria ya existe.');            
        }

        $subcategoria = new SubCategoria(array(
            'nombre' => $nombre,
            'categoria_id' => $categoria_padre,
            'descripcion' => $descripcion
        ));

        $subcategoria->save();

        return redirect('/configuracion/subcategorias')->with('status', 'Registro Exitoso.');
    }

    public function show($id) {
        //
        $subcategoria = SubCategoria::whereId($id)->firstOrFail();
        return view('configuracion.subcategoria.show', compact('subcategoria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $subcategoria = SubCategoria::whereId($id)->firstOrFail();
        $categorias = Categoria::all();
        return view('configuracion.subcategoria.edit', compact('subcategoria','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //

        
        $nombre = strtoupper($request->get('nombre'));
        $descripcion = strtoupper($request->get('descripcion'));
        $categoria_padre = $request->get('categoria_padre');

        $subcategoria = SubCategoria::updateOrCreate(
            ['id' => $id],
            ['nombre' => $nombre, 'descripcion' => $descripcion]
        );
        
        return redirect('/configuracion/subcategorias')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $subcategoria = SubCategoria::whereId($id)->firstOrFail();
        //dd($subcategoria,$subcategoria->productos);
        if($subcategoria->productos->count() > 0){
            return redirect('/configuracion/subcategorias')->with('error', 'No se puede eliminar esta subcategoria. Esta siendo usada ');    
        }

        $subcategoria->delete();
        return redirect('/configuracion/subcategorias')->with('status', 'Borrado Exitoso ');
    }

    public function registrarSubCategoriaAjax(Request $request){

        if($request->ajax()){
            $nombre = strtoupper($request->nombre);
            $categoria = strtoupper($request->categoria);

            //buscamos si ya existe
            /*$this->validate(request(),[
                'nombre_categoria' => 'required|unique:categorias,nombre'
            ]);*/
            $existe = SubCategoria::where('nombre','=',$nombre)->where('categoria_id','=',$categoria)->first();

            if(isset($existe->nombre) && $existe->nombre == $nombre && $existe->categoria_id == $categoria){
                return response()->json(['resp' => 'existe']);
            }

            $subcategoria = new SubCategoria(array(
            'nombre' => $nombre,
            'categoria_id' => $categoria
            ));

            $subcategoria->save();
            
            $subcategorias = SubCategoria::where('categoria_id','=',$categoria)->get();

            $data = view('ajax.select-subcategorias',compact('subcategorias'))->render();
            return response()->json(['resp' => 'ok', 'options'=>$data]);
            
        }
    }

    public function cargarSubCategoriasAjax(Request $request){
        if($request->ajax()){
            $categoria = $request->categoria;

            $subcategorias = SubCategoria::where('categoria_id','=',$categoria)->get();
            $data = view('ajax.select-subcategorias',compact('subcategorias'))->render();
            return response()->json(['resp' => 'ok', 'options'=>$data]);
        }
    }

    
}
