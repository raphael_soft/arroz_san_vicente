<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests\CategoriaFormRequest;
use Illuminate\Http\Request;

class CategoriaController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $categorias = Categoria::all();
        return view('configuracion.categoria.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.categoria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        // dd($request;
        // dd($request->all());
        $this->validate(request(),[
                'nombre' => 'required|unique:categorias,nombre'
        ]);    

        $nombre = strtoupper($request->get('nombre'));
        $descripcion = strtoupper($request->get('descripcion'));    
        
        $categoria = new Categoria(array(
            'nombre' => $nombre,
            'descripcion' => $descripcion
        ));

        $categoria->save();

        return redirect('/configuracion/categorias')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $categoria = Categoria::whereId($id)->firstOrFail();
        return view('configuracion.categoria.show', compact('categoria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $categoria = Categoria::whereId($id)->firstOrFail();
        return view('configuracion.categoria.edit', compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        $this->validate(request(),[
                'nombre' => 'unique:categorias,nombre,'.$id
        ]);        
        $categoria    = Categoria::whereId($id)->firstOrFail();
        $nombre = strtoupper($request->get('nombre'));
        $descripcion = strtoupper($request->get('descripcion'));

        $categoria->nombre = $nombre;
        $categoria->descripcion = $descripcion;
        $categoria->update();

        return redirect('/configuracion/categorias')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        
        $categoria = Categoria::whereId($id)->firstOrFail();

        if($categoria->productos->count() > 0){
                return redirect('/configuracion/categorias')->with('error', 'No se puede borrar esta categoria. Esta en uso');    
        }

        $categoria->delete();

        return redirect('/configuracion/categorias')->with('status', 'Borrado Exitoso ');
    }

    public function registrarCategoriaAjax(Request $request){

        if($request->ajax()){
            $nombre = strtoupper($request->nombre_categoria);
            $descripcion = strtoupper($request->descripcion_categoria);

            //buscamos si ya existe
            /*$this->validate(request(),[
                'nombre_categoria' => 'required|unique:categorias,nombre'
            ]);*/
            $existe = Categoria::where('nombre','=',$nombre)->pluck('nombre')->first();

            if($existe == $nombre){
                return response()->json(['resp' => 'existe']);
            }

            $categoria = new Categoria(array(
            'nombre' => $nombre,
            'descripcion' => $descripcion
            ));

            $categoria->save();

            $categorias = Categoria::all();

            $data = view('ajax.select-categorias',compact('categorias'))->render();
            
            return response()->json(['resp' => 'ok' , 'options'=>$data]);
        }
    }

}
