<?php

namespace App\Http\Controllers;

use App\Http\Requests\TipoMedicionFormRequest;
use App\TipoMedicion;
use Illuminate\Http\Request;

class TipoMedicionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $datos = TipoMedicion::all();
        return view('configuracion.tipo_medicion.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.tipo_medicion.create');
    }

    public function store(Request $request){
        
        $this->validate(request(),[
            'nombre' => 'required|unique:tipo_mediciones,nombre',
            'simbolo' => 'required|unique:tipo_mediciones,simbolo'
        ]);

        $nombre = strtoupper($request->get('nombre'));
        $simbolo = strtoupper($request->get('simbolo'));

        $datos = new TipoMedicion(array(
            'nombre' => $nombre,
            'simbolo' => $simbolo
        ));

        $datos->save();

        return redirect('/configuracion/mediciones')->with('status', 'Registro Exitoso.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registrarMedicionAjax(Request $request) {
        //
        $nombre = strtoupper($request->get('nombre'));
        $simbolo = strtoupper($request->get('simbolo'));

        $datos = new TipoMedicion(array(
            'nombre' => $nombre,
            'simbolo' => $simbolo
        ));
        $existe = TipoMedicion::where('nombre','=',$nombre)->pluck('nombre')->first();

            if($existe == $nombre){
                return response()->json(['resp' => 'existe' ]);
            }

        $datos->save();

        $mediciones = TipoMedicion::all();
        $data = view('ajax.select-mediciones',compact('mediciones'))->render();

        return response()->json(['resp' => 'ok' , 'options'=>$data]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $datos = TipoMedicion::whereId($id)->firstOrFail();
        return view('configuracion.tipo_medicion.show', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $datos = TipoMedicion::whereId($id)->firstOrFail();
        return view('configuracion.tipo_medicion.edit', compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TipoMedicionFormRequest $request, $id) {
        //

        $datos = TipoMedicion::whereId($id)->firstOrFail();
        $nombre    = strtoupper($request->get('nombre'));
        $simbolo    = strtoupper($request->get('simbolo'));

        $datos->nombre = $nombre;
        $datos->simbolo = $simbolo;
        $datos->update();

        return redirect('/configuracion/mediciones')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $datos = TipoMedicion::whereId($id)->first();

   
        if(isset($datos->productos) and $datos->productos->count() > 0){
                return redirect('/configuracion/mediciones')->with('error', 'No se puede borrar este registro. Esta siendo usado');    
        }else{
                $datos->delete();    
        }
        
        return redirect('/configuracion/mediciones')->with('status', 'Borrado Exitoso ');
    }
}
