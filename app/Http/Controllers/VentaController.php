<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\InventarioProduccion;
use App\Iva;
use App\Mail\OrdenVenta;
use App\Precio;
use App\Socio;
use App\TipoMedicion;
use App\TipoProduccion;
use App\Venta;
use App\Venta_Item;
use App\Movimiento;
use App\Factura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FacturaventaController extends Controller
{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {

        //

        $datos = Facturaventa::all();

        return view('factura_venta.index', compact('datos'));

    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()
    {

        $socios = Socio::all();
        $productos = Producto::where('status','=',1)->get();
        $impuestos   = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        $categorias   = Categoria::where('status','=',1)->get();
        $subcategorias   = Subcategoria::where('status','=',1)->get();
        $mediciones   = TipoMedicion::where('status','=',1)->get();
        
        //ORDEN ID
        $order_id = uniqid();

        //FECHA ACTUAL

        $fecha =date("Y-m-d");

        
        return view('factura_compra.create', compact('order_id','fecha','socios','impuestos','productos','categorias','subcategorias','mediciones','tipo'));

    }

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function GetDatosVentas(Request $request)
    {
        $tipo_produccion = explode("|", $request->get('tipo_produccion'));
        $tipo_medicion   = explode("|", $request->get('tipo_medicion'));
        $categoria       = explode("|", $request->get('categoria'));

        $precio = Precio::where('tipo_produccion_id', '=', $tipo_produccion[0])
            ->where('tipo_medicion_id', '=', $tipo_medicion[0])
            ->where('categoria_id', '=', $categoria[0])->value('precio');

        $cantidad = InventarioProduccion::where('tipo_produccion_id', '=', $tipo_produccion[0])
            ->where('tipo_medicion_id', '=', $tipo_medicion[0])
            ->where('categoria_id', '=', $categoria[0])->value('cantidad');

        if ($cantidad <= 0) {
            $datos = ['precio' => $precio, 'cantidad' => 0];
            return response()->json($datos);
        }else {
            $datos = ['precio' => $precio, 'cantidad' => $cantidad];
            return response()->json($datos);
        }
    }

    public function store(Request $request)
    {
        $factura = new Facturaventa($request->factura);

        $existe = Facturaventa::where('n_factura','=',$factura->n_factura)->orWhere('n_orden','=',$factura->n_orden)->get()->count();
        
        if($existe > 0)
           return response()->json(["resp" => "existe"]);
        
        $factura->save();
        
        $calculo_subtotal = 0;
        
        if(isset($request->productosFactura) and !empty($request->productosFactura)){
        
	        foreach ($request->productosFactura as $key => $value) {

	            $detalleFactura =  new Detallefactura($value);

	            $calculo_subtotal += ($detalleFactura->precio * $detalleFactura->cantidad);//SUBTOTAL

	            $monto_iva         = ($calculo_subtotal*$detalleFactura->impuesto->valor/100);

	            $total             = $monto_iva + $calculo_subtotal;

	            $detalleFactura->save();
			}
		}
            
        $factura->subtotal    =  $calculo_subtotal;

        $factura->monto_iva   =  $monto_iva;

        $factura->total       =  $total;

        $factura->update();

        return response()->json(["resp" => "ok"]);
    }


    public function invoice(Request $request,$tipo)
    {
        $factura                     = new Facturacompra();

        $factura->socio_id           = $request['socio'];
        $factura->status = 1;//0 es cancelado, 1 es pendiente, y 2 es aprobado 
        if($tipo == "producciones")
            $factura->tipo = 0; //0 es de produccion y 1 normal
        if($tipo == "productos")
        	$factura->tipo = 1; //0 es de produccion y 1 normal
        //CODIGO DE FACTURA PARA EL SIGUIENTE REGISTRO

        $factura->n_factura = $request['n_factura'];

        $factura->n_orden            = $request['orden_id'];

        $factura->fecha_facturacion   = date("Y-m-d");

        $productosFactura = array();
        $calculo_subtotal = 0;
        if(isset($request['listaproductos'])){
	        foreach ($request['listaproductos'] as $key => $value) {

	            //COSTOS

	            $iva_producto = Iva::find($request['listaimpuestos'][$key]);

	            $calculo_subtotal += ($request['listaprecios'][$key] * $request['listacantidades'][$key]);//SUBTOTAL

	            $monto_iva         = ($calculo_subtotal*$iva_producto->valor/100);

	            $total             = $monto_iva + $calculo_subtotal;

	            $detalleFactura                    = new Detallefactura;

	            $detalleFactura->producto_id       = $value;

	            $detalleFactura->n_factura         = $factura->n_factura;

	            $detalleFactura->impuesto_id         = $request['listaimpuestos'][$key];

	            $detalleFactura->cantidad          = $request['listacantidades'][$key];

	            $detalleFactura->precio          = $request['listaprecios'][$key];

	            array_push($productosFactura,$detalleFactura);

	            $factura->subtotal    =  $calculo_subtotal;

	            $factura->monto_iva   =  $monto_iva;

	            $factura->total       =  $total;

				//$update->save();

			}
		    $request->flash();//guardo momentaneamente en la sesion todo lo que esta en el request
		    $vista = "confirmacion";
		    return view("factura_venta.invoice",compact('factura','productosFactura','vista','tipo'));
        }

        $request->flash();

        return redirect("create_factura_venta/{$tipo}")->with('error', 'Error al hacer la compra. Intente nuevamente');
    }

    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($codigo_factura)
    {

        factura   = Facturaventa::find($id);
        $tipo = null;    
        if($factura->tipo == 0)
            $tipo = "producciones";
        if($factura->tipo == 1)
            $tipo = "productos";    

        $productosFactura  = Detallefactura::where('n_factura', '=', $factura->n_factura)->get();

        $vista = 'final';
        return view('factura_venta.invoice', compact('factura','productosFactura','vista','tipo'));

    }

    // ESTADISTICA DE VENTA

    public function estadistica()
    {

        //A#O ACTUAL DE LA PRIMERA VISTA

        $anio_actual = date("Y");

        //VARIABLES

        //CONSULTA ENERO

        $enero_inicial = $anio_actual . "-" . "01" . "-" . "01";

        $enero_final = $anio_actual . "-" . "01" . "-" . "31";

        $ventas_enero = Venta::where('fecha', '>=', $enero_inicial)->where('fecha', '<=', $enero_final)->where('status', '=', '2')->count();

        //CONSULTA FEBRERO

        $febrero_inicial = $anio_actual . "-" . "02" . "-" . "01";

        $febrero_final = $anio_actual . "-" . "02" . "-" . "28";

        $ventas_febrero = Venta::where('fecha', '>=', $febrero_inicial)->where('fecha', '<=', $febrero_final)->where('status', '=', '2')->count();

        //CONSULTA MARZO

        $marzo_inicial = $anio_actual . "-" . "03" . "-" . "01";

        $marzo_final = $anio_actual . "-" . "03" . "-" . "31";

        $ventas_marzo = Venta::where('fecha', '>=', $marzo_inicial)->where('fecha', '<=', $marzo_final)->where('status', '=', '2')->count();

        //CONSULTA ABRIL

        $abril_inicial = $anio_actual . "-" . "04" . "-" . "01";

        $abril_final = $anio_actual . "-" . "04" . "-" . "30";

        $ventas_abril = Venta::where('fecha', '>=', $abril_inicial)->where('fecha', '<=', $abril_final)->where('status', '=', '2')->count();

        //CONSULTA MAYO

        $mayo_inicial = $anio_actual . "-" . "05" . "-" . "01";

        $mayo_final = $anio_actual . "-" . "05" . "-" . "31";

        $ventas_mayo = Venta::where('fecha', '>=', $mayo_inicial)->where('fecha', '<=', $mayo_final)->where('status', '=', '2')->count();

        //CONSULTA JUNIO

        $junio_inicial = $anio_actual . "-" . "06" . "-" . "01";

        $junio_final = $anio_actual . "-" . "06" . "-" . "30";

        $ventas_junio = Venta::where('fecha', '>=', $junio_inicial)->where('fecha', '<=', $junio_final)->where('status', '=', '2')->count();

        //CONSULTA JULIO

        $julio_inicial = $anio_actual . "-" . "07" . "-" . "01";

        $julio_final = $anio_actual . "-" . "07" . "-" . "31";

        $ventas_julio = Venta::where('fecha', '>=', $julio_inicial)->where('fecha', '<=', $julio_final)->where('status', '=', '2')->count();

        //CONSULTA AGOSTO

        $agosto_inicial = $anio_actual . "-" . "08" . "-" . "01";

        $agosto_final = $anio_actual . "-" . "08" . "-" . "31";

        $ventas_agosto = Venta::where('fecha', '>=', $agosto_inicial)->where('fecha', '<=', $agosto_final)->where('status', '=', '2')->count();

        //CONSULTA SEPTIEMBRE

        $septiembre_inicial = $anio_actual . "-" . "09" . "-" . "01";

        $septiembre_final = $anio_actual . "-" . "09" . "-" . "30";

        $ventas_septiembre = Venta::where('fecha', '>=', $septiembre_inicial)->where('fecha', '<=', $septiembre_final)->where('status', '=', '2')->count();

        //CONSULTA OCTUBRE

        $octubre_inicial = $anio_actual . "-" . "10" . "-" . "01";

        $octubre_final = $anio_actual . "-" . "10" . "-" . "30";

        $ventas_octubre = Venta::where('fecha', '>=', $octubre_inicial)->where('fecha', '<=', $octubre_final)->where('status', '=', '2')->count();

        //CONSULTA NOVIEMBRE

        $noviembre_inicial = $anio_actual . "-" . "11" . "-" . "01";

        $noviembre_final = $anio_actual . "-" . "11" . "-" . "30";

        $ventas_noviembre = Venta::where('fecha', '>=', $noviembre_inicial)->where('fecha', '<=', $noviembre_final)->where('status', '=', '2')->count();

        //CONSULTA DICIEMBRE

        $diciembre_inicial = $anio_actual . "-" . "12" . "-" . "01";

        $diciembre_final = $anio_actual . "-" . "12" . "-" . "30";

        $ventas_diciembre = Venta::where('fecha', '>=', $diciembre_inicial)->where('fecha', '<=', $diciembre_final)->where('status', '=', '2')->count();

        //CONSULTA POS A#OS

        //CONSULTA 2017

        $anio_inicio_2017 = "2017" . "-" . "01" . "-" . "01";

        $anio_final_2017 = "2017" . "-" . "12" . "-" . "31";

        $total_2017 = Venta::where('fecha', '>=', $anio_inicio_2017)->where('fecha', '<=', $anio_final_2017)->where('status', '=', '2')->count();

        //CONSULTA 2018

        $anio_inicio_2018 = "2018" . "-" . "01" . "-" . "01";

        $anio_final_2018 = "2018" . "-" . "12" . "-" . "31";

        $total_2018 = Venta::where('fecha', '>=', $anio_inicio_2018)->where('fecha', '<=', $anio_final_2018)->where('status', '=', '2')->count();

        //CONSULTA 2019

        $anio_inicio_2019 = "2019" . "-" . "01" . "-" . "01";

        $anio_final_2019 = "2019" . "-" . "12" . "-" . "31";

        $total_2019 = Venta::where('fecha', '>=', $anio_inicio_2019)->where('fecha', '<=', $anio_final_2019)->where('status', '=', '2')->count();

        //CONSULTA 2020

        $anio_inicio_2020 = "2020" . "-" . "01" . "-" . "01";

        $anio_final_2020 = "2020" . "-" . "12" . "-" . "31";

        $total_2020 = Venta::where('fecha', '>=', $anio_inicio_2020)->where('fecha', '<=', $anio_final_2020)->where('status', '=', '2')->count();

        //CONSULTA 2021

        $anio_inicio_2021 = "2020" . "-" . "01" . "-" . "01";

        $anio_final_2021 = "2020" . "-" . "12" . "-" . "31";

        $total_2021 = Venta::where('fecha', '>=', $anio_inicio_2021)->where('fecha', '<=', $anio_final_2021)->where('status', '=', '2')->count();

        return view('venta.estadistica', compact('anio_actual', 'ventas_enero', 'ventas_febrero', 'ventas_marzo', 'ventas_abril', 'ventas_mayo', 'ventas_junio', 'ventas_julio', 'ventas_agosto', 'ventas_septiembre', 'ventas_octubre', 'ventas_noviembre', 'ventas_diciembre', 'total_2017', 'total_2018', 'total_2019', 'total_2020', 'total_2021'));

    }

    public function consultaanio()
    {

        //A#O ACTUAL DE LA PRIMERA VISTA

        $anio_actual = $_POST['anio'];

        //VARIABLES

        //CONSULTA ENERO

        $enero_inicial = $anio_actual . "-" . "01" . "-" . "01";

        $enero_final = $anio_actual . "-" . "01" . "-" . "31";

        $ventas_enero = Venta::where('fecha', '>=', $enero_inicial)->where('fecha', '<=', $enero_final)->where('status', '=', '2')->count();

        //CONSULTA FEBRERO

        $febrero_inicial = $anio_actual . "-" . "02" . "-" . "01";

        $febrero_final = $anio_actual . "-" . "02" . "-" . "28";

        $ventas_febrero = Venta::where('fecha', '>=', $febrero_inicial)->where('fecha', '<=', $febrero_final)->where('status', '=', '2')->count();

        //CONSULTA MARZO

        $marzo_inicial = $anio_actual . "-" . "03" . "-" . "01";

        $marzo_final = $anio_actual . "-" . "03" . "-" . "31";

        $ventas_marzo = Venta::where('fecha', '>=', $marzo_inicial)->where('fecha', '<=', $marzo_final)->where('status', '=', '2')->count();

        //CONSULTA ABRIL

        $abril_inicial = $anio_actual . "-" . "04" . "-" . "01";

        $abril_final = $anio_actual . "-" . "04" . "-" . "30";

        $ventas_abril = Venta::where('fecha', '>=', $abril_inicial)->where('fecha', '<=', $abril_final)->where('status', '=', '2')->count();

        //CONSULTA MAYO

        $mayo_inicial = $anio_actual . "-" . "05" . "-" . "01";

        $mayo_final = $anio_actual . "-" . "05" . "-" . "31";

        $ventas_mayo = Venta::where('fecha', '>=', $mayo_inicial)->where('fecha', '<=', $mayo_final)->where('status', '=', '2')->count();

        //CONSULTA JUNIO

        $junio_inicial = $anio_actual . "-" . "06" . "-" . "01";

        $junio_final = $anio_actual . "-" . "06" . "-" . "30";

        $ventas_junio = Venta::where('fecha', '>=', $junio_inicial)->where('fecha', '<=', $junio_final)->where('status', '=', '2')->count();

        //CONSULTA JULIO

        $julio_inicial = $anio_actual . "-" . "07" . "-" . "01";

        $julio_final = $anio_actual . "-" . "07" . "-" . "31";

        $ventas_julio = Venta::where('fecha', '>=', $julio_inicial)->where('fecha', '<=', $julio_final)->where('status', '=', '2')->count();

        //CONSULTA AGOSTO

        $agosto_inicial = $anio_actual . "-" . "08" . "-" . "01";

        $agosto_final = $anio_actual . "-" . "08" . "-" . "31";

        $ventas_agosto = Venta::where('fecha', '>=', $agosto_inicial)->where('fecha', '<=', $agosto_final)->where('status', '=', '2')->count();

        //CONSULTA SEPTIEMBRE

        $septiembre_inicial = $anio_actual . "-" . "09" . "-" . "01";

        $septiembre_final = $anio_actual . "-" . "09" . "-" . "30";

        $ventas_septiembre = Venta::where('fecha', '>=', $septiembre_inicial)->where('fecha', '<=', $septiembre_final)->where('status', '=', '2')->count();

        //CONSULTA OCTUBRE

        $octubre_inicial = $anio_actual . "-" . "10" . "-" . "01";

        $octubre_final = $anio_actual . "-" . "10" . "-" . "30";

        $ventas_octubre = Venta::where('fecha', '>=', $octubre_inicial)->where('fecha', '<=', $octubre_final)->where('status', '=', '2')->count();

        //CONSULTA NOVIEMBRE

        $noviembre_inicial = $anio_actual . "-" . "11" . "-" . "01";

        $noviembre_final = $anio_actual . "-" . "11" . "-" . "30";

        $ventas_noviembre = Venta::where('fecha', '>=', $noviembre_inicial)->where('fecha', '<=', $noviembre_final)->where('status', '=', '2')->count();

        //CONSULTA DICIEMBRE

        $diciembre_inicial = $anio_actual . "-" . "12" . "-" . "01";

        $diciembre_final = $anio_actual . "-" . "12" . "-" . "30";

        $ventas_diciembre = Venta::where('fecha', '>=', $diciembre_inicial)->where('fecha', '<=', $diciembre_final)->where('status', '=', '2')->count();

        //CONSULTA POS A#OS

        //CONSULTA 2017

        $anio_inicio_2017 = "2017" . "-" . "01" . "-" . "01";

        $anio_final_2017 = "2017" . "-" . "12" . "-" . "31";

        $total_2017 = Venta::where('fecha', '>=', $anio_inicio_2017)->where('fecha', '<=', $anio_final_2017)->where('status', '=', '2')->count();

        //CONSULTA 2018

        $anio_inicio_2018 = "2018" . "-" . "01" . "-" . "01";

        $anio_final_2018 = "2018" . "-" . "12" . "-" . "31";

        $total_2018 = Venta::where('fecha', '>=', $anio_inicio_2018)->where('fecha', '<=', $anio_final_2018)->where('status', '=', '2')->count();

        //CONSULTA 2019

        $anio_inicio_2019 = "2019" . "-" . "01" . "-" . "01";

        $anio_final_2019 = "2019" . "-" . "12" . "-" . "31";

        $total_2019 = Venta::where('fecha', '>=', $anio_inicio_2019)->where('fecha', '<=', $anio_final_2019)->where('status', '=', '2')->count();

        //CONSULTA 2020

        $anio_inicio_2020 = "2020" . "-" . "01" . "-" . "01";

        $anio_final_2020 = "2020" . "-" . "12" . "-" . "31";

        $total_2020 = Venta::where('fecha', '>=', $anio_inicio_2020)->where('fecha', '<=', $anio_final_2020)->where('status', '=', '2')->count();

        //CONSULTA 2021

        $anio_inicio_2021 = "2020" . "-" . "01" . "-" . "01";

        $anio_final_2021 = "2020" . "-" . "12" . "-" . "31";

        $total_2021 = Venta::where('fecha', '>=', $anio_inicio_2021)->where('fecha', '<=', $anio_final_2021)->where('status', '=', '2')->count();

        return view('venta.estadistica', compact('anio_actual', 'ventas_enero', 'ventas_febrero', 'ventas_marzo', 'ventas_abril', 'ventas_mayo', 'ventas_junio', 'ventas_julio', 'ventas_agosto', 'ventas_septiembre', 'ventas_octubre', 'ventas_noviembre', 'ventas_diciembre', 'total_2017', 'total_2018', 'total_2019', 'total_2020', 'total_2021'));

    }

}
