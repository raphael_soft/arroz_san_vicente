<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ClienteVentas;
use App\Socio;



class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datos = ClienteVentas::all();
        return view('configuracion.clientes.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->flash();
        $this->validate(request(),[
            'cedula_ruc' => 'unique:clientes_ventas,cedula_ruc|min:1',
            'nombre' => 'required'
        ]);
        $slug = uniqid();
        $nombre = strtoupper($request['nombre']);

        $registro           = new ClienteVentas;
        $registro->razon_social   = $nombre;
        $registro->email    = $request['email'];
        $registro->cedula_ruc    = $request['cedula_ruc'];
        $registro->telefono = $request['telefono'];
        $registro->direccion = $request['direccion'];
        
        $registro->save();

        //LISTADO DE SOCIOS
        return redirect('/configuracion/clientes')->with('status', 'Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$datos = ClienteVentas::find($id);
    	return view('configuracion.clientes.show', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = ClienteVentas::find($id);
    	return view('configuracion.clientes.edit', compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {	
    	
        $cliente    = ClienteVentas::find($id);
        //$request->flash();
        $this->validate(request(),[
            'cedula_ruc' => 'min:1|unique:clientes_ventas,cedula_ruc,'.$id,
            'nombre' => 'required'
        ]);
        $cliente->cedula_ruc   = $_POST['cedula_ruc'];
        $cliente->razon_social  	= strtoupper($_POST['nombre']);
        $cliente->email   	= strtoupper($_POST['email']);
        $cliente->direccion     = strtoupper($_POST['direccion']);
        $cliente->telefono 	= $_POST['telefono'];
        
        
        $cliente->update();

		return redirect("/configuracion/clientes")->with('status', 'Actualizaci&oacute;n Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = ClienteVentas::whereId($id)->firstOrFail();
		
        //CONFIRMAR EXISTENCIA DE UNA RES ASOCIADA
        if(isset($cliente->facturas_venta) and $cliente->facturas_venta->count() > 0){

            return redirect('/configuracion/clientes')->with('error', 'No se puede eliminar este registro ya que esta siendo usado');    
        }    

        $cliente->delete();

        return redirect('/configuracion/clientes')->with('status', 'Borrado Exitoso');
        
    }

    public function registrarClienteAjax(Request $request){

        if($request->ajax()){
            
            $nombre = strtoupper(isset($request->nombre)?$request->nombre:'');
            $cedula_ruc = isset($request->ruc)?$request->ruc:'';
            $direccion = strtoupper(isset($request->direccion)?$request->direccion:'');
            $telefono = isset($request->telefono)?$request->telefono:'';

            if($nombre == '' || $cedula_ruc == ''){
                return response()->json(["resp" => "error"]);
            }

            $existe = ClienteVentas::where('cedula_ruc','=',$cedula_ruc)->get()->count();

            if($existe > 0){
                return response()->json(["resp" => "existe"]);
            }

            $cliente = new ClienteVentas(array(
            'razon_social' => $nombre,
            'cedula_ruc' => $cedula_ruc,
            'direccion' => $direccion,
            'telefono' => $telefono
            ));

            $cliente->save();
            
            $collection = ClienteVentas::all();

            $data = view('ajax.select-clientes',compact('collection'))->render();

            return response()->json(['options'=>$data, 'resp' => 'ok']);
        }
    }

    public function cargarClientesAjax(Request $request){

        if($request->ajax()){
            
            $collection = ClienteVentas::all();

            $data = view('ajax.select-clientes',compact('collection'))->render();
            return response()->json(['options'=>$data]);
        }
    }
}
