<?php

namespace App\Http\Controllers;

use App\Http\Requests\IvaFormRequest;
use App\Iva;
use Illuminate\Http\Request;

class IvaController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $ivas = Iva::all();
        return view('configuracion.iva.index', compact('ivas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.iva.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IvaFormRequest $request) {
        //
        // dd($request;
        // dd($request->all());
        $this->validate(request(),[
            'nombre' => 'required|unique:ivas,nombre',
            'valor' => 'required'
        ]);

        $nombre = strtoupper($request->get('nombre'));

        $iva = new Iva(array(
            'nombre'    => $nombre,
            'valor'     => $request->get('valor'),
        ));

        $iva->save();

        return redirect('/configuracion/iva')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $iva = Iva::whereId($id)->firstOrFail();
        return view('configuracion.iva.show', compact('iva'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $iva = Iva::whereId($id)->firstOrFail();
        return view('configuracion.iva.edit', compact('iva'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IvaFormRequest $request, $id) {
        //

        $iva = Iva::whereId($id)->firstOrFail();
        $nombre = strtoupper($request->get('nombre'));

        $iva->nombre = $nombre;
        $iva->valor = $request->get('valor');
        $iva->save();

        return redirect('/configuracion/iva/')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $iva = Iva::whereId($id)->first();

        if($iva->productos_compras->count() > 0 or $iva->productos_ventas->count() > 0){
                return redirect('/configuracion/iva')->with('error', 'No se puede borrar esta impuesto. Esta en uso');    
        }

        $iva->delete();
        return redirect('/configuracion/iva')->with('status', 'Borrado Exitoso ');
    }

    public function registrarImpuestoAjax(Request $request){

        if($request->ajax()){
            $nombre = isset($request->nombre)?$request->nombre:'';
            $valor = isset($request->valor)?$request->valor:'';
            
            $existe = Iva::where('nombre','=',$nombre)->get()->count();

            if($existe > 0){
                return response()->json([ 'resp' => 'error']);
            }

            $impuesto = new Iva(array(
            'nombre' => $nombre,
            'valor' => $valor
            ));

            $impuesto->save();
            
            $collection = Iva::all();

            $data = view('ajax.select-impuestos',compact('collection'))->render();
            return response()->json(['options'=>$data, 'resp' => 'ok']);
        }
    }

    public function cargarImpuestosAjax(Request $request){

        if($request->ajax()){
            
            $collection = Iva::all();

            $data = view('ajax.select-proveedores',compact('collection'))->render();
            return response()->json(['options'=>$data]);
        }
    }
}
