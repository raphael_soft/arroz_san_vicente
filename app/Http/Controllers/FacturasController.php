<?php



namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Socio;
use App\Sistema;
use App\Currency;
use App\Iva;

use App\Categoria;

use App\SubCategoria;

use App\TipoMedicion;

use App\Facturacompras;

use App\Facturaventas;

use App\Detallefacturacompra;

use App\Inventario;

use App\Inventarioproduccion;

use App\Producto;

use App\Movimiento;

use App\Factura;

use App\Http\Requests\FacturacompraFormRequests;



class FacturasController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */


     public function index()

    {
        $todas = Facturaventas::all();
        $sistema = Sistema::first();
        $ultima = 0;
        $ultima_productos = 0;
        $ultima_produccion = 0;
        $siguiente = 0;
        $cant_facturas = 0;
        $cant_facturas_canc = 0;    
        $cant_facturas_apro = 0;
        $cant_facturas_pen = 0;
        
        $status_table = DB::select("SHOW TABLE STATUS LIKE 'facturaventas'");

        
            // datos de todas las facturas de venta
            if($todas->count() > 0)
                $ultima = $todas->last()->id;
            
            $status_table = $status_table[0];
            $siguiente = $status_table->Auto_increment; 
            $cant_facturas = $todas->count();
            $cant_facturas_canc = $todas->where('status','=',0)->count();
            $cant_facturas_apro = $todas->where('status','=',2)->count();
            $cant_facturas_pen = $todas->where('status','=',1)->count();

            //datos de todas las facturas de compra
            $facturas_compras = Facturacompras::all();
            $cant_facturas_compras = $facturas_compras->count();

            $cant_facturas_canc_compras = $facturas_compras->where('status','=',0)->count();
            $cant_facturas_apro_compras = $facturas_compras->where('status','=',2)->count();
            $cant_facturas_pen_compras = $facturas_compras->where('status','=',1)->count();
            //datos de todas las facturas de compra de produccion
            $facturas_compras_produccion = $facturas_compras->where('tipo','=',0);
            $ultima_produccion = $sistema->last_facturacompra_producciones;
            $siguiente_produccion = $sistema->next_facturacompra_producciones;
            $cant_facturas_compras_produccion = $facturas_compras_produccion->count();
            $cant_facturas_canc_compras_produccion = $facturas_compras_produccion->where('status','=',0)->count();
            $cant_facturas_apro_compras_produccion = $facturas_compras_produccion->where('status','=',2)->count();
            $cant_facturas_pen_compras_produccion = $facturas_compras_produccion->where('status','=',1)->count();
            //datos de todas las facturas de compra de productos
            $facturas_compras_productos = $facturas_compras->where('tipo','=',1);
            
            if($facturas_compras_productos->count() > 0)
                $ultima_productos = $facturas_compras_productos->last()->n_factura;
            
            $cant_facturas_compras_productos = $facturas_compras_productos->count();
            $cant_facturas_canc_compras_productos = $facturas_compras_productos->where('status','=',0)->count();
            $cant_facturas_apro_compras_productos = $facturas_compras_productos->where('status','=',2)->count();
            $cant_facturas_pen_compras_productos = $facturas_compras_productos->where('status','=',1)->count();
        
        
////////////////////// datos del sistemao de la empresa para las facturas /////////////////////////

        $data = Sistema::first();            

        $currencies = Currency::all();
        //dd($status_table);
        return view('configuracion.facturas.index', compact('ultima','ultima_produccion','ultima_productos','siguiente','siguiente_produccion','cant_facturas_canc','cant_facturas_pen','cant_facturas_apro','cant_facturas','cant_facturas_canc_compras','cant_facturas_pen_compras','cant_facturas_apro_compras','cant_facturas_compras','cant_facturas_canc_compras_produccion','cant_facturas_pen_compras_produccion','cant_facturas_apro_compras_produccion','cant_facturas_compras_produccion','cant_facturas_canc_compras_productos','cant_facturas_pen_compras_productos','cant_facturas_apro_compras_productos','cant_facturas_compras_productos','data','currencies'));

    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()
    {
        
    	

        
    }

    public function store(Request $request)

    {
        
        $sistema = Sistema::first();

        $sistema->show_razon_social = $request->has('show_razon_social') ? true: false;
        $sistema->telefono1 = $request->telefono1;
        $sistema->show_telefono1 = $request->has('show_telefono1') ? true : false;
        $sistema->telefono2 = $request->telefono2;
        $sistema->show_telefono2 = $request->has('show_telefono2') ? true : false;
        $sistema->email1 = $request->email1;
        $sistema->show_email1 = $request->has('show_email1') ? true : false;
        $sistema->email2 = $request->email2;
        $sistema->show_email2 = $request->has('show_email2') ? true : false;
        $sistema->facebook = $request->facebook;
        $sistema->show_facebook = $request->has('show_facebook') ? true: false;
        $sistema->twiter = $request->twiter;
        $sistema->show_twiter = $request->has('show_twiter') ? true : false;
        $sistema->web = $request->web;
        $sistema->show_web = $request->has('show_web') ? true : false;
        $sistema->direccion_fiscal = $request->direccion_fiscal;
        $sistema->show_direccion_fiscal = $request->has('show_direccion_fiscal') ? true : false;
        /*$sistema->currencies_id = $request->currency;
        //$sistema->show_currency = $request->has('show_currency') ? true: false;*/
        $sistema->show_logo1 = $request->has('show_logo1') ? true : false;
        
        //$sistema->show_logo2 = $request->has('show_logo2') ? true : false;
        /*
        if($request->has('logo2')){
            $path_logo2 =  $request->file("logo2")->store('public');
            //$path_logo2 = Storage::putFile('public', $request->file("logo2"),'public');
            $sistema->logo2 = $path_logo2;
        }
*/
        $sistema->update();

        ///// cargar datos //////////////////////////////



        return redirect('/configuracion/facturas')->with('status','Actualizacion exitosa!');
    }


    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function resetCounterAjax(Request $request){
        if($request->ajax()){
            $num = $request->num;
            $ultimo = 0;
            $facturas = Facturaventas::all();
            $query = false;
            if($facturas->count() > 0){
                $ultimo = $facturas->last()->id;
            }

            if($num < 0 or $num <= $ultimo){
                return response()->json(['resp' => 'error', 'msg' => 'El numero ingresado es invalido']);
            }
            try{
            $query = DB::statement("ALTER TABLE facturaventas AUTO_INCREMENT = ".$num);
            }catch(Exception $e){
               return response()->json(['resp' => 'error', 'msg' => report($e)]); 
            }
            if($query){
                return response()->json(['resp' => 'ok', "value" => $num]);
            }
        }
    }

    public function resetCounterProduccionAjax(Request $request){
        if($request->ajax()){
            $num = $request->num;
            $ultimo = 0;
            $sistema = Sistema::first();
            $query = false;
            
            if($num < 0 or $num <= $sistema->last_facturacompra_producciones){
                return response()->json(['resp' => 'error', 'msg' => 'El numero ingresado es invalido']);
            }

            $sistema->next_facturacompra_producciones = $num;
            $sistema->update();

            return response()->json(['resp' => 'ok', "value" => $num]);
            
        }
    }
}

