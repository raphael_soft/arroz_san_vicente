<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Inventario;

use Illuminate\Support\Facades\DB;


class InventarioController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index($tipo)
    {
        
        if($tipo == "producciones")
            $tipo_ = 0;
        if($tipo == "productos")
            $tipo_ = 1;
        if($tipo == "categorias")
            $tipo_ = 2;

            if($tipo_ == 2){
                $inventario = DB::table('inventario')->join('productos', 'inventario.producto_id','=','productos.id')->join('categorias','productos.categoria_id','=','categorias.id')->join('tipo_mediciones','productos.medicion_id','=','tipo_mediciones.id')->groupBy('categorias.nombre','tipo_mediciones.id')->select('categorias.nombre', DB::raw('SUM(inventario.cantidad) AS cantidad'), /*DB::raw('SUM(productos.costo_produccion) AS costo_total'), DB::raw('SUM(productos.precio_venta) AS precio_total'),*/ 'tipo_mediciones.simbolo')->get();
                $view = view('inventario.index_categorias', compact('inventario','tipo'));
            }
            else{
                $inventario = Inventario::where('tipo','=',$tipo_)->get();
                $view = view('inventario.index', compact('inventario','tipo'));
            }
        
        //dd($inventario);
        return $view;
    }

    public function verificarExistenciaAjax(Request $request){

        if($request->ajax()){
            
            $id_producto = isset($request->id_producto) ? $request->id_producto : 0;

            if($id_producto == '' || $id_producto == 0){
                return response()->json(["resp" => "error", "msg" => "El id del producto seleccionado es invalido."]);
            }

            $existencia = Inventario::where('producto_id','=',$id_producto)->get();

            if($existencia->count() == 0 || $existencia == null || $existencia->sum('cantidad') == 0 || $existencia->sum('cantidad') == null){
                return response()->json(["resp" => "error", "msg" => "Este producto no existe en inventario."]);   
            }
            
            return response()->json(["resp" => "ok", "value" => $existencia->sum('cantidad')]);

        }
    }
}

