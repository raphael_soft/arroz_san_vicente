<?php

namespace App\Http\Controllers;

use App\Rol;
use App\User;
use App\Cargo;
use Illuminate\Http\Request;

class UsuarioController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $datos = User::all();
        return view('configuracion.usuario.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        $roles = Rol::all();
        $cargos = Cargo::all();
        return view('configuracion.usuario.create', compact('roles','cargos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        $today = date('Y-m-d');
        $anios_antes = strtotime( '-18 year', strtotime($today));
        $min_date = date('Y-m-d',$anios_antes);

        $request->flash();
        $this->validate(request(),[
            'nombres' => 'required',
            'apellidos' => 'required',
            'email' => 'required|unique:users,email',
            'cargo' => 'required',
            'rol' => 'required',
            'clave' => 'required',
            'dni' => 'required|unique:users,dni',
            'fecha_nacimiento' => 'sometimes|date|before_or_equal:'.$min_date,
            'fecha_ingreso' => 'sometimes|date|before_or_equal:'.$today
        ]);

        $datos = new User(array(
            'nombre'     => strtoupper($request->get('nombres')),
            'apellido'     => strtoupper($request->get('apellidos')),
            'dni'     => strtoupper($request->get('dni')),
            'email'    => strtoupper($request->get('email')),
            'direccion'   => strtoupper($request->get('direccion')),
            'ciudad'   => strtoupper($request->get('ciudad')),
            'telefono'    => $request->get('telefono'),
            'fecha_ingreso'    => $request->get('fecha_ingreso'),
            'fecha_nacimiento'    => $request->get('fecha_nacimiento'),
            'rol_id'   => $request->get('rol'),
            'cargo_id'   => $request->get('cargo'),
            'password' => bcrypt($request->get('clave'))
        ));

        $datos->save();

        return redirect('/configuracion/usuario')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $datos = User::whereId($id)->firstOrFail();
        return view('configuracion.usuario.show', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $roles = Rol::all();
        $datos = User::whereId($id)->firstOrFail();
        $cargos = Cargo::all();
        return view('configuracion.usuario.edit', compact('datos', 'roles','cargos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //

        // dd($request->all());
        $today = date('Y-m-d');
        $anios_antes = strtotime( '-18 year', strtotime($today));
        $min_date = date('Y-m-d',$anios_antes);

        $request->flash();

        $this->validate(request(),[
            'nombre' => 'required',
            'apellido' => 'required',
            'email' => 'required|unique:users,email,'.$id,
            'cargo' => 'required',
            'rol' => 'required',
            'clave' => 'required',
            'dni' => 'required|unique:users,dni,'.$id,
            'fecha_nacimiento' => 'sometimes|date|before_or_equal:'.$min_date,
            'fecha_ingreso' => 'sometimes|date|before_or_equal:'.$today
        ]);

        $datos = User::find($id);

        
            $datos->nombre   = strtoupper($request->get('nombre'));
            $datos->apellido   = strtoupper($request->get('apellido'));
            $datos->dni   = strtoupper($request->get('dni'));
            $datos->email    = strtoupper($request->get('email'));
            $datos->direccion   = strtoupper($request->get('direccion'));
            $datos->telefono    = $request->get('telefono');
            $datos->rol_id   = $request->get('rol');
            $datos->cargo_id   = $request->get('cargo');
            $datos->fecha_ingreso   = $request->get('fecha_ingreso');
            $datos->fecha_nacimiento   = $request->get('fecha_nacimiento');
            $datos->password = bcrypt($request->get('clave'));
        

        $datos->update();
        
        return redirect('/configuracion/usuario')->with('status', 'Actualizacion Exitosa.');
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        if($id == 1){
            return redirect('/configuracion/usuario/show/'.$id)->with('error', 'No se puede borrar este usuario. Es del sistema');    
        }
        $datos = User::whereId($id)->firstOrFail();

        $datos->delete();
        
        return redirect('/configuracion/usuario')->with('status', 'Borrado Exitoso');
    }
}
