<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Facturaventas;
use App\Producto;
use App\Categoria;
use App\Sistema;
use App\Socio;
use App\Facturacompras;
use App\Detallefacturacompra;
use App\Detallefacturaventa;

class EstadisticaController extends Controller

{
  public function ingreso(Request $request)

    {
        $anio_actual = date("Y");
        if($request->has('anio'))
            $anio_selected = $request->anio;
        else
        	$anio_selected = $anio_actual;

        $ventas = array();
        $totales = array();
        
        for($i=0;$i<12;$i++){
            $m = ($i+1) < 10?'0'.($i+1):($i+1);
            $ventas[$i]  = Facturaventas::where('fecha_facturacion', 'like', $anio_selected.'-'.$m.'%')->where('status', '=', '2')->sum('subtotal');

        }

        for($i=0;$i<=4;$i++){
            $totales[$i]  = Facturaventas::where('fecha_facturacion', 'like', (($anio_selected-1)+$i).'-%')->where('status', '=', '2')->sum('subtotal');

        }        
        
        return view('estadistica.ingreso', compact('anio_actual', 'anio_selected', 'ventas','totales'));

    }

    public function egreso(Request $request)

    {

        $anio_actual = date("Y");
        if($request->has('anio'))
            $anio_selected = $request->anio;
        else
        	$anio_selected = $anio_actual;

        $compras = array();
        $totales = array();

        for($i = 0; $i < 12; $i++){
            $m = ($i + 1) < 10 ? '0' . ($i + 1) : ($i + 1);
            $compras[$i]  = Facturacompras::where('fecha_facturacion', 'like', $anio_selected.'-'.$m.'%')->where('status', '=', '2')->sum('subtotal');

        }

        for($i = 0; $i <= 4; $i++){
            $totales[$i]  = Facturacompras::where('fecha_facturacion', 'like', (($anio_selected-1)+$i).'-%')->where('status', '=', '2')->sum('subtotal');

        }        
        
        return view('estadistica.egreso', compact('anio_actual', 'anio_selected', 'compras','totales'));

    }

    public function compras($fromDate = '',$toDate = '',Request $request){

        $tipo = 0; 
        if($request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 0;
        }

        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != ''  ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $compras_produccion = array();
        $compras_productos = array();
        $compras_todas = array();
        $currentDate = strtotime($fromDate);
//inicio calculo de subtotales de compras por dias
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal1  = Facturacompras::where('fecha_facturacion', '=', $date)->where('tipo','=',0)->where('status', '=', '2')->sum('subtotal');
            $subtotal2  = Facturacompras::where('fecha_facturacion', '=', $date)->where('tipo','=',1)->where('status', '=', '2')->sum('subtotal');
            $subtotal3  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            $compras_productos[$currentDate] = $subtotal2;
            $compras_produccion[$currentDate] = $subtotal1;
            $compras_todas[$currentDate] = $subtotal3;
            $currentDate += $aDay; //se le suma un dia a la fecha
        }
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorCompras = Facturacompras::where('status', '=', '2')->count();
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
        $totalCompras = $todasCompras->sum('subtotal');
        $todosProducciones = $todasCompras->where('tipo','=',0)->pluck('subtotal')->all();
        $todosProductos = $todasCompras->where('tipo','=',1)->pluck('subtotal')->all();
        $totalComprasProducciones = array_sum($todosProducciones);
        $totalComprasProductos = array_sum($todosProductos);
        $total_cant_comprasProducciones = count($todosProducciones);
        $total_cant_comprasProductos = count($todosProductos);

        $porcentaje1 = 0;
        $porcentaje2 = 0;

        if($totalCompras != 0){
            $porcentaje1 = $totalComprasProducciones * 100 / $totalCompras;
            $porcentaje2 = $totalComprasProductos * 100 / $totalCompras;
        }

        $tipos = array(
            array('cantidad' => count($todosProducciones), 'titulo' => 'producciones', 'porcentaje' => $porcentaje1, 'total' => $totalComprasProducciones),
            array('cantidad' => count($todosProductos), 'titulo' => 'productos', 'porcentaje' => $porcentaje2, 'total' => $totalComprasProductos)
        );
//fin de calculo por produccion
//inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasComprados1 = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
    FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
     WHERE A.n_factura = B.n_factura 
     AND B.status = 2 
     AND B.tipo = 0 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (por produccion)
//inicio calculo de los 10 productos mas comprados (normal)
 $productosMasComprados2 = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
    FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
     WHERE A.n_factura = B.n_factura 
     AND B.status = 2 
     AND B.tipo = 1 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (normal)
 // inicio calculo de categorias mas compradas
 $categoriasMasCompradas = DB::select("SELECT C.id as id, C.nombre as nombre,COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra
    FROM detalle_factura_compra A
    INNER JOIN facturacompras B ON B.n_factura = A.n_factura
    INNER JOIN productos P ON P.id = A.producto_id
    INNER JOIN categorias C ON C.id = P.categoria_id
    WHERE B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
 //dd($categoriasMasCompradas);
 $subcategoriasMasCompradas = [];
 
 foreach ($categoriasMasCompradas as $key => $value) {
     # code...
    $subcategoriasMasCompradas[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra 
        FROM detalle_factura_compra A
        INNER JOIN facturacompras B ON A.n_factura = B.n_factura 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }

 /*$productosSubcategoriasMasCompradas = [];

 foreach ($categoriasMasCompradas as $key1 => $value1) {
     # code...
    foreach ($subcategoriasMasCompradas[$value1->nombre] as $key2 => $value2) {
    
    $productos = DB::select("SELECT P.id, P.nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra 
        FROM detalle_factura_compra A
        INNER JOIN facturacompras B ON A.n_factura = B.n_factura 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE P.subcategoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY P.id",[$value2->id,$fromDate,$toDate]);
    $productosSubcategoriasMasCompradas[$value2->id] = $productos;
    }
    
    }*/
 //dd($productosSubcategoriasMasCompradas);
//dd($productosSubcategoriasMasCompradas);    
 //inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
if($tipo != 2){
    $facturas = Facturacompras::where('status','=',2)->where('tipo','=',$tipo)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es     
}else{
    $facturas = Facturacompras::where('status','=',2)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es 
}

//dd($totalComprasProducciones,$totalComprasProductos,$totalCompras,$productosMasComprados1,$productosMasComprados2,$porcentaje1,$porcentaje2);
 //fin

return view('estadistica.compras', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','compras_productos','compras_produccion','compras_todas','tipos','productosMasComprados1','productosMasComprados2','totalComprasProducciones','totalComprasProductos','tipo_informe','listaproductos','productosMasComprados','categoriasMasCompradas','subcategoriasMasCompradas','listacategorias','facturas','tipo','contadorCompras'));   
}

    public function get_compras(Request $request){

        if($request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 0;
        }

        $today = time(); //timestamp en formato unix
        
        $fromDate = date('Y-m-d',strtotime($request->fromDate));
        $toDate = date('Y-m-d',strtotime($request->toDate));
        $tipo_informe = $request->tipo_informe;

        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $compras_produccion = array(); // array que guardara el subtotal diario de las compras de produccion
        $compras_productos = array(); // array que guardara el subtotal diario de las compras de productos
        $compras_todas = array(); // array que guardara el subtotal diario de todas las compras
        $currentDate = strtotime($fromDate); //fecha inicial en formato UNIX, (inicializada en la fecha de inicio recibida)

        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal1  = Facturacompras::where('fecha_facturacion', '=', $date)->where('tipo','=',0)->where('status', '=', '2')->sum('subtotal');
            $subtotal2  = Facturacompras::where('fecha_facturacion', '=', $date)->where('tipo','=',1)->where('status', '=', '2')->sum('subtotal');
            $subtotal3  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            $compras_productos[$currentDate] = $subtotal2; //el total de la suma de los subtotales ddel dia, de las compras de productos
            $compras_produccion[$currentDate] = $subtotal1; //el total de la suma de los subtotales ddel dia, de las compras de produccion
            $compras_todas[$currentDate] = $subtotal3; //el total de la suma de los subtotales ddel dia, de todas las compras 
            $currentDate += $aDay; //se le suma un dia a la fecha actual
        }

    //inicio calculo por produccion
        $contadorCompras = Facturacompras::where('status', '=', '2')->count();
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', '2')->get();
        $cantTotalCompras = $todasCompras->count();
        $totalCompras = $todasCompras->sum('subtotal');
        $todosProducciones = $todasCompras->where('tipo','=',0)->pluck('subtotal')->all();
        $todosProductos = $todasCompras->where('tipo','=',1)->pluck('subtotal')->all();
        $totalComprasProducciones = array_sum($todosProducciones);
        $totalComprasProductos = array_sum($todosProductos);
        $total_cant_comprasProducciones = count($todosProducciones);
        $total_cant_comprasProductos = count($todosProductos);
        $porcentaje1 = 0;
        $porcentaje2 = 0;
        if($totalCompras > 0){
            $porcentaje1 = $totalComprasProducciones * 100 / $totalCompras;
            $porcentaje2 = $totalComprasProductos * 100 / $totalCompras;
        }
        $tipos = array(
            array('cantidad' => count($todosProducciones), 'titulo' => 'producciones', 'porcentaje' => $porcentaje1, 'total' => $totalComprasProducciones),
            array('cantidad' => count($todosProductos), 'titulo' => 'productos', 'porcentaje' => $porcentaje2, 'total' => $totalComprasProductos)
        );
//fin de calculo por produccion

//inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasComprados1 = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
    FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
     WHERE A.n_factura = B.n_factura 
     AND B.status = 2 
     AND B.tipo = 0 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (por produccion)
//inicio calculo de los 10 productos mas comprados (normal)
 $productosMasComprados2 = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
    FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
     WHERE A.n_factura = B.n_factura 
     AND B.status = 2 
     AND B.tipo = 1 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (normal)

 // inicio calculo de categorias mas compradas
 $categoriasMasCompradas = DB::select("SELECT C.id as id, C.nombre as nombre,COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra
    FROM detalle_factura_compra A
    INNER JOIN facturacompras B ON B.n_factura = A.n_factura
    INNER JOIN productos P ON P.id = A.producto_id
    INNER JOIN categorias C ON C.id = P.categoria_id
    WHERE B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
 //dd($categoriasMasCompradas);

//inicio de los productos seleccionados por el usuario

$productosMasComprados = array();
$listaproductos2 = array();
if($request->has('listaproductos')){
    $listaproductos2 = $request->listaproductos;
    
    //dd($listaproductos2);
    if(!empty($listaproductos2)){
    
    $listaproductos_ = implode(',',$listaproductos2);
    //dd($listaproductos2,$listaproductos_);
    $productosMasComprados = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
    FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
     WHERE A.n_factura = B.n_factura 
     AND B.status = 2 
     AND B.tipo = 0 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     AND A.producto_id IN (".$listaproductos_.")
     GROUP BY C.id
     ORDER BY (total_compra) DESC",[$fromDate,$toDate]);
    
    }
}
    $subcategoriasMasCompradas = [];

 if(isset($categoriasMasCompradas)){
 foreach ($categoriasMasCompradas as $key => $value) {
     # code...
    $subcategoriasMasCompradas[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra 
        FROM detalle_factura_compra A
        INNER JOIN facturacompras B ON A.n_factura = B.n_factura 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
    }
 }
    //inicio de los categorias seleccionados por el usuario
$listacategorias = Categoria::all();

$categoriasMasCompradas2 = array();
$listacategorias2 = array();
if($request->has('listacategorias')){
    $listacategorias2 = $request->listacategorias;
    
    //dd($listacategorias2);
    if(!empty($listacategorias2)){
    
    $listacategorias_ = implode(',',$listacategorias2);
    //dd($listacategorias2,$listacategorias_);
    $query = "SELECT C.id AS id, C.nombre AS nombre,COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra 
    FROM detalle_factura_compra A
    INNER JOIN facturacompras B ON A.n_factura = B.n_factura 
    INNER JOIN productos P ON P.id = A.producto_id 
    INNER JOIN categorias C ON C.id = P.categoria_id 
    WHERE B.status = 2
    AND C.id IN (".$listacategorias_.")
    GROUP BY C.id
    ORDER BY (total_compra) DESC";

    $categoriasMasCompradas2 = DB::select($query,[$fromDate,$toDate]);    
    
    }
   // dd($categoriasMasCompradas,$categoriasMasCompradas2);
    
}
 foreach ($categoriasMasCompradas2 as $key => $value) {
     # code...
    $subcategoriasMasCompradas2[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra 
        FROM detalle_factura_compra A
        INNER JOIN facturacompras B ON A.n_factura = B.n_factura 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }

    $listaproductos = Producto::all();
    $listacategorias = Categoria::all();
    if($tipo != 2){
    $facturas = Facturacompras::where('status','=',2)->where('tipo','=',$tipo)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es 
    }else{
        $facturas = Facturacompras::where('status','=',2)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es 
    }
        return view('estadistica.compras', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProductos','total_cant_comprasProducciones','compras_productos','compras_produccion','compras_todas','tipos','productosMasComprados1','productosMasComprados2','totalComprasProducciones','totalComprasProductos','tipo_informe','listaproductos','listaproductos2','listacategorias2','listacategorias','listacategorias_','productosMasComprados','categoriasMasCompradas','subcategoriasMasCompradas','categoriasMasCompradas2','subcategoriasMasCompradas2','facturas','tipo','contadorCompras'));   
    

    }


    public function ventas($fromDate = '',$toDate = ''){
        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (120*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturaventas::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != ''  ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array();
        $currentDate = strtotime($fromDate);
//inicio calculo de subtotales de compras por dias
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturaventas::where('fecha_facturacion', '=', $date)->where('status', '=', 2)->sum('subtotal');
            $ventas[$currentDate] = $subtotal;
            $currentDate += $aDay; //se le suma un dia a la fecha
        }
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');
        
        //fin calculos genericos


//inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
     WHERE B.id = A.factura_id 
     AND B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id
     GROUP BY C.id 
     ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas vendidos (por produccion)


//inicio calculo de los 10 productos menos vendidos
$productosMenosVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
 WHERE B.id = A.factura_id 
 AND B.status = 2 
 AND B.fecha_facturacion >= ? 
 AND B.fecha_facturacion <= ? 
 AND A.producto_id = C.id
 AND C.medicion_id = M.id 
 GROUP BY C.id
 ORDER BY (total_venta) ASC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos menos vendidos
//inicio calculo de los 5 categorias mas vendidas
 $categoriasMasVendidas = DB::select("SELECT C.id as id, C.nombre as nombre,P.nombre as nombre_p, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta A 
    INNER JOIN facturaventas B ON B.id = A.factura_id 
    INNER JOIN productos P ON P.id = A.producto_id 
    INNER JOIN categorias C ON C.id = P.categoria_id 
    INNER JOIN tipo_mediciones M ON M.id = P.medicion_id 
    WHERE B.status = 2 
    AND B.fecha_facturacion >= ? 
    AND B.fecha_facturacion <= ? 
    GROUP BY C.id 
    ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
 
 //dd($categoriasMasVendidas);
//fin calculo de los 5 categorias mas vendidas
$subcategoriasMasVendidas = [];
 
 foreach ($categoriasMasVendidas as $key => $value) {
     # code...
    $subcategoriasMasVendidas[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }
 //dd($productosMasVendidos,$productosMenosVendidos);
 //dd($productosSubcategoriasMasCompradas);
//dd($productosSubcategoriasMasCompradas);    
 //inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos

        return view('estadistica.ventas', compact('toDate', 'fromDate','cantTotalVentas','ventas','productosMasVendidos','productosMenosVendidos','totalVentas','tipo_informe','listaproductos','categoriasMasVendidas','subcategoriasMasVendidas','productosSubcategoriasMasVendidas','listacategorias','facturas','contadorVentas'));   
    }

    public function get_ventas(Request $request){
        $today = time(); //timestamp en formato unix
        
        
        $fromDate = date('Y-m-d',strtotime($request->fromDate));
        $toDate = date('Y-m-d',strtotime($request->toDate));
        $tipo_informe = $request->tipo_informe;

        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array(); // array que guardara el subtotal diario de las compras de produccion
        //dd($fromDate,$toDate);
        $currentDate = strtotime($fromDate); //fecha inicial en formato UNIX, (inicializada en la fecha de inicio recibida)

        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            
            $ventas[$currentDate] = $subtotal; //el total de la suma de los subtotales ddel dia, de las compras de productos
            
            $currentDate += $aDay; //se le suma un dia a la fecha actual
        }

    //inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', '2')->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');
//fin de calculo por produccion

//inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
     WHERE B.id = A.factura_id 
     AND B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (por produccion)

//inicio calculo de los 10 productos mas comprados (normal)
 $productosMenosVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
     WHERE B.id = A.factura_id 
     AND B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_venta) ASC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (normal)

//inicio de los productos seleccionados por el usuario

if($request->has('listaproductos')){
    $listaproductos = Producto::all(); // esta es la lista de todos los productos registrados

    $listaproductos2 = array();
    $listaproductos2 = $request->listaproductos;//esta es la lista de productos seleccionados y enviados por el usuario
    
    
    if(!empty($listaproductos2)){
       
         $listaproductos_ = implode(',',$listaproductos2);
       
     //dd($listaproductos_);
    $productosMasVendidosN = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
     WHERE A.factura_id = B.id 
     AND B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND A.producto_id IN (".$listaproductos_.")
     AND C.medicion_id = M.id
     GROUP BY C.id 
     ORDER BY (total_venta) DESC",[$fromDate,$toDate]);
    
    }
}
if($request->has('listacategorias')){
    $listacategorias = Categoria::all(); // esta es la lista de todos los productos registrados

    $listacategorias2 = array();
    $listacategorias2 = $request->listacategorias;//esta es la lista de productos seleccionados y enviados por el usuario
    
    //inicio calculo de los 10 productos mas comprados (normal)
 $categoriasMasVendidas = DB::select("SELECT C.id as id, C.nombre as nombre,P.nombre as nombre_p, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta A 
    INNER JOIN facturaventas B ON B.id = A.factura_id 
    INNER JOIN productos P ON P.id = A.producto_id 
    INNER JOIN categorias C ON C.id = P.categoria_id 
    INNER JOIN tipo_mediciones M ON M.id = P.medicion_id 
    WHERE B.status = 2 
    AND B.fecha_facturacion >= ? 
    AND B.fecha_facturacion <= ? 
    GROUP BY C.id 
    ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
 //dd($categoriasMasVendidas);
//fin calculo de los 10 productos mas comprados (normal)
 //inicio de los categorias seleccionados por el usuario

    $subcategoriasMasVendidas = [];
 
 foreach ($categoriasMasVendidas as $key => $value) {
     # code...
    $subcategoriasMasVendidas[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }
    //dd($listaproductos2);
    if(!empty($listacategorias2)){
    
    $listacategorias_ = implode(',',$listacategorias2);
    $query = "SELECT C.id AS id, C.nombre AS nombre,COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
    FROM detalle_factura_venta A
    INNER JOIN facturaventas B ON A.factura_id = B.id 
    INNER JOIN productos P ON P.id = A.producto_id 
    INNER JOIN categorias C ON C.id = P.categoria_id 
    WHERE B.status = 2
    AND C.id IN (".$listacategorias_.")
    GROUP BY C.id
    ORDER BY (total_venta) DESC";

    $categoriasMasVendidas2 = DB::select($query,[$fromDate,$toDate]);    
    
    }
        
 $productosSubcategoriasMasVendidas = [];

 foreach ($categoriasMasVendidas as $key1 => $value1) {
     # code...
    foreach ($subcategoriasMasVendidas[$value1->nombre] as $key2 => $value2) {
    
    $productos = DB::select("SELECT P.id, P.nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE P.subcategoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY P.id",[$value2->id,$fromDate,$toDate]);
    $productosSubcategoriasMasVendidas[$value2->id] = $productos;
    }
    
    }
 
    $subcategoriasMasVendidas2 = [];
 
 foreach ($categoriasMasVendidas2 as $key => $value) {
     # code...
    $subcategoriasMasVendidas2[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }

 $productosSubcategoriasMasVendidas2 = [];

 foreach ($categoriasMasVendidas2 as $key1 => $value1) {
     # code...
    foreach ($subcategoriasMasVendidas2[$value1->nombre] as $key2 => $value2) {
    
    $productos = DB::select("SELECT P.id, P.nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE P.subcategoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY P.id",[$value2->id,$fromDate,$toDate]);
    $productosSubcategoriasMasVendidas2[$value2->id] = $productos;
    }
    
    }
    $listaproductos = Producto::all();
$listacategorias = Categoria::all();
}
 //fin
//dd($productosMasVendidos,$productosMenosVendidos);
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es 

        return view('estadistica.ventas', compact('toDate', 'fromDate','cantTotalVentas','totalVentas','ventas','productosMasVendidos','productosMasVendidosN','productosMenosVendidos','tipo_informe','listaproductos','listaproductos2','categoriasMasVendidas2','subcategoriasMasVendidas','productosSubcategoriasMasVendidas2','subcategoriasMasVendidas2','listacategorias','listacategorias2','categoriasMasVendidas','facturas','contadorVentas'));   

    }

    public function diferenciaFechas($date1=null, $date2=null, $format = '%a'){
        if($date1 == null or $date2 == null or $date1 == '' or $date2 == '')
            return null;

        $date1 = date_create($date1);
        $date2 = date_create($date2);

        $intervalo = date_diff($date1,$date2);

        return $intervalo->format($format);
    }

    public function get_compra(Request $request){

    }

    public function produccion()

    {
        $fecha_facturacion = null;

        $producciones = TipoProduccion::all();

        $valores = [];

        foreach ($producciones as $key => $value) {

            // $valores[$key]['valor']  = Produccion::where('tipo_produccion_id', '=', $value['id'])->value('cantidad');
            $aux  = Produccion::where('tipo_produccion_id', '=', $value['id'])->get();
            $temp = 0;
            foreach ($aux as $key2 => $value2) {
            	$temp = $temp+ $value2['cantidad'];
            }
            $valores[$key]['valor'] = $temp;

        }

        return view('estadistica.produccion', compact('producciones', 'valores', 'fecha_facturacion') );

    }



    public function get_produccion()

    {
        $fecha_facturacion = $_POST['fecha_facturacion'];

        $producciones = TipoProduccion::all();

        $valores = [];

        foreach ($producciones as $key => $value) {

            // $valores[$key]['valor']  = Produccion::where('tipo_produccion_id', '=', $value['id'])->where('fecha_facturacion', '=', $fecha_facturacion)->value('cantidad');
            
            $aux  = Produccion::where('tipo_produccion_id', '=', $value['id'])->where('fecha_facturacion', '=', $fecha_facturacion)->get();
            $temp = 0;
            foreach ($aux as $key2 => $value2) {
            	$temp = $temp+ $value2['cantidad'];
            }
            $valores[$key]['valor'] = $temp;

        }



        return view('estadistica.produccion', compact('producciones', 'valores', 'fecha_facturacion') );

    }


    static public function getEgresoByAnio($anio)
    {

        $compras = array();

        for($i=0;$i<12;$i++){
            $m = ($i+1) < 10?'0'.($i+1):($i+1);
            $compras[$i]  = Facturacompras::where('fecha_facturacion', 'like', $anio.'-'.$m.'%')->where('status', '=', '2')->sum('subtotal');

        }
        
        return $compras;

    }
    
    static public function getIngresoByAnio($anio)
    {

        $ventas = array();

        for($i=0;$i<12;$i++){
            $m = ($i+1) < 10?'0'.($i+1):($i+1);
            $ventas[$i]  = Facturaventas::where('fecha_facturacion', 'like', $anio.'-'.$m.'%')->where('status', '=', '2')->sum('subtotal');

        }
        
        return $ventas;

    }

    static public function getUtilidadesByAnio($ingresos,$egresos)
    {

        $utilidades = array();

        for($i=0;$i<12;$i++){
            $m = ($i+1) < 10?'0'.($i+1):($i+1);
            $utilidades[$i]  = $ingresos[$i] - $egresos[$i];

        }
        
        return $utilidades;

    }

    static public function getVolumenVentas($tipo,$limite){//tipo indica si sera de productos, categoria o subcategoria y limite el numero de items

        switch($tipo){
            case "productos": 

            break;
            case "categorias": break;
            case "subcategorias": break;
            default: break;
        }

    }

    public function imprimirVentas($fromDate = "n" , $toDate = "n")
    {
        $wheres_array = array();
        if($fromDate != "n"){
            
            array_push($wheres_array, ['fecha_facturacion', '>=', $fromDate]);
        }
        if($toDate != "n"){
            
            array_push($wheres_array, ['fecha_facturacion', '<=', $toDate]);
        }

        array_push($wheres_array, ['status', '=', 2]);    

        //dd($wheres_array);

        $facturas = Facturaventas::where($wheres_array)->get();
        $title = Sistema::first()->nombre;

         return view('estadistica.ventas_imprimir',compact('facturas','fromDate','toDate','title'));
    }

    public function imprimirCompras($fromDate = "n" , $toDate = "n", $tipo = 0, Request $request)
    {
        
        $wheres_array = array();

        if($fromDate != "n"){
            
            array_push($wheres_array, ['fecha_facturacion', '>=', $fromDate]);
        }
        if($toDate != "n"){
            
            array_push($wheres_array, ['fecha_facturacion', '<=', $toDate]);
        }
        

        if($request->has("tipo"))
                $tipo = $request->tipo;

        if($tipo != 2)
        array_push($wheres_array, ['tipo', '=', $tipo]);
        
        array_push($wheres_array, ['status', '=', 2]);    
        //dd($wheres_array);

        $facturas = Facturacompras::where($wheres_array)->get();
        $title = Sistema::first()->nombre;

         return view('estadistica.compras_imprimir',compact('facturas','fromDate','toDate','title','tipo'));
    }

       

    public function estadisticasCompras2($fromDate = '', $toDate = '',Request $request){
        $tipo = 0; 
        if($request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 0;
        }

        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

//inicio calculo por produccion
        $contadorCompras = Facturacompras::where('status', '=', '2')->count();
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
        $totalComprasProducciones = 0;
        $totalComprasProductos = 0;
        $contProducciones = 0;
        $contProductos = 0;
        
        $totalCompras = 0;
foreach ($todasCompras as $key => $value) {
    # code...
    $detalles = $value->detalles;

    foreach ($detalles as $key => $value2) {
        # code...
        $totalCompras += ($value2->cantidad * $value2->precio);
        if($value->tipo == 0){ //si es produccion
            $totalComprasProducciones += ($value2->cantidad * $value2->precio);
            $contProducciones++;
        }else{
            $totalComprasProductos += ($value2->cantidad * $value2->precio);
            $contProductos++;
        }
    }
}
        $porcentaje1 = 0;
        $porcentaje2 = 0;

        if($totalCompras != 0){
            $porcentaje1 = $totalComprasProducciones * 100 / $totalCompras;
            $porcentaje2 = $totalComprasProductos * 100 / $totalCompras;
        }

        $tipos = array(
            array('cantidad' => $contProducciones, 'titulo' => 'producciones', 'porcentaje' => $porcentaje1, 'total' => $totalComprasProducciones),
            array('cantidad' => $contProductos, 'titulo' => 'productos', 'porcentaje' => $porcentaje2, 'total' => $totalComprasProductos)
        );
        

//inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasComprados1 = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
    FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
     WHERE A.n_factura = B.n_factura 
     AND B.status = 2 
     AND B.tipo = 0 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (por produccion)

//inicio calculo de los 10 productos mas comprados (normal)
 $productosMasComprados2 = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
    FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
     WHERE A.n_factura = B.n_factura 
     AND B.status = 2 
     AND B.tipo = 1 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (normal)
   
        return view('estadistica.compras2', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','compras_productos','compras_produccion','compras_todas','tipos','productosMasComprados2','productosMasComprados1','totalComprasProducciones','totalComprasProductos','tipo_informe','listaproductos','listacategorias','facturas','tipo','contadorCompras'));
    }

    public function estadisticasCompras3($fromDate = '', $toDate = '',Request $request){
        $tipo = 2; 
        if($request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 2;
        }

        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

       
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorCompras = Facturacompras::where('status', '=', '2')->count();
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
        $totalCompras = $todasCompras->sum('subtotal');
        $todosProducciones = $todasCompras->where('tipo','=',0)->pluck('subtotal')->all();
        $todosProductos = $todasCompras->where('tipo','=',1)->pluck('subtotal')->all();
        $totalComprasProducciones = array_sum($todosProducciones);
        $totalComprasProductos = array_sum($todosProductos);
        $total_cant_comprasProducciones = count($todosProducciones);
        $total_cant_comprasProductos = count($todosProductos);

                $totalCompras = 0;
foreach ($todasCompras as $key => $value) {
    # code...
    $detalles = $value->detalles;

    foreach ($detalles as $key => $value2) {
        # code...
        $totalCompras += ($value2->cantidad * $value2->precio);
    }
}

      
    // inicio calculo de categorias mas compradas
 $categoriasMasCompradas = DB::select("SELECT C.id as id, C.nombre as nombre,COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra
    FROM detalle_factura_compra A
    INNER JOIN facturacompras B ON B.n_factura = A.n_factura
    INNER JOIN productos P ON P.id = A.producto_id
    INNER JOIN categorias C ON C.id = P.categoria_id
    WHERE B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
 //dd($categoriasMasCompradas);
 $subcategoriasMasCompradas = [];
 
 foreach ($categoriasMasCompradas as $key => $value) {
   
     # code...
    $subcategoriasMasCompradas[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra 
        FROM detalle_factura_compra A
        INNER JOIN facturacompras B ON A.n_factura = B.n_factura 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }
   
    
        return view('estadistica.compras3', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','compras_productos','compras_produccion','compras_todas','tipos','productosMasComprados2','productosMasComprados1','totalComprasProducciones','totalComprasProductos','tipo_informe','listaproductos','listacategorias','facturas','tipo','contadorCompras','categoriasMasCompradas','subcategoriasMasCompradas'));
    }

    public function get_estadisticasCompras(Request $request){
        $tipo = 0; 
        if( $request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 2;
        }
        $fromDate = '';
        $toDate = '';
        $today = time(); //timestamp en formato unix
        if($request->has('fromDate') and $request->fromDate != '')
            $fromDate = date('Y-m-d',strtotime($request->fromDate));
        if($request->has('toDate') and $request->toDate != '')
            $toDate = date('Y-m-d',strtotime($request->toDate));

        $tipo_informe = $request->tipo_informe;
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        
//inicio calculo por produccion
        $contadorCompras = Facturacompras::where('status', '=', '2')->count();
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
       
        
        $totalCompras = 0;
        foreach ($todasCompras as $key => $value) {
            # code...
            $detalles = $value->detalles;

            foreach ($detalles as $key => $value2) {
                # code...
                $totalCompras += ($value2->cantidad * $value2->precio);
            }
        }

               
        //fin de calculo por produccion
        $productosMasComprados = array();
        $listaproductos2 = array();
        if($request->has('listaproductos')){
            $listaproductos2 = $request->listaproductos;
            
            //dd($listaproductos2);
            if(!empty($listaproductos2)){
            
            $listaproductos_ = implode(',',$listaproductos2);

            
            //dd($listaproductos2,$listaproductos_);
            $productosMasComprados = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
            FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
             WHERE A.n_factura = B.n_factura 
             AND B.status = 2 
             AND B.tipo = 0 
             AND B.fecha_facturacion >= ? 
             AND B.fecha_facturacion <= ? 
             AND A.producto_id = C.id
             AND C.medicion_id = M.id 
             AND A.producto_id IN (".$listaproductos_.")
             GROUP BY C.id
             ORDER BY (total_compra) DESC",[$fromDate,$toDate]);
            
                
            }
        }
            
            $listaproductos = Producto::all();

        if($tipo != 2){
            $facturas = Facturacompras::where('status','=',2)->where('tipo','=',$tipo)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es     
        }else{
            $facturas = Facturacompras::where('status','=',2)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es 
        }

    
        return view('estadistica.compras1', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','compras_productos','compras_produccion','compras_todas','tipos','productosMasComprados','totalComprasProducciones','totalComprasProductos','tipo_informe','listaproductos2','listaproductos','listacategorias','facturas','tipo','contadorCompras'));
    }

   
    
    public function get_estadisticasCompras2(Request $request){
        $tipo = 0; 
        if( $request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 2;
        }
        $fromDate = '';
        $toDate = '';

        $today = time(); //timestamp en formato unix

        if($request->has('fromDate') and $request->fromDate != '')
            $fromDate = date('Y-m-d',strtotime($request->fromDate));
        if($request->has('toDate') and $request->toDate != '')
            $toDate = date('Y-m-d',strtotime($request->toDate));

        $tipo_informe = $request->tipo_informe;
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        $timePrimeraFecha = date('d-m-Y',strtotime($primeraFecha));
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

       
//inicio calculo por produccion
        $contadorCompras = Facturacompras::where('status', '=', '2')->count();
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
        $totalComprasProducciones = 0;
        $totalComprasProductos = 0;
        $contProducciones = 0;
        $contProductos = 0;
        
        $totalCompras = 0;
foreach ($todasCompras as $key => $value) {
    # code...
    $detalles = $value->detalles;

    foreach ($detalles as $key => $value2) {
        # code...
        $totalCompras += ($value2->cantidad * $value2->precio);
        if($value->tipo == 0){ //si es produccion
            $totalComprasProducciones += ($value2->cantidad * $value2->precio);
            $contProducciones++;
        }else{
            $totalComprasProductos += ($value2->cantidad * $value2->precio);
            $contProductos++;
        }
    }
}
        $porcentaje1 = 0;
        $porcentaje2 = 0;

        if($totalCompras != 0){
            $porcentaje1 = $totalComprasProducciones * 100 / $totalCompras;
            $porcentaje2 = $totalComprasProductos * 100 / $totalCompras;
        }

        $tipos = array(
            array('cantidad' => $contProducciones, 'titulo' => 'producciones', 'porcentaje' => $porcentaje1, 'total' => $totalComprasProducciones),
            array('cantidad' => $contProductos, 'titulo' => 'productos', 'porcentaje' => $porcentaje2, 'total' => $totalComprasProductos)
        );
//fin de calculo por produccion
$productosMasComprados = array();
//dd($listaproductos2,$listaproductos_);
    //inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasComprados1 = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
    FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
     WHERE A.n_factura = B.n_factura 
     AND B.status = 2 
     AND B.tipo = 0 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (por produccion)

//inicio calculo de los 10 productos mas comprados (normal)
 $productosMasComprados2 = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra, M.nombre AS medicion 
    FROM detalle_factura_compra AS A,facturacompras AS B,productos AS C, tipo_mediciones AS M
     WHERE A.n_factura = B.n_factura 
     AND B.status = 2 
     AND B.tipo = 1 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas comprados (normal)

    
        return view('estadistica.compras2', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','compras_productos','compras_produccion','compras_todas','tipos','productosMasComprados2','productosMasComprados1','totalComprasProducciones','totalComprasProductos','listaproductos','listacategorias','facturas','tipo','contadorCompras'));
    }

    public function get_estadisticasCompras3(Request $request){
        $tipo = 0; 
        if( $request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 2;
        }
        $fromDate = '';
        $toDate = '';

        $today = time(); //timestamp en formato unix

        if($request->has('fromDate') and $request->fromDate != '')
            $fromDate = date('Y-m-d',strtotime($request->fromDate));
        if($request->has('toDate') and $request->toDate != '')
            $toDate = date('Y-m-d',strtotime($request->toDate));

        $tipo_informe = $request->tipo_informe;
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        $timePrimeraFecha = date('d-m-Y',strtotime($primeraFecha));
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

       
//inicio calculo por produccion
        $contadorCompras = Facturacompras::where('status', '=', '2')->count();
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
        $totalCompras = $todasCompras->sum('subtotal');
        $todosProducciones = $todasCompras->where('tipo','=',0)->pluck('subtotal')->all();
        $todosProductos = $todasCompras->where('tipo','=',1)->pluck('subtotal')->all();
        $totalComprasProducciones = array_sum($todosProducciones);
        $totalComprasProductos = array_sum($todosProductos);
        $total_cant_comprasProducciones = count($todosProducciones);
        $total_cant_comprasProductos = count($todosProductos);

              $totalCompras = 0;
foreach ($todasCompras as $key => $value) {
    # code...
    $detalles = $value->detalles;

    foreach ($detalles as $key => $value2) {
        # code...
        $totalCompras += ($value2->cantidad * $value2->precio);
    }
}

        
// inicio calculo de categorias mas compradas
 $categoriasMasCompradas = DB::select("SELECT C.id as id, C.nombre as nombre,COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra
    FROM detalle_factura_compra A
    INNER JOIN facturacompras B ON B.n_factura = A.n_factura
    INNER JOIN productos P ON P.id = A.producto_id
    INNER JOIN categorias C ON C.id = P.categoria_id
    WHERE B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     GROUP BY C.id
     ORDER BY (total_compra) DESC LIMIT 10",[$fromDate,$toDate]);
 //dd($categoriasMasCompradas);

 $subcategoriasMasCompradas = [];
 
 foreach ($categoriasMasCompradas as $key => $value) {
     # code...
    $subcategoriasMasCompradas[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra 
        FROM detalle_factura_compra A
        INNER JOIN facturacompras B ON A.n_factura = B.n_factura 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }


    
        return view('estadistica.compras3', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','compras_productos','compras_produccion','compras_todas','tipos','subcategoriasMasCompradas','categoriasMasCompradas','totalComprasProducciones','totalComprasProductos','tipo_informe','listaproductos','listacategorias','facturas','tipo','contadorCompras'));
    }

    public function get_estadisticasCompras4(Request $request){
        $tipo = 0; 
        if( $request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 2;
        }
        $fromDate = '';
        $toDate = '';

        $today = time(); //timestamp en formato unix

        if($request->has('fromDate') and $request->fromDate != '')
            $fromDate = date('Y-m-d',strtotime($request->fromDate));
        if($request->has('toDate') and $request->toDate != '')
            $toDate = date('Y-m-d',strtotime($request->toDate));

        $tipo_informe = $request->tipo_informe;
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        $timePrimeraFecha = date('d-m-Y',strtotime($primeraFecha));
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorCompras = Facturacompras::where('status', '=', '2')->count();
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
        $totalCompras = $todasCompras->sum('subtotal');
        $todosProducciones = $todasCompras->where('tipo','=',0)->pluck('subtotal')->all();
        $todosProductos = $todasCompras->where('tipo','=',1)->pluck('subtotal')->all();
        $totalComprasProducciones = array_sum($todosProducciones);
        $totalComprasProductos = array_sum($todosProductos);
        $total_cant_comprasProducciones = count($todosProducciones);
        $total_cant_comprasProductos = count($todosProductos);

                $totalCompras = 0;
foreach ($todasCompras as $key => $value) {
    # code...
    $detalles = $value->detalles;

    foreach ($detalles as $key => $value2) {
        # code...
        $totalCompras += ($value2->cantidad * $value2->precio);
    }
}

   
$listacategorias = Categoria::all();

$categoriasMasCompradas2 = array();
$listacategorias2 = array();
if($request->has('listacategorias')){
    $listacategorias2 = $request->listacategorias;
    
    //dd($listacategorias2);
    if(!empty($listacategorias2)){
    
    
    $listacategorias_ = implode(',',$listacategorias2);
    //dd($listacategorias2,$listacategorias_);
    $query = "SELECT C.id AS id, C.nombre AS nombre,COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra 
    FROM detalle_factura_compra A
    INNER JOIN facturacompras B ON A.n_factura = B.n_factura 
    INNER JOIN productos P ON P.id = A.producto_id 
    INNER JOIN categorias C ON C.id = P.categoria_id 
    WHERE B.status = 2
    AND B.fecha_facturacion >= ? 
    AND B.fecha_facturacion <= ? 
    AND C.id IN (".$listacategorias_.")
    GROUP BY C.id
    ORDER BY (total_compra) DESC";

    $categoriasMasCompradas2 = DB::select($query,[$fromDate,$toDate]);    
    
    }
   // dd($categoriasMasCompradas,$categoriasMasCompradas2);
    
}

 foreach ($categoriasMasCompradas2 as $key => $value) {
     # code...
    $subcategoriasMasCompradas2[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.n_factura) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_compra 
        FROM detalle_factura_compra A
        INNER JOIN facturacompras B ON A.n_factura = B.n_factura 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }

    
        return view('estadistica.compras4', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','compras_productos','compras_produccion','compras_todas','tipos','categoriasMasCompradas2','subcategoriasMasCompradas2','totalComprasProducciones','totalComprasProductos','tipo_informe','listaproductos','listacategorias','facturas','tipo','contadorCompras','listacategorias2'));
    }

    public function get_estadisticasCompras5(Request $request){
        $tipo = 0; 
        if( $request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 2;
        }
        $fromDate = '';
        $toDate = '';

        $today = time(); //timestamp en formato unix

        if($request->has('fromDate') and $request->fromDate != '')
            $fromDate = date('Y-m-d',strtotime($request->fromDate));
        if($request->has('toDate') and $request->toDate != '')
            $toDate = date('Y-m-d',strtotime($request->toDate));

        $tipo_informe = $request->tipo_informe;
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        $timePrimeraFecha = date('d-m-Y',strtotime($primeraFecha));
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $compras_produccion = array();
        $compras_productos = array();
        $compras_todas = array();
        $currentDate = strtotime($fromDate);
        $totalComprasProducciones = 0;
        $totalComprasProductos = 0;
        $totalCompras = 0;
        $dias = 0;
//inicio calculo de subtotales de compras por dias
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            $subtotal1 = 0;
            $subtotal2 = 0;
            $subtotal3 = 0;

            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $comprasProduccion  = Facturacompras::where('fecha_facturacion', '=', $date)->where('tipo','=',0)->where('status', '=', '2')->get();
            foreach ($comprasProduccion as $key => $value) {
    # code...
    				$detalles = $value->detalles;

    				foreach ($detalles as $key => $value2) {
        # code...
        				$subtotal1 += ($value2->cantidad * $value2->precio);
    				}
				}
            $comprasProductos  = Facturacompras::where('fecha_facturacion', '=', $date)->where('tipo','=',1)->where('status', '=', '2')->get();
            foreach ($comprasProductos as $key => $value) {
    # code...
    				$detalles = $value->detalles;

    				foreach ($detalles as $key => $value2) {
        # code...
        				$subtotal2 += ($value2->cantidad * $value2->precio);
    				}
				}

            $compras_productos[$currentDate] = $subtotal2;
            $compras_produccion[$currentDate] = $subtotal1;
            $compras_todas[$currentDate] = $subtotal1 + $subtotal2;
            $totalCompras += $subtotal1 + $subtotal2;
            $totalComprasProducciones += $subtotal1;
            $totalComprasProductos += $subtotal2;
            $currentDate += $aDay; //se le suma un dia a la fecha
            if($comprasProduccion->count() + $comprasProductos->count() > 0 )
                $dias ++; 
        }
       // dd($dias);
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $aux = Facturacompras::where('status', '=', '2')->get();
        $contadorCompras = count($aux);
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
        
        $total_cant_comprasProducciones = $todasCompras->where('tipo','=',0)->count();
        $total_cant_comprasProductos = $todasCompras->where('tipo','=',1)->count();



        return view('estadistica.compras5', compact('toDate', 'fromDate','cantTotalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','totalCompras','compras_productos','compras_produccion','compras_todas','tipos','totalComprasProducciones','totalComprasProductos','tipo_informe','listaproductos','listacategorias','facturas','tipo','contadorCompras'));
    }

    public function estadisticasCompras6($fromDate = '', $toDate = '' , Request $request){
        $tipo = 2; 
        if( $request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 2;
        }
        $fromDate = '';
        $toDate = '';

        $today = time(); //timestamp en formato unix

        if($request->has('fromDate') and $request->fromDate != '')
            $fromDate = date('Y-m-d',strtotime($request->fromDate));
        if($request->has('toDate') and $request->toDate != '')
            $toDate = date('Y-m-d',strtotime($request->toDate));

        $tipo_informe = $request->tipo_informe;
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        $timePrimeraFecha = date('d-m-Y',strtotime($primeraFecha));
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy


//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contTodasCompras = Facturacompras::where('status', '=', '2')->count();
        //$contadorCompras = count($contTodasCompras);
        

if($tipo != 2){
    $facturas = Facturacompras::where('status','=',2)->where('tipo','=',$tipo)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es     
}else{
    $facturas = Facturacompras::where('status','=',2)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es 
}

    
        return view('estadistica.compras6', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','compras_productos','compras_produccion','compras_todas','tipos','totalComprasProducciones','totalComprasProductos','contTodasCompras','listaproductos','listacategorias','facturas','tipo'));
    }

    public function get_estadisticasCompras6(Request $request){
        $tipo = 0; 
        if( $request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 2;
        }
        $fromDate = '';
        $toDate = '';

        $today = time(); //timestamp en formato unix

        if($request->has('fromDate') and $request->fromDate != '')
            $fromDate = date('Y-m-d',strtotime($request->fromDate));
        if($request->has('toDate') and $request->toDate != '')
            $toDate = date('Y-m-d',strtotime($request->toDate));

        $tipo_informe = $request->tipo_informe;
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        $timePrimeraFecha = date('d-m-Y',strtotime($primeraFecha));
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        
        //fin de calculo de subtotales por dia
        //inicio calculo por produccion
        $contTodasCompras = Facturacompras::where('status', '=', '2')->count();
        $contadorCompras = count($contTodasCompras);
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
        //$totalCompras = $todasCompras->sum('subtotal');
        
        //$totalComprasProducciones = array_sum($todosProducciones);
        //$totalComprasProductos = array_sum($todosProductos);
        $total_cant_comprasProducciones = $todasCompras->where('tipo','=',0)->count();
        $total_cant_comprasProductos = $todasCompras->where('tipo','=',1)->count();


        if($tipo != 2){
            $facturas = Facturacompras::where('status','=',2)->where('tipo','=',$tipo)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es     
        }else{
            $facturas = Facturacompras::where('status','=',2)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es 
        }

    
        return view('estadistica.compras6', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','total_cant_comprasProducciones','total_cant_comprasProductos','compras_productos','compras_produccion','compras_todas','tipos','totalComprasProducciones','totalComprasProductos','contTodasCompras','listaproductos','listacategorias','facturas','tipo','contadorCompras'));
    }

    public function get_estadisticasCompras7(Request $request){
        $tipo = 0; 
        if( $request->has("tipo")){
            $tipo = $request->tipo;
        }else{
            $tipo = 2;
        }
        $fromDate = '';
        $toDate = '';
        $today = time(); //timestamp en formato unix
        if($request->has('fromDate') and $request->fromDate != '')
            $fromDate = date('Y-m-d',strtotime($request->fromDate));
        if($request->has('toDate') and $request->toDate != '')
            $toDate = date('Y-m-d',strtotime($request->toDate));

        $tipo_informe = $request->tipo_informe;
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturacompras::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != '' ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        
        //inicio calculo por produccion
        $contadorCompras = Facturacompras::where('status', '=', '2')->count();
        $todasCompras = Facturacompras::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalCompras = $todasCompras->count();
       
        
        $totalCompras = 0;
        foreach ($todasCompras as $key => $value) {
            # code...
            $detalles = $value->detalles;

            foreach ($detalles as $key => $value2) {
                # code...
                $totalCompras += ($value2->cantidad * $value2->precio);
            }
        }

               
        //fin de calculo por produccion
        $sociosMasComprados = array();
        $listasocios2 = array();
        if($request->has('listasocios')){
            $listasocios2 = $request->listasocios;
            
            //dd($listasocios2);
            if(!empty($listasocios2)){
            
            $listasocios_ = implode(',',$listasocios2);

            
            //dd($listasocios2,$listasocios_);
            $sociosMasComprados = DB::select("SELECT S.id as id, S.nombre as nombre, SUM(FC.total) AS total_compra 
            FROM facturacompras AS FC
            INNER JOIN socios S ON FC.socio_id = S.id
             WHERE FC.status = 2 
             AND FC.tipo = 0 
             AND FC.fecha_facturacion >= ? 
             AND FC.fecha_facturacion <= ? 
             AND FC.socio_id IN (".$listasocios_.")
             GROUP BY FC.socio_id
             ORDER BY (total_compra) DESC",[$fromDate,$toDate]);
            
                
            }
        }
            
        $listasocios = Socio::all();

        if($tipo != 2){
            $facturas = Facturacompras::where('status','=',2)->where('tipo','=',$tipo)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es     
        }else{
            $facturas = Facturacompras::where('status','=',2)->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es 
        }

    
        return view('estadistica.compras7', compact('toDate', 'fromDate','cantTotalCompras','totalCompras','sociosMasComprados','totalComprasProducciones','tipo_informe','listasocios2','listasocios','facturas','tipo','contadorCompras'));
    }

    //------------------------- CALCULOS PARA LAS ESTADISTICAS DE VENTAS  ----------
    
    public function estadisticasVentas($fromDate = '',$toDate = ''){
        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (120*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturaventas::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != ''  ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array();
        $currentDate = strtotime($fromDate);

//inicio calculo de subtotales de compras por dias
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturaventas::where('fecha_facturacion', '=', $date)->where('status', '=', 2)->sum('subtotal');
            $ventas[$currentDate] = $subtotal;
            $currentDate += $aDay; //se le suma un dia a la fecha
        }
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos

        //inicio calculo de los 10 productos menos vendidos
$productosMenosVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
 WHERE B.id = A.factura_id 
 AND B.status = 2 
 AND B.fecha_facturacion >= ? 
 AND B.fecha_facturacion <= ? 
 AND A.producto_id = C.id
 AND C.medicion_id = M.id 
 GROUP BY C.id
 ORDER BY (total_venta) ASC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos menos vendidos
//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas1', compact('toDate', 'fromDate','cantTotalVentas','ventas','productosMenosVendidos','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function get_estadisticasVentas(Request $request){
    $today = time(); //timestamp en formato unix
        
        
        $fromDate = date('Y-m-d',strtotime($request->fromDate));
        $toDate = date('Y-m-d',strtotime($request->toDate));
        $tipo_informe = $request->tipo_informe;

        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array(); // array que guardara el subtotal diario de las compras de produccion
        //dd($fromDate,$toDate);
        $currentDate = strtotime($fromDate); //fecha inicial en formato UNIX, (inicializada en la fecha de inicio recibida)

        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            
            $ventas[$currentDate] = $subtotal; //el total de la suma de los subtotales ddel dia, de las compras de productos
            
            $currentDate += $aDay; //se le suma un dia a la fecha actual
        }

    //inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', '2')->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos

        //inicio calculo de los 10 productos menos vendidos
$productosMenosVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
 WHERE B.id = A.factura_id 
 AND B.status = 2 
 AND B.fecha_facturacion >= ? 
 AND B.fecha_facturacion <= ? 
 AND A.producto_id = C.id
 AND C.medicion_id = M.id 
 GROUP BY C.id
 ORDER BY (total_venta) ASC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos menos vendidos
//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas1', compact('toDate', 'fromDate','cantTotalVentas','ventas','productosMenosVendidos','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function estadisticasVentas2($fromDate = '',$toDate = ''){
    $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (120*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturaventas::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != ''  ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array();
        $currentDate = strtotime($fromDate);

//inicio calculo de subtotales de compras por dias
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturaventas::where('fecha_facturacion', '=', $date)->where('status', '=', 2)->sum('subtotal');
            $ventas[$currentDate] = $subtotal;
            $currentDate += $aDay; //se le suma un dia a la fecha
        }
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos

//inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
     WHERE B.id = A.factura_id 
     AND B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id
     GROUP BY C.id 
     ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas vendidos (por produccion)


//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas2', compact('toDate', 'fromDate','cantTotalVentas','ventas','productosMasVendidos','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function get_estadisticasVentas2(Request $request){
    $today = time(); //timestamp en formato unix
        
        
        $fromDate = date('Y-m-d',strtotime($request->fromDate));
        $toDate = date('Y-m-d',strtotime($request->toDate));
        $tipo_informe = $request->tipo_informe;

        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array(); // array que guardara el subtotal diario de las compras de produccion
        //dd($fromDate,$toDate);
        $currentDate = strtotime($fromDate); //fecha inicial en formato UNIX, (inicializada en la fecha de inicio recibida)

        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            
            $ventas[$currentDate] = $subtotal; //el total de la suma de los subtotales ddel dia, de las compras de productos
            
            $currentDate += $aDay; //se le suma un dia a la fecha actual
        }

    //inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', '2')->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');
        
        //fin de calculos genericos

//inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
     WHERE B.id = A.factura_id 
     AND B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id
     GROUP BY C.id 
     ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas vendidos (por produccion)


//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas2', compact('toDate', 'fromDate','cantTotalVentas','ventas','productosMasVendidos','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function estadisticasVentas3($fromDate = '',$toDate = ''){
    $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (120*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturaventas::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != ''  ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array();
        $currentDate = strtotime($fromDate);

//inicio calculo de subtotales de compras por dias
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturaventas::where('fecha_facturacion', '=', $date)->where('status', '=', 2)->sum('subtotal');
            $ventas[$currentDate] = $subtotal;
            $currentDate += $aDay; //se le suma un dia a la fecha
        }
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos

//inicio calculo de los 5 categorias mas vendidas
 $categoriasMasVendidas = DB::select("SELECT C.id as id, C.nombre as nombre,P.nombre as nombre_p, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta A 
    INNER JOIN facturaventas B ON B.id = A.factura_id 
    INNER JOIN productos P ON P.id = A.producto_id 
    INNER JOIN categorias C ON C.id = P.categoria_id 
    INNER JOIN tipo_mediciones M ON M.id = P.medicion_id 
    WHERE B.status = 2 
    AND B.fecha_facturacion >= ? 
    AND B.fecha_facturacion <= ? 
    GROUP BY C.id 
    ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
 
 //dd($categoriasMasVendidas);
//fin calculo de los 5 categorias mas vendidas
$subcategoriasMasVendidas = [];
 
 foreach ($categoriasMasVendidas as $key => $value) {
     # code...
    $subcategoriasMasVendidas[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }
 //dd($productosMasVendidos,$productosMenosVendidos);
 $productosSubcategoriasMasVendidas = [];

 foreach ($categoriasMasVendidas as $key1 => $value1) {
     # code...
    foreach ($subcategoriasMasVendidas[$value1->nombre] as $key2 => $value2) {
    
    $productos = DB::select("SELECT P.id, P.nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE P.subcategoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY P.id",[$value2->id,$fromDate,$toDate]);
    $productosSubcategoriasMasVendidas[$value2->id] = $productos;
    }
    
    }

//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas3', compact('toDate', 'fromDate','cantTotalVentas','ventas','categoriasMasVendidas','subcategoriasMasVendidas','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function get_estadisticasVentas3(Request $request){
    $today = time(); //timestamp en formato unix
        
        
        $fromDate = date('Y-m-d',strtotime($request->fromDate));
        $toDate = date('Y-m-d',strtotime($request->toDate));
        $tipo_informe = $request->tipo_informe;

        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array(); // array que guardara el subtotal diario de las compras de produccion
        //dd($fromDate,$toDate);
        $currentDate = strtotime($fromDate); //fecha inicial en formato UNIX, (inicializada en la fecha de inicio recibida)

        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            
            $ventas[$currentDate] = $subtotal; //el total de la suma de los subtotales ddel dia, de las compras de productos
            
            $currentDate += $aDay; //se le suma un dia a la fecha actual
        }

    //inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', '2')->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos

        //inicio calculo de los 5 categorias mas vendidas
 $categoriasMasVendidas = DB::select("SELECT C.id as id, C.nombre as nombre,P.nombre as nombre_p, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta A 
    INNER JOIN facturaventas B ON B.id = A.factura_id 
    INNER JOIN productos P ON P.id = A.producto_id 
    INNER JOIN categorias C ON C.id = P.categoria_id 
    INNER JOIN tipo_mediciones M ON M.id = P.medicion_id 
    WHERE B.status = 2 
    AND B.fecha_facturacion >= ? 
    AND B.fecha_facturacion <= ? 
    GROUP BY C.id 
    ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
 
 //dd($categoriasMasVendidas);
//fin calculo de los 5 categorias mas vendidas
$subcategoriasMasVendidas = [];
 
 foreach ($categoriasMasVendidas as $key => $value) {
     # code...
    $subcategoriasMasVendidas[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }
 //dd($productosMasVendidos,$productosMenosVendidos);
 $productosSubcategoriasMasVendidas = [];

 foreach ($categoriasMasVendidas as $key1 => $value1) {
     # code...
    foreach ($subcategoriasMasVendidas[$value1->nombre] as $key2 => $value2) {
    
    $productos = DB::select("SELECT P.id, P.nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE P.subcategoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY P.id",[$value2->id,$fromDate,$toDate]);
    $productosSubcategoriasMasVendidas[$value2->id] = $productos;
    }
    
    }

//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas3', compact('toDate', 'fromDate','cantTotalVentas','ventas','categoriasMasVendidas','subcategoriasMasVendidas','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function estadisticasVentas4($fromDate = '',$toDate = ''){
        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (120*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturaventas::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != ''  ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array();
        $currentDate = strtotime($fromDate);

//inicio calculo de subtotales de compras por dias
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturaventas::where('fecha_facturacion', '=', $date)->where('status', '=', 2)->sum('subtotal');
            $ventas[$currentDate] = $subtotal;
            $currentDate += $aDay; //se le suma un dia a la fecha
        }
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos

    
 //dd($productosMasVendidos,$productosMenosVendidos);
 
//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas4', compact('toDate', 'fromDate','cantTotalVentas','ventas','categoriasMasVendidas2','subcategoriasMasVendidas2','totalVentas','tipo_informe','listaproductos','listacategorias','listacategorias2','facturas','contadorVentas'));  
}

public function get_estadisticasVentas4(Request $request){
    $today = time(); //timestamp en formato unix
        
        
        $fromDate = date('Y-m-d',strtotime($request->fromDate));
        $toDate = date('Y-m-d',strtotime($request->toDate));
        $tipo_informe = $request->tipo_informe;

        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array(); // array que guardara el subtotal diario de las compras de produccion
        //dd($fromDate,$toDate);
        $currentDate = strtotime($fromDate); //fecha inicial en formato UNIX, (inicializada en la fecha de inicio recibida)

        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            
            $ventas[$currentDate] = $subtotal; //el total de la suma de los subtotales ddel dia, de las compras de productos
            
            $currentDate += $aDay; //se le suma un dia a la fecha actual
        }

    //inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', '2')->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos

        if($request->has('listacategorias')){
    $listacategorias = Categoria::all(); // esta es la lista de todos los productos registrados

    $listacategorias2 = array();
    $listacategorias2 = $request->listacategorias;//esta es la lista de productos seleccionados y enviados por el usuario
    
    //dd($listaproductos2);
    if(!empty($listacategorias2)){
    
    $listacategorias_ = implode(',',$listacategorias2);
    $query = "SELECT C.id AS id, C.nombre AS nombre,COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
    FROM detalle_factura_venta A
    INNER JOIN facturaventas B ON A.factura_id = B.id 
    INNER JOIN productos P ON P.id = A.producto_id 
    INNER JOIN categorias C ON C.id = P.categoria_id 
    WHERE B.status = 2
    AND B.fecha_facturacion >= ? 
    AND B.fecha_facturacion <= ?  
    AND C.id IN (".$listacategorias_.")
    GROUP BY C.id
    ORDER BY (total_venta) DESC";

    $categoriasMasVendidas2 = DB::select($query,[$fromDate,$toDate]);    
    
    }
        
 
    $subcategoriasMasVendidas2 = [];
 
 foreach ($categoriasMasVendidas2 as $key => $value) {
     # code...
    $subcategoriasMasVendidas2[$value->nombre] = DB::select("SELECT S.id, S.nombre, COUNT(A.factura_id) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta 
        FROM detalle_factura_venta A
        INNER JOIN facturaventas B ON A.factura_id = B.id 
        INNER JOIN productos P ON A.producto_id = P.id 
        INNER JOIN categorias C ON P.categoria_id = C.id 
        INNER JOIN subcategorias S ON S.id = P.subcategoria_id 
        WHERE S.categoria_id = ?
        AND B.status = 2
        AND B.fecha_facturacion >= ? 
        AND B.fecha_facturacion <= ?  
        GROUP BY S.id",[$value->id,$fromDate,$toDate]);
 }

 
}
//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas4', compact('toDate', 'fromDate','cantTotalVentas','ventas','categoriasMasVendidas2','subcategoriasMasVendidas2','totalVentas','tipo_informe','listaproductos','listacategorias','listacategorias2','facturas','contadorVentas'));  
}

public function estadisticasVentas5($fromDate = '',$toDate = ''){
    $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (120*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturaventas::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != ''  ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array();
        $currentDate = strtotime($fromDate);

//inicio calculo de subtotales de compras por dias
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturaventas::where('fecha_facturacion', '=', $date)->where('status', '=', 2)->sum('subtotal');
            $ventas[$currentDate] = $subtotal;
            $currentDate += $aDay; //se le suma un dia a la fecha
        }
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos
//inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
     WHERE B.id = A.factura_id 
     AND B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id
     GROUP BY C.id 
     ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas vendidos (por produccion)

        //inicio calculo de los 10 productos menos vendidos
$productosMenosVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
 WHERE B.id = A.factura_id 
 AND B.status = 2 
 AND B.fecha_facturacion >= ? 
 AND B.fecha_facturacion <= ? 
 AND A.producto_id = C.id
 AND C.medicion_id = M.id 
 GROUP BY C.id
 ORDER BY (total_venta) ASC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos menos vendidos
//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas5', compact('toDate', 'fromDate','cantTotalVentas','ventas','productosMenosVendidos','productosMasVendidos','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function get_estadisticasVentas5(Request $request){
    $today = time(); //timestamp en formato unix
        
        
        $fromDate = date('Y-m-d',strtotime($request->fromDate));
        $toDate = date('Y-m-d',strtotime($request->toDate));
        $tipo_informe = $request->tipo_informe;

        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array(); // array que guardara el subtotal diario de las compras de produccion
        //dd($fromDate,$toDate);
        $currentDate = strtotime($fromDate); //fecha inicial en formato UNIX, (inicializada en la fecha de inicio recibida)

        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            
            $ventas[$currentDate] = $subtotal; //el total de la suma de los subtotales ddel dia, de las compras de productos
            
            $currentDate += $aDay; //se le suma un dia a la fecha actual
        }

    //inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', '2')->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos
//inicio calculo de los 10 productos mas comprados (por produccion)
$productosMasVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
    FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
     WHERE B.id = A.factura_id 
     AND B.status = 2 
     AND B.fecha_facturacion >= ? 
     AND B.fecha_facturacion <= ? 
     AND A.producto_id = C.id
     AND C.medicion_id = M.id
     GROUP BY C.id 
     ORDER BY (total_venta) DESC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos mas vendidos (por produccion)

        //inicio calculo de los 10 productos menos vendidos
$productosMenosVendidos = DB::select("SELECT C.id as id, C.nombre as nombre, SUM(A.cantidad) AS total_cantidad, SUM(A.cantidad * A.precio) AS total_venta, M.nombre AS medicion 
FROM detalle_factura_venta AS A,facturaventas AS B,productos AS C, tipo_mediciones AS M
 WHERE B.id = A.factura_id 
 AND B.status = 2 
 AND B.fecha_facturacion >= ? 
 AND B.fecha_facturacion <= ? 
 AND A.producto_id = C.id
 AND C.medicion_id = M.id 
 GROUP BY C.id
 ORDER BY (total_venta) ASC LIMIT 10",[$fromDate,$toDate]);
//fin calculo de los 10 productos menos vendidos
//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas5', compact('toDate', 'fromDate','cantTotalVentas','ventas','productosMenosVendidos','productosMasVendidos','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}


public function estadisticasVentas6($fromDate = '',$toDate = ''){
    $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (120*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturaventas::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != ''  ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array();
        $currentDate = strtotime($fromDate);

//inicio calculo de subtotales de compras por dias
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturaventas::where('fecha_facturacion', '=', $date)->where('status', '=', 2)->sum('subtotal');
            $ventas[$currentDate] = $subtotal;
            $currentDate += $aDay; //se le suma un dia a la fecha
        }
//fin de calculo de subtotales por dia
//inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos

//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas6', compact('toDate', 'fromDate','cantTotalVentas','ventas','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function get_estadisticasVentas6(Request $request){
    $today = time(); //timestamp en formato unix
        
        
        $fromDate = date('Y-m-d',strtotime($request->fromDate));
        $toDate = date('Y-m-d',strtotime($request->toDate));
        $tipo_informe = $request->tipo_informe;

        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array(); // array que guardara el subtotal diario de las compras de produccion
        //dd($fromDate,$toDate);
        $currentDate = strtotime($fromDate); //fecha inicial en formato UNIX, (inicializada en la fecha de inicio recibida)
        $totalVentas = 0;
        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            
            $ventas[$currentDate] = $subtotal; //el total de la suma de los subtotales ddel dia, de las compras de productos
            $totalVentas += $subtotal;
            $currentDate += $aDay; //se le suma un dia a la fecha actual
        }

    //inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', '2')->get();
        $cantTotalVentas = $todasVentas->count();
        //$totalVentas = $todasVentas->sum('subtotal');
        
        //fin de calculos genericos

//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas6', compact('toDate', 'fromDate','cantTotalVentas','ventas','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function estadisticasVentas7($fromDate = '',$toDate = ''){
    $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (120*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $primeraFecha = Facturaventas::where('status','!=',0)->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        //$fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $fromDate = $fromDate != ''  ? $fromDate: $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $ventas = array();
		        
		//fin de calculo de subtotales por dia
		//inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', 2)->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');

        //fin de calculos genericos


 //fin
	$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas7', compact('toDate', 'fromDate','cantTotalVentas','ventas','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}

public function get_estadisticasVentas7(Request $request){
    $today = time(); //timestamp en formato unix
        
        
        $fromDate = date('Y-m-d',strtotime($request->fromDate));
        $toDate = date('Y-m-d',strtotime($request->toDate));
        $tipo_informe = $request->tipo_informe;

        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        $fromDate = $fromDate != '' ? $fromDate: $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $toDate = $toDate != '' ? $toDate : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $days = $this->diferenciaFechas($fromDate,$toDate); // calculamos la diferencia en dias entre la fecha de inicio y la fecha final
        
        $ventas = array(); // array que guardara el subtotal diario de las compras de produccion
        //dd($fromDate,$toDate);
        $currentDate = strtotime($fromDate); //fecha inicial en formato UNIX, (inicializada en la fecha de inicio recibida)

        for($i=0;$i<=$days;$i++){ //la idea es tener la suma de los subtotales por cada dia
            
            $date = date('Y-m-d',$currentDate); // currentDay en formato año-mes-dia

            $subtotal  = Facturacompras::where('fecha_facturacion', '=', $date)->where('status', '=', '2')->sum('subtotal');
            
            $ventas[$currentDate] = $subtotal; //el total de la suma de los subtotales ddel dia, de las compras de productos
            
            $currentDate += $aDay; //se le suma un dia a la fecha actual
        }

    //inicio calculo por produccion
        $contadorVentas = Facturaventas::where('status','=',2)->count();
        $todasVentas = Facturaventas::where('fecha_facturacion', '>=', $fromDate)->where('fecha_facturacion', '<=', $toDate)->where('status', '=', '2')->get();
        $cantTotalVentas = $todasVentas->count();
        $totalVentas = $todasVentas->sum('subtotal');
        
        //fin de calculos genericos

//inicio de los productos seleccionados por el usuario
$listaproductos = Producto::all();
$listacategorias = Categoria::all();
 //dd($productosMasVendidos,$productosMenosVendidos);
 //fin
$facturas = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->where('status','=',2)->get();//0 es produccion y 1 es productos
    
    return view('estadistica.ventas7', compact('toDate', 'fromDate','cantTotalVentas','ventas','totalVentas','tipo_informe','listaproductos','listacategorias','facturas','contadorVentas'));  
}


	public function estadisticasVentas8(Request $request){
	    $today = date('Y-m-d'); 	    
	    $now = strtotime($today);
	    $categorias = Categoria::all();
	    $totales_diarios = array();
	    $promedios_diarios = array();
	    $totales_semanales = array();
	    $promedios_semanales = array();
	    $totales_mensuales = array();
	    $promedios_mensuales = array();
	    $total_diario = 0;
	    $promedio_diario = 0;
	    $total_semanal = 0;
	    $promedio_semanal = 0;
	    $total_mensual = 0;
	    $promedio_mensual = 0;

	    $ventas_hoy = Facturaventas::where('status',2)->where('fecha_facturacion', '=', $today)->where('hora_facturacion', '<=', date('H:i:s'))->get();

	    // para ventas diarias
	    for($i = 0; $i <= date('H') + 1; $i++){ //la idea es tener la suma de los subtotales por cada dia
	    
	    	if($i == 24) break;

	    	$current_ventas  = $ventas_hoy->where('hora_facturacion', '<=', date('H:i:s', $now))->pluck('subtotal')->all();
	    	
	        $current_total_diario  = array_sum($current_ventas);
	    	    
	        $current_promedio_diario = round(count($current_ventas) > 0 ? $current_total_diario / count($current_ventas) : 0, 2);

	        array_push($totales_diarios, $current_total_diario);

	        array_push($promedios_diarios, $current_promedio_diario); 
	        
	        $now = strtotime('+1 hours', $now); //se le suma una hora a la hora actual del ciclo = $i
	    }

	    $total_diario  = $ventas_hoy->sum('subtotal');
	    	    
	    $promedio_diario = round((count($ventas_hoy) > 0 ? ($total_diario / count($ventas_hoy)) : 0), 2);

	    // para ventas semanales
	    $startDate = date(sprintf('Y-m-%s', "01"));
	    $startDateTime = strtotime($startDate);
	    $finishDate = $today;
	    $finishDateTime = strtotime($finishDate);
	    $finishWeek = date('W', $finishDateTime);
	    $startWeek = date('W', $startDateTime);
	    $currentWeek = (int)$startWeek;    
	    
	    
	    while($currentWeek <= $finishWeek + 1) {
	    		
    		$result = DB::select("SELECT SUM(subtotal) AS subtotal FROM facturaventas WHERE status = 2 AND WEEK(fecha_facturacion) = " . $currentWeek . " GROUP BY WEEK(fecha_facturacion)");
    		
    		$currentWeek++;
    		$subtotal = isset($result[0]->subtotal) ? $result[0]->subtotal : 0;
    		$promedio_semanal = $subtotal / (count($promedios_semanales) > 0 ? count($promedios_semanales) : 1);
    		array_push($totales_semanales, round($subtotal,2));
    		array_push($promedios_semanales, round($promedio_semanal,2));
	    }
	    $promedio_semanal = array_sum($promedios_semanales);
	    $total_semanal = array_sum($totales_semanales);

	    for($i = 1 ; $i <= 12 ; $i++){
	    		
    		$result = DB::select("SELECT SUM(subtotal) AS subtotal FROM facturaventas WHERE status = 2 AND MONTH(fecha_facturacion) = " . $i . " AND YEAR(fecha_facturacion) = " . date('Y') . "  GROUP BY MONTH(fecha_facturacion)");
    		
    		
    		$subtotal = isset($result[0]->subtotal) ? $result[0]->subtotal : 0;
    		$promedio_mensual = $subtotal / (count($promedios_mensuales) > 0 ? count($promedios_mensuales) : 1);
    		array_push($totales_mensuales, round($subtotal,2));
    		array_push($promedios_mensuales, round($promedio_mensual,2));
	    }
	    //dd($totales_mensuales);
	    $promedio_mensual = array_sum($promedios_mensuales);
	    $total_mensual = array_sum($totales_mensuales);

	    return view('estadistica.ventas8', compact('totales_diarios','total_diario','promedios_diarios','promedio_diario','categorias','totales_semanales','total_semanal','startDate','promedios_semanales','promedio_semanal','total_mensual','promedios_mensuales','promedio_mensual','totales_mensuales'));
	}

	public function getEstadisticasVentas8(Request $request){
	    $today = date('Y-m-d'); 	    
	    $now = strtotime("00:00:00");
	    $categorias = Categoria::all();
	    $totales_diarios = array();
	    $promedios_diarios = array();
	    $totales_semanales = array();
	    $promedios_semanales = array();
	    $totales_mensuales = array();
	    $promedios_mensuales = array();
	    $total_diario = 0;
	    $promedio_diario = 0;
	    $total_semanal = 0;
	    $promedio_semanal = 0;
	    $total_mensual = 0;
	    $promedio_mensual = 0;
	    $categoria_id = null;
	    $whereCategoria = '';

	    if($request->has("categoria_id") && $request->categoria_id != 0){
	    	$categoria_id = $request->categoria_id;
	    	$whereCategoria .= ' AND P.categoria_id = ' . $categoria_id;
	    }
	    
	    // para ventas diarias
	    for($i = 0; $i <= date('H') + 1; $i++){ //la idea es tener la suma de los subtotales por cada dia
	    
	    	if($i == 24) break;

	    	$query = 'SELECT  SUM(DFV.precio * DFV.cantidad) AS subtotal FROM detalle_factura_venta DFV INNER JOIN facturaventas FV ON DFV.factura_id = FV.id	INNER JOIN productos P ON DFV.producto_id = P.id WHERE FV.status = 2 AND FV.fecha_facturacion = "' . $today . '" AND HOUR(FV.hora_facturacion) <= "' . date('H:i:s', $now) .'"';

	    	if($categoria_id != null)
	    		$query .= $whereCategoria;
	    	
	    	$result = DB::select($query);

	        $current_total_diario  = isset($result[0]->subtotal) ? $result[0]->subtotal : 0;
	    	    
	        $current_promedio_diario = count($promedios_diarios) > 0 ? $current_total_diario / count($totales_diarios) : $current_total_diario;

	        array_push($totales_diarios, round($current_total_diario, 2));

	        array_push($promedios_diarios, round($current_promedio_diario, 2)); 
	        
	        $now = strtotime('+1 hours', $now); //se le suma una hora a la hora actual del ciclo = $i
	    }

	    $total_diario  = $current_total_diario;
	    	    
	    $promedio_diario = $current_promedio_diario;

	    // para ventas semanales
	    $startDate = date(sprintf('Y-m-%s', "01"));
	    $startDateTime = strtotime($startDate);
	    $finishDate = $today;
	    $finishDateTime = strtotime($finishDate);
	    $finishWeek = date('W', $finishDateTime);
	    $startWeek = date('W', $startDateTime);
	    $currentWeek = (int)$startWeek;    
	    	    
	    while($currentWeek <= $finishWeek + 1) {

	    	$query = 'SELECT SUM(DFV.precio * DFV.cantidad) AS subtotal FROM detalle_factura_venta DFV INNER JOIN facturaventas FV ON DFV.factura_id = FV.id	INNER JOIN productos P ON DFV.producto_id = P.id WHERE FV.status = 2 AND WEEK(FV.fecha_facturacion) = ' . $currentWeek . ' GROUP BY WEEK(FV.fecha_facturacion)';

	    	if($categoria_id != null)
	    		$query .= $whereCategoria;
	    	
	    	$result = DB::select($query);
    		
    		$currentWeek++;
    		$subtotal = isset($result[0]->subtotal) ? $result[0]->subtotal : 0;
    		$promedio_semanal = $subtotal / (count($promedios_semanales) > 0 ? count($promedios_semanales) : 1);
    		array_push($totales_semanales, round($subtotal,2));
    		array_push($promedios_semanales, round($promedio_semanal,2));
	    }
	    $promedio_semanal = array_sum($promedios_semanales);
	    $total_semanal = array_sum($totales_semanales);

	    for($i = 1 ; $i <= 12 ; $i++){

	    	$query = 'SELECT SUM(DFV.precio * DFV.cantidad) AS subtotal FROM detalle_factura_venta DFV INNER JOIN facturaventas FV ON DFV.factura_id = FV.id	INNER JOIN productos P ON DFV.producto_id = P.id WHERE FV.status = 2 AND MONTH(FV.fecha_facturacion) = ' . $i . ' AND YEAR(FV.fecha_facturacion) = ' . date('Y') . ' GROUP BY MONTH(FV.fecha_facturacion)';

	    	if($categoria_id != null)
	    		$query .= $whereCategoria;
	    	
	    	$result = DB::select($query);
    		
    		$subtotal = isset($result[0]->subtotal) ? $result[0]->subtotal : 0;
    		$promedio_mensual = $subtotal / (count($promedios_mensuales) > 0 ? count($promedios_mensuales) : 1);
    		array_push($totales_mensuales, round($subtotal,2));
    		array_push($promedios_mensuales, round($promedio_mensual,2));
	    }
	    //dd($totales_mensuales);
	    $promedio_mensual = array_sum($promedios_mensuales);
	    $total_mensual = array_sum($totales_mensuales);
 
	    return view('estadistica.ventas8', compact('totales_diarios','total_diario','promedios_diarios','promedio_diario','categorias','totales_semanales','total_semanal','startDate','promedios_semanales','promedio_semanal','total_mensual','promedios_mensuales','promedio_mensual','totales_mensuales','categoria_id'));
	}
        
}

