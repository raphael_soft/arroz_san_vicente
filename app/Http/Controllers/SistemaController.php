<?php

namespace App\Http\Controllers;


use App\Sistema;
use App\Currency;
use Illuminate\Http\Request;

class SistemaController extends Controller {
    
    
    public function index()

    {
        $data = Sistema::first();
        
        $currencies = Currency::all();

        if(!is_link("/home/pajdiblz/public_html/storage")){
                

        }else{
            //dd(is_link("/home/pajdiblz/public_html/storage"));    
        }
        //dd($status_table);
        return view('configuracion.sistema.index', compact('data','currencies'));

    }

    public function store(Request $request)

    {
        
        $sistema = Sistema::first();

        $sistema->nombre = $request->nombre;    

        $sistema->razon_social = $request->razon_social;    
        
        $sistema->telefono1 = $request->telefono1;
        
        $sistema->email1 = $request->email;

        $sistema->telefono2 = $request->telefono2;
        
        $sistema->email2 = $request->email2;
        
        $sistema->ruc = $request->ruc;
        
        $sistema->direccion = $request->direccion;

        $sistema->membrete = $request->membrete;

        //$sistema->currencies_id = $request->currency;
        
        $sistema->nombre_representante = $request->nombre_representante;
        
        $sistema->cedula_representante = $request->cedula_representante;
        
        $sistema->telefono_representante = $request->telefono_representante;
        
        $sistema->direccion_representante = $request->direccion_representante;

        $sistema->show_logo1_repo = $request->has('show_logo1_repo') ? true : false;

        $sistema->show_membrete = $request->has('show_membrete') ? true : false;

        if($request->has('logo1')){

            //verificamos si existe el symlink o acceso directoa
            /*if(!is_link("/home/pajdiblz/public_html/storage")){

                
            }*/
            
            $path_logo1 = $request->file("logo1")->store("public");
            //$path_logo1 = Storage::putFile('public', $request->file("logo1"),'public');
            $sistema->logo1 = $path_logo1;
        }
        
        $sistema->update();


        return redirect('/configuracion/sistema')->with('status','Actualizacion exitosa!');
    }

}
