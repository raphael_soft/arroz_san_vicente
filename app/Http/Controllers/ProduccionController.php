<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests\ProduccionFormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\InventarioProduccion;
use App\Produccion;
use App\Producto;
use App\Terreno;
use App\Cosecha;
use App\Inventario;
use App\TipoMedicion;
use App\TipoCosecha;
use App\Hectarea;
use App\Calificacion;
use App\Insumo;
use App\Sistema;
use Illuminate\Http\Request;

class ProduccionController extends Controller
{
    public function calificaciones(Request $request)
    {
        $calificaciones = Calificacion::all();
        return view('produccion.calificaciones.index', compact('calificaciones'));
    }

    public function terrenos(Request $request)
    {
        $terrenos = Terreno::all();
        return view('produccion.terrenos.index', compact('terrenos'));
    }

    public function insumos(Request $request)
    {
        $insumos = Insumo::all();
        return view('produccion.insumos.index', compact('insumos'));
    }

    public function asignar_insumo(Request $request)
    {
        $productos = inventario::with('producto')->where('cantidad','>',0)->get();
        $terrenos = Terreno::all();
        return view('produccion.insumos.create', compact('productos', 'terrenos'));
    }

    public function store_insumo(Request $request){
        if(!$request->has('insumos') || empty($request->insumos))
            abort(404);
        $response = ["msg" => "Registro Fallido", "code" => "no"];

        $insumos = $request->insumos;

        DB::beginTransaction();
        try{
            foreach ($insumos as $key => $insumo) {

                $insertedInsumo = Insumo::create([
                    "producto_id" => $insumo['productoId'],
                    "terreno_id" => $insumo['terrenoId'],
                    "cantidad" => $insumo['cantidad'],
                    "user_id" => Auth::user()->id,
                    "costo_unitario" => $insumo['costo_unitario'],
                    "costo_total" => $insumo['cantidad'] * $insumo['costo_unitario'],
                    "fecha" => date('Y-m-d H:i:s')
                ]);

                if(!isset($insertedInsumo->id)){
                    DB::rollback();
                    $response = ["msg" => "Registro Fallido: Error al insertar insumo", "cod" => "no"];
                    return response()->json($response);
                }

                $inventario = Inventario::where('producto_id', $insumo['productoId'])->where('tipo', 1)->first();
                if(!isset($inventario->id)){
                	DB::rollback();
                	$response = ["msg" => "Registro Fallido: Error al obtener el inventario del producto. Verifique la existencia de este producto en inventario", "cod" => "no"];
                	return response()->json($response);
                }
                
                $inventario->cantidad -= $insumo['cantidad'];
                $inventario->update();
            }
            $response = ["msg" => "Registro Exitoso!", "cod" => "ok"];
            DB::commit();
        }catch(\Exception $e){            
            DB::rollback();
            $response = ["msg" => "Registro Fallido: " . $e->getMessage(), "cod" => "no"];
            return response()->json($response);
        }

        return response()->json($response);
    }

    public function show_insumo($id){

        $insumo = Insumo::findOrFail($id);
        $terrenos = Terreno::all();
        $productos = inventario::with('producto')->where('cantidad','>',0)->get();
        $editMode = false;
        return view('produccion.insumos.show', compact('insumo','editMode','terrenos','productos'));
    }

    public function tipos_cosechas(Request $request)
    {
        $tipos = TipoCosecha::all();
        return view('produccion.tipos_cosechas.index', compact('tipos'));
    }

    public function cosechas(Request $request)
    {
        $cosechas = Cosecha::all();
        return view('produccion.cosechas.index', compact('cosechas'));
    }

    public function create_calificacion(Request $request)
    {
        $cosechas = DB::select('SELECT CO.* FROM cosechas CO 
		LEFT JOIN calificaciones CA ON CO.id = CA.cosecha_id
		WHERE CO.peso > 0 HAVING SUM(CA.peso) IS NULL OR SUM(CA.peso) < CO.peso');
        return view('produccion.calificaciones.create', compact('cosechas'));
    }

    public function create_terreno(Request $request)
    {
        $hectareas = Hectarea::all();
        return view('produccion.terrenos.create', compact('hectareas'));
    }

    public function create_cosecha(Request $request)
    {
        $terrenos = Terreno::all();
        $tipos = TipoCosecha::all();
        $productos = Producto::where('categoria_id','1')->get();
        return view('produccion.cosechas.create', compact('terrenos', 'tipos','productos'));
    }

    public function create_tipo_cosecha(Request $request)
    {
        return view('produccion.tipos_cosechas.create');
    }

    public function store_calificacion(Request $request)
    {
    	$this->validate(request(),[
            'fecha' => 'required',
            'peso' => 'required',
            'cosecha' => 'required',
            'calificacion' => 'required|max:100|min:0'
        ]);
        $pesoCalificado = 0;
        $calificaciones = Calificacion::where('cosecha_id', $request->cosecha)->get();
        
        if($calificaciones->count() > 0)
        	$pesoCalificado = $calificaciones->sum('peso');

        $cosecha = Cosecha::findOrFail($request->cosecha);
        
        if($pesoCalificado + $request->peso > $cosecha->peso)
			return back()->with('error', 'El peso total calificado es superior al peso de la cosecha');

        Calificacion::create(
            [
                "fecha" => $request->fecha,
                "peso" => $request->peso,
                "cosecha_id" => $request->cosecha,
                "calificacion" => $request->calificacion,
            ]);

        return redirect(route('calificaciones'))->with('status', 'Registro exitoso');
    }

    public function store_terreno(Request $request)
    {
        Terreno::updateOrCreate(
            [
                "nombre" => $request->nombre,
                "hectareas" => $request->hectareas
            ]);

        return redirect('/produccion/terrenos')->with('status', 'Registro exitoso');
    }

    public function store_cosecha(Request $request)
    {
        DB::beginTransaction();
        try{
            $cosecha = Cosecha::create(
                [
                    "nombre" => $request->nombre,
                    "producto_id" => $request->producto,
                    "peso" => $request->peso,
                    "tipo_id" => $request->tipo,
                    "precio_venta" => $request->precio_venta,
                    "terreno_id" => $request->terreno,
                    "fecha" => $request->fecha
                ]);

            if(isset($cosecha->id)){
                $inventario = Inventario::where('producto_id', $request->producto)->where('tipo', 0)->first();

                if(isset($inventario->id)){

                    $inventario->cantidad += $request->peso;
                    $inventario->update();
                }else{
                    $inventario = Inventario::create(
                    [
                        "producto_id" => $request->producto,
                        "tipo" => 0, //tipo inventario de produccion
                        "cantidad" => $request->peso
                    ]);

                    if(!isset($inventario->id)){
                        DB::rollback();
                        return redirect(route('cosechas'))->with('error', 'Fallo al crear inventario');
                    }
                }
                DB::commit();
            }else{
                DB::rollback();
                return redirect(route('cosechas'))->with('error', 'Fallo al crear cosecha');
            }
        }catch(\Exception $e){
            DB::rollback();
            return redirect(route('cosechas'))->with('error', 'Fallo al registrar: ' . $e->getMessage());
        }

        return redirect(route('cosechas'))->with('status', 'Registro exitoso');
    }

    public function store_tipo_cosecha(Request $request)
    {
        TipoCosecha::updateOrCreate(
            [
                "nombre" => $request->nombre
            ]);

        return redirect(route('tipos_cosechas'))->with('status', 'Registro exitoso');
    }

    public function show_terreno(Request $request, $id)
    {
        $terreno = Terreno::findOrFail($id);
        $mode = "show";
        $anio_actual = date('Y');
        if($request->has('anio_costo_prod'))
        	$anio_actual = $request->anio_costo_prod;
        
        $sistema = Sistema::first();
        for($i = 0; $i < 12; $i++){

            $sql = DB::select('SELECT SUM(C.peso * C.precio_venta) AS produccion, SUM(C.peso) AS peso, T.hectareas AS hectareas, (SELECT SUM(costo_total) FROM insumos WHERE terreno_id = ' . $id . ' AND YEAR(fecha) = ' . $anio_actual . ' AND MONTH(fecha) = ' . ($i + 1)  . ') AS costo FROM cosechas AS C
                INNER JOIN terrenos AS T ON T.id = C.terreno_id
                WHERE C.terreno_id = ' . $id . ' AND YEAR(C.fecha) = ' . $anio_actual . ' AND MONTH(C.fecha) = ' . ($i + 1));

            $total_peso_mes = $sql[0]->peso > 0 ? $sql[0]->peso : 0;
            $total_hectareas = $sql[0]->hectareas > 0 ? $sql[0]->hectareas : 0;
            $costos[] = $sql[0]->costo > 0 ? $sql[0]->costo : 0;
            $produccionesVenta[] = $sql[0]->produccion > 0 ? $sql[0]->produccion/$total_hectareas : 0;
            $producciones[] = $total_hectareas > 0 ? $total_peso_mes / $total_hectareas : 0;
        }
        return view('produccion.terrenos.show', compact('terreno', 'mode','anio_actual','sistema','producciones','produccionesVenta','costos'));
    }

    public function show_cosecha($id)
    {
        $cosecha = Cosecha::findOrFail($id);
        $terrenos = Terreno::all();
        $productos = Producto::all();
        $tipos = TipoCosecha::all();
        $mode = "show";
        return view('produccion.cosechas.show', compact('cosecha', 'mode', 'tipos', 'terrenos','productos'));
    }

    public function show_tipo_cosecha($id)
    {
        $tipo = TipoCosecha::findOrFail($id);
        $mode = "show";
        return view('produccion.tipos_cosechas.show', compact('tipo', 'mode'));
    }

    public function hectareas(Request $request)
    {
        $hectareas = Hectarea::all();
        return view('produccion.hectareas.index', compact('hectareas'));
    }

    public function create_hectarea(Request $request)
    {
        $terrenos = Terreno::all();
        return view('produccion.hectareas.create', compact('terrenos'));
    }

    public function store_hectarea(Request $request)
    {
        //dd($request->nombre, $request->metros, $request->terreno);
        Hectarea::create(
            [
                "nombre" => $request->nombre,
                "metros" => floatval($request->metros),
                "terreno_id" => $request->terreno
            ]
        );

        return redirect('/produccion/hectareas')->with('status', 'Registro exitoso');
    }

    public function show_calificacion($id)
    {
        $calificacion = Calificacion::findOrFail($id);
        $cosechas = Cosecha::all();
        return view('produccion.calificaciones.show', compact('calificacion', 'cosechas'));
    }

    public function show_hectarea($id)
    {
        $hectarea = Hectarea::findOrFail($id);
        $terrenos = Terreno::all();
        $mode = 'show';
        return view('produccion.hectareas.show', compact('hectarea', 'terrenos', 'mode'));
    }

    public function edit_insumo($id)
    {
        $insumo = Insumo::findOrFail($id);
        $terrenos = Terreno::all();
        $productos = inventario::with('producto')->where('cantidad','>', 0)->get();
        $editMode = false;
        return view('produccion.insumos.edit', compact('insumo','editMode','terrenos','productos'));
    }

    public function edit_hectarea($id)
    {
        $hectarea = Hectarea::findOrFail($id);
        $terrenos = Terreno::all();
        $mode = 'edit';
        return view('produccion.hectareas.show', compact('hectarea', 'terrenos', 'mode'));
    }

    public function edit_calificacion($id)
    {
        $calificacion = Calificacion::findOrFail($id);
        $cosechas = Cosecha::all();
        return view('produccion.calificaciones.edit', compact('calificacion', 'cosechas'));
    }

    public function edit_cosecha($id)
    {
        $cosecha = Cosecha::findOrFail($id);
        $terrenos = Terreno::all();
        $tipos = TipoCosecha::all();
        $productos = Producto::all();
        return view('produccion.cosechas.edit', compact('cosecha', 'terrenos', 'tipos','productos'));
    }

    public function update_edit_hectarea(Request $request, $id)
    {
        if(!$request->has('nombre') || !$request->has('terreno') || !$request->has('metros') || !$request->has('id'))
            return redirect('edit_hectarea', $id)->with('error', 'Error: Faltaron algunos parametros');

        $hectarea = Hectarea::updateOrCreate(
            [
                "nombre" => $request->nombre,
                "metros" => $request->metros,
                "terreno_id" => $request->terreno,
            ]);        
        
        
        return redirect(route('show_hectarea', $id))->with('status', 'Actualizacion exitosa');
    }

    public function edit_terreno($id)
    {
        $terreno = Terreno::findOrFail($id);
        $anio_actual = date('Y');
        $sistema = Sistema::first();
        $mode = 'edit';
        for($i = 0; $i < 12; $i++){

            $sql = DB::select('SELECT SUM(C.peso * C.precio_venta) AS produccion, SUM(C.peso) AS peso, T.hectareas AS hectareas, (SELECT SUM(costo_total) FROM insumos WHERE terreno_id = ' . $id . ' AND YEAR(fecha) = ' . $anio_actual . ' AND MONTH(fecha) = ' . ($i + 1)  . ') AS costo FROM cosechas AS C
                INNER JOIN terrenos AS T ON T.id = C.terreno_id
                WHERE C.terreno_id = ' . $id . ' AND YEAR(C.fecha) = ' . $anio_actual . ' AND MONTH(C.fecha) = ' . ($i + 1));

            $total_peso_mes = $sql[0]->peso > 0 ? $sql[0]->peso : 0;
            $total_hectareas = $sql[0]->hectareas > 0 ? $sql[0]->hectareas : 0;
            $costos[] = $sql[0]->costo > 0 ? $sql[0]->costo : 0;
            $produccionesVenta[] = $sql[0]->produccion > 0 ? $sql[0]->produccion/$total_hectareas : 0;
            $producciones[] = $total_hectareas > 0 ? $total_peso_mes / $total_hectareas : 0;
        }
        return view('produccion.terrenos.show', compact('terreno', 'mode','anio_actual','sistema','costos','produccionesVenta','producciones'));
    }

    public function update_calificacion(Request $request, $id)
    {
        if(!$request->has('fecha') || !$request->has('cosecha') || !$request->has('peso') || !$request->has('calificacion'))
            return redirect(route('edit_calificacion', $id))->with('error', 'Error: Faltaron algunos parametros');

        $calificacion = Calificacion::updateOrCreate(
            ["id" => $id],
            [
                "calificacion" => $request->calificacion,
                "cosecha_id" => $request->cosecha,
                "fecha" => $request->fecha,
                "peso" => $request->peso
            ]);        
                
        return redirect(route('show_calificacion', $id))->with('status', 'Actualizacion exitosa');
    }

    public function update_cosecha(Request $request, $id)
    {
        if(!$request->has('nombre') || !$request->has('terreno') || !$request->has('tipo') || !$request->has('peso') || !$request->has('fecha'))
            return redirect(route('edit_cosecha', $id))->with('error', 'Error: Faltaron algunos parametros');

        DB::beginTransaction();

        try{
            $oldCosecha = Cosecha::findOrFail($id);

            if($oldCosecha->producto_id != $request->producto){
                /*restar toda la cantidad de esta cosecha al inventario del producto anterior y sumarla al 
                inventario del producto nuevo*/
                $diferencia = $oldCosecha->peso;

                $inventarioProductoAnterior = Inventario::where('producto_id', $oldCosecha->producto_id)->where('tipo', 0)->first();

                if(isset($inventarioProductoAnterior->id)){
                    $inventarioProductoAnterior->cantidad -= $diferencia;
                    $inventarioProductoAnterior->update();

                    $inventarioProductoNuevo = Inventario::where('producto_id', $request->producto)->where('tipo', 0)->first();
                    if(isset($inventarioProductoNuevo->id)){
                        $inventarioProductoNuevo->cantidad += $request->peso;
                        $inventarioProductoNuevo->update();
                    }else{
                        
                        $inventarioProductoNuevo = Inventario::create([
                            "producto_id" => $request->producto,
                            "cantidad" => $reques->cantidad,
                            "tipo" => 0
                        ]);

                        if(!isset($inventarioProductoNuevo->id)){
                            DB::rollback();
                            return redirect(route('show_cosecha', $id))->with('error', 'Error al crear inventario nuevo');
                        }
                    }
                }else{
                    DB::rollback();
                    return redirect(route('show_cosecha', $id))->with('error', 'Error al actualizar inventario anterior');
                }
            }else if($oldCosecha->peso > $request->peso){
                //restar diferencia del inventario de produccion para el producto de esta cosecha
                $diferencia = $oldCosecha->peso - $request->peso;

                $inventario = Inventario::where('producto_id', $request->producto)->where('tipo', 0)->first();

                if(isset($inventario->id)){
                    $inventario->cantidad -= $diferencia;
                    $inventario->update();
                }else{
                    DB::rollback();
                    return redirect(route('show_cosecha', $id))->with('error', 'Error al actualizar inventario anterior');
                }
            }

            $cosecha = Cosecha::update(
                ["id" => $id],
                [
                    "nombre" => $request->nombre,
                    "producto_id" => $request->producto_id,
                    "terreno_id" => $request->terreno,
                    "tipo_id" => $request->tipo,
                    "fecha" => $request->fecha,
                    "peso" => $request->peso,
                    "precio_venta" => $request->precio_venta
                ]);
            if(isset($cosecha->id))
                DB::commit();
            else{
                DB::rollback();
                return redirect(route('show_cosecha', $id))->with('error', 'Error al actualizar cosecha');
            }

        }catch(\Exception $e){
            DB::rollback();
            return redirect(route('show_cosecha', $id))->with('error', 'Error actualizar cosecha');
        }
                
        return redirect(route('show_cosecha', $id))->with('status', 'Actualizacion exitosa');
    }

    public function update_insumo(Request $request, $id){
        $this->validate(request(),[
            'producto' => 'required',
            'terreno'=> 'required',
            'costo_unit' => 'required',
            'cantidad' => 'required'
        ]);

        DB::beginTransaction();
        try{
            
        	$oldInsumo = Insumo::findOrFail($id);

            $insertedInsumo = Insumo::updateOrCreate(
            	[
            		"id" => $id
            	],
            	[
                "producto_id" => $request->producto,
                "terreno_id" => $request->terreno,
                "cantidad" => $request->cantidad,
                "user_id" => Auth::user()->id,
                "costo_unitario" => $request->costo_unit,
                "costo_total" => $request->cantidad * $request->costo_unit
            ]);

            if(!isset($insertedInsumo->id)){
                DB::rollback();
                return redirect(route('edit_insumo', $id))->with('error', 'Error al actualizar insumo');
            }

            $inventario = Inventario::where('producto_id', $request->producto)->where('tipo', 1)->first();
            if(!isset($inventario->id)){
            	DB::rollback();
            	return redirect(route('edit_insumo', $id))->with('error', 'Error al obtener el inventario del producto. Verifique la existencia de este producto en inventario');
            }
            if($oldInsumo->producto_id != $request->producto){
            	$diferencia = $oldInsumo->cantidad;
            	$oldInventario = Inventario::where('producto_id', $oldInsumo->producto_id)->where('tipo', 1)->first();
            	if(!isset($oldInventario->id)){
            		DB::rollback();
	            	return redirect(route('edit_insumo', $id))->with('error', 'Error al obtener el inventario del producto. Verifique la existencia de este producto en inventario');
	            }	            
	            $oldInventario->cantidad += $diferencia;
	            $oldInventario->update();
            }

            if($oldInsumo->cantidad > $request->cantidad){
            	$diferencia = $oldInsumo->cantidad - $request->cantidad;
            	$inventario->cantidad += $diferencia;
            }
            if($oldInsumo->cantidad < $request->cantidad){
            	$diferencia = $request->cantidad - $oldInsumo->cantidad;
            	$inventario->cantidad -= $diferencia;
            }
	        
            $inventario->update();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return redirect(route('edit_insumo', $id))->with('error', "Registro Fallido: " . $e->getMessage());
        }

        return redirect(route('edit_insumo', $id))->with('status', "Actualizacion exitosa");
    }

    public function update_terreno(Request $request, $id)
    {
        if(!$request->has('nombre') || !$request->has('hectareas'))
            return redirect(route('edit_terreno', $id))->with('error', 'Error: Faltaron algunos parametros');

        $hectarea = Terreno::updateOrCreate(
            ["id" => $id],
            [
                "nombre" => $request->nombre,
                "hectareas" => $request->hectareas
            ]);        
                
        return redirect(route('show_terreno', $id))->with('status', 'Actualizacion exitosa');
    }

    public function edit_tipo_cosecha($id)
    {
        $tipo = TipoCosecha::findOrFail($id);        
        $mode = 'edit';
        return view('produccion.tipos_cosechas.show', compact('tipo', 'mode'));
    }

    public function estadistica_cosechas(Request $request)
    {
        $anio_actual = date("Y");
        if($request->has('anio'))
            $anio_selected = $request->anio;
        else
        	$anio_selected = $anio_actual;

        for($i = 0; $i < 12; $i++){
            $sql = DB::select('SELECT SUM(C.peso) AS peso, SUM(T.hectareas) AS hectareas FROM cosechas AS C INNER JOIN terrenos AS T ON T.id = C.terreno_id WHERE YEAR(C.fecha) = ' . $anio_selected . ' AND MONTH(C.fecha) = ' . ($i + 1));
            $total_peso_mes = $sql[0]->peso > 0 ? $sql[0]->peso : 0;
            $total_hectareas = $sql[0]->hectareas > 0 ? $sql[0]->hectareas : 0;
            $producciones[] = $total_hectareas > 0 ? $total_peso_mes/$total_hectareas : 0;
        }
        
        $totales = [];
        return view('produccion.estadisticas.cosechas', compact('anio_actual', 'anio_selected', 'producciones', 'totales'));
    }

    public function estadistica_calificaciones(Request $request)
    {
        $anio_actual = date("Y");
        if($request->has('anio'))
            $anio_selected = $request->anio;
        else
        	$anio_selected = $anio_actual;

        for($i = 0; $i < 12; $i++){

            $sql = DB::select('SELECT AVG(C.calificacion) AS promedio FROM calificaciones C WHERE YEAR(C.fecha) = ' . $anio_selected . ' AND MONTH(C.fecha) = ' . ($i + 1));

            $calificaciones[] = $sql[0]->promedio;            
        }
        //dd($producciones);
        $totales = [];

        return view('produccion.estadisticas.calificaciones', compact('anio_actual', 'anio_selected', 'calificaciones', 'totales'));
    }

    public function update_tipo_cosecha(Request $request, $id)
    {
        if(!$request->has('nombre'))
            return redirect(route('edit_tipo_cosecha', $id))->with('error', 'Error: Faltaron algunos parametros');

        $tipo = TipoCosecha::updateOrCreate(
            ["id" => $id],
            [
                "nombre" => $request->nombre
            ]);        
                
        return redirect(route('show_tipo_cosecha', $id))->with('status', 'Actualizacion exitosa');
    }

    public function destroy_cosecha($id)
    {
        $cosecha = Cosecha::findOrFail($id);
        $cosecha->delete();
        
        return redirect(route('cosechas'))->with('status', 'Borrado Exitoso');
    }

    public function destroy_insumo($id)
    {
        $insumo = Insumo::findOrFail($id);

        $inventario = Inventario::where('producto_id', $insumo->producto_id)->where('tipo', 1)->first();
        if(isset($inventario->id))
        {
	        $inventario->cantidad += $insumo->cantidad;
	        $inventario->update();
        }

        $insumo->delete();
        
        return redirect(route('insumos'))->with('status', 'Borrado Exitoso');
    }
    
    public function destroy_hectarea($id)
    {
        $hectarea = Hectarea::findOrFail($id);
        $hectarea->delete();
        
        return redirect(route('hectareas'))->with('status', 'Borrado Exitoso');
    }

    public function destroy_terreno($id)
    {
        $terreno = Terreno::findOrFail($id);
        $terreno->delete();
        
        return redirect(route('terrenos'))->with('status', 'Borrado Exitoso');
    }

    public function destroy_tipo_cosecha($id)
    {
        $tipo = TipoCosecha::findOrFail($id);
        $tipo->delete();
        
        return redirect(route('tipos_cosechas'))->with('status', 'Borrado Exitoso');
    }

    public function destroy_calificacion($id)
    {
        $calificacion = Calificacion::findOrFail($id);
        $calificacion->delete();

        return redirect(route('calificaciones'))->with('status', 'Borrado Exitoso');
    }



}
