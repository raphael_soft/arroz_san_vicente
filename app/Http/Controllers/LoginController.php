<?php

namespace App\Http\Controllers;

use App\Http\Requests\InicioFormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Sistema;

class LoginController extends Controller {
    //

    // inicio de sesion
    public function inicio() {
      if(Auth::check() and Session::has('correo')){
         return redirect()->intended('/');
      }else{
        
        return view('auth.login');
      }
    }

    public function authenticate(Request $request) {
        // dd($request->all());
        $email = $request->get('email');
        $password = $request->get('password');
        $remember = ($request->get('remember')) ? true : false ;

           
                    if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {

                       
                       if(Auth::check() and !Session::has('correo')){
                        $usuario = Auth::user();
                       
                       //VALORES DE LA SESSION 
                       Session::put('correo', $usuario->email);
                       Session::put('nombre', $usuario->nombre);
                       Session::put('editar', $usuario->rol->editar);
                       Session::put('ver', $usuario->rol->ver);
                       Session::put('registrar', $usuario->rol->registrar);
                       Session::put('borrar', $usuario->rol->borrar);
                       Session::put('superuser', $usuario->rol->superuser);
                       Session::put('nombre_cargo', $usuario->cargo->nombre);
                       Session::put('nombre_rol', $usuario->rol->nombre);
        }
                       return redirect()->intended('/');
                    }else{
                        return redirect('/inicio')->with('error_datos', 'Error con los datos.');
                        
                    }
              
    }

    public function logout() {
        Auth::logout();

        //OLVIDAR VARIABLES DE LA SESSION
        Session::flush();
        return redirect()->intended('/inicio')->with('status', 'Gracias por visitarnos.');
    }



}
