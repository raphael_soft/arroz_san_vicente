<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Controllers\Controller;
use App\Precio;
use App\TipoMedicion;
use App\TipoProduccion;
use Illuminate\Http\Request;

class PrecioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = Precio::all();
        //dd($datos);
        return view('precio.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categorias = Categoria::all();

        $tipo_mediciones = TipoMedicion::all();

        $tipo_producciones = TipoProduccion::all();

        return view('precio.create', compact('categorias', 'tipo_mediciones', 'tipo_producciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $registro = new Precio;

        $registro->categoria_id       = $request['categoria'];
        $registro->tipo_medicion_id   = $request['tipo_medicion'];
        $registro->tipo_produccion_id = $request['tipo_produccion'];
        $registro->precio             = $request['precio'];

        $datos = Precio::all();

        $registro->save();

        //LISTADO DE SOCIOS

        return redirect('precios')->with('status', 'Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $precio = Precio::find($id);

        $tipo_medicion   = TipoMedicion::where('id', '=', $precio->tipo_medicion_id)->firstOrFail();
        $tipo_produccion = TipoProduccion::where('id', '=', $precio->tipo_produccion_id)->firstOrFail();
        $categoria       = Categoria::where('id', '=', $precio->categoria_id)->firstOrFail();

        return view('precio.show', compact('precio', 'tipo_medicion', 'tipo_produccion', 'categoria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $precio = Precio::find($id);

        $tipo_medicion   = TipoMedicion::where('id', '=', $precio->tipo_medicion_id)->firstOrFail();
        $tipo_produccion = TipoProduccion::where('id', '=', $precio->tipo_produccion_id)->firstOrFail();
        $categoria       = Categoria::where('id', '=', $precio->categoria_id)->firstOrFail();

        $categorias = Categoria::all();

        $tipo_mediciones = TipoMedicion::all();

        $tipo_producciones = TipoProduccion::all();

        return view('precio.edit', compact('precio', 'tipo_medicion', 'tipo_produccion', 'categoria', 'categorias', 'tipo_mediciones', 'tipo_producciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $id     = $_POST['id'];
        $update = Precio::find($id);

        $update->categoria_id       = $_POST['categoria'];
        $update->tipo_medicion_id   = $_POST['tipo_medicion'];
        $update->tipo_produccion_id = $_POST['tipo_produccion'];
        $update->precio             = $_POST['precio'];

        $update->save();

        return redirect("/precios")->with('status', 'Actualizaci&oacute;n Exitosa');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $precio = Precio::whereId($id)->firstOrFail();
        $precio->delete();
        return redirect('/precios')->with('status', 'Borrado Exitoso ');
    }

    
}
