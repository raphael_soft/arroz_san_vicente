<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Socio;
use App\Iva;
use App\TipoMedicion;
use App\Producto;
use App\Inventario;
use App\Categoria;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProductoFormRequests;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tipo = "todos")
    {
        
       if($tipo == "todos"){
           $titulo_lista = "Todos los productos";     
           $productos = Producto::all();
       }
       if($tipo == "con_iva"){
           $titulo_lista = "Productos que pagan IVA";
           $productos = Producto::where('iva_id','!=',null)->where('iva_id','>',0)->get();
       }
       if($tipo == "sin_iva"){
           $titulo_lista = "Productos que no pagan IVA";
           $productos = Producto::where('iva_id','=',null)->orWhere('iva_id','=',0)->get();
       }

        return view('inventario.productos.index', compact('productos','titulo_lista'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {	
    	$socio = Socio::all();
        $categorias = Categoria::all();
        $mediciones = TipoMedicion::all();
        $ivas = Iva::all();
        $codigo = uniqid('P');
        return view('inventario.productos.create', compact('socio','ivas','categorias','mediciones','codigo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->flash();
        $this->validate(request(),[
            'nombre' => 'required',
            'codigo'=> 'required|unique:productos,codigo',
            'categoria' => 'required',
            'subcategoria' => 'required',
            'medicion' => 'required',
            'costo_produccion' => 'required|min:0',
            'precio_venta' => 'required|min:0'
            
        ]);
        $existe = Producto::where('nombre','=',$request->nombre)->where('categoria_id','=',$request->categoria)->where('subcategoria_id','=',$request->subcategoria)->get()->count();
        if($existe > 0){
            $request->flash();
            return redirect('/create_producto')->with('error', 'Este Producto ya existe.');            
        }
        $registro            = Producto::updateOrCreate(
                ["nombre" => strtoupper($request['nombre']), "categoria_id" => $request['categoria'],
                 "subcategoria_id" => $request['subcategoria']],
                [
                 "codigo" => strtoupper($request['codigo']),   
                 "medicion_id" => $request['medicion'],
                 "iva_id" => $request->has('iva') ? $request['iva'] : null,
                 "costo_produccion" => $request['costo_produccion'],
                 "precio_venta" => $request['precio_venta']
                ]
        );
        //LISTADO DE PRODUCTOS
        
        return redirect('conf_productos')->with('status', 'Registro Exitoso');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto   = Producto::find($id);
        
    	return view('inventario.productos.show', compact('producto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::find($id);
        $categorias = Categoria::all();
        $mediciones = TipoMedicion::all();
        $ivas = Iva::all();
    	return view('inventario.productos.edit', compact('producto', 'ivas', 'categorias','mediciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate(request(),[
            'nombre' => 'required',
            'codigo'=> 'required|unique:productos,codigo,'.$id,
            'categoria' => 'required',
            'subcategoria' => 'required',
            'medicion' => 'required',
            'costo_produccion' => 'required|min:0',
            'precio_venta' => 'required|min:0'
        ]);    

        $existe = Producto::where('nombre','=',$request->nombre)->where('categoria_id','=',$request->categoria)->where('subcategoria_id','=',$request->subcategoria)->where('id','!=',$id)->get()->count();
        if($existe > 0){
            $request->flash();
            return redirect('/edit_producto/'.$id)->with('error', 'Este Producto ya existe.');            
        }
        $id  			   = $id;
        $update            = Producto::find($id);
       	$update->nombre    = strtoupper($request['nombre']);
        $update->categoria_id    = $request['categoria'];
        $update->subcategoria_id  = $request['subcategoria'];
        $update->codigo  = $request['codigo'];
        $update->costo_produccion  = $request['costo_produccion'];
        $update->precio_venta  = $request['precio_venta'];
        $update->medicion_id  = $request['medicion'];
        $update->iva_id  = $request['iva'];
        $update->status    = $request['status'];
        $update->update();

        
		return redirect('conf_productos')->with('status', 'Actualizaci&oacute;n Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroys($id)
    {
        $producto = Producto::whereId($id)->firstOrFail();
        
        if((isset($producto->detalle_factura_compra) and count($producto->detalle_factura_compra) > 0) or (isset($producto->detalle_factura_venta) and count($producto->detalle_factura_vent) > 0) or (isset($producto->inventario) and count($producto->inventario) > 0)){
            return redirect('/conf_productos')->with('error', 'No se puede borrar este producto. Esta siendo usado');    
        }

		$producto->delete();
		
        return redirect('/conf_productos')->with('status', 'Borrado Exitoso');
    }

    public function registrarProductoAjax(Request $request){

        if($request->ajax()){
            $nombre = $request->nombre_producto;
            $categoria_id = $request->categoria_producto;
            $subcategoria_id = $request->subcategoria_producto;
            $codigo = $request->codigo_producto;
            $medicion = $request->medicion_producto;
            $iva = $request->iva_producto;
            $costo_produccion = $request->costo_producto;
            $precio_venta = $request->precio_producto;
            $inventario = $request->inventario_producto;
            $existe = Producto::where('nombre','=',$nombre)->orWhere('codigo','=',$codigo)->get()->count();

            DB::beginTransaction();
            try{
	            if($existe > 0){
	                return response()->json(['resp' => 'existe']);    
	            }

	            $producto = new Producto(array(
	            'nombre' => strtoupper($nombre),
	            'categoria_id' => $categoria_id,
	            'codigo' => strtoupper($codigo),
	            'subcategoria_id' => $subcategoria_id,
	            'medicion_id' => $medicion,
	            'iva_id' => $iva,
	            'costo_produccion' => $costo_produccion,
	            'precio_venta' => $precio_venta
	            ));

	            $producto->save();
	            if($inventario != null){
		            Inventario::updateOrCreate(
		                ['producto_id' => $producto->id, 'tipo' => 1],//tipo 1 es de inventario de productos

		                ['cantidad' => $inventario]
		            );		            
	            
	            	$collection = Producto::whereExists(function ($query){
	                $query->select(DB::raw(1))->from('inventario')->whereRaw('productos.id = inventario.producto_id')->where('cantidad','>',0);
	        		})->get();
	        	}else{
	        		$collection = Producto::all();	        		
	        	}

	            $data = view('ajax.select-productos',compact('collection'))->render();
	            DB::commit();
	            return response()->json(['options'=>$data, 'resp' => 'ok', 'id' => $producto->id]);
	        }catch(\Exception $e){
	        	return response()->json(['resp' => 'error', 'msg' => 'Error al registrar producto']);
	        	DB::rollBack();
	        }
        }
    }

    public function cargarProveedoresAjax(Request $request){

        if($request->ajax()){
            
            $collection = Socio::all();

            $data = view('ajax.select-proveedores',compact('collection'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    public function registrarPrecioAjax(Request $request){
        if($request->ajax()){
            $tipo = $request->tipo; // 0 es producto y 1 es produccion
            $valor = $request->valor;
            $id_producto = $request->producto;

            if($tipo == "producciones")
            $existe = Producto::where('id','=',$id_producto)->where('costo_produccion','>',0)->exists();

            if($existe)
                return response()->json(['resp' => 'existe']);                
            
            $update = Producto::find($id_producto);

            if($tipo == "producciones")
                    $update->costo_produccion = $valor;
            

            $update->update();


            //$precios = Precio::where('producto_id','=',$id_producto)->where('tipo','=',$tipo)->get();
            //$data = view('ajax.select-precios',compact('precios'))->render();

            return response()->json(['resp' => 'ok', 'value' => $valor]);
        }
    }

    public function cargarCostosAjax(Request $request){
        if($request->ajax()){
            $tipo = $request->tipo; // 0 es producto y 1 es produccion
            $valor = $request->valor;
            $id_producto = $request->producto;
            $precio = null;

            if($tipo =="producciones")
                $precio = Producto::where('id','=',$id_producto)->first()->costo_produccion;
            

            if($precio != null)
                return response()->json(['resp' => 'ok', 'value' => $precio]);                
            
            //$precios = Precio::where('producto_id','=',$id_producto)->where('tipo','=',$tipo)->get();
            //$data = view('ajax.select-precios',compact('precios'))->render();

            return response()->json(['resp' => 'no']);
        }       
    }

    public function cargarPreciosAjax(Request $request){
        if($request->ajax()){
            
            
            $id_producto = $request->producto;
            $precio = null;

            $producto = Producto::find($id_producto);

            if($producto != null)
                $precio = $producto->precio_venta;
            

            if($precio != null)
                return response()->json(['resp' => 'ok', 'value' => $precio]);                
            
            //$precios = Precio::where('producto_id','=',$id_producto)->where('tipo','=',$tipo)->get();
            //$data = view('ajax.select-precios',compact('precios'))->render();

            return response()->json(['resp' => 'no']);
        }       
    }    
}
