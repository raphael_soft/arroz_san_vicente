<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Socio;
use App\Res;
use App\Http\Requests\SocioFormRequests;

class SocioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $socios = Socio::all();
        return view('asociacion.socios.index', compact('socios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('asociacion.socios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $slug = uniqid();
        $this->validate(request(),[
            'nombre' => 'required|unique:socios,nombre',
            'ruc' => 'required|unique:socios,ruc'
        ]);

        $nombre = isset($request['nombre']) ? strtoupper($request['nombre']) : '';
        $ruc = isset($request['ruc']) ? strtoupper($request['ruc']) : '';
        $email = isset($request['email']) ? strtolower($request['email']) : '';
        $telefono = isset($request['telefono']) ? $request['telefono'] : '';
        $direccion = isset($request['direccion']) ? strtoupper($request['direccion']) : '';
        $email_representante = isset($request['email_representante']) ? strtoupper($request['email_representante']) : '';
        $nombre_representante = isset($request['nombre_representante']) ? strtoupper($request['nombre_representante']) : '';
        $cedula_representante = isset($request['cedula_representante']) ? strtoupper($request['cedula_representante']) : '';
        $telefono_representante = isset($request['telefono_representante']) ? $request['telefono_representante'] : '';
        $direccion_representante = isset($request['direccion_representante']) ? $request['direccion_representante'] : '';

        $registro           = new Socio;
        $registro->nombre   = $nombre;
        $registro->ruc    = $ruc;
        $registro->email    = $email;
        $registro->telefono = $telefono;
        $registro->direccion = $direccion;
        $registro->nombre_representante = $nombre_representante;
        $registro->cedula_representante = $cedula_representante;
        $registro->telefono_representante = $telefono_representante;
        $registro->direccion_representante = $direccion_representante;
        $registro->email_representante = $email_representante;
        $registro->codigo   = $slug;
        $registro->save();

        //LISTADO DE SOCIOS
        return redirect('socios')->with('status', 'Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$socios = Socio::find($id);
    	return view('asociacion.socios.show', compact('socios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $socios = Socio::find($id);
    	return view('asociacion.socios.edit', compact('socios'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {	

    	$registro             = Socio::find($id);

        $this->validate(request(),[
            'nombre' => 'required|unique:socios,nombre,'.$id,
            'ruc' => 'required|unique:socios,ruc,'.$id,
            'codigo' => 'required|unique:socios,codigo,'.$id
        ]);

        $nombre = isset($request['nombre']) ? strtoupper($request['nombre']) : '';
        $ruc = isset($request['ruc']) ? strtoupper($request['ruc']) : '';
        $email = isset($request['email']) ? strtoupper($request['email']) : '';
        $telefono = isset($request['telefono']) ? $request['telefono'] : '';
        $direccion = isset($request['direccion']) ? strtoupper($request['direccion']) : '';
        $nombre_representante = isset($request['nombre_representante']) ? strtoupper($request['nombre_representante']) : '';
        $cedula_representante = isset($request['cedula_representante']) ? strtoupper($request['cedula_representante']) : '';
        $telefono_representante = isset($request['telefono_representante']) ? $request['telefono_representante'] : '';
        $direccion_representante = isset($request['direccion_representante']) ? $request['direccion_representante'] : '';
        $email_representante = isset($request['email_representante']) ? $request['email_representante'] : '';
        $codigo = isset($request['codigo']) ? $request['codigo'] : '';

        
        $registro->nombre   = $nombre;
        $registro->ruc    = $ruc;
        $registro->email    = $email;
        $registro->telefono = $telefono;
        $registro->direccion = $direccion;
        $registro->nombre_representante = $nombre_representante;
        $registro->cedula_representante = $cedula_representante;
        $registro->telefono_representante = $telefono_representante;
        $registro->direccion_representante = $direccion_representante;
        $registro->email_representante = $email_representante;
        $registro->codigo   = $codigo;
        $registro->update();

		return redirect("/socios")->with('status', 'Actualizaci&oacute;n Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $socio = Socio::whereId($id)->firstOrFail();
		$socio->delete();
        
        return redirect('socios')->with('status', 'Borrado Exitoso');
    }

    public function registrarProveedorAjax(Request $request){

        if($request->ajax()){
            
            $nombre = strtoupper(isset($request->nombre)?$request->nombre:'');
            $nombre_representante = strtoupper(isset($request->nombre_representante)?$request->nombre_representante:'');
            $ruc = strtoupper(isset($request->ruc)?$request->ruc:'');
            if($nombre == '' || $ruc == ''){
                return response()->json(["resp" => "error"]);
            }

            $existe = Socio::where('nombre','=',$nombre)->orWhere('ruc','=',$ruc)->get()->count();

            if($existe > 0){
                return response()->json(["resp" => "existe"]);
            }

            $proveedor = new Socio(array(
            'nombre' => $nombre,
            'nombre_representante' => $nombre_representante,
            'ruc' => $ruc
            ));

            $proveedor->save();
            
            $collection = Socio::all();

            $data = view('ajax.select-proveedores',compact('collection'))->render();

            return response()->json(['options'=>$data, 'resp' => 'ok']);
        }
    }

    public function cargarProveedoresAjax(Request $request){

        if($request->ajax()){
            
            $collection = Socio::all();

            $data = view('ajax.select-proveedores',compact('collection'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    public function cargarDatosAjax(Request $request){

        if($request->ajax()){
            $id_socio = $request->socio;

            $socio = Socio::find($id_socio);

            if($socio == null){
                return response()->json(['resp'=> 'no-existe']);    
            }

            if($socio->nombre == null || $socio->nombre == '' || $socio->ruc == null || $socio->ruc == ''){
                return response()->json(['resp'=> 'no']);       
            }
            
            return response()->json(['resp'=> 'ok','nombre' => $socio->nombre, 'ruc' => $socio->ruc,'telefono' => $socio->telefono, 'direccion' => $socio->direccion]);
        }
    }
}
