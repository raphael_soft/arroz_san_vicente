<?php

namespace App\Http\Controllers;

use App\Http\Requests\RolFormRequest;
use App\Rol;
use Illuminate\Http\Request;

class RolController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $rols = Rol::all();
        return view('configuracion.rol.index', compact('rols'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.rol.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolFormRequest $request) {
        //
        // dd($request;
        // dd($request->all());

        $nombre = strtoupper($request->get('nombre'));
        $responsabilidades = strtoupper($request->get('responsabilidades'));
        $ver = $request->has('ver')? true : false;
        $editar = $request->has('editar') ? true : false;
        $registrar = $request->has('registrar')? true : false;
        $borrar = $request->has('borrar')? true : false;
        $superuser = $request->has('superuser')? true : false;

        $rol = new Rol();

        $rol->nombre = $nombre;
        $rol->responsabilidades = $responsabilidades;
        $rol->ver = $ver;
        $rol->registrar = $registrar;
        $rol->editar = $editar;
        $rol->borrar = $borrar;
        $rol->superuser = $superuser;
        

        $rol->save();

        return redirect('/configuracion/roles')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $rol = Rol::whereId($id)->firstOrFail();
        return view('configuracion.rol.show', compact('rol'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $rol = Rol::whereId($id)->firstOrFail();
        return view('configuracion.rol.edit', compact('rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        $this->validate(request(),[
            'nombre' => 'unique:rol,nombre,'.$id
        ]);
        $nombre = strtoupper($request->get('nombre'));
        $responsabilidades = strtoupper($request->get('responsabilidades'));
        $ver = $request->has('ver')? true : false;
        $editar = $request->has('editar') ? true : false;
        $registrar = $request->has('registrar')? true : false;
        $borrar = $request->has('borrar')? true : false;
        $superuser = $request->has('superuser')? true : false;

        $rol = Rol::find($id);
            $rol->nombre = $nombre;
            $rol->responsabilidades = $responsabilidades;
            $rol->ver = $ver;
            $rol->registrar = $registrar;
            $rol->editar = $editar;
            $rol->borrar = $borrar;
            $rol->superuser = $superuser;

        $rol->update();


        return redirect('/configuracion/roles')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        if($id == 1)
            return redirect('/configuracion/roles')->with('error', 'Este rol no puede ser eliminado. Es superusuario');    
    
        $rol = Rol::whereId($id)->first();
            
        if($rol->usuarios->count() > 0)
              return redirect('/configuracion/roles')->with('error', 'Este rol no puede ser eliminado. Esta siendo usado.');      

        $rol->delete();
        return redirect('/configuracion/roles')->with('status', 'Borrado Exitoso ');
    }
}
