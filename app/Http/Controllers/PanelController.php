<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Res;
use App\User;
use App\Socio;
use App\Produccion;
use App\Venta;
use App\Facturacompras;
use App\Facturaventas;
use App\Revision;
use App\Terreno;
use App\Insumo;
use App\Cosecha;
use App\Inventario;

class PanelController extends Controller {

    public function __construct() {
        $this->middleware('auth');

    }

    // INICIO
    public function Home() {

    	$users = User::all()->count();
    	$socios = Socio::all()->count();
      $ventas = Facturaventas::all()->count();
      $compras_produccion = Facturacompras::where('tipo','=',0)->count();
      $compras_productos = Facturacompras::where('tipo','=',1)->count();
      //nuevos
      $inventario_productos = Inventario::where('tipo', 0)->count();
      $inventario_produccion = Inventario::where('tipo', 1)->count();
      $revisiones = Revision::all()->count();
      $terrenos = Terreno::all()->count();
      $insumos = Insumo::all()->count();
      $cosechas = Cosecha::all()->count();
      //fin nuevos

    	$anio_actual = date('Y');
      $ingresos = EstadisticaController::getIngresoByAnio($anio_actual);
      $egresos = EstadisticaController::getEgresoByAnio($anio_actual);
      $utilidades = EstadisticaController::getUtilidadesByAnio($ingresos,$egresos);

         //DATOS DEL USUARIO
        if(Auth::check() and !Session::has('correo')){
                        $usuario = Auth::user();
                       
                       //VALORES DE LA SESSION 
                       Session::put('correo', $usuario->email);
                       Session::put('nombre', $usuario->nombre);
                       Session::put('editar', $usuario->rol->editar);
                       Session::put('ver', $usuario->rol->ver);
                       Session::put('registrar', $usuario->rol->registrar);
                       Session::put('borrar', $usuario->rol->borrar);
                       Session::put('superuser', $usuario->rol->superuser);
                       Session::put('nombre_cargo', $usuario->cargo->nombre);
                       Session::put('nombre_rol', $usuario->rol->nombre);
        }

    	return view('index', compact('ventas','compras_produccion','compras_productos','users', 'socios','anio_actual','ingresos','egresos','utilidades','inventario_produccion','inventario_productos','terrenos','revisiones','insumos','cosechas'));
    }

}
