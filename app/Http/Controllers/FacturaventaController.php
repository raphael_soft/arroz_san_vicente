<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\ClienteVentas;

use App\Socio;

use App\Sistema;

use App\Abono;

use App\ModoPago;

use App\Iva;

use App\Categoria;

use App\SubCategoria;

use App\TipoMedicion;

use App\Facturaventas;

use App\Detallefacturaventa;

use App\Inventario;

use App\Inventarioproduccion;

use App\Producto;

use App\Movimiento;

use App\Factura;





class FacturaventaController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */


    public function index(Request $request)
    {
        
        $factura = Facturaventas::all();//0 es produccion y 1 es productos
        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        //$fromDate = $request->has("from") != "" ? $request->from : $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $primeraFecha = Facturaventas::min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        $fromDate = $request->has("from") != "" ? $request->from : $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $request->has("to") != "" ? $request->to : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $ventas = array();
        $aux = strtotime($fromDate);
        $fromDate = date('Y-m-d',$aux);
        $aux = strtotime($toDate);
        $toDate = date('Y-m-d',$aux);
        $factura = Facturaventas::where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->get();//0 es produccion y 1 es productos

        return view('factura_venta.index', compact('factura','fromDate','toDate'));

    }

    public function index_por_cobrar(Request $request)
    {
        
        $factura = ModoPago::where('nombre','Credito')->orWhere('nombre','credito')->first()->facturaventas;//obtenemos todas las facturas con modo de pago credito
        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        //$fromDate = $request->has("from") != "" ? $request->from : $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $primeraFecha = $factura->min('fecha_facturacion');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        $fromDate = $request->has("from") != "" ? $request->from : $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro

        $toDate = $request->has("to") != "" ? $request->to : $todayDate; // si recibimos el parametro toDate lo usamos, sino usamos la fecha de hoy

        $ventas = array();
        $aux = strtotime($fromDate);
        $fromDate = date('Y-m-d',$aux);
        $aux = strtotime($toDate);
        $toDate = date('Y-m-d',$aux);
        $factura = $factura->where('fecha_facturacion','>=',$fromDate)->where('fecha_facturacion','<=',$toDate)->all();//0 es produccion y 1 es productos
        $modos_pago = ModoPago::all();
        return view('factura_venta.index_por_cobrar', compact('factura','modos_pago','fromDate','toDate'));

    }

    public function index_abonos(Request $request)
    {
        
        $abonos = Abono::all();//obtenemos todas las facturas con abonos
        $today = time(); //timestamp en formato unix
        
        $aMonthAgo = $today - (30*24*60*60);//hoy menos 30 dias
        $todayDate = date('Y-m-d',$today); //formateamos la fecha de hoy
        $aMonthAgoDate = date('Y-m-d',$aMonthAgo); // formateamos la fecha de hace 30 dias
        $aDay = (24*60*60); //un dia en segundos
        //$fromDate = $request->has("from") != "" ? $request->from : $aMonthAgoDate; // si recibimos fromDate usamos ese valor, sino el de hace 30 dias
        $primeraFecha = $abonos->min('fecha');
        $primeraFecha = $primeraFecha != null ? $primeraFecha : $todayDate;
        $fromDate = $request->has("from") != "" ? $request->from : $primeraFecha; // si recibimos fromDate usamos ese valor, sino desde el primer registro
        $toDate = $request->has("to") != "" ? $request->to : $todayDate; // si recibimos el     parametro toDate lo usamos, sino usamos la fecha de hoy

        $aux = strtotime($fromDate);
        $fromDate = date('Y-m-d',$aux);
        $aux = strtotime($toDate);
        $toDate = date('Y-m-d',$aux);
        $abonos = $abonos->where('fecha','>=',$fromDate)->where('fecha','<=',$toDate)->all();

        return view('factura_venta.index_abonos', compact('abonos','fromDate','toDate'));
    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()
    {

        $socios = Socio::all();
    	$clientes = ClienteVentas::all();
        //aqui deberiamos enviar solo los productos que se encuentren en inventario
        //$productos = Producto::where('status','=',1)->whereIn()->get(); // estos son todos los productos activos de la lista de productos
        $modos_pago = ModoPago::all();
        $productos = Producto::whereExists(function ($query){
                $query->select(DB::raw(1))->from('inventario')->whereRaw('productos.id = inventario.producto_id')->where('cantidad','>',0);
        })->get();
    	$impuestos   = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        $categorias   = Categoria::where('status','=',1)->get();
        $subcategorias   = SubCategoria::where('status','=',1)->get();
        $mediciones   = TipoMedicion::where('status','=',1)->get();
        
    	//ORDEN ID
    	$order_id = uniqid();

    	//FECHA ACTUAL
    	$fecha =date("Y-m-d");
    	return view('factura_venta.create', compact('order_id','fecha','clientes','impuestos','productos','categorias','subcategorias','mediciones','socios','modos_pago'));
    }

    public function create_abono()
    {

        
        $facturas = ModoPago::whereNombre('credito')->first()->facturaventas;
        
        $fecha =date("Y-m-d");

        
        return view('factura_venta.create_abono', compact('facturas','fecha'));
        
    }
        public function imprimir($id)

    {

        $factura   = Facturaventas::find($id);
          

        $productosFactura  = Detallefacturaventa::where('factura_id', '=', $id)->get();
        $impuestos = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        $vista = 'final';
        return view('factura_venta.invoice_print', compact('factura','productosFactura','vista','tipo','impuestos'));

    }

    public function imprimirAbonos($id)
    {

        $factura   = Facturaventas::find($id);
        return view('factura_venta.invoice_print_abonos', compact('factura'));

    }

    public function imprimirAbono($id)
    {

        $abono   = Abono::find($id);
        return view('factura_venta.invoice_abono_print', compact('abono'));

    }


    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $tip = 0; //este es el tipo de inventario de donde restaremos las cantidades vendidas, 0 es de produccion y 1 normal
            
            $factura = new Facturaventas($request->factura);

            $existe = Facturaventas::where('id','=',$factura->id)->orWhere('n_orden','=',$factura->n_orden)->get()->count();
            
            if($existe > 0){
               DB::rollBack();
               return response()->json(["resp" => "existe"]);
            }
            
            $factura->status = 2; //se marca como aprobada de una vez, 0 es cancelada, 1 pendiente y 2 aprobado
            //si no es venta a credito, se marca la propiedad pagado con 1 o true
            if(isset($factura->modo_pago->nombre)){
                if(strtolower($factura->modo_pago->nombre) == 'credito'){
                    //si es credito, no hacemos nada. pagado es 0 o false por defecto.
                }else{
                    //se marca pagado = 1, es false o 0 por defecto
                    $factura->pagado = true;
                }
            }else{
                //no hay modo de pago asociado, se retorna error
                DB::rollBack();
                return response()->json(["resp" => "error", "msg" => "Existe un error con el modo de pago seleccionado para esta factura. Modo de pago no existe o presenta fallas"]);   
            }
            $factura->hora_facturacion = date('H:i:s');
            $factura->save();

            $calculo_subtotal = 0;
            $monto_iva = 0;

            if(isset($request->productosFactura) and !empty($request->productosFactura)){
                
                foreach ($request->productosFactura as $key => $value) {

                    $detalleFacturaventa =  new Detallefacturaventa($value);
                    
                    $iva_valor = isset($detalleFacturaventa->impuesto) ? $detalleFacturaventa->impuesto->valor : 0;

                    $temp1  = ($detalleFacturaventa->precio * $detalleFacturaventa->cantidad);//SUBTOTAL

                    $temp2  = ($temp1 * $iva_valor/100);

                    $monto_iva += $temp2;
                    $calculo_subtotal += $temp1;

                    //por defecto se restara la cantidad vendida del inventario de produccion
                    $cant_a_restar = $detalleFacturaventa->cantidad; //inicializamos la cantidad que necesitamos restar al/los inventarios
                    $inventario = Inventario::where('producto_id','=',$detalleFacturaventa->producto_id)->get();//obtenemos el producto de ambos inventarios, asi que retorna dos productos si este se encuentra en ambos inventarios: el de produccion y el normal
                    if($inventario->sum('cantidad') < $cant_a_restar){
                        DB::rollBack();
                        return response()->json(["resp" => "error", "msg" => "Error al intentar restar de inventario, las cantidades a ingresar superan la existencia en inventario"]);       
                    }

                    //recorremos el inventario de este producto, restandole las cantidades
                    //si no queda suficiente en el inventario de produccion, se usa el inventario normal
                    foreach($inventario as $inv){
                    	
                    	if($cant_a_restar <= 0)
                    		break;

                        if($cant_a_restar <= $inv->cantidad){
                            $inv->cantidad -= $cant_a_restar;

                            if($inv->tipo == 0)//inventario de produccion
                       			$detalleFacturaventa->cant_inventario_produccion = $cant_a_restar;

                       		if($inv->tipo == 1)//inventario de productos o inventario normal
                       			$detalleFacturaventa->cant_inventario_producto = $cant_a_restar;

                            $cant_a_restar = 0;
                        }else{
                            $cant_a_restar -= $inv->cantidad;

                            if($inv->tipo == 0)//inventario de produccion
                       			$detalleFacturaventa->cant_inventario_produccion = $inv->cantidad;

                       		if($inv->tipo == 1)//inventario de productos o inventario normal
                       			$detalleFacturaventa->cant_inventario_producto = $inv->cantidad;

                            $inv->cantidad = 0;
                        }
                       
                       	$inv->update();
                    }

                    $detalleFacturaventa->factura_id = $factura->id;
                    
                    $detalleFacturaventa->save();

                    //modificamos el precio del producto, en caso de que haya cambiado
                    Producto::where("id", $detalleFacturaventa->producto_id)->update(["precio_venta" => $detalleFacturaventa->precio]);
                }
            }
            //si todo ha transcurrido sin problemas agregamos la factura
            $factura->subtotal    =  $calculo_subtotal;

            $factura->monto_iva   =  $monto_iva;

            $factura->total       =  $monto_iva + $calculo_subtotal;

            $factura->update();

            DB::commit();
            return response()->json(["resp" => "ok"]);
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["resp" => "error: " . $e->getMessage()]);
        }
        
    }

    public function invoice(Request $request)

    {

        $factura                     = new Facturaventas();

        $factura->cliente_id         = $request['cliente'];
        $factura->status = 1; //0 es cancelado, 1 es pendiente, y 2 es aprobado 
        
        $factura->n_orden            = $request['orden_id'];

        $factura->fecha_facturacion  = date("Y-m-d");

        $factura->modo_pago_id = $request['modo_pago'];

        
        $productosFactura = array();
        $calculo_subtotal = 0;
        $monto_iva = 0;

        if(isset($request['listaproductos'])){
        foreach ($request['listaproductos'] as $key => $value) {

            $iva_producto = null;
            
            if($request['listaimpuestos'][$key] != null and $request['listaimpuestos'][$key] != "null" and $request['listaimpuestos'][$key] != ""){
                
                $iva_producto = Iva::find($request['listaimpuestos'][$key]);

            }

            $temp1 = ($request['listaprecios'][$key] * $request['listacantidades'][$key]);//SUBTOTAL

            if($iva_producto != null)
                $temp2  = ($temp1 * $iva_producto->valor / 100);//impuesto
            else
                $temp2 = 0;

            $calculo_subtotal += $temp1;
            $monto_iva += $temp2;

            $detalleFactura                    = new Detallefacturaventa;

            $detalleFactura->producto_id       = $value;

            $detalleFactura->impuesto_id         = $request['listaimpuestos'][$key];

            $detalleFactura->cantidad          = $request['listacantidades'][$key];

            $detalleFactura->precio          = $request['listaprecios'][$key];

            array_push($productosFactura,$detalleFactura);

            
//            $update->save();

}
            $factura->subtotal    =  $calculo_subtotal;

            $factura->monto_iva   =  $monto_iva;

            $factura->total       =  $monto_iva + $calculo_subtotal;

    $request->flash();//guardo momentaneamente en la sesion todo lo que esta en el request
    $vista = "confirmacion";
    $impuestos = Iva::where('status','=',1)->orderBy('valor','asc')->get();
    return view("factura_venta.invoice",compact('factura','productosFactura','vista','tipo','impuestos'));
        }



        $request->flash();
        return redirect("create_facturaventa")->with('error', 'Error al hacer la venta. Intente nuevamente');

    }

    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)
    {

        $factura   = Facturaventas::find($id);
        
        $productosFactura  = Detallefacturaventa::where('factura_id', '=', $factura->id)->get();
        $impuestos = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        $vista = 'final';
    	return view('factura_venta.invoice', compact('factura','productosFactura','vista','impuestos'));

    }

    public function show_abonos($id)
    {

        $factura   = Facturaventas::find($id);

        $impuestos = Iva::where('status','=',1)->orderBy('valor','asc')->get();
        $vista = 'final';
        return view('factura_venta.invoice_abonos', compact('factura','vista','impuestos'));

    }

    public function show_abono($id)
    {

        $abono   = Abono::find($id);

        return view('factura_venta.frame_invoice_abono', compact('abono'));

    }

    public function aprobar($id){ //aqui se deben ingresar los productos a inventario
        $factura = Facturaventas::find($id);
        $factura->status = 2; // 2 es aprobado, 1 es pendiente y 0 cancelado
        $factura->update();
        $tip = 0; //tomamos por defecto los productos del inventario de produccion, si no queda suficiente alli, se usa el inventario normal: 1

        $productosFactura  = Detallefacturaventa::where('factura_id', '=', $factura->id)->get();

        /*foreach($productosFactura as $key => $p){//agregamos sus cantidades al inventario de produccion

                  
        }    */    

        $vista = 'final';
        return view('factura_venta.invoice', compact('factura','productosFactura','vista'));
    }

    public function anular($id){
        $factura = Facturaventas::find($id);
        $factura->status = 0; // 2 es aprobado, 1 es pendiente y 0 cancelado
        $factura->update();
        //$vista = 'final';
        $productosFactura  = Detallefacturaventa::where('factura_id', '=', $id)->get();
        //$inventario = new Inventario();
        //Ahora se debe retornar todos los productos al inventario
        foreach($productosFactura as $key => $prod){
            //cantidad es $prod->cantidad
            //el producto es $prod->producto_id
            $inventarios = Inventario::where('producto_id','=',$prod->producto_id)->get();//obtenemos el producto de cualquiera de los dos inventarios, donde se encuentre su primera aparicion.
            if($inventarios != null){
            	foreach ($inventarios as $key => $inventario) {
            		if($inventario->tipo == 0){
            			$inventario->cantidad += $prod->cant_inventario_produccion;
            		}else if($inventario->tipo == 1){
            			$inventario->cantidad += $prod->cant_inventario_producto;
            		}	
                	$inventario->update();
            	}                
            }
        }
        
        return redirect('/facturas_ventas')->with('status','Venta anulada con exito');
    }

    public function registrarModoPagoAjax(Request $request){
        
        if($request->has('nombre')){
            $nombre = strtoupper(request('nombre'));
        }else{
            return response()->json(["resp" => "error", "msg" => "No se recibio un nombre"]);
        }

        if(ModoPago::whereNombre($nombre)->exists()){
            return response()->json(["resp" => "error", "msg" => "El nombre ingresado ya existe"]);
        }

        $id = ModoPago::create(["nombre" => $nombre]);

        if($id != null){
            $html_option = '<option value="' . $id . '">' . $nombre . "</opcion>";
        }else{
            return response()->json(["resp" => "error", "msg" => "ocurrio un error al intentar crear el registro"]);   
        }

        return response()->json(["resp" => "ok", "msg" => "El nombre se registro con exito", "option" => $html_option]);
    }

    public function registrarAbonoAjax(Request $request){
        
        if($request->has('modo')){
            $modo = request('modo');
        }else{
        
            return response()->json(["resp" => "error", "msg" => "No se recibio un modo valido"]);
        }

        if($request->has('monto')){
            $monto = (double)request('monto');
        }else{
            return response()->json(["resp" => "error", "msg" => "No se recibio un monto"]);
        }

        if($modo == 'normal' and $monto <= 0){
            return response()->json(["resp" => "error", "msg" => "El monto recibido es invalido"]);
        }

        if($request->has('facturaventa_id')){
            $facturaventa_id = (int)request('facturaventa_id');
        }else{
            return response()->json(["resp" => "error", "msg" => "No se recibio un id de factura"]);
        }

        if(!Facturaventas::whereId($facturaventa_id)->exists()){
            return response()->json(["resp" => "error", "msg" => "La factura indicada (" . $facturaventa_id . ") no existe"]);
        }

        
        $factura = Facturaventas::find($facturaventa_id);
        $totalAbonos = (double)Abono::where('facturaventas_id',$facturaventa_id)->sum('monto');
        $total = (double)$factura->total;
        $restante = $total - $totalAbonos;
        $fecha = date('Y-m-d');

        if($modo == 'completar'){
            $monto = $restante;
        }
       // dd($monto,$totalAbonos,$total,$restante);
        if($monto > $restante){
            return response()->json(["resp" => "error", "msg" => "El monto introducido excede el monto restante. Ingrese un monto igual o inferior a " . $restante]);   
        }

        $abono = new Abono(["facturaventas_id" => $facturaventa_id, "monto" => $monto, "fecha" => $fecha]);

        $abono->codigo = uniqid();

        $abono->save();

        if(isset($abono->id)){
            $html = sprintf("%.2f",($totalAbonos + $monto));
            $html_tiket = view('factura_venta.invoice_abono',compact('abono'))->render();
        }else{
            return response()->json(["resp" => "error", "msg" => "ocurrio un error al intentar crear el registro"]);   
        }

        return response()->json(["resp" => "ok", "msg" => "El monto se abono con exito con el codigo " . $abono->codigo . ". Anote este codigo para futuras referencias." , "total_abonado" => $html, "codigo" => $abono->codigo, "tiket" => $html_tiket]);
    }

    public function cargarAbonosAjax(Request $request){
        
        if($request->has('factura_id')){
            $id = (int)request('factura_id');
        }else{
            return response()->json(["resp" => "error", "msg" => "No se recibio un id de factura"]);
        }

        if(!Facturaventas::whereId($id)->exists()){
            return response()->json(["resp" => "error", "msg" => "La factura indicada no existe"]);
        }

        $factura = Facturaventas::find($id);

        $html = view('factura_venta/invoice_abonos',compact('factura'))->render();

        return response()->json(["resp" => "ok", "msg" => "Registros cargados", "html" => $html]);
    }
}

