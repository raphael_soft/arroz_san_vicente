<?php

namespace App\Http\Controllers;

use App\Cargo;
use Illuminate\Http\Request;

class CargosController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $cargos = Cargo::all();
        return view('configuracion.cargos.index', compact('cargos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.cargos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        // dd($request;
        // dd($request->all());

        $nombre = strtoupper($request->get('nombre'));
        $responsabilidades = strtoupper($request->get('responsabilidades'));
        
        $cargo = new Cargo();

        $cargo->nombre = $nombre;
        $cargo->responsabilidades = $responsabilidades;
        
        $cargo->save();

        return redirect('/configuracion/cargos')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $cargo = Cargo::whereId($id)->firstOrFail();
        return view('configuracion.cargos.show', compact('cargo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $cargo = Cargo::whereId($id)->firstOrFail();
        return view('configuracion.cargos.edit', compact('cargo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        $this->validate(request(),[
            'nombre' => 'unique:cargo,nombre,'.$id
        ]);
        $nombre = strtoupper($request->get('nombre'));
        $responsabilidades = strtoupper($request->get('responsabilidades'));
        
        $cargo = Cargo::find($id);
        $cargo->nombre = $nombre;
        $cargo->responsabilidades = $responsabilidades;
            
        $cargo->update();


        return redirect('/configuracion/cargos')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        if($id == 1)
            return redirect('/configuracion/cargos')->with('error', 'Este cargo no puede ser eliminado. Es del sistema');    
    
        $cargo = Cargo::whereId($id)->first();
            
        if($cargo->usuarios->count() > 0)
              return redirect('/configuracion/cargos')->with('error', 'Este cargo no puede ser eliminado. Esta siendo usado.');      

        $cargo->delete();
        return redirect('/configuracion/cargos')->with('status', 'Borrado Exitoso ');
    }
}
