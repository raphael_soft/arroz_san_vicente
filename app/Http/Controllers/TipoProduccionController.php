<?php

namespace App\Http\Controllers;

use App\Http\Requests\TipoProduccionFormRequest;
use App\TipoProduccion;
use Illuminate\Http\Request;

class TipoProduccionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $datos = TipoProduccion::all();
        return view('configuracion.tipo_produccion.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.tipo_produccion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoProduccionFormRequest $request) {
        //
        // dd($request;
        // dd($request->all());

        $nombre = strtoupper($request->get('nombre'));

        $datos = new TipoProduccion(array(
            'nombre' => $nombre,
        ));

        $datos->save();

        return redirect('/configuracion/tipo-de-produccion')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $datos = TipoProduccion::whereId($id)->firstOrFail();
        return view('configuracion.tipo_produccion.show', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $datos = TipoProduccion::whereId($id)->firstOrFail();
        return view('configuracion.tipo_produccion.edit', compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TipoProduccionFormRequest $request, $id) {
        //

        $datos  = TipoProduccion::whereId($id)->firstOrFail();
        $nombre = strtoupper($request->get('nombre'));

        $datos->nombre = $nombre;
        $datos->save();

        return redirect('/configuracion/tipo-de-produccion')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $datos = TipoProduccion::whereId($id)->firstOrFail();
        $datos->delete();
        return redirect('/configuracion/tipo-de-produccion')->with('status', 'Borrado Exitoso ');
    }
}
