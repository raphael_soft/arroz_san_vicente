<?php

namespace App\Http\Controllers;

use App\Rol;
use App\Revision;
use App\Terreno;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class RevisionesController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $revisiones = Revision::all();
        return view('revisiones.index', compact('revisiones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        $terrenos = Terreno::all();
        return view('revisiones.create', compact('terrenos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

    	$request->validate([
		    'fecha_inicio' => 'before:' . $request->fecha_fin
		]);
        $revision = Revision::create([
            "terreno_id" => $request->terreno,
            "user_id" => Auth::user()->id,
            "fecha" => date('Y-m-d'),
            "fecha_inicio" => $request->fecha_inicio,
            "fecha_fin" => $request->fecha_fin,
            "observaciones" => $request->observaciones,
            "area" => $request->area
        ]);
        
        return redirect(route('revisiones'))->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $revision = Revision::findOrFail($id);
        $terrenos = Terreno::all();
        return view('revisiones.show', compact('revision', 'terrenos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $revision = Revision::findOrFail($id);
        $terrenos = Terreno::all();
        return view('revisiones.edit', compact('revision','terrenos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        $this->validate(request(),[
            'observaciones' => 'required',
            'fecha_inicio' => 'required',
            'area' => 'required',
            'fecha_fin' => 'required',
            'terreno' => 'required'
        ]);
        
        $revision = Revision::updateOrCreate(
            ["id" => $id],
            [
                "terreno_id" => $request->terreno,
                "fecha_inicio" => $request->fecha_inicio,
                "fecha_fin" => $request->fecha_fin,
                "observaciones" => $request->observaciones,
                "area" => $request->area
            ] 
        );

        return redirect(route('revisiones'))->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $revision = Revision::findOrFail($id);
        $revision->delete();
        return redirect(route('revisiones'))->with('status', 'Borrado Exitoso ');
    }
}
