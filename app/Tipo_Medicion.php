<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoMedicion extends Model
{
    //
    protected $table = "tipo_mediciones";

    protected $fillable = ['id', 'nombre','simbolo', 'status'];

    
    public function productos()
    {
        return $this->hasMany('App\Producto','medicion_id','id');
    }
    
}
