<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terreno extends Model
{

    protected $table = "terrenos";

    protected $fillable = ['nombre', 'id', 'codigo', 'hectareas'];

    public function insumos(){
    	return $this->hasMany('App\Insumo');
    }
}
