<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModoPago extends Model
{
    //
    protected $table = "modo_pago";

    protected $fillable = ['id', 'nombre', 'imagen', 'status'];

    public function facturaventas()
    {
        return $this->hasMany('App\Facturaventas');
    }

}
