<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    //
    protected $table = "inventario";

    protected $fillable = ['producto_id', 'cantidad', 'status','tipo'];

    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }

}
