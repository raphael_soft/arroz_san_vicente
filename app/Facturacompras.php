<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturacompras extends Model
{
    protected $table = "facturacompras";

    protected $fillable = ['id', 'socio_id', 'n_orden', 'n_factura', 'tipo','fecha_facturacion','subtotal','monto_iva','total'];

     public function socio() {
        return $this->belongsTo('App\Socio');
    }

    public function detalles(){
    	return $this->hasMany('App\Detallefacturacompra','n_factura','n_factura');
    }
}
