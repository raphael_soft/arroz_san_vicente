<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detallefacturacompra extends Model
{
    protected $table = "detalle_factura_compra";

    protected $fillable = ['n_factura', 'producto_id', 'cantidad','precio','impuesto_id'];

    function producto(){
    	return $this->belongsTo('App\Producto');
    }

    function factura(){
    	return $this->belongsTo('App\Facturacompras','n_factura','n_factura');
    }

    function impuesto(){
    	return $this->belongsTo('App\Iva','impuesto_id');
    } 
}
