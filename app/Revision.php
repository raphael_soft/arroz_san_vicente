<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model {
    //
    protected $table = "revisiones";

    protected $fillable = ['id', 'user_id',  'terreno_id', 'area', 'observaciones', 'fecha_inicio', 'fecha_fin', 'fecha'];

    public function usuario() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function terreno() {
        return $this->belongsTo('App\Terreno');
    }

}
