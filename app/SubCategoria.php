<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoria extends Model
{
    //
    protected $table = "subcategorias";

    protected $fillable = ['id', 'categoria_id', 'nombre', 'descripcion', 'status'];

    public function productos()
    {
        return $this->hasMany('App\Producto','subcategoria_id','id');
    }

    public function categoria(){
    	return $this->belongsTo('App\Categoria','categoria_id');
    }

}
