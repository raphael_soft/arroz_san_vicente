<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistema extends Model
{
    protected $table = "sistema";

    protected $fillable = ['id', 'nombre', 'descripcion', 'direccion', 'telefono', 'email','razon_social','email1','email2','telefono1','telefono2','logo1','logo2','direccion_fiscal','nombre_representante','cedula_representante','telefono_representante','ruc','currencies_id','last_facturacompra_producciones','show_logo1_repo','show_logo2_repo','membrete','show_membrete'];

    function currency(){
    	return $this->belongsTo('App\Currency','currencies_id');
    }

    function last_facturacompra_producciones(){
    	return $this->belongsTo('App\facturacompras','last_facturacompra_producciones');
    }

}
