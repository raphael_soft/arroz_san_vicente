<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insumo extends Model {
    //
    protected $table = "insumos";

    protected $fillable = ['id', 'user_id', 'producto_id', 'terreno_id', 'cantidad' , 'costo_unitario', 'costo_total', 'fecha'];

    public function terreno(){
    	return $this->belongsTo('App\Terreno');
    }

	public function producto(){
    	return $this->belongsTo('App\Producto');
    }

    public function usuario(){
    	return $this->belongsTo('App\User', 'user_id');
    }
}
