<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    
    protected $table = "movimientos";

    protected $fillable = ['id', 'producto_id', 'cantidad', 'fecha', 'tipo'];


    public function productos() {

        return $this->belongsToMany('App\Producto');

    }


}
