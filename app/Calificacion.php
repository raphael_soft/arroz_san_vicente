<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    //
    protected $table = "calificaciones";

    protected $fillable = ['id', 'calificacion', 'fecha', 'cosecha_id', 'peso'];

    public function cosecha()
    {
        return $this->belongsTo('App\Cosecha');
    }

}
