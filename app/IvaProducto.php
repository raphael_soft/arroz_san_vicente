<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IvaProducto extends Model {
    //
    protected $table = "iva_producto";

    protected $fillable = ['id', 'iva_id', 'producto_id' ,'status'];

    public function producto(){
    	return $this->belongsTo('App\Producto');
    }

    public function iva(){
    	return $this->belongsTo('App\Iva');
    }
}
