<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre','apellido','dni','fecha_registro','fecha_nacimiento','fecha_ingreso','direccion','direccion','ciudad','avatar', 'email', 'password', 'telefono', 'rol_id', 'cargo_id','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol() {
        return $this->belongsTo('App\Rol');
    }
    public function cargo() {
        return $this->belongsTo('App\Cargo');
    }
    public function revisiones() {
        return $this->hasMany('App\Revision');
    }
}
