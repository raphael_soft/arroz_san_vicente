<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iva extends Model {
    //
    protected $table = "ivas";

    protected $fillable = ['id', 'nombre', 'valor' ,'status'];

    public function productos_compras(){
    	return $this->hasMany('App\Detallefacturacompra','impuesto_id','id');
    }

	public function productos(){
    	return $this->hasMany('App\Producto');
    }    

    public function productos_ventas(){
    	return $this->hasMany('App\Detallefacturaventa','impuesto_id','id');
    }
}
