<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hectarea extends Model
{
    protected $table = "hectareas";

    protected $fillable = ['id', 'terreno_id', 'nombre', 'codigo','metros'];

    public function terreno()
    {
		return $this->belongsTo('App\Terreno');
    }
}
