<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detallefacturaventa extends Model
{
    protected $table = "detalle_factura_venta";

    protected $fillable = ['factura_id', 'producto_id', 'cantidad','precio','impuesto_id','cant_inventario_producto','cant_inventario_producccion'];

    function producto(){
    	return $this->belongsTo('App\Producto');
    }

    function factura(){
    	return $this->belongsTo('App\Facturaventas');
    }

    function impuesto(){
    	return $this->belongsTo('App\Iva','impuesto_id');
    } 
}
