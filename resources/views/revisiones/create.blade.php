@extends('master')
@section('title', 'Nueva Revision')
@section('active-revisiones', 'active')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>
		Revisiones
		</h1>
		<ol class="breadcrumb">
			<li class="">
				<a href="{{ url('/') }}">
					<i class="fa fa-dashboard">
					</i>
					Inicio
				</a>
			</li>
			<li class="">
				<a href="{{ url('/configuracion/roles/') }}">
					Revisiones
				</a>
			</li>
			<li class="active">
				<a href="#">
					Nueva Revision
				</a>
			</li>
		</ol>
	</section>
	@foreach($errors->all() as $error)
	<p class="alert alert-danger">{{$error}}</p>
	@endforeach
	@if (session('status'))
	<div class="alert alert-success">
		{{session('status')}}
	</div>
	@endif
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Nueva Revision</h3>
					</div>
					<form class="form-horizontal" method="POST" id="formulario-revision">
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">Terreno</label>
								<div class="col-sm-10">
									<select name="terreno" class="form-control" required="required">
										<option value="">Seleccione el terreno</option>
										@foreach($terrenos as $h)
										<option value="{{$h->id}}">{{$h->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
					            <label class="col-sm-2 control-label">Fecha Inicio</label>
					            <div class="col-sm-10">
					        	    <input type="date" name="fecha_inicio" id="fecha_inicio" class="form-control" placeholder="Fecha inicio" required="required">
					            </div>
					        </div>
					        <div class="form-group">
					            <label class="col-sm-2 control-label">Fecha Fin</label>
					            <div class="col-sm-10">
					                <input type="date" name="fecha_fin" id="fecha_fin" class="form-control" placeholder="Fecha Fin" required="required">
					            </div>
					        </div>
					        <div class="form-group">
					            <label class="col-sm-2 control-label">Area Observada</label>
					            <div class="col-sm-10">
					                <input type="text" name="area" class="form-control" placeholder="Area observada" />
					            </div>
					        </div>
							<div class="form-group">
					            <label class="col-sm-2 control-label">Observaciones</label>
					            <div class="col-sm-10">
					                <textarea name="observaciones" class="form-control" placeholder="Coloque aqui sus observaciones"></textarea>
					            </div>
					        </div>
						</div>
						<div class="box-footer">
							<a onclick="window.history.back()"><button type="button" class="btn btn-warning">Regresar</button></a>
							<button type="submit" class="btn btn-info pull-right">Registrar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
@section('js')
@parent
<script> 
	$(document).ready(function(){
		$('#formulario-revision').on('submit', function(e){
			
			const fecha_inicio = new Date($('#fecha_inicio').val());
			const fecha_fin = new Date($('#fecha_fin').val());

			if(fecha_inicio >= fecha_fin ){
				e.preventDefault();
				alert("La fecha de inicio debe ser menor a la fecha final");
				$('#fecha_inicio').focus();
				return;
			}
		});
	});
</script>
@endsection