@extends('master')
@section('title', 'Revision - '. $revision->id )
@section('active-revisiones', 'active')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>
		Revisiones
		</h1>
		<ol class="breadcrumb">
			<li class="">
				<a href="{{ url('/') }}">
					<i class="fa fa-dashboard">
					</i>
					Inicio
				</a>
			</li>
			<li class="">
				<a href="{{ url('/configuracion/roles') }}">
					Revisiones
				</a>
			</li>
			<li class="active">
				<a href="#">
					{{ $revision->id }}
				</a>
			</li>
		</ol>
	</section>
	@foreach($errors->all() as $error)
	<p class="alert alert-danger">{{$error}}</p>
	@endforeach
	@if (session('status'))
	<div class="alert alert-success">
		{{session('status')}}
	</div>
	@endif
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Nueva Revision</h3>
					</div>
					<form class="form-horizontal" method="POST">
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<div class="box-body">
							<div class="form-group">
					            <label class="col-sm-2 control-label">Usuario</label>
					            <div class="col-sm-10">
					                <input type="text" name="usuario" class="form-control" placeholder="" readonly="readonly" value="{{$revision->usuario->nombre}}" />
					            </div>
					        </div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Terreno</label>
								<div class="col-sm-10">
									<select name="terreno" class="form-control" required="required" readonly="readonly">
										<option value="">Seleccione el terreno</option>
										@foreach($terrenos as $h)
										<option value="{{$h->id}}" @if($revision->terreno_id == $h->id) selected @endif>{{$h->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
					            <label class="col-sm-2 control-label">Fecha Inicio</label>
					            <div class="col-sm-10">
					        	    <input type="date" name="fecha_inicio" class="form-control" placeholder="Fecha inicio" required="required" readonly="readonly" value="{{date('Y-m-d', strtotime($revision->fecha_inicio))}}">
					            </div>
					        </div>
					        <div class="form-group">
					            <label class="col-sm-2 control-label">Fecha Fin</label>
					            <div class="col-sm-10">
					                <input type="text" name="fecha_fin" class="form-control" placeholder="Fecha Fin" required="required" readonly="readonly" value="{{date('Y-m-d', strtotime($revision->fecha_fin))}}">
					            </div>
					        </div>
					        <div class="form-group">
					            <label class="col-sm-2 control-label">Area Observada</label>
					            <div class="col-sm-10">
					                <input type="text" name="area" class="form-control" placeholder="Area observada" readonly="readonly" value="{{$revision->area}}"/>
					            </div>
					        </div>
							<div class="form-group">
					            <label class="col-sm-2 control-label">Observaciones</label>
					            <div class="col-sm-10">
					                <textarea name="observaciones" class="form-control" placeholder="Coloque aqui sus observaciones" readonly="readonly">{{$revision->observaciones}}</textarea>
					            </div>
					        </div>
						</div>						
						<div class="box-footer text-right">
							<a onclick="window.history.back()" class="btn btn-warning">Regresar</a>
							<a href="{{ action('RevisionesController@edit', $revision->id) }}" class="btn btn-info">Modificar</a>
							<a href="javascript: eliminar({{$revision->id}},'{{route('destroy_revision',$revision->id)}}');"  class="btn btn-danger">Eliminar</a>
						</div>						
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection