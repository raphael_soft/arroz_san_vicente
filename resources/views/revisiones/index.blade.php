@extends('master')
@section('title', 'Revisiones')
@section('active-revisiones', 'active')
@section('content')


	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Revisiones
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Revisiones
	                </a>
	            </li>
	        </ol>
	    </section>

	    @if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif
		@if (session('error'))
			<div class="alert alert-danger">
				{{session('error')}}
			</div>
		@endif
	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
	    			<div class="box">
	    				<div class="box-header">
	    					<h3 class="box-title"><a href="{{route('create_revision')}}" class="btn btn-primary">Crear Nueva</a></h3>
	    				</div>
	    				<div class="box-body">
	    						<table class="table table-bordered table-hover informacion responsive">
	    							<thead>
	    								<th>Código</th>
	    								<th>Terreno</th>
	    								<th>Usuario</th>
	    								<th>Fecha Creacion</th>
	    								<th>Fecha Inicio</th>
	    								<th>Fecha Fin</th>
	    								<th>Ultima Mod.</th>
	    								<th style="min-width: 150px;" class="no-print">Opciones</th>
	    							</thead>
	    							<tbody>
    								@foreach($revisiones as $revision)
	    								<tr>
		    								<td>{{$revision->id}}</td>
		    								<td><a href="{{ route('show_terreno', $revision->terreno->id) }}">{{ $revision->terreno->nombre }}</a></td>
		    								<td><a href="{{ route('show_usuario', $revision->usuario->id) }}">{{ $revision->usuario->nombre }}</a></td>
		    								<td>{{$revision->fecha}}</td>
		    								<td>{{$revision->fecha_inicio}}</td>
		    								<td>{{$revision->fecha_fin}}</td>
		    								<td>{{$revision->updated_at}}</td>
		    								<td>
                        						<a href="{{route('show_revision',$revision->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                    							<a href="{{route('edit_revision',$revision->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i></a>
                    							<a href="javascript: eliminar({{$revision->id}},'{{route('destroy_revision',$revision->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    						</td>
		    							</tr>
		    							@endforeach
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </section>

	    <script>
	        $(document).ready(function(){
	            $('.informacion').DataTable({
	                dom: '<"html5buttons"B>lTfgitp',
	                select: true,
	                buttons: [
	                     {extend: 'csv', title: 'Lista de Ivas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
	                     {extend: 'excel', title: 'Lista de Ivas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
	                     {extend: 'pdf', title: 'Lista de Ivas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

	                     {extend: 'print',
	                         customize: function (win){
	                             $(win.document.body).addClass('white-bg');
	                             $(win.document.body).css('font-size', '10px');
	                             $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
	                         },
	                         exportOptions: {
						        columns: ':not(.no-print)'
						    }
	                     }
	                ],
	            });
	        });

	    </script>
	</div>

@endsection