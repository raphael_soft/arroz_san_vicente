@extends('master')

@section('title', 'Precios')

@section('active-venta', 'active')
@section('active-venta-precio', 'active')

@section('content')





	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Precios

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/"') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    Precios

	                </a>

	            </li>

	        </ol>

	    </section>



	    @if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif

		@if (session('status-error'))

			<div class="alert alert-danger">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

	    			<div class="box">

	    				<div class="box-header">

	    					<h3 class="box-title"><a href="{{url('/create_precio')}}" class="btn btn-primary">Crear Precio</a></h3>

	    				</div>

	    				<div class="box-body">

	    					<div class="table-responsive">

	    						<table class="table table-bordered table-hover informacion">

	    							<thead>

	    								<th>Tipo Producci&oacute;n</th>
	    								
	    								<th>Categoria</th>

	    								<th>Tipo Medida</th>

	    								<th>Precio</th>

	    							</thead>

	    							<tbody>
	    									
	    								@foreach($datos as $dato)

		    								<tr>
		    									<td><a href="{!! action('PrecioController@show', $dato->id) !!}">{{ $dato->tipo_produccion->nombre }}</a></td>
			
		    									<td><a href="{!! action('PrecioController@show', $dato->id) !!}">{{ $dato->categoria->nombre }}</a></td>

		    									<td><a href="{!! action('PrecioController@show', $dato->id) !!}">{{ $dato->tipo_medicion->nombre }}</a></td>

		    									<td><a href="{!! action('PrecioController@show', $dato->id) !!}">${{ $dato->precio }}</a></td>
			
		    								</tr>

		    							@endforeach

	    							</tbody>

	    						</table>

	    					</div>

	    				</div>

	    			</div>

	    		</div>

	    	</div>

	    </section>



	    <script>

	        $(document).ready(function(){

	            $('.informacion').DataTable({

	                dom: '<"html5buttons"B>lTfgitp',

	                select: true,

	                buttons: [

	                    // {extend: 'csv', title: 'Lista de Ivas'},

	                    // {extend: 'excel', title: 'Lista de Ivas'},

	                    // {extend: 'pdf', title: 'Lista de Ivas'},



	                    // {extend: 'print',

	                    //     customize: function (win){

	                    //         $(win.document.body).addClass('white-bg');

	                    //         $(win.document.body).css('font-size', '10px');

	                    //         $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

	                    //     }

	                    // }

	                ],

	            });

	        });



	    </script>

	</div>



@endsection