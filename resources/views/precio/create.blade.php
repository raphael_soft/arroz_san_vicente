
@extends('master')

@section('title', 'Precios' )

@section('active-venta', 'active')

@section('active-venta-precio', 'active')

@section('content')


	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Precios

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="">

	                <a href="{{ url('/precios') }}">

	                    Precios

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    Nuevo Precio

	                </a>

	            </li>

	        </ol>

	    </section>



	    @foreach($errors->all() as $error)

			<p class="alert alert-danger">{{$error}}</p>

		@endforeach



		@if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

					<div class="box box-info">

			            <div class="box-header with-border">

			              	<h3 class="box-title">Nueva Produccion</h3>

			            </div>

			            <form class="form-horizontal" method="POST">

			            	<input type="hidden" name="_token" value="{!! csrf_token() !!}">

			              	<div class="box-body">


				                <div class="form-group">

				                  	<label for="tipo_produccion" class="col-sm-2 control-label">Tipo de Produccion</label>

					                <div class="col-sm-10">

					                   	<select name="tipo_produccion" id="tipo_produccion" class="form-control" required>

					                   		<option value="">Seleccione una opcion</option>

					                   		@foreach ($tipo_producciones as $tipo_produccion)

					                   			<option value="{{ $tipo_produccion->id }}">{{ $tipo_produccion->nombre }}</option>

					                   		@endforeach

					                   	</select>

					                </div>

				                </div>



				                <div class="form-group">

				                  	<label for="categoria" class="col-sm-2 control-label">Categoria</label>

					                <div class="col-sm-10">

					                   	<select name="categoria" id="categoria" class="form-control" required>

					                   		<option value="">Seleccione una opcion</option>

					                   		@foreach ($categorias as $categoria)

					                   			<option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>

					                   		@endforeach

					                   	</select>

					                </div>

				                </div>



				                <div class="form-group">

				                  	<label for="tipo_medicion" class="col-sm-2 control-label">Tipo de Medicion</label>

					                <div class="col-sm-10">

					                   	<select name="tipo_medicion" id="tipo_medicion" class="form-control" required>

					                   		<option value="">Seleccione una opcion</option>

					                   		@foreach ($tipo_mediciones as $tipo_medicion)

					                   			<option value="{{ $tipo_medicion->id }}">{{ $tipo_medicion->nombre }}</option>

					                   		@endforeach

					                   	</select>

					                </div>

				                </div>

				                

				                <div class="form-group">

				                  	<label for="cantidad" class="col-sm-2 control-label">Precio</label>

					                <div class="col-sm-10">

					                   	<input type="tel"  class="form-control precio" id="precio" name="precio" placeholder="Cantidad" value="{{ old('cantidad') }}" required />

					                </div>

				                </div>



			              	</div>

			              <div class="box-footer">

			                <a href="{{ url('/precios') }}"><button type="button" class="btn btn-warning">Regresar</button></a>

			                <button type="submit" class="btn btn-info pull-right">Registrar</button>

			              </div>

			            </form>

			          </div>

				</div>

			</div>

	    </section>

	</div>





	<script>

		$(document).ready(function() {

			$('select#socio').on('change',function(){

	            var socio = $('#socio').val();

	            var parametros = {'socio' : socio};

	            $.ajax({

	            	headers: {

						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

					},

	                url: '/produccion/get_reses',

	                data: parametros,

	                type: 'POST',

	                dataType: 'JSON',

	                beforeSend: function(){
	                	showLoading();
	                    

	                },

	                success: function (data){
	                	hideLoading();
	                    // $("#cargando").html("<i></i>");

	                    console.log(data);

                        var html = '<option>Seleccione una opcion</option>';

                        for(var o =0; o < data.length; o++){

                            html += '<option value="'+data[o]['id']+'">'+data[o]['nombre']+'</option>';

                        }

                        $('#res').html(html);

	                },
	                error : function(e){
	                	hideLoading();
	                }

	            });



	        });

		});

	</script>



@endsection