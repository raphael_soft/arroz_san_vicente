
@extends('master')

@section('title', 'Precios' )

@section('active-venta', 'active')

@section('active-venta-precio', 'active')


@section('content')

  <div class="content-wrapper">



      <section class="content-header">

          <h1>

            Datos del precio

            <small>Secci&oacute;n para visualizar los datos del Precio</small>

          </h1>

          <ol class="breadcrumb">

            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>

            <li><a href="#">Ventas</a></li>

            <li><a href="{{url('/precios')}}">Precios</a></li>

            <li class="active">Ver Precio</li>

          </ol>

        </section>



      @foreach($errors->all() as $error)

      <p class="alert alert-danger">{{$error}}</p>

    @endforeach



    @if (session('status'))

      <div class="alert alert-success">

        {{session('status')}}

      </div>

    @endif



      <section class="content">

        <div class="row">

          <div class="col-md-3">

            

          </div>

          <div class="col-md-12 box-body">

                <div class="box box-widget widget-user-2">

                   <form  method="POST">

                          <div class="box-body">

                              <div class="form-group">

                              <label class="col-sm-2 control-label">Categor&iacute;a</label>

                              <div class="col-sm-10">
                                <input  value="{{$precio->id}}" type="hidden" name="id" class="form-control">

                                <select name="categoria" id="categoria" class="form-control" required>

			                   		<option value="{{$categoria->id}}" style="background-color: green; color: white;">{{$categoria->nombre}}</option>
									<option disabled="disabled">&nbsp;</option>

			                   		@foreach ($categorias as $c)

			                   			<option value="{{ $c->id }}">{{ $c->nombre }}</option>

			                   		@endforeach

					              </select>
                              </div>

                              <label class="col-sm-2 control-label">Tip .Produc.</label>

                              <div class="col-sm-10">

			                   	<select name="tipo_produccion" id="tipo_produccion" class="form-control" required>

			                   		<option value="{{$tipo_produccion->id}}" style="background-color: green; color: white;">{{$tipo_produccion->nombre}}</option>
									<option disabled="disabled">&nbsp;</option>

			                   		@foreach ($tipo_producciones as $tp)

			                   			<option value="{{ $tp->id }}">{{ $tp->nombre }}</option>

			                   		@endforeach

			                   	</select>								
                              </div>

                              <label class="col-sm-2 control-label">Tipo Medicion</label>

                              <div class="col-sm-10">

								<select name="tipo_medicion" id="tipo_medicion" class="form-control" required>

			                   		<option value="{{$tipo_medicion->id}}" style="background-color: green; color: white;">{{$tipo_medicion->nombre}}</option>
									<option disabled="disabled">&nbsp;</option>
			                   		@foreach ($tipo_mediciones as $t)

			                   			<option value="{{ $t->id }}">{{$t->nombre }}</option>

			                   		@endforeach

					              </select>
                              </div>

                              <label class="col-sm-2 control-label">Precio</label>

                              <div class="col-sm-10">

                                <input value="{{$precio->precio}}" type="text" name="precio" class="form-control" placeholder="C&oacute;digo del Socio">

                              </div>

                            </div>

                         </div>

                              <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                             </div>

                                    <!-- /.box-body -->

                            <div class="box-footer col-sm-12 text-center">

                              <a href="{{url('/show_precio',$precio->id)}}"  class="btn btn-default pull-left"><< Volver</a>

                              <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Guardar</button>



                            </div>

                            <!-- /.box-footer -->

                        </form>

                 

                </div>

            </div>

          <div class="col-md-3"></div>

      </div>

      </section>



@endsection
