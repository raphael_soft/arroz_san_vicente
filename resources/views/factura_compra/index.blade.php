@extends('master')

@section('title', 'Compras')

@section('active-compra', 'active')

@if($tipo == "producciones")
@section('active-compra-producciones', 'active')
@endif
@if($tipo == "productos")
@section('active-compra-productos', 'active')
@endif

@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Facturas de compra de {{$tipo}}

        </h1>

        <ol class="breadcrumb">

            <li>

                <a href="{{ url('/"') }}">

                    <i class="fa fa-dashboard">

                    </i>

                    Inicio

                </a>

            </li>
            <li class="active">Facturas de compras de {{$tipo}}</li>
        </ol>

    </section>

    <section class="content">

       <div class="box">

            <div class="box-header">

              <h3 class="box-title">Listado de Facturas de Compra de {{$tipo}}</h3>             

                @if (session('status'))

                    <div class="alert alert-success">

                        {{session('status')}}

                    </div>

                @endif
              <!-- Split button -->
              @if(session('registrar'))
<a href="{{url('create_facturacompra',$tipo)}}"><button type="button" class="btn btn-primary" style="float: right;">
    <i class="fa fa-plus"></i> Facturar
  </button></a>
    @endif

            </div>

            <!-- /.box-header -->

            <div class="box-body">
              <div class="row">
                <form method="GET">
              <div class="col-sm-12"><label>Consultar por fechas</label></div>
                        <div class="form-group col-sm-3">
                               <label >Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="from" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label >Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="to" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
                      </form>
              </div>
              <table id="example1" class="table table-bordered table-striped table-responsive informe responsive">	<thead>

                  <tr>

                    <th  style="max-width: 78px;">N #</th>

                    <!--th  style="max-width: 78px;">N. Orden</th-->

                    <th >Socio</th>
                    <th  style="max-width: 100px;">Subtotal ({{$sistema->currency->symbol}})</th>
                    <th  style="max-width: 100px;">Impuesto ({{$sistema->currency->symbol}})</th>
                    <th  style="max-width: 100px;">Total ({{$sistema->currency->symbol}})</th>

                    <th  style="max-width: 67px;">Status</th>

                    <th  style="max-width: 83px;">Fecha</th>

                    <th style="max-width: 100px;" class="no-print">Opciones</th>

                  </tr>

                </thead>

                <tbody id="tabla_datos">



                @foreach($factura as $f)

                    <?php

                       $socio = DB::table('socios')->where('id', $f->socio_id)->get();

                       $datos = json_decode($socio,true);?>

                  <tr>

                    <td>{{$f->n_factura}}</td>

                    <!--td>{{$f->n_orden}}</td-->

                    <td>{{$datos[0]['nombre']}}</td>
                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f->subtotal)}}</td>
                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f->monto_iva)}}</td>
                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f->total)}}</td>

                    @if($f->status == 0)

                        <td style="color: red;"><b>{{"Cancelada"}}</b></td>

                    @elseif($f->status == 1)

                        <td style="color: gray;"><b>{{"Pendiente"}}</b></td>

                    @elseif($f->status == 2)

                        <td style="color: green;"><b>{{"Aceptada"}}</b></td>

                    @endif

                    <td>{{$f->fecha_facturacion}}</td>

                    <td>
                      @if(session('ver'))
                        <a href="{{url('show_facturacompra',$f->id)}}" data-toggle="tooltip" data-placement="top" title="Presione aqui para ver esta factura" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                        <a href="{{url('imprimir_facturacompra',$f->id)}}" target="_new" data-toggle="tooltip" data-placement="top" title="Presione aqui para imprimir esta factura" class="btn btn-warning"><i class="fa fa-print"></i></a>
                        
                        @endif
                        @if(session('superuser'))
                        @if($f->status == 1)
                        <a href="{{url('cancelar_factura',$f->id)}}" data-toggle="tooltip" data-placement="top" title="Presione aqui para cancelar esta factura" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                        <a href="{{url('aprobar_factura',$f->id)}}" data-toggle="tooltip" data-placement="top" title="Presione aqui para aprobar esta factura" class="btn btn-success"><i class="fa fa-check-square"></i></a>
                        @endif
                        @endif
                    </td>

                  </tr>

                @endforeach

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->



          



    </section>

</div>

    <script>

        $(document).ready(function(){

            $('.informe').DataTable({

                 dom: '<"html5buttons"B>lTfgitp',

                 select: true,

                 buttons: [

                     {extend: 'csv', title: 'Facturas de Compra de {{$tipo}}', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

                     {extend: 'excel', title: 'Facturas de Compra de {{$tipo}}', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

                     {extend: 'pdf', title: 'Facturas de Compra de {{$tipo}}', exportOptions: {
					        columns: ':not(.no-print)'
					    }},



                     {extend: 'print',

                         customize: function (win){

                             $(win.document.body).addClass('white-bg');

                             $(win.document.body).css('font-size', '10px');

                             $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                         },
                         exportOptions: {
					        columns: ':not(.no-print)'
					    }

                     }

                 ],

             });

        });



    </script>

@endsection

