@php
use App\Sistema;
@endphp
@extends('master')
@section('title', 'Facturacion')
@section('active-compra', 'active')

@if($tipo == "producciones")
@section('active-compra-producciones', 'active')
@endif
@if($tipo == "productos")
@section('active-compra-productos', 'active')
@endif

@section('content')

  @php
    $sistema = Sistema::first();
  @endphp

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if($vista == "confirmacion")
        Confirmacion de 
        @endif
        Factura de Compra

        <small># {{$factura->n_factura}}</small>

      </h1>
      <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="{{url('/facturas_compras/'.$tipo)}}">Compras de {{$tipo}}</a></li>
            @if($vista == "confirmar")
            <li><a href="{{url('/create_facturacompra/'.$tipo)}}">Nueva Factura</a></li>
            <li class="active">Confirmar Nueva Factura</li>
            @else
            <li class="active">Ver Factura</li>
            @endif
          </ol>
    </section>
    <?php
      if($factura->status == 2){
        $a = 'success';
      }elseif ($factura->status == 0){
        $a = 'danger';
      }elseif($factura->status == 1){
        $a = 'info';
      }
      ?>
    <div class="pad margin no-print">
      <div class="callout callout-{{$a}}" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> Nota:</h4>
        @if($vista == "confirmacion")
        Por favor, verifique toda la información de la compra antes de proceder a guardarla. Una vez, guardada, no se podrá modificar la información.
        @endif
        @if($factura->status == 1 )
        Esta factura esta pendiente por aprobacion. 
        @if(session('superuser'))
        Abajo tiene las opciones para APROBAR o CANCELAR esta factura de compra.
        @else
        Solo el personal autorizado posee permisos para aprobar y/o cancelar facturas de compra y venta.
        @endif
        @endif
        @if($factura->status == 2)
        Esta factura esta aprobada. 
        @endif
        @if($factura->status == 0)
        Esta factura fue Cancelada. 
        @endif
      </div>
    </div>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            Factura de Compra de {{$tipo}}
            <small class="pull-right">{{date('Y-m-d')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="pull-right col-sm-4">
          @if($factura->status == 2)
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content" >
              <span class="info-box-text" style="font-size: 30px;">APROBADO</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          @endif
          @if($factura->status == 1)
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text" style="font-size: 30px;">PENDIENTE</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          @endif
          @if($factura->status == 0)
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text" style="font-size: 30px;">CANCELADO</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          @endif
        </div>
        <div class="col-sm-2">
          @if($sistema->show_logo1 == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1); ?>
          <img src="{{url('/storage/'.$filename)}}" style="width: 100px; max-width: 100px;z-index: 1">
          @endif
        </div>
        <div class="col-sm-6" style="text-align: center;">
          
            @if($sistema->show_razon_social)
            <h3><b>{{$sistema->razon_social}}</b><br></h3>
            @endif
            <address>
            <b>RUC: {{$sistema->ruc}}</b><br>
            @if($sistema->show_direccion)
            <b>Direccion:</b> {{$sistema->direccion}}<br>
            @endif
            @if($sistema->show_telefono1)
            <b>Telefono 1:</b> {{$sistema->telefono1}}<br>
            @endif
            @if($sistema->show_telefono2)
            <b>Telefono 2:</b> {{$sistema->telefono2}}<br>
            @endif
            @if($sistema->show_email1)
            <b>Email 1:</b> {{$sistema->email1}}<br>
            @endif
            @if($sistema->show_email2)
            <b>Email 2:</b> {{$sistema->email2}}
            @endif
          </address>
        </div>
        @if($sistema->show_logo2 == true)
        <div class="col-sm-2">
              
          <?php $filename = str_replace("public/",'',$sistema->logo2); ?>
          <img src="{{url('/storage/'.$filename)}}"  style="width:100px; max-width: 100px;z-index: 1">
          
        </div>
        @endif
        
        <div class="col-sm-12">
        <div class="col-sm-6">
          @if($tipo == "productos")
            <h3>Proveedor</h3>
          @else
            <h3>Socio</h3>
          @endif
          <address>
            <b>Razon social/Nombre:</b> {{$factura->socio->nombre}}<br>
            <b>Cedula/RUC:</b> {{$factura->socio->ruc}}<br>
            <b>Direccion:</b> {{$factura->socio->direccion}}<br>
            <b>Telefono:</b> {{$factura->socio->telefono}}<br>
            
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-6" style="text-align: right;">
          
          <input type="hidden" name="n_factura" value="{{$factura->n_factura}}">
          <strong>Registro # </strong>
          
          <span style="font-size: 30px; color: red">
        {{$factura->n_factura}}</span><br>
          
          <br>
          <!--b>Orden ID:</b> {{$factura->n_orden}}<br-->
          <input type="hidden" name="orden_id" value="{{$factura->n_orden}}">
        </div>
        <!-- /.col -->
        
      </div>
    </div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Cant</th>
              <th>Codigo</th>
              <th>Producto</th>
              <th>Precio Unit</th>
              <!--th>Subtotal</th>
              <th>Impuesto</th-->
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
  @if(isset($productosFactura) and !empty($productosFactura))            
      @foreach($productosFactura as $key => $value)
         @php
            
            $subtotal = ($value->precio * $value->cantidad);
            if(isset($value->impuesto)){
              $impuesto_valor = $value->impuesto->valor;
              $impuesto = ($value->impuesto->valor * $subtotal) / 100;
            }else{
              $impuesto_valor = 0;
              $impuesto = 0;
            }
            $total = $subtotal + $impuesto;
            //$subtotal_general += $subtotal;
            //$impuesto_general += $impuesto;
            //$total_general += $total;
            
         @endphp
            <tr>
              <td>{{sprintf("%.2f",$value->cantidad)}} {{$value->producto->medicion->simbolo}}</td>
              <td>{{$value->producto->codigo}}</td>
              <td>{{$value->producto->nombre}}</td>
              <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$value->precio)}} x {{$value->producto->medicion->simbolo}}</td>
              
              <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$subtotal)}}</td>
            </tr>
      @endforeach      
  @endif          
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        @if($vista == "confirmacion")
        <div class="col-xs-12 col-md-6 col-lg-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Por favor verifique los montos. Si esta de acuerdo, presione CONTINUAR, sino, presione volver.
          </p>
        </div>
        @endif
        <!-- /.col -->
        <div class="col-xs-12 col-md-6 col-lg-6 pull-right">
          <div class="table-responsive">
            <table class="table">
              <tbody>
                @php 
                  $impuestos_iva = array();
                  $subtotal_noiva = 0;
                @endphp
                <tr>
                <td>Subtotal sin IVA:</td>
                <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$factura->subtotal)}}</td>
              </tr>
                <!-- ciclo para calcular los montos subtotales por cada iva -->
                @foreach($impuestos as $imp)
              <tr>
                <td>IVA {{$imp->valor}} %</td>
                <td>{{$sistema->currency->symbol}} 
                  @php 
                  
                  $impuestos_iva[$imp->id] =  0;
                  $temp_suma_iva = 0;
                  $subtotal_iva0 = 0;
                  //dd($productosFactura);  
                  @endphp

                  @foreach($productosFactura as $key => $value)
                    @php

                    if(isset($value->impuesto) and $value->impuesto->id == $imp->id){
                        //echo($imp->id."->".$imp->valor);
                        if($imp->valor > 0)
                          $impuestos_iva[$imp->id] += ($value->cantidad * $value->precio) * $imp->valor/100;  
                        else
                          $subtotal_iva0 += ($value->cantidad * $value->precio);
                    }
                    @endphp  

                  @endforeach
                  {{sprintf("%.2f",$impuestos_iva[$imp->id])}}  
                </td>
              </tr>
              @endforeach
              
              <tr>
                <td><h3>Total:</h3></td>
                <td><h3>{{$sistema->currency->symbol}} {{sprintf("%.2f",$factura->total)}}</h3></td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<div class="row page-footer">
@if($sistema->show_facebook == true)
<div class="col-sm-12">
  <label>
<i class="fa fa-facebook" style="width: 20px; max-width: 20px;"></i>
{{$sistema->facebook}}
</label>
</div>
@endif
@if($sistema->show_twiter == true)
<div class="col-sm-12">
  <label>
<i class="fa fa-twiter" style="width: 20px; max-width: 20px;"></i>
{{$sistema->twiter}}
</label>
</div>
@endif
@if($sistema->show_web == true)
<div class="col-sm-12">
 <label> 
<i class="fa fa-globe" style="width: 20px; max-width: 20px;"></i>
{{$sistema->web}}
</label>
</div>          
@endif
</div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12 text-right">
        @if($vista == "final")
          <a href="{{url('/imprimir_facturacompra',$factura->id)}}" target="_new" class="btn btn-default col-lg-2 pull-right" title="imprimir factura"><i class="fa fa-print"></i> Imprimir</a>
        @endif
        
        @if(session('registrar'))
          @if($vista == "confirmacion" and $factura->status == 1)
            <a title="Finalizar compra" onclick="finalizarCompra();">
            <button type="button" class="btn btn-primary col-lg-2 pull-right" ><i class="fa fa-arrow-right col-lg-1"></i>CONTINUAR</button>
            </a>
          @endif
          @if($factura->status == 1 and $vista == "final")
            <a title="Aprobar compra" href="{{url('/aprobar_factura',$factura->id)}}">
            <button type="button" class="btn btn-success col-lg-2 pull-right" ><i class="fa fa-check-square"></i>APROBAR</button>
            </a>
            <a title="Cancelar compra" href="{{url('/cancelar_factura',$factura->id)}}">
            <button type="button" class="btn btn-danger col-lg-2 pull-right" ><i class="fa fa-close"></i>CANCELAR</button>
            </a>
          @endif
        @endif 

        @if($vista == "final")
        	<a href="{{ url('/facturas_compras', $tipo) }}" class="btn btn-primary col-lg-2 pull-right"><i class="fa fa-arrow-left"></i> Volver</a>
        @endif
        
        @if($vista == "confirmacion")
          <div>
            <form method="GET" action="{{ route('create_facturacompra', $tipo) }}"> 
              <input type="hidden" name="orden_id" value="{{ old('orden_id') }}">
              <input type="hidden" name="socio" value="{{ old('socio') }}">
              <input type="hidden" name="n_factura" value="{{ old('n_factura') }}">
              
              <input type="hidden" id="_token" name="_token" value="{!! csrf_token() !!}">
                @if(old('listaproductos') != null)
                  @foreach(old('listaproductos') as $producto)
                    <input type="hidden" name="listaproductos[]" value="{{ $producto }}">
                  @endforeach
                @endif
                
                @if(old('listaprecios') != null)
                  @foreach(old('listaprecios') as $precio)
                  <input type="hidden" name="listaprecios[]" value="{{ $precio }}">
                  @endforeach
                @endif
                
                @if(old('listacantidades') != null)            
                  @foreach(old('listacantidades') as $cantidad)
                  <input type="hidden" name="listacantidades[]" value="{{ $cantidad }}">
                  @endforeach
                @endif
                
                @if(old('listaimpuestos') != null)            
                  @foreach(old('listaimpuestos') as $impuesto)
                  <input type="hidden" name="listaimpuestos[]" value="{{ $impuesto }}">
                  @endforeach
                @endif

                @if(old('listaimpuestosval') != null)            
                  @foreach(old('listaimpuestosval') as $impuestoval)
                  <input type="hidden" name="listaimpuestosval[]" value="{{ $impuestoval }}">
                  @endforeach
                @endif              
              <button type="submit" class="btn btn-primary col-lg-2 pull-right" style="margin-right: 5px">
                <i class="fa fa-arrow-left"></i>Volver
              </button>
            </form>
          </div>
        @endif
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
    <input type="hidden" id="_token" name="_token" value="{!! csrf_token() !!}">
  </div>
        
  </div>
  <script type="text/javascript">
  @if($vista == "confirmacion")  
   function finalizarCompra(){
    _token = $('#_token').val();
    //$('#form-compra').submit();
    
      $.ajax({
            url: "{{url('finish_facturacompra')}}",
            data: {
              _token: _token ,factura: @json($factura), productosFactura: @json($productosFactura)
            },
            method: "POST",
            success: function(data){
            	hideLoading();
              	if(data.resp == "existe"){
          			alert("Error: Este registro ya se encuentra en la base de datos.");
        		}else if(data.resp == "ok"){
          			alert("¡Registro Exitoso!");
          			window.location.href = "{{url('/facturas_compras',$tipo)}}";
        		}else{
        			alert(data.resp);
        		}
            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error al hacer la peticion ajax");
            }

            });
   } 
@endif

</script>

@endsection
