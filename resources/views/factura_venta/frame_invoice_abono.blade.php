@php
use App\Sistema;
@endphp
@extends('master')
@section('title', 'Facturacion')
@section('active-venta', 'active')

@section('active-ventas-por-cobrar', 'active')

@section('active-ventas-abonos', 'active')

@section('content')
@php
            $sistema = Sistema::first();
          @endphp
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      
        Abono de Factura de Venta

        <small># {{$abono->codigo}}</small>
      </h1>
      <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Ventas</a></li>
            <li><a href="{{url('facturasventas')}}">Facturas</a></li>
            <li class="active">Ver Abono</li>
          </ol>
    </section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            Abono de Factura de Venta
            <small class="pull-right">{{date('Y-m-d')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-2">
          @if($sistema->show_logo1 == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1); ?>
          <img src="{{url('/storage/'.$filename)}}" style="width: 100px; max-width: 100px;">
          @endif
        </div>
        <div class="col-sm-8" style="text-align: center;">
          
            @if($sistema->show_razon_social)
            <h3><b>{{$sistema->razon_social}}</b><br></h3>
            @endif
            <address>
            @if($sistema->show_ruc)
            <b>RUC: {{$sistema->ruc}}</b><br>
            @endif
            @if($sistema->show_direccion)
            <b>Direccion:</b> {{$sistema->direccion}}<br>
            @endif
            @if($sistema->show_telefono1)
            <b>Telefono 1:</b> {{$sistema->telefono1}}<br>
            @endif
            @if($sistema->show_telefono2)
            <b>Telefono 2:</b> {{$sistema->telefono2}}<br>
            @endif
            @if($sistema->show_email1)
            <b>Email 1:</b> {{$sistema->email1}}<br>
            @endif
            @if($sistema->show_email2)
            <b>Email 2:</b> {{$sistema->email2}}
            @endif
          </address>
        </div>
        <div class="col-sm-2">
              @if($sistema->show_logo2 == true)
          <?php $filename = str_replace("public/",'',$sistema->logo2); ?>
          <img src="{{url('/storage/'.$filename)}}"  style="width:100px; max-width: 100px;">
          @endif
        </div>
        <div class="col-sm-12">
        <div class="col-sm-6">
          <h3>Cliente</h3>
          
          <address>
            <b>Razon social/Nombre:</b> {{$abono->facturaventas->cliente->razon_social}}<br>
            <b>Cedula/RUC:</b> {{$abono->facturaventas->cliente->cedula_ruc}}<br>
            <b>Direccion:</b> {{$abono->facturaventas->cliente->direccion}}<br>
            <b>Telefono:</b> {{$abono->facturaventas->cliente->telefono}}<br>
            
          </address>
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
        <div class="col-sm-6" style="text-align: right;">
          @if(isset($abono->codigo))
          <input type="hidden" name="codigo" value="{{$abono->codigo}}">
          <strong>Codigo # </strong>
          <span style="font-size: 30px; color: red">
          {{$abono->codigo}}</span><br>
          @endif
          <br>
          <b>Factura #:</b> {{$abono->facturaventas->id}}<br>
          
          @if(isset($abono->facturaventas->modo_pago->nombre))
          <b>Modo de pago:</b> {{$abono->facturaventas->modo_pago->nombre}}<br>
          <input type="hidden" name="modo_pago_id" value="{{$abono->facturaventas->modo_pago_id}}">
          @endif
        </div>
      </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Factura</th>
              <th>Codigo</th>
              <th>Fecha</th>
              <th>Monto</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>{{$abono->facturaventas->id}}</td>
              <td>{{$abono->codigo}}</td>
              <td>{{$abono->fecha}}</td>
              <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$abono->monto)}}</td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- /.col -->
        <div class="col-xs-6 pull-right">
          <div class="table-responsive">
            <table class="table">
              <tbody>
                @php
                  $monto = (double) $abono->monto;
                  $abonado = (double)$abono->facturaventas->abonos->sum('monto');
                  $total_factura = (double)$abono->facturaventas->total;
                  
                  $total_restante = $total_factura - $abonado;
                @endphp

                <tr>
                  <td>Total Factura:</td>
                  <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$total_factura)}}</td>
                </tr>
                <tr>
                  <td>Total Abonado:</td>
                  <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$abonado)}}</td>
                </tr>
                <tr>
                  <td><h3>Total Restante:</h3></td>
                  <td><h3>{{$sistema->currency->symbol}} {{sprintf("%.2f",$total_restante)}}</h3></td>
                </tr>
            </tbody>
          </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<div class="row page-footer">
@if($sistema->show_facebook == true)
<div class="col-sm-6">
  <label>
<i class="fa fa-facebook" style="width: 20px; max-width: 20px;"></i>
{{$sistema->facebook}}
</label>
</div>
@endif
@if($sistema->show_twiter == true)
<div class="col-sm-6">
  <label>
<i class="fa fa-twiter" style="width: 20px; max-width: 20px;"></i>
{{$sistema->twiter}}
</label>
</div>
@endif
@if($sistema->show_web == true)
<div class="col-sm-6">
 <label> 
<i class="fa fa-globe" style="width: 20px; max-width: 20px;"></i>
{{$sistema->web}}
</label>
</div>          
@endif
</div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="{{url('/imprimir_abono',$abono->id)}}" target="_new" class="btn btn-default" title="imprimir abono"><i class="fa fa-print"></i> Imprimir</a>
          
          
          <a onclick="window.history.back()" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-arrow-left"></i>Volver
          </a>
        
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
    <input type="hidden" id="_token" name="_token" value="{!! csrf_token() !!}">
  </div>
        
  </div>


@endsection
