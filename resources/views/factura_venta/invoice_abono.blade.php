@php
use App\Sistema;

$sistema = Sistema::first();
@endphp

<!-- Content Wrapper. Contains page content -->
  
    <!-- Main content -->
    <div class="invoice">
      
      <!-- info row -->
      <div class="row invoice-info">
        
        <div class="col-sm-2">
              @if($sistema->show_logo2 == true)
          @php 

          $filename = str_replace("public/",'',$sistema->logo2); 

          @endphp
          <img src="{{url('/storage/'.$filename)}}"  style="width:100px; max-width: 100px;">
          @endif
        </div>
        <div class="col-sm-12">
        <div class="col-sm-6">
          <h3>Cliente</h3>
          
          <address>
            <b>Razon social/Nombre:</b> {{$abono->facturaventas->cliente->razon_social}}<br>
            <b>Cedula/RUC:</b> {{$abono->facturaventas->cliente->cedula_ruc}}<br>
            <b>Direccion:</b> {{$abono->facturaventas->cliente->direccion}}<br>
            <b>Telefono:</b> {{$abono->facturaventas->cliente->telefono}}<br>
            
          </address>
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
        <div class="col-sm-6" style="text-align: right;">
          
          <input type="hidden" name="codigo" value="{{$abono->facturaventas->id}}">
          <strong>Codigo </strong>
          <span style="font-size: 30px; color: red">
          {{$abono->codigo}}</span><br>
          
          <br>
          @if(isset($abono->facturaventas->id))
          <b>Factura #</b> 00{{$abono->facturaventas->id}}<br>
          <input type="hidden" name="factura_id" value="{{$abono->facturaventas->id}}">
          @endif
          @if(isset($abono->facturaventas->n_orden))
          <b>Orden ID:</b> {{$abono->facturaventas->n_orden}}<br>
          <input type="hidden" name="orden_id" value="{{$abono->facturaventas->n_orden}}">
          @endif
          @if(isset($abono->facturaventas->modo_pago->nombre))
          <b>Modo de pago:</b> {{$abono->facturaventas->modo_pago->nombre}}<br>
          <input type="hidden" name="modo_pago_id" value="{{$abono->facturaventas->modo_pago_id}}">
          @endif
        </div>
      </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row table-responsive col-lg-12">
          <table class="table responsive">
            <thead>
            <tr>
              <th>Fecha</th>
              <th>Monto</th>              
            </tr>
            </thead>
            <tbody>
              @php
                $monto = (double) $abono->monto;
                $abonado = (double)$abono->facturaventas->abonos->sum('monto');
                $total_factura = (double)$abono->facturaventas->total;
                
                $total_restante = $total_factura - $abonado;
              @endphp
            <tr>
              <td>{{$abono->fecha}}</td>
              <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$monto)}}</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
            </tr>
            <tr>
                <td>Total Factura:</td>
                <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$total_factura)}}</td>  
              </tr>
              <tr>
                <td>Total abonado:</td>
                <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$abonado)}}</td>
              </tr>
              <tr>
                <td><h3>Total Restante:</h3></td>
                <td><h3>{{$sistema->currency->symbol}} {{sprintf("%.2f",$total_restante)}}</h3></td>
              </tr>
            </tbody>
          </table>
      </div>
      <!-- /.row -->
<div class="row">
@if($sistema->show_facebook == true)
<div class="col-sm-6">
  <label>
<i class="fa fa-facebook" style="width: 20px; max-width: 20px;"></i>
{{$sistema->facebook}}
</label>
</div>
@endif
@if($sistema->show_twiter == true)
<div class="col-sm-6">
  <label>
<i class="fa fa-twiter" style="width: 20px; max-width: 20px;"></i>
{{$sistema->twiter}}
</label>
</div>
@endif
@if($sistema->show_web == true)
<div class="col-sm-6">
 <label> 
<i class="fa fa-globe" style="width: 20px; max-width: 20px;"></i>
{{$sistema->web}}
</label>
</div>          
@endif
</div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          
          <a href="{{url('/imprimir_abono',$abono->id)}}" target="_new" class="btn btn-default" title="imprimir abono"><i class="fa fa-print"></i> Imprimir</a>
          
        </div>
      </div>
    </div>
