@extends('master')
@section('title', 'Facturacion')
@section('active-venta', 'active')

@section('active-venta-factura', 'active')

@section('content')
<!-- Content Wrapper. Contains page content -->
@php
use App\Producto;

use App\Iva;
use App\Sistema;

$sistema = Sistema::first();

@endphp
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Registro de Ventas
            <small>Secci&oacute;n para el registro de Ventas</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Ventas</a></li>
            <li><a href="{{url('facturasventas')}}">Facturas</a></li>
            <li class="active">Nueva Factura</li>
          </ol>
        </section>

         <div class="pad margin no-print">
          <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info-circle"></i> Nota:</h4>
            Aqu&iacute; podr&aacute; generar sus facturas de venta, seleccione el cliente, los productos, el precio de cada producto, su impuesto y cantidad y luego presione el boton verde con el simbolo + para agregar el producto a la lista. Presione ENTER al culminar de editar cada campo, automaticamente se saltará al siguiente. Haga click en CONTINUAR para finalizar la venta.
          </div>
        </div>

  <form method="POST" id="form-venta">
        <!-- Main content -->
    <section class="content">
      <!-- title row -->
      <div class="box container">
        <div class="box-header">
          <h4 class="page-header">
            <i class="fa fa-dollar"></i> Facturaci&oacute;n de Venta
            <span class="pull-right">Fecha: {{$fecha}}</span>
          </h4>
        </div>
        
      <!-- info row -->
      <div >
        <div class="col-sm-6" style="float: left;">
        <b>Orden ID #</b>
         <?php
          if(old('orden_id') != null)
            $order_id = old('orden_id');
        
          ?>
          {{$order_id}}
        <input type="hidden" name="orden_id" value="{{$order_id}}">
        </div>
        
        <div class="col-sm-6">
        <input type="hidden" name="n_factura" value="">
        <b style="float: right;">N. Factura # <span style="font-size: 30px; color: red">
        </span></b>
        </div>
        <div class="col-sm-12"></div>
            <div class="form-group col-sm-5">
      <label><strong style="color:red; font-size: 20px;">*</strong>&nbsp;<strong>Cliente</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Cliente es a quien se le va a vender. Si es un cliente nuevo, puede agregarlo aqui mismo" style="cursor: pointer;"></i></label>
      <div class="input-group">
          <span class="btn btn-success input-group-addon" style="cursor: pointer;"  title="Agregar un nuevo cliente" data-toggle="modal" data-target="#modalCliente"><i class="fa fa-plus" ></i></span>
       <?php 
       
        $cliente = old('cliente');
       
       ?>
        <select name="cliente" id="cliente" class="form-control campo selectpicker" data-live-search="true" required="">
                                    <option value="" selected="">Escoja un cliente</option>
                                    @if(isset($clientes))
                                    @foreach($clientes as $s)
                                    <option value="{{$s->id}}"
                                      @if($cliente == $s->id)
                                        selected
                                        @endif
                                      >{{$s->cedula_ruc}}-{{$s->razon_social}}</option>
                                    @endforeach
                                    @endif
                                </select>
        
      </div>

</div>
 <div class="form-group col-sm-5">
      <label><strong style="color:red; font-size: 20px;">*</strong>&nbsp;<strong>Modo de pago</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Seleccione el modo de pago para esta factura. Las facturas con modo de pago credito pasaran directamente a cuentas por cobrar. Si desea agregar un nuevo modo de pago, puede hacerlo desde aqui" style="cursor: pointer;"></i></label>
      <div class="input-group">
        
          <span class="btn btn-success input-group-addon" style="cursor: pointer;"  title="Agregar un nuevo modo de pago" data-toggle="modal" data-target="#modalModoPago"><i class="fa fa-plus" ></i></span>
       <?php 
        
        $cliente = old('cliente');
        $modo_pago = old('modo_pago');
       
       ?>
        <select name="modo_pago" id="modo_pago" class="form-control campo selectpicker" data-live-search="true" required="">                                    
                                    @if(isset($modos_pago))
                                    @foreach($modos_pago as $t)
                                    <option value="{{$t->id}}"
                                      @if($modo_pago == $t->id)
                                        selected
                                        @endif
                                      >{{strtoupper($t->nombre)}}</option>
                                    @endforeach
                                    @endif
                                </select>
        
      </div>

</div>
 <div class="form-group">
  <label class="page-header col-sm-12 col-md-12 col-lg-12 control-label"><h4>Sección de Productos</h4></label>
                          <div class="col-sm-4">
                            <label>Producto&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Aqui se listan todos los productos con existencia en el inventario. Si no encuentra el que desea, debe verificar su existencia en inventario" style="cursor: pointer;"></i></label>
                            <div class="input-group">
          						<span class="btn btn-success input-group-addon" style="cursor: pointer;"  title="Agregar un nuevo producto" data-toggle="modal" data-target="#modalProducto"><i class="fa fa-plus" ></i></span>
                              <select name="productos" class="form-control campo selectpicker" id="productos" data-live-search="true">
                                @if(isset($productos))
                                @foreach($productos as $s)
                                <option value="{{$s->id}}" id="prod_{{$s->id}}" medicion="{{$s->medicion->simbolo}}" codigo="{{$s->codigo}}" title="{{$s->nombre}}" costo-prod="{{$s->costo_produccion}}" precioventa="{{$s->precio_venta}}" iva-val="{{isset($s->iva) ? $s->iva->valor : null}}" iva-id="{{isset($s->iva) ? $s->iva->id : null}}">{{$s->codigo}} {{strtoupper($s->nombre)}} {{$s->medicion->nombre}}</option>
                              @endforeach
                              @endif
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-2">
                              <label>Precio&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="El precio del producto seleccionado" style="cursor: pointer;"></i></label>
                              <div class="input-group">
                            	<input  name="precio_producto" id="precio_producto" value="0" type="number" min="0"   maxlength="10" class="form-control" placeholder="precio" >  
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <label>Impuesto&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Este producto puede pagar un IVA propio. Si no existe en esta lista el IVA que desea, debe verificar su existencia en el modulo Configuracion/Iva y luego asignarlo al producto deseado en el modulo Configuracion/Productos. El valor N/A indica que el producto no tiene IVA asociado" style="cursor: pointer;"></i></label>
          
          
                          <select readonly="" name="impuesto_producto" id="impuesto_producto" class="form-control campo" required="" >
                          </select>
          
                            </div>
                            <div class="col-sm-2">
                              <label>Cantidad&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="La cantidad del producto indicado en esta linea" style="cursor: pointer;"></i></label>
                              <div class="input-group">
                              <span class="input-group-addon" style="cursor: pointer;" data-toggle="tooltip" ><i class="fa fa-calculator" ></i></span>
                            <input  name="cantidad" id="cantidad" value="0" type="number" min="0" max="100"  maxlength="10" class="form-control campo" placeholder="cantidad">  
                            </div>
                          </div>
                            <div class="col-sm-1">
                              <label>&nbsp;</label>
                                
                            <button class="btn btn-success form-control btn_finish" id="btn_agregar_producto" title="Agregar a la lista" style="cursor: pointer;"><i class="fa fa-plus"></i></button>
                            </div>
                      

</div>
        
    
<div class="form-group col-sm-12"></div>
<div class="form-group col-sm-12"></div>
<div class="form-group col-sm-12"></div>    
    <div class="col-sm-12">    
      <!-- Table row -->
        <div class="table-responsive">
          
            <table class="table table-striped">
              <thead>
              <tr>
                <th>#</th>
                <th>Codigo</th>
                <th style="width: 30%;">Producto</th>
                <th style="width: 10%;">Precio Unitario</th>
                <th style="width: 10%;">Cantidad</th>
                <th style="width: 10%;">Sub Total</th>
                <th style="width: 20%;">Impuesto</th>
                <th style="width: 10%;">Total</th>
                <th style="width: 5%;">Opciones</th>
              </tr>
              </thead>
              <!--DATOS PARA MOSTRAR-->
          
              <tbody id="tabla_datos">
                @php

                  $listaproductos = [];
                  $listacantidades = [];
                  $listaprecios = [];
                  $listaimpuestos = [];
                  $listaimpuestosval = [];

                  if(old('listaproductos') != null)
                    $listaproductos = old('listaproductos');

                  if(old('listacantidades') != null)
                    $listacantidades = old('listacantidades');

                  if(old('listaprecios') !== null)  
                    $listaprecios = old('listaprecios');

                  if(old('listaimpuestos') != null)  
                    $listaimpuestos = old('listaimpuestos');

                  if(old('listaimpuestosval') != null)  
                    $listaimpuestosval = old('listaimpuestosval');

                  $contador = count($listaproductos);
                  $subtotal = 0;
                  $total_iva = 0;
                  $total = 0;
                  $subtotal_general = 0;
                  $impuesto_general = 0;
                  $total_general = 0;
                @endphp
                @if($listaproductos != null and !empty($listaproductos))
                  @foreach($listaproductos as $key => $value)
                     @php
                        $producto = Producto::find($value);
                        $iva = Iva::find($listaimpuestos[$key]);

    
                            $subtotal = $listaprecios[$key]*$listacantidades[$key];

                            if($iva != null){
                              $iva_valor = $iva->valor;
                              $iva_id = $iva->id;
                              $total_iva  = ($subtotal * $iva->valor) / 100;

                            }else{
                              $total_iva = 0;
                              $iva_valor = 0;
                              $iva_id = 0;
                            }

                            $total = $total_iva + $subtotal;

                            $subtotal_general += $subtotal;
                            $impuesto_general += $total_iva;
                            $total_general += $total;

                          @endphp
                       <tr  id="producto_{{$value}}_{{$key}}" subtotal="{{$subtotal}}" total_impuesto="{{$total_iva}}">
                          <td>{{$key+1}}</td>
                          <td>{{$producto->codigo}}</td>
                          <td>{{$producto->nombre}}</td>
                          <td id="prec_{{$value}}">{{$sistema->currency->symbol}} {{sprintf("%.2f",round($listaprecios[$key],2))}}</td>
                          <td id="cant_{{$value}}">{{sprintf("%.2f",round($listacantidades[$key],2))}} {{$producto->medicion->simbolo}}</td>
                          <td id="subt_{{$value}}">{{$sistema->currency->symbol}} {{sprintf("%.2f",round($subtotal,2))}}</td>
                          @if($iva != null)
                            <td id="imp_{{$value}}">({{ sprintf(round($iva_valor,2)) }}%) {{$sistema->currency->symbol}} {{sprintf("%.2f",round($total_iva,2))}}</td>
                          @else
                            <td id="imp_{{$value}}">N/A</td>
                          @endif
                          <td id="tot_{{$value}}">{{$sistema->currency->symbol}} {{sprintf("%.2f",round($total,2))}}</td>
                          <td><a  href="javascript:eliminarProducto({{$producto->id}},{{$key}})" role="button" title="Eliminar" id="del_{{$producto->id}}" class="btn-danger btn-sm"><i class=" glyphicon-minus"></i></a></td>
                          <input type="hidden" id="input_producto_{{$value}}" name="listaproductos[]" value="{{$producto->id}}">
                          <input type="hidden" id="input_precio_{{$value}}" name="listaprecios[]" value="{{$listaprecios[$key]}}">
                          <input type="hidden" id="input_cantidad_{{$value}}" name="listacantidades[]" value="{{$listacantidades[$key]}}">
                          <input type="hidden" id="input_impuesto_{{$value}}" name="listaimpuestos[]" value="{{$iva_id}}">
                          <input type="hidden" id="impuestos_val_{{$value}}" name="listaimpuestosval[]" value="{{$iva_valor}}">
                        </tr>
                        
                  @endforeach
                @endif
              </tbody>

            </table>
          
          <!-- /.col -->
        </div>
    </div>
        <div class="form-group col-sm-12"></div>
        <strong>

      <div class="text-right">
        <div class="col-sm-4">Total Cantidad de Productos</div>
        <div class="col-sm-8" id="cant_productos">{{$contador}}</div>
        <br/>
        <div class="col-sm-4">Subtotal</div>
        <div class="col-sm-8" id="subtotal">{{$sistema->currency->symbol}} {{sprintf("%.2f",round($subtotal_general,2))}}</div>
        <br/>
        <div class="col-sm-4">Total Impuestos</div>
        <div class="col-sm-8" id="total_iva">{{$sistema->currency->symbol}} {{sprintf("%.2f",round($impuesto_general,2))}}</div>
        <br/>
        <h2><div class="col-sm-4">TOTAL</div>
        <div class="col-sm-8" id="total">{{$sistema->currency->symbol}} {{sprintf("%.2f",round($total_general,2))}}</div></h2>
      </div>
      </strong>
      <!-- /.row -->
      <!-- this row will not appear when printing -->
      
        <div class="box-footer col-sm-12">
          <div class="form-group">
          <button id="btn_finaliza_venta"  class="btn btn-success" style="float: right;"><i class="fa fa-arrow-right"></i> Continuar</button>
        </div>
        
      </div>
    </div>
    </section>
          <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
          <input type="hidden" value="{{$order_id}}" name="orden_id">
  </form>
    <!-- /.content -->

        
  </div>
  
  <?php echo view('modales.modal-cliente',compact('socios'))->render(); ?>
  <?php echo view('modales.modal-producto-ventas',compact('mediciones', 'categorias', 'impuestos'))->render(); ?>
  <?php // echo view('modales.modal-socio',compact('socios'))->render(); ?>
  <?php //echo view('modales.modal-impuesto')->render(); ?>
<?php echo view('modales.modal-categoria')->render(); ?>
<?php echo view('modales.modal-subcategoria', compact('categorias'))->render(); ?>
<?php //echo view('modales.modal-medicion')->render(); ?>
<?php echo view('modales.modal-modo_pago')->render(); ?>
  <!-- /.content-wrapper -->
<script type="text/javascript">
  cant_disponible = 0;
  productos = [];
  precios = [];
  cantidades = [];
  impuestos = [];
  impuestosval = [];
  subtotal_general = 0;
  impuesto_general = 0;
  total_general = 0;
  contador = 0;
  $(document).ready(function(){
    
    $('#cliente').focus();

    $('#btn_agregar_producto').click(function(e){
        e.preventDefault();
        verificarExistencia();
        
    });
    $('#btn_finaliza_venta').click(function(e){
        e.preventDefault();
        finalizarVenta();
    });
    $('#productos').on('change',function(){
    //TODO: cambiar el nombre en el modal de precios
    element_producto = $("#productos option[value="+$('#productos').val()+"]");

    $('#nombre_producto_precio').html(element_producto.attr('title'));
      //TODO: cargar  los precios de venta
      precioventa = element_producto.attr('precioventa');
      $('#precio_producto').val(parseFloat(Number(precioventa).toFixed(2))); 
      
      var iva_id = element_producto.attr('iva-id');
    if(iva_id == null || iva_id == "null" || iva_id == ""){
      iva_title = "N/A";
      iva_id = null;
      iva_val = null;
    }else{
      iva_val = element_producto.attr('iva-val');
      iva_id = element_producto.attr('iva-id');
      iva_title = iva_val+"%";
    }

    $('#impuesto_producto').html("<option value='"+iva_id+"'> "+iva_title+"</option>");

  });
    //cargamos valores en caso de haber valores existentes en la sesion
    @if($listaproductos != null)
      productos = @json($listaproductos);
      cantidades = @json($listacantidades);
      precios = @json($listaprecios);
      impuestos = @json($listaimpuestos);
      impuestosval = @json($listaimpuestosval);
      /*
      for(i=0;i<productos.length;i++){
         //llenar los arrays JS, y las variables
         temp = 0;   
         subtotal_general += parseFloat(cantidades[i]) * parseFloat(precios[i]);
         impuesto = parseFloat(impuestosval[i]);
         temp =  ((parseFloat(cantidades[i]) * parseFloat(precios[i])*impuesto)/100);
         impuesto_general += temp;
         contador++;
      }*/
      contador = productos.length;
      impuesto_general = {{$impuesto_general}};
      total_general = {{$total_general}};
      subtotal_general = {{$subtotal_general}};
      total_general = subtotal_general + impuesto_general;
    @endif
  
  $("#productos").trigger('change');
  });

    $(document).keypress(function(e) {
    if(e.which == 13) { // al presionar de enter, simulamos la presion de tabulacion para ir al siguiente focus
      //comprobamos si no estamos en la fila de productos, si estamos, verificamos si estamos en el boton agregar, si estamos en el boton agregar, se regresa el focus al imput producto
      e.preventDefault();
      element = $( document.activeElement );
      
      if(!element.hasClass('btn_finish')){
      
      $('input, select, textarea,button')
         [$('input,select,textarea,button').index(element)+1].focus();
      newFocused = $( document.activeElement );   
      newFocused.select();
      }
      else{
      
        element.click();
      }
    }
});
  

</script>

    <script type="text/javascript">
  function cargarPrecioProducto(){
    if(!$('#productos').length){
      alert("Error: No hay productos en INVENTARIO. Para agregar productos al inventario, debe crear facturas de compra");
      return;
    }
    _token = $("#_token").val();
      $.ajax({
            url: "{{url('cargar-precios')}}",
            data: {
              _token : _token,
              producto : $('#productos').val()
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                
                //agregamos el html
                $('#precio_producto').val(parseFloat(Number(data.value).toFixed(2))); 
                //$('#precio_producto').html(data.options);
                
                //cerramos la ventana
                $('#close_modal_precio').click();//cerramos la ventana
                $('#precio_producto').focus();
              }else if(data.resp == "no"){
                $('#precio_producto').val(0); //just testing
                alert("Informacion: Este producto NO tiene precios asociados. Por favor, registre precios para este producto.");
                $('#close_modal_precio').click();//cerramos la ventana
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
    
  }
  
  function agregarProducto(){
    producto = $('#productos');
    cantidad = $('#cantidad');
    precio = parseFloat($('#precio_producto').val());
    //verifico que la cantidad sea valida
    if(producto.val() <= 0 || cantidad.val() <= 0 || precio <= 0){
      alert('Verifique los datos del producto, la cantidad o el precio');
      return;
    }
    
    var id_producto = $('#productos').val(); 
    var cant = parseFloat(cantidad.val());
    //verificamos si el producto ya se encuentra agregado a la lista, buscandolo en el array productos
    if(productos.length > 0)
      index = $.inArray(id_producto.toString(),productos,0);
    else
      index = -1;


    var option = $("#productos option[value="+id_producto+"]");
    titulo = option.attr('title');
    codigo = option.attr('codigo');
    simbolo_medicion = option.attr('medicion');
    simbolo_moneda = '{{$sistema->currency->symbol}}';
    impuesto = 0;
    impuesto_id = 0;
    impuesto_titulo = 'N/A';
    impuesto_producto = $('#impuesto_producto').val();
    element_producto = $("#productos option[value="+producto.val()+"]");

    if(impuesto_producto == null || impuesto_producto == "null" || impuesto_producto == ""){
      impuesto_id = null;
      impuesto_titulo = "N/A";
      impuesto = 0;
    }else{
      impuesto_id = $('#impuesto_producto').val();
      impuesto_titulo = element_producto.attr('iva-val')+"%";
      impuesto = parseFloat(element_producto.attr('iva-val'));
    }
    subtotal = parseFloat(cant * precio);
    total_impuesto = parseFloat((subtotal * impuesto)/100);
    
    
    if(index >= 0){
        
        
        if((parseFloat(cantidades[index]) + cant) > cant_disponible){
          alert("La cantidad total de este producto supera la cantidad de existencia en inventario. Por favor ingrese una cantidad menor o igual a "+cant_disponible);
          return;
        }
        
        producto = $('#producto_' + id_producto + '_' + index);
        
        prec_element = $('#prec_' + id_producto);  
        prec_element.html(simbolo_moneda + ' ' + precio.toFixed(2));
        precios[index] = precio;
        //se encuentra en el array, asi que solo modificaremos la cantidad, el subtotal, el impuesto y el total
        cant_element = $('#cant_' + id_producto);
        temp_cant = parseFloat(cantidades[index])+cant;// obtenemos la cantidad nueva
        cant_element.html(temp_cant.toFixed(2) + ' ' + simbolo_medicion);
        cantidades[index] = temp_cant; //modificamos la cantidad en el arreglo de cantidades. los demas no se modifican porque solo contienen 
        subt_element = $('#subt_' + id_producto);
        temp_subt = parseFloat(cantidades[index]) * parseFloat(precios[index]); //obtenemos el subtotal
        subt_element.html(simbolo_moneda + ' ' + temp_subt.toFixed(2));
        
        imp_element = $('#imp_' + id_producto);
        temp_imp = impuesto * parseFloat(cantidades[index]) * parseFloat(precios[index]) / 100; //obtenemos el impuesto
        imp_element.attr('value',temp_imp); //modificamos el impuesto
        impuestosval[index] = impuesto;
        impuestos[index] = impuesto_id;
        imp_element.html(impuesto_titulo + ' ' + simbolo_moneda + ' ' + temp_imp.toFixed(2));

        if(impuesto_id != null && impuesto_id != "null"){
          imp_element.html(impuesto_titulo + ' ' + simbolo_moneda + ' ' + temp_imp.toFixed(2));
        }else{
          imp_element.html('N/A');
        }

        tot_element = $('#tot_' + id_producto);
        temp_tot = (temp_imp + temp_subt);
        tot_element.attr('value',temp_tot);
        tot_element.html(simbolo_moneda + ' ' + temp_tot.toFixed(2));

        $("#input_precio_"+id_producto).val(precio);
        $("#input_cantidad_"+id_producto).val(temp_cant);
        $("#input_impuesto_"+id_producto).val(impuesto_id);
        $("#impuestos_val_"+id_producto).val(impuesto);
    }else{
    
        //se encuentra en el array, asi que solo modificaremos la cantidad, el subtotal, el impuesto y el total
    if(cant > cant_disponible){
          alert("La cantidad total de este producto supera la cantidad de existencia en inventario. Por favor ingrese una cantidad menor o igual a "+cant_disponible);
          return;
        }
    impuestosval.push(impuesto);    
    impuestos.push(impuesto_id);
    precios.push(precio);//lo agregamos a la lista de items seleccionados (guarda los id de cada item seleccionado)
    productos.push(id_producto);//lo agregamos a la lista de items seleccionados (guarda los id de cada item seleccionado)
    cantidades.push(cant);

    /*html = '<div class="row" id="producto_'+id_producto+'_'+contador+'"><div class="col-sm-10 col-md-8 col-lg-8">'+titulo+'</div><div class="col-sm-2 col-md-2 col-lg-2">'+cantidad.val()+'</div><div class="col-sm-2 col-md-2 col-lg-2"><a class="btn btn-danger" id="del_'+id_producto+'" onclick="eliminarProducto('+id_producto+','+contador+');">-</a>'+
      '<input type="hidden" value="'+id_producto+'" name="listaproductos[]" title="'+titulo+'" id="sum_'+id_producto+'_'+contador+'">'+ 
      '<input type="hidden" value="'+cantidad.val()+'" name="listacantidades[]" id="cant_'+id_producto+'_'+contador+'"  >'+ 
      '</div></div>';//preparamos el html
*/

var html = '<tr  id="producto_'+id_producto+'_'+contador+'" >'; // variable que contendra el html
                          html += '<td>'+(contador+1)+'</td>';
                          html += '<td>'+codigo+'</td>';
                          html += '<td>'+titulo+'</td>';
                          html += '<td id="prec_'+id_producto+'" >'+ simbolo_moneda + ' ' + Number(precio).toFixed(2)+'</td>';
                          html += '<td id="cant_'+id_producto+'" >'+cant+' '+simbolo_medicion+'</td>';
                          html += '<td id="subt_'+id_producto+'" >'+simbolo_moneda + ' ' + Number(subtotal).toFixed(2)+'</td>';
                          
                          if(impuesto_id != null && impuesto_id != "null"){
                            html += '<td id="imp_'+id_producto+'" >('+impuesto_titulo+') '+simbolo_moneda + ' ' + Number(total_impuesto).toFixed(2)+'</td>';
                          }else{
                            html += '<td id="imp_'+id_producto+'" >N/A</td>';
                          }

                          html += '<td id="tot_'+id_producto+'" >'+simbolo_moneda + ' ' + Number(subtotal+total_impuesto).toFixed(2)+'</td>';

                          html += '<td><a  href="javascript:eliminarProducto('+id_producto+','+contador+')" role="button" title="Eliminar" id="del_'+id_producto+'" class="btn-danger btn-sm"><i class=" glyphicon-minus"></i></button></td>';
                          html += '<input type="hidden" name="listaproductos[]" value="'+id_producto+'">';
                          html += '<input id="input_precio_'+id_producto+'" type="hidden" name="listaprecios[]" value="'+precio+'">';
                          html += '<input id="input_cantidad_'+id_producto+'" type="hidden" name="listacantidades[]" value="'+cant+'">';
                          html += '<input id="input_impuesto_'+id_producto+'" type="hidden" name="listaimpuestos[]" value="'+impuesto_id+'">';
                          html += '<input id="impuestos_val_'+id_producto+'" type="hidden" name="listaimpuestosval[]" value="'+impuesto+'">';
                      html += '</tr>';
                    $('#tabla_datos').append(html);//funcion p

                    contador++;
        }
    //$('#lista_productos').prepend(html);//agregamos al contendedor html
    //reiniciamos el input de cantidad
    //$('#cantidad').val('');
    //$('#precio_producto').val('');
    //actualizamos los calculos generales
    subtotal_general = 0;
    for(var i=0;i<productos.length;i++){
      calc = parseFloat(cantidades[i]) * parseFloat(precios[i]);
      subtotal_general = subtotal_general + calc;
    }
    //subtotal_general += subtotal;
    impuesto_general = 0;
    for(var i=0;i<productos.length;i++){
      calc = (parseFloat(cantidades[i]) * parseFloat(precios[i]) * parseFloat(impuestosval[i]))/100;
      impuesto_general = impuesto_general + calc;
    }
    //impuesto_general += total_impuesto;
    total_general = (subtotal_general + impuesto_general); 

    //subtotal_general = parseFloat(subtotal_general.toFixed(2));
    //impuesto_general = parseFloat(impuesto_general.toFixed(2));
    //total_general = parseFloat(total_general.toFixed(2));

    $('#cant_productos').html(contador);
    $('#subtotal').html(simbolo_moneda + ' ' + subtotal_general.toFixed(2));
    $('#total_iva').html(simbolo_moneda + ' ' + impuesto_general.toFixed(2));
    $('#total').html(simbolo_moneda + ' ' + total_general.toFixed(2));
      
    $('#productos').focus();
  }

  function eliminarProducto(id,cont){
    //option = $('#prod_'+id+'_'+cont); 
    var index = $.inArray(id.toString(),productos,0);//obtenemos el indice del id del producto en el array de productos
    producto = $('#producto_'+id+'_'+cont);
    subtotal_producto = parseFloat(precios[index]) * parseFloat(cantidades[index]);
    
    total_impuesto_producto = subtotal_producto * parseFloat(impuestosval[index])/100;
    total_producto = (subtotal_producto + total_impuesto_producto);
    
    console.log("subtotal: "+subtotal_producto+" impuesto: "+total_impuesto_producto+" total: "+total_producto);  
    
    //subtotal_general = parseFloat(subtotal_general.toFixed(2));
    //impuesto_general = parseFloat(impuesto_general.toFixed(2));
    //total_general = parseFloat(total_general.toFixed(2));  

    subtotal_general -= subtotal_producto;
    impuesto_general -= total_impuesto_producto;
    total_general -= total_producto;
    --contador;  

    if(subtotal_general <= 0)
        subtotal_general = 0;
    if(impuesto_general <= 0)
        impuesto_general = 0;
    if(total_general <= 0)
        total_general = 0;
    if(contador <= 0){
      contador = 0;
      total_general = 0;
      impuesto_general = 0;
      subtotal_general = 0;
    }
    
    
    
    $('#cant_productos').html(contador);
    $('#subtotal').html('{{$sistema->currency->symbol}} ' + subtotal_general.toFixed(2));
    $('#total_iva').html('{{$sistema->currency->symbol}} ' + impuesto_general.toFixed(2));
    $('#total').html('{{$sistema->currency->symbol}} ' + total_general.toFixed(2));

     productos.splice(index,1);//remover 1 elemento en la posicion index del array suministros
     cantidades.splice(index,1);//remover 1 elemento en la posicion index del array cantidades
     precios.splice(index,1);//remover 1 elemento en la posicion index del array suministros
     impuestos.splice(index,1);//remover 1 elemento en la posicion index del array cantidades
     impuestosval.splice(index,1);//remover 1 elemento en la posicion index del array cantidades
     
     producto.remove();
  }

  function verificarExistencia(){
  
    $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url: "{{url('/verificar-existencia')}}",
            data: {
              id_producto : $("#productos").val()
            },
            dataType: 'JSON',
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                
                cant_disponible = parseFloat(data.value);
                agregarProducto();

              }else if(data.resp == "error"){
                alert("Error: "+data.msg);
                return -1;  
              }
              
            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
              return -1;
            }

            });
  }

   function finalizarVenta(){
    if(contador <= 0){
      alert("Necesita agregar al menos un producto para continuar.");
      $('#productos').focus();
      return;
    }
    if($('#cliente').val() == 0){
      alert("Necesita seleccionar ingresar un cliente para continuar.");
      $('#cliente').focus();
      return;
    }else{
      $('#form-venta').submit();
    }
   } 
</script>

@endsection
