<html>
<head>
  <link rel="stylesheet" href="{{ url('/css/Template/bootstrap.css') }}">

        <!--link rel="stylesheet" href="{{ url('/css/Template/ionicons.min.css') }}"-->

        <link rel="stylesheet" href="{{ url('/css/Template/AdminLTE.css') }}">

        <link rel="stylesheet" href="{{ url('/css/Template/_all-skins.css') }}">

        <link rel="stylesheet" href="{{ url('/css/Template/plugins/dataTables/datatables.min.css') }}">

        <link rel="stylesheet" href="{{ url('/font-awesome-4.7.0/css/font-awesome.css') }}">

        
        <title>Abonos de Factura de Venta # 00{{$factura->id}}</title>        

        
<style type="text/css">
  body{
    font-size: 10px; 
  }
  table{
    font-size: inherit;
  }
</style>

<style>
.webera {
    color: lightgrey !important;
    opacity: 0.5 !important;
    display: block;
    width: auto;
    font-size: 10rem !important;
    border: 3px !important;
    border-style: solid !important;
    z-index: 1 !important;

    top: 50% !important;
    left: 10% !important;
    text-align: center;
    vertical-align: middle;
    position: absolute !important;
}
.rotate{
    -webkit-transform: rotate(-45deg) !important; 
    -moz-transform: rotate(-45deg) !important;
    -o-transform: rotate(-45deg) !important;
    transform: rotate(-45deg) !important;
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=0);
    
}
.inline-block{
    display:-moz-inline-stack;
    display:inline-block;
    zoom:1;
    *display:inline; 
}
</style>
</head>
@php
use App\Sistema;
@endphp

@php
            $sistema = Sistema::first();
          @endphp
  <body onload="window.print();window.close();">
    <table width="100%"> 
          <tr>
            <td width="20%">
              @if($sistema->show_logo1 == true)
              <?php $filename = str_replace("public/",'',$sistema->logo1); ?>
              <img src="{{url('/storage/'.$filename)}}" style="width: 100px; max-width: 100px;margin-left: 20px;">
              @endif
            </td>
            <td style="text-align: center;">
          
            @if($sistema->show_razon_social)
            <h3><b>{{$sistema->razon_social}}</b><br></h3>
            @endif
            <address>
            <b>RUC: {{$sistema->ruc}}</b><br>
            @if($sistema->show_direccion)
            <b>Direccion:</b> {{$sistema->direccion}}<br>
            @endif
            @if($sistema->show_telefono1)
            <b>Telefono 1:</b> {{$sistema->telefono1}}<br>
            @endif
            @if($sistema->show_telefono2)
            <b>Telefono 2:</b> {{$sistema->telefono2}}<br>
            @endif
            @if($sistema->show_email1)
            <b>Email 1:</b> {{$sistema->email1}}<br>
            @endif
            @if($sistema->show_email2)
            <b>Email 2:</b> {{$sistema->email2}}
            @endif
          </address>
        </td>
        <td width="20%">
              @if($sistema->show_logo2 == true)
          <?php $filename = str_replace("public/",'',$sistema->logo2); ?>
          <img src="{{url('/storage/'.$filename)}}"  style="width:100px; max-width: 100px;">
          @endif
        </td>
        </tr>
      </table>
   
<?php echo view('factura_venta/invoice_abonos', compact('factura'))->render() ?>
  </body>
  </html>