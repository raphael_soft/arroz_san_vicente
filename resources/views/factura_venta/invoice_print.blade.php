<html>
<head>
  <link rel="stylesheet" href="{{ url('/css/Template/bootstrap.css') }}">

        <!--link rel="stylesheet" href="{{ url('/css/Template/ionicons.min.css') }}"-->

        <link rel="stylesheet" href="{{ url('/css/Template/AdminLTE.css') }}">

        <link rel="stylesheet" href="{{ url('/css/Template/_all-skins.css') }}">

        <link rel="stylesheet" href="{{ url('/css/Template/plugins/dataTables/datatables.min.css') }}">

        <link rel="stylesheet" href="{{ url('/font-awesome-4.7.0/css/font-awesome.css') }}">

        
        <title>Factura de Venta # {{$factura->id}}</title>        

        <script src="{{ url('/js/Template/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

        <script src="{{ url('/js/Template/plugins/jQuery/jquery-ui.min.js') }}"></script>


        <script src="{{ url('/js/Template/bootstrap.js') }}"></script>

        <script src="{{ url('/js/Template/global.js') }}"></script>

        <script src="{{ url('/js/Template/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

        <script src="{{ url('/js/Template/app.js') }}"></script>
        

        <script src="{{ url('/js/Template/plugins/dataTables/datatables.min.js') }}"></script>
<style type="text/css">
  body{
    font-size: 10px; 
  }
  table{
    font-size: inherit;
  }
</style>

<style>
.webera {
    color: lightgrey !important;
    opacity: 0.5 !important;
    display: block;
    width: auto;
    font-size: 10rem !important;
    border: 3px !important;
    border-style: solid !important;
    z-index: 1 !important;

    top: 50% !important;
    left: 10% !important;
    text-align: center;
    vertical-align: middle;
    position: absolute !important;
}
.rotate{
    -webkit-transform: rotate(-45deg) !important; 
    -moz-transform: rotate(-45deg) !important;
    -o-transform: rotate(-45deg) !important;
    transform: rotate(-45deg) !important;
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=0);
    
}
.inline-block{
    display:-moz-inline-stack;
    display:inline-block;
    zoom:1;
    *display:inline; 
}
</style>
</head>
@php
use App\Sistema;
@endphp

@php
            $sistema = Sistema::first();
          @endphp

    <?php
      if($factura->status == 2){
        $a = 'success';
        $estado = "APROBADA";
      }elseif ($factura->status == 0){
        $a = 'danger';
        $estado = "CANCELADA";
      }elseif($factura->status == 1){
        $a = 'info';
        $estado = "PENDIENTE";
      }
      ?>
    <body onload="window.print();window.close();">
      
      <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  


    <!-- Main content -->
    <section class="invoice" >
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            Factura de Venta | Orden ID: {{$factura->n_orden}}
            <small class="pull-right">{{date('Y-m-d')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        
          <table width="100%"> 
          <tr>
            <td width="20%">
              @if($sistema->show_logo1 == true)
              <?php $filename = str_replace("public/",'',$sistema->logo1); ?>
              <img src="{{url('/storage/'.$filename)}}" style="width: 100px; max-width: 100px;margin-left: 20px;">
              @endif
            </td>
            <td style="text-align: center;">
          
            @if($sistema->show_razon_social)
            <h3><b>{{$sistema->razon_social}}</b><br></h3>
            @endif
            <address>
            <b>RUC: {{$sistema->ruc}}</b><br>
            @if($sistema->show_direccion)
            <b>Direccion:</b> {{$sistema->direccion}}<br>
            @endif
            @if($sistema->show_telefono1)
            <b>Telefono 1:</b> {{$sistema->telefono1}}<br>
            @endif
            @if($sistema->show_telefono2)
            <b>Telefono 2:</b> {{$sistema->telefono2}}<br>
            @endif
            @if($sistema->show_email1)
            <b>Email 1:</b> {{$sistema->email1}}<br>
            @endif
            @if($sistema->show_email2)
            <b>Email 2:</b> {{$sistema->email2}}
            @endif
          </address>
        </td>
        <td width="20%">
              @if($sistema->show_logo2 == true)
          <?php $filename = str_replace("public/",'',$sistema->logo2); ?>
          <img src="{{url('/storage/'.$filename)}}"  style="width:100px; max-width: 100px;">
          @endif
        </td>
        </tr>
      </table>
        
        
        <div class="col-sm-2">
              @if($sistema->show_logo2 == true)
          <?php $filename = str_replace("public/",'',$sistema->logo2); ?>
          <img src="{{url('/storage/'.$filename)}}"  style="width:100px; max-width: 100px;">
          @endif
        </div>
        
        
        <div class="col-sm-12">
        <div class="col-sm-6">
          <h3>Cliente</h3>
          
          <address>
            <b>Razon social/Nombre:</b> {{$factura->cliente->razon_social}}<br>
            <b>Cedula/RUC:</b> {{$factura->cliente->cedula_ruc}}<br>
            <b>Direccion:</b> {{$factura->cliente->direccion}}<br>
            <b>Telefono:</b> {{$factura->cliente->telefono}}<br>
            
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-6" style="text-align: right;">
          @if(isset($factura->id))
          <input type="hidden" name="codigo" value="{{$factura->id}}">
          <strong>FACTURA DE VENTA # </strong>
          <span style="font-size: 30px; color: red">
        {{$factura->id}}</span><br>
          @endif
          <br>
          @if(isset($factura->n_orden))
          <b>Orden ID:</b> {{$factura->n_orden}}<br>
          <input type="hidden" name="orden_id" value="{{$factura->n_orden}}">
          @endif
          @if(isset($factura->modo_pago->nombre))
          <b>Modo de pago:</b> {{$factura->modo_pago->nombre}}<br>
          <input type="hidden" name="modo_pago_id" value="{{$factura->modo_pago_id}}">
          @endif
        </div>
        <!-- /.col -->
        
      </div>
    </div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table compact">
            <thead>
            <tr>
              <th>Cant</th>
              <th>Codigo</th>
              <th>Producto</th>
              <th>Precio Unit</th>
              <!--th>Subtotal</th>
              <th>Impuesto</th-->
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
              @php
              $subtotal_noiva = 0;
              @endphp
  @if(isset($productosFactura) and !empty($productosFactura))            
      @foreach($productosFactura as $key => $value)
         @php
            
            $subtotal = ($value->precio * $value->cantidad);
            if(isset($value->impuesto)){
              $impuesto_valor = $value->impuesto->valor;
              $impuesto = ($value->impuesto->valor * $subtotal) / 100;
            }else{
              $impuesto_valor = 0;
              $impuesto = 0;
              $subtotal_noiva += $subtotal;
            }
            $total = $subtotal + $impuesto;
            //$subtotal_general += $subtotal;
            //$impuesto_general += $impuesto;
            //$total_general += $total;
            
         @endphp
            <tr>
              <td>{{sprintf("%.2f",$value->cantidad)}} {{$value->producto->medicion->simbolo}}</td>
              <td>{{$value->producto->codigo}}</td>
              <td>{{$value->producto->nombre}}</td>
              <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$value->precio)}} x {{$value->producto->medicion->simbolo}}</td>
              
              <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$subtotal)}}</td>
            </tr>
      @endforeach      
  @endif          
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
    
        <div class="col-xs-6 pull-right">
          <div class="table-responsive">
            <table class="table">
              <tbody>
                @php 
                  $impuestos_iva = array();
                  $subtotal_noiva = 0;
                @endphp
                <tr>
                <td>Subtotal sin IVA:</td>
                <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$factura->subtotal)}}</td>
              </tr>
                <!-- ciclo para calcular los montos subtotales por cada iva -->
                @foreach($impuestos as $imp)
              <tr>
                <td>IVA {{$imp->valor}} %</td>
                <td>{{$sistema->currency->symbol}} 
                  @php 
                  
                  $impuestos_iva[$imp->id] =  0;
                  $temp_suma_iva = 0;
                  $subtotal_iva0 = 0;
                  //dd($productosFactura);  
                  @endphp

                  @foreach($productosFactura as $key => $value)
                    @php

                    if(isset($value->impuesto) and $value->impuesto->id == $imp->id){
                        //echo($imp->id."->".$imp->valor);
                        if($imp->valor > 0)
                          $impuestos_iva[$imp->id] += ($value->cantidad * $value->precio) * $imp->valor/100;  
                        else
                          $subtotal_iva0 += ($value->cantidad * $value->precio);
                    }
                    @endphp  

                  @endforeach
                  {{sprintf("%.2f",$impuestos_iva[$imp->id])}}  
                </td>
              </tr>
              @endforeach
              
              <tr>
                <td><h3>Total:</h3></td>
                <td><h3>{{$sistema->currency->symbol}} {{sprintf("%.2f",$factura->total)}}</h3></td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<div class="row page-footer">
@if($sistema->show_facebook == true)
<div class="col-sm-12">
  <label>
<i class="fa fa-facebook" style="width: 20px; max-width: 20px;"></i>
{{$sistema->facebook}}
</label>
</div>
@endif
@if($sistema->show_twiter == true)
<div class="col-sm-12">
  <label>
<i class="fa fa-twiter" style="width: 20px; max-width: 20px;"></i>
{{$sistema->twiter}}
</label>
</div>
@endif
@if($sistema->show_web == true)
<div class="col-sm-12">
 <label> 
<i class="fa fa-globe" style="width: 20px; max-width: 20px;"></i>
{{$sistema->web}}
</label>
</div>          
@endif
</div>
      <!-- this row will not appear when printing -->
        <span class="webera rotate inline-block" style="color:">{{$estado}}</span> 
    </section>
  </div>
    <!-- /.content -->
    <div class="clearfix"></div>
    <input type="hidden" id="_token" name="_token" value="{!! csrf_token() !!}">
  </body>
  </html>