<html>
<head>
  <link rel="stylesheet" href="{{ url('/css/Template/bootstrap.css') }}">

        <!--link rel="stylesheet" href="{{ url('/css/Template/ionicons.min.css') }}"-->

        <link rel="stylesheet" href="{{ url('/css/Template/AdminLTE.css') }}">

        <link rel="stylesheet" href="{{ url('/css/Template/plugins/dataTables/datatables.min.css') }}">

        <link rel="stylesheet" href="{{ url('/font-awesome-4.7.0/css/font-awesome.css') }}">

        
        <title>Abono de Factura de Venta # {{$abono->codigo}}</title>        

        <script src="{{ url('/js/Template/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

        <script src="{{ url('/js/Template/plugins/jQuery/jquery-ui.min.js') }}"></script>


        <script src="{{ url('/js/Template/bootstrap.js') }}"></script>

        <script src="{{ url('/js/Template/global.js') }}"></script>

        <script src="{{ url('/js/Template/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

        <script src="{{ url('/js/Template/app.js') }}"></script>
        

        <script src="{{ url('/js/Template/plugins/dataTables/datatables.min.js') }}"></script>

<style type="text/css">
  body{
    font-size: 10px; 
  }
  table{
    font-size: inherit;
  }
</style>

<style>
.webera {
    color: lightgrey !important;
    opacity: 0.5 !important;
    display: block;
    width: auto;
    font-size: 10rem !important;
    border: 3px !important;
    border-style: solid !important;
    z-index: 1 !important;

    top: 50% !important;
    left: 10% !important;
    text-align: center;
    vertical-align: middle;
    position: absolute !important;
}
.rotate{
    -webkit-transform: rotate(-45deg) !important; 
    -moz-transform: rotate(-45deg) !important;
    -o-transform: rotate(-45deg) !important;
    transform: rotate(-45deg) !important;
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=0);
    
}
.inline-block{
    display:-moz-inline-stack;
    display:inline-block;
    zoom:1;
    *display:inline; 
}
</style>
</head>

  <body onload="window.print();window.close();">
    <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            Abono A Factura 
            <small class="pull-right">{{date('Y-m-d')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
<?php echo view('factura_venta/invoice_abono', compact('abono'))->render() ?>
  </body>
  </html>