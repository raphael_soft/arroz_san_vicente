@php
use App\Sistema;

$sistema = Sistema::first();
@endphp

<!-- Content Wrapper. Contains page content -->
  
    <!-- Main content -->
    <div>
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            Lista de Abonos a Factura 
            <small class="pull-right">{{date('Y-m-d')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-6">
          <h3>Cliente</h3>
          
          <address>
            <b>Razon social/Nombre:</b> {{$factura->cliente->razon_social}}<br>
            <b>Cedula/RUC:</b> {{$factura->cliente->cedula_ruc}}<br>
            <b>Direccion:</b> {{$factura->cliente->direccion}}<br>
            <b>Telefono:</b> {{$factura->cliente->telefono}}<br>
            
          </address>
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
        <div class="col-sm-6" style="text-align: right;">
          
          <input type="hidden" name="codigo" value="{{$factura->id}}">
          <strong>Factura # </strong>
          <span style="font-size: 30px; color: red">
          00{{$factura->id}}</span><br>
          
          <br>
          @if(isset($factura->n_orden))
          <b>Orden ID:</b> {{$factura->n_orden}}<br>
          <input type="hidden" name="orden_id" value="{{$factura->n_orden}}">
          @endif
          @if(isset($factura->modo_pago->nombre))
          <b>Modo de pago:</b> {{$factura->modo_pago->nombre}}<br>
          <input type="hidden" name="modo_pago_id" value="{{$factura->modo_pago_id}}">
          @endif
        </div>
      </div>
      <!-- /.row -->
            @php
              $total_factura = (double)$factura->total;
              $abonado = 0;
            @endphp
      <!-- Table row -->
      <div class="row">
          <table class="table responsive">
            <thead>
            <tr>
              <td>#</td>
              <th>Fecha</th>
              <th>Monto</th>              
            </tr>
            </thead>
            <tbody>
            @foreach($factura->abonos as $key => $abono)  
              @php
                  $monto = (double) $abono->monto;
                  $abonado += $monto;                  
              @endphp
            <tr>
              <td>{{($key + 1)}}</td>
              <td>{{$abono->fecha}}</td>
              <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$abono->monto)}}</td>
            </tr>
            @endforeach

            <tr>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
                <td></td>
                <td>Total Factura:</td>
                <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$total_factura)}}</td>  
              </tr>
              <tr>
                <td></td>
                <td>Total abonado:</td>
                <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$abonado)}}</td>
              </tr>
              <tr>
                <td></td>
                <td><h3>Total Restante:</h3></td>
                <td><h3>{{$sistema->currency->symbol}} {{sprintf("%.2f",($total_factura - $abonado))}}</h3></td>
              </tr>
            </tbody>
          </table>
      </div>
      <!-- /.row -->
<div class="row">
@if($sistema->show_facebook == true)
<div class="col-sm-6">
  <label>
<i class="fa fa-facebook" style="width: 20px; max-width: 20px;"></i>
{{$sistema->facebook}}
</label>
</div>
@endif
@if($sistema->show_twiter == true)
<div class="col-sm-6">
  <label>
<i class="fa fa-twiter" style="width: 20px; max-width: 20px;"></i>
{{$sistema->twiter}}
</label>
</div>
@endif
@if($sistema->show_web == true)
<div class="col-sm-6">
 <label> 
<i class="fa fa-globe" style="width: 20px; max-width: 20px;"></i>
{{$sistema->web}}
</label>
</div>          
@endif
</div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          
          <a href="{{url('/imprimir_abonos_facturaventa',$factura->id)}}" target="_new" class="btn btn-default" title="imprimir abonos"><i class="fa fa-print"></i> Imprimir</a>
          
        </div>
      </div>
    </div>
