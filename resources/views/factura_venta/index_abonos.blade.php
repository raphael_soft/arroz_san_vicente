@extends('master')

@section('title', 'Abonos a Ventas por Cobrar')

@section('active-venta', 'active')

@section('active-ventas-por-cobrar', 'active')

@section('active-ventas-abonos', 'active')

@section('content')

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Abonos a Facturas de Ventas por Cobrar

        </h1>

        <ol class="breadcrumb">

            <li class="active">

                <a href="{{ url('/"') }}">

                    <i class="fa fa-dashboard">

                    </i>

                    Inicio

                </a>

            </li>

        </ol>

    </section>

    <section class="content">

       <div class="box">

            <div class="box-header">

              <h3 class="box-title">Listado de Abonos a Facturas de Ventas por Cobrar</h3>             

                @if (session('status'))

                    <div class="alert alert-success">

                        {{session('status')}}

                    </div>

                @endif
              <!-- Split button -->
              @if(session('registrar'))
<a href="{{url('create_abono')}}"><button type="button" class="btn btn-primary" style="float: right;">
    <i class="fa fa-plus"></i> Abonar
  </button></a>
    @endif

            </div>

            <!-- /.box-header -->
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
            <div class="box-body">
              <div class="row">
                <form method="GET">
              <div class="col-sm-12"><label>Consultar por fechas</label></div>
                        <div class="form-group col-sm-3">
                               <label >Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="from" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label >Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="to" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
                      </form>
              </div>
              <table id="example1" class="table table-bordered table-striped table-responsive informe responsive">

                <thead>

                  <tr>
                    <th>Nº </th>
                    <th >Cedula/RUC</th>
                    <th >Cliente</th>
                    <th>Abonado</th>
                    <th>Fecha</th>
                    <th class="no-print">Opciones</th>

                  </tr>

                </thead>

                <tbody id="tabla_datos">



                @foreach($abonos as $abono)


                  <tr>

                    <td>{{$abono->facturaventas->id}}</td>

                    <td>{{$abono->facturaventas->cliente->cedula_ruc}}</td>

                    <td>{{$abono->facturaventas->cliente->razon_social}}</td>

                    <td id="abono{{$abono->id}}">{{$sistema->currency->symbol}} {{sprintf("%.2f",$abono->monto)}}</td>

                    <td>{{$abono->fecha}}</td>

                    <td>
                        @if(session('ver'))
                        <a href="{{url('show_abono',$abono->id)}}" data-toggle="tooltip" data-placement="top" title="Presione aqui para ver este abono" class="btn btn-primary"><i class="fa fa-eye"></i></a>

                        <a href="{{url('imprimir_abono',$abono->id)}}" target="_new" data-toggle="tooltip" data-placement="top" title="Presione aqui para imprimir este abono" class="btn btn-warning"><i class="fa fa-print"></i></a>

                        @endif
                        @if(session('borrar'))
                        @if($abono->status == 2)
                        <a onclick="anular('{{$abono->id}}','{{url('anular_abono',$abono->id)}}')"  data-toggle="tooltip" data-placement="top" title="Presione aqui para anular esta factura" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                        @endif
                        @endif
                    </td>

                  </tr>

                @endforeach

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->



          



    </section>

</div>
<?php echo view('modales.modal-abono')->render(); ?>

    <script>

        $(document).ready(function(){

            $('.informe').DataTable({

                 dom: '<"html5buttons"B>lTfgitp',

                 select: true,

                 buttons: [

                     {extend: 'csv', title: 'Facturas de Ventas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

                     {extend: 'excel', title: 'Facturas de Ventas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

                     {extend: 'pdf', title: 'Facturas de Ventas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},



                     {extend: 'print',

                         customize: function (win){

                             $(win.document.body).addClass('white-bg');

                             $(win.document.body).css('font-size', '10px');

                             $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                         }, exportOptions: {
					        columns: ':not(.no-print)'
					    }

                     }

                 ],

             });

        });

    </script>

@endsection

