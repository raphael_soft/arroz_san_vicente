@php
use App\Sistema;
@endphp
@extends('master')
@section('title', 'Facturacion')
@section('active-venta', 'active')


@section('active-venta-factura', 'active')

@section('content')
@php
            $sistema = Sistema::first();
          @endphp
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if($vista == "confirmacion")
        Confirmacion de 
        @endif
        Factura de Venta

        <small># 00{{$factura->id}}</small>
      </h1>
      <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Ventas</a></li>
            <li><a href="{{url('facturasventas')}}">Facturas</a></li>
            @if($vista == "confirmar")
            <li><a href="{{url('/create_facturaventa')}}">Nueva Factura</a></li>
            <li class="active">Confirmar Nueva Factura</li>
            @else
            <li class="active">Ver Factura</li>
            @endif
          </ol>
    </section>
    <?php
      if($factura->status == 2){
        $a = 'success';
      }elseif ($factura->status == 0){
        $a = 'danger';
      }elseif($factura->status == 1){
        $a = 'info';
      }
      ?>
    <div class="pad margin no-print">
      <div class="callout callout-{{$a}}" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> Nota:</h4>
        @if($vista == "confirmacion")
        Por favor, verifique toda la información de la venta antes de proceder a guardarla. Una vez, guardada, no se podrá modificar la información.
        @endif
        @if($factura->status == 1 )
        Esta factura esta pendiente por aprobacion. Abajo tiene las opciones para APROBAR o CANCELAR esta factura de venta.
        @endif
        @if($factura->status == 2)
        Esta factura esta aprobada. 
        @endif
        @if($factura->status == 0)
        Esta factura fue Cancelada. 
        @endif
      </div>
    </div>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            Factura de Venta
            <small class="pull-right">{{date('Y-m-d')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-2">
          @if($sistema->show_logo1 == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1); ?>
          <img src="{{url('/storage/'.$filename)}}" style="width: 100px; max-width: 100px;">
          @endif
        </div>
        <div class="col-sm-8" style="text-align: center;">
          
            @if($sistema->show_razon_social)
            <h3><b>{{$sistema->razon_social}}</b><br></h3>
            @endif
            <address>
            @if($sistema->show_ruc)
            <b>RUC: {{$sistema->ruc}}</b><br>
            @endif
            @if($sistema->show_direccion)
            <b>Direccion:</b> {{$sistema->direccion}}<br>
            @endif
            @if($sistema->show_telefono1)
            <b>Telefono 1:</b> {{$sistema->telefono1}}<br>
            @endif
            @if($sistema->show_telefono2)
            <b>Telefono 2:</b> {{$sistema->telefono2}}<br>
            @endif
            @if($sistema->show_email1)
            <b>Email 1:</b> {{$sistema->email1}}<br>
            @endif
            @if($sistema->show_email2)
            <b>Email 2:</b> {{$sistema->email2}}
            @endif
          </address>
        </div>
        <div class="col-sm-2">
              @if($sistema->show_logo2 == true)
          <?php $filename = str_replace("public/",'',$sistema->logo2); ?>
          <img src="{{url('/storage/'.$filename)}}"  style="width:100px; max-width: 100px;">
          @endif
        </div>
        <div class="col-sm-12">
        <div class="col-sm-6">
          <h3>Cliente</h3>
          
          <address>
            <b>Razon social/Nombre:</b> {{$factura->cliente->razon_social}}<br>
            <b>Cedula/RUC:</b> {{$factura->cliente->cedula_ruc}}<br>
            <b>Direccion:</b> {{$factura->cliente->direccion}}<br>
            <b>Telefono:</b> {{$factura->cliente->telefono}}<br>
          </address>
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
        <div class="col-sm-6" style="text-align: right;">
          @if(isset($factura->id))
          <input type="hidden" name="codigo" value="{{$factura->id}}">
          <strong>Registro # </strong>
          <span style="font-size: 30px; color: red">
        	00{{$factura->id}}</span><br>
          @endif
          <br>
          @if(isset($factura->n_orden))
          <!--b>Orden ID:</b> {{$factura->n_orden}}<br-->
          <input type="hidden" name="orden_id" value="{{$factura->n_orden}}">
          @endif
          @if(isset($factura->modo_pago->nombre))
          <b>Modo de pago:</b> {{$factura->modo_pago->nombre}}<br>
          <input type="hidden" name="modo_pago_id" value="{{$factura->modo_pago_id}}">
          @endif
        </div>
      </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table">
            <thead>
            <tr>
              <th>Cant</th>
              <th>Codigo</th>
              <th>Producto</th>
              <th>Precio Unit</th>
              <!--th>Subtotal</th>
              <th>Impuesto</th-->
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
  @if(isset($productosFactura) and !empty($productosFactura))            
      @foreach($productosFactura as $key => $value)
         @php
            
            $subtotal = ($value->precio * $value->cantidad);
            if(isset($value->impuesto)){
              $impuesto_valor = $value->impuesto->valor;
              $impuesto = ($value->impuesto->valor * $subtotal) / 100;
            }else{
              $impuesto_valor = 0;
              $impuesto = 0;
            }

            
            $total = $subtotal + $impuesto;
            //$subtotal_general += $subtotal;
            //$impuesto_general += $impuesto;
            //$total_general += $total;
            
         @endphp
            <tr>
              <td>{{sprintf("%.2f",$value->cantidad)}} {{$value->producto->medicion->simbolo}}</td>
              <td>{{$value->producto->codigo}}</td>
              <td>{{$value->producto->nombre}}</td>
              <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$value->precio)}} x {{$value->producto->medicion->simbolo}}</td>
              
              <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$subtotal)}}</td>
            </tr>
      @endforeach      
  @endif          
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        @if($vista == "confirmacion")
        <div class="col-xs-12 col-md-6 col-lg-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Por favor verifique los montos. Si esta de acuerdo, presione CONFIRMAR, sino, presione volver.
          </p>
        </div>
        @endif
        <!-- /.col -->
        <div class="col-xs-12 col-md-6 col-lg-6 pull-right">
          <div class="table-responsive">
            <table class="table">
              <tbody>
                @php 
                  $impuestos_iva = array();
                  $subtotal_noiva = 0;
                @endphp
                <tr>
                <td>Subtotal sin IVA:</td>
                <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",round($factura->subtotal,2))}}</td>
              </tr>
                <!-- ciclo para calcular los montos subtotales por cada iva -->
                @foreach($impuestos as $imp)
              <tr>
                <td>IVA {{$imp->valor}} %</td>
                <td>{{$sistema->currency->symbol}} 
                  @php 
                  
                  $impuestos_iva[$imp->id] =  0;
                  $temp_suma_iva = 0;
                  $subtotal_iva0 = 0;
                  //dd($productosFactura);  
                  @endphp

                  @foreach($productosFactura as $key => $value)
                    @php

                    if(isset($value->impuesto) and $value->impuesto->id == $imp->id){
                        //echo($imp->id."->".$imp->valor);
                        if($imp->valor > 0)
                          $impuestos_iva[$imp->id] += ($value->cantidad * $value->precio) * $imp->valor/100;  
                        else
                          $subtotal_iva0 += ($value->cantidad * $value->precio);
                    }
                    @endphp  

                  @endforeach
                  {{sprintf("%.2f",round($impuestos_iva[$imp->id],2))}}  
                </td>
              </tr>
              @endforeach
              <!--tr>
                @php
                foreach($productosFactura as $key => $value){
                  if(!isset($value->impuesto))
                    $subtotal_noiva += $value->cantidad * $value->precio;
                }
                @endphp
                <td>Subtotal objeto no IVA</td>
                <td>{{sprintf("%.2f",round($subtotal_noiva,2))}} {{$sistema->currency->symbol}}</td>
              </tr>
                <tr>
                <td>Subtotal sin IVA:</td>
                <td>{{sprintf("%.2f",round($factura->subtotal,2))}} {{$sistema->currency->symbol}}</td>
              </tr>
              <tr>
                <td>Subtotal IVA</td>
                <td>{{sprintf("%.2f",round($factura->monto_iva,2))}} {{$sistema->currency->symbol}}</td>
              </tr-->
              <tr>
                <td><h3>Total:</h3></td>
                <td><h3>{{$sistema->currency->symbol}} {{sprintf("%.2f",round($factura->total,2))}}</h3></td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<div class="row page-footer">
@if($sistema->show_facebook == true)
<div class="col-sm-12">
  <label>
<i class="fa fa-facebook" style="width: 20px; max-width: 20px;"></i>
{{$sistema->facebook}}
</label>
</div>
@endif
@if($sistema->show_twiter == true)
<div class="col-sm-12">
  <label>
<i class="fa fa-twiter" style="width: 20px; max-width: 20px;"></i>
{{$sistema->twiter}}
</label>
</div>
@endif
@if($sistema->show_web == true)
<div class="col-sm-12">
 <label> 
<i class="fa fa-globe" style="width: 20px; max-width: 20px;"></i>
{{$sistema->web}}
</label>
</div>          
@endif
</div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          @if($vista == "final")
          <a href="{{url('/imprimir_facturaventa',$factura->id)}}" target="_new" class="btn btn-default" title="imprimir factura"><i class="fa fa-print"></i> Imprimir</a>
          @endif
          @if($vista == "confirmacion" and $factura->status == 1)
          <a title="Finalizar venta" onclick="finalizarVenta();">
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-arrow-right"></i>CONTINUAR</button>
          </a>
          @endif
          @if($vista == "final" and $factura->status == 2)
          
          <a onclick="anular('{{$factura->id}}','{{url('anular_facturaventa',$factura->id)}}')" type="button" class="btn btn-danger pull-right" style="margin-right: 5px;"><i class="fa fa-close"></i>ANULAR</a>
          </a>
          @endif
          @if($vista == "final" and ($factura->status == 2 or $factura->status == 0))
        	<a onclick="window.history.back()" title="volver atras" class="btn btn-primary col-lg-2 pull-right" style="margin-right: 5px"><i class="fa fa-arrow-left"></i>Volver</a>
          @endif
          @if($vista == "confirmacion")
          <div>
            <form method="GET" action="{{ route('create_facturaventa') }}"> 
              <input type="hidden" name="orden_id" value="{{ old('orden_id') }}">
              <input type="hidden" name="cliente" value="{{ old('cliente') }}">
              <input type="hidden" name="modo_pago" value="{{ old('modo_pago') }}">
              
              <input type="hidden" id="_token" name="_token" value="{!! csrf_token() !!}">
                @if(old('listaproductos') != null)
                  @foreach(old('listaproductos') as $producto)
                    <input type="hidden" name="listaproductos[]" value="{{ $producto }}">
                  @endforeach
                @endif
                
                @if(old('listaprecios') != null)
                  @foreach(old('listaprecios') as $precio)
                  <input type="hidden" name="listaprecios[]" value="{{ $precio }}">
                  @endforeach
                @endif
                
                @if(old('listacantidades') != null)            
                  @foreach(old('listacantidades') as $cantidad)
                  <input type="hidden" name="listacantidades[]" value="{{ $cantidad }}">
                  @endforeach
                @endif
                
                @if(old('listaimpuestos') != null)            
                  @foreach(old('listaimpuestos') as $impuesto)
                  <input type="hidden" name="listaimpuestos[]" value="{{ $impuesto }}">
                  @endforeach
                @endif

                @if(old('listaimpuestosval') != null)            
                  @foreach(old('listaimpuestosval') as $impuestoval)
                  <input type="hidden" name="listaimpuestosval[]" value="{{ $impuestoval }}">
                  @endforeach
                @endif              
              <button type="submit" class="btn btn-primary col-lg-2 pull-right" style="margin-right: 5px">
                <i class="fa fa-arrow-left"></i>Volver
              </button>
            </form>
          </div>
        @endif
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
    <input type="hidden" id="_token" name="_token" value="{!! csrf_token() !!}">
  </div>
        
  </div>

  <script type="text/javascript">
  @if($vista == "confirmacion")  
   function finalizarVenta(){
    _token = $('#_token').val();
    //$('#form-venta').submit();
    $.ajax({
        url: "{{url('finish_facturaventa')}}",
       data: { _token: _token ,factura: @json($factura), productosFactura: @json($productosFactura) },
       method: 'POST',
       success: function( data ) {
        hideLoading();
          if(data.resp == "existe"){
            alert("Error: Esta factura ya se encuentra registrada.");
            return;
          }
          if(data.resp == "error"){
            alert("Error: "+data.msg);
            return;
          }
          if(data.resp == "ok"){
            alert("¡Registro Exitoso!");
            window.location.href = "{{url('/facturas_ventas')}}";
            return;
          }
        },        
        beforeSend: function(){
          showLoading();
        },
        error: function(){
          hideLoading();
          alert("Ocurrio un error al hacer la peticion ajax");
        }

    });
   } 
@endif

</script>

@endsection
