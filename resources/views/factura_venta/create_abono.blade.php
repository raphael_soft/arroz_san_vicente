@extends('master')
@section('title', 'Facturacion')
@section('active-venta', 'active')

@section('active-ventas-por-cobrar', 'active')

@section('active-ventas-abonos', 'active')

@section('content')
<!-- Content Wrapper. Contains page content -->
@php
use App\Producto;

use App\Iva;
use App\Sistema;

$sistema = Sistema::first();

@endphp
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Nuevo Abono            
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="{{url('facturas_ventas')}}">Ventas</a></li>
            <li><a href="{{url('ventas_por_cobrar')}}">Ventas por Cobrar</a></li>
            <li><a href="{{url('ventas_abonos')}}">Abonos</a></li>
            <li class="active">Nuevo Abono</li>
          </ol>
        </section>

         <div class="pad margin no-print">
          <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info-circle"></i> Nota:</h4>
            Aqu&iacute; podr&aacute; registrar un abono a alguna factura de credito. El boton 'Registrar Monto Ingresado' se usa para abonar una cantidad ingresada. El boton 'Registrar Monto Restante' se usa hacer un abono por el total del restante, sin importar el monto ingresado en el formulario. Este boton suele ser ultil para hacer el pago del ultimo abono donde el monto restante es un numero con muchos decimales y resulta complicado ingresarlo a mano.
          </div>
        </div>

        <!-- Main content -->
    <section class="content">
      <!-- title row -->
      <div class="box container">
        <div class="box-header">
          <h4 class="page-header">
            <i class="fa fa-dollar"></i> Abono a Factura de Venta
            <span class="pull-right">Fecha: {{$fecha}}</span>
          </h4>
        </div>
        
      <!-- info row -->
  <div class="box-body">
  	<label></label>
  	<div class="col-sm-12"></div>
    <div class="form-group col-xs-12 col-sm-6 col-md-3 col-lg-2">
      <label><strong>Factura</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Escriba el numero de factura de venta. Si no aparece el numero de factura. Solo se listan las facturas con modo de pago a credito." style="cursor: pointer;"></i></label>
      <div class="input-group">
        
       <?php 
       
        $factura = old('factura');
       
       ?>
        <select name="factura" id="factura" class="form-control campo selectpicker" data-live-search="true" required="">
            <option value="" selected="">Escoja una factura</option>
            @if(isset($facturas))
            @foreach($facturas as $f)
            <option id="factura{{$f->id}}" value="{{$f->id}}" total-factura="{{$f->total}}" total-abonado="{{$f->abonos->sum('monto')}}"
              @if($factura == $f->id)
                selected
                @endif
              ># 00{{$f->id}}</option>
            @endforeach
            @endif
        </select>
        
      </div>
	</div>
	<div class="form-group col-xs-12 col-sm-6 col-md-3 col-lg-2">
      <label><strong>Total Factura</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Aqui se indica el monto total de la factura seleccionada" style="cursor: pointer;"></i></label>
      <div class="input-group">
       <?php 
        
        $total_factura = old('total_factura');
       
       ?>
        <input name="total_factura" id="total_factura" class="form-control precio" value="{{$total_factura ? $total_factura : 0}}" readonly="">
      </div>
	</div>
 	<div class="form-group col-xs-12 col-sm-6 col-md-3 col-lg-2">
      <label><strong>Total Abonado</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Aqui se muestra el total abonado para la factura seleccionada" style="cursor: pointer;"></i></label>
      <div class="input-group">
       <?php 
        
        $total_abonado = old('total_abonado');
       
       ?>
        <input name="total_abonado" id="total_abonado" class="form-control precio" value="{{$total_abonado ? $total_abonado : 0}}" readonly="">
      </div>
	</div>
	<div class="form-group col-xs-12 col-sm-6 col-md-3 col-lg-2">
      <label><strong>Total Restante</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Aqui se muestra el total restante para la factura seleccionada" style="cursor: pointer;"></i></label>
      <div class="input-group">
       <?php 
        
        $total_restante = old('total_restante');
       
       ?>
        <input name="total_restante" id="total_restante" class="form-control precio" value="{{$total_restante ? $total_restante : 0}}" readonly="">
      </div>
	</div>
	<div class="form-group col-xs-12 col-sm-6 col-md-3 col-lg-2">
      <label><strong>Monto a Abonar</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Ingrese aqui el monto a abonar a la factura" style="cursor: pointer;"></i></label>
      <div class="input-group">
       <?php 
        
        $monto = old('monto');
       
       ?>
        <input name="monto" id="monto" class="form-control precio" value="{{$monto ? $monto : 1}}" ></div>
	</div>
    
  </div>
  <div class="box-footer">
          
          <button id="btn_registrar_abono"  class="btn btn-lg btn-info col-xs-12 col-sm-12 col-md-3 col-lg-4" ><i class="fa fa-arrow-right"></i> Registrar Monto Ingresado</button>
          <button id="btn_registrar_restante"  class="btn btn-lg btn-success col-xs-12 col-sm-12 col-md-3 col-lg-4" ><i class="fa fa-arrow-right"></i> Registrar Monto Restante</button>
        
        
   </div>
</div>
</section>
          <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
        
  </div>
  <div class="modal fade" id="modalAbonoTiket" tabindex="-1" role="dialog" aria-labelledby="modalAbonoTiket" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div id="modal-abono-content"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="close_modal_abono_invoice" data-dismiss="modal">Cerrar</button>
      </div>
</div>
</div>
</div>
  <!-- /.content-wrapper -->
<script type="text/javascript">

$(document).ready(function(){

      _token = $("#_token").val();

    $("#btn_registrar_abono").click(function(e){

        monto = $("#monto").val();
        id_factura = $("#factura").val();

        registrarAbono(monto,id_factura,'normal');
    });

    $("#btn_registrar_restante").click(function(e){

        monto = $("#monto").val();
        id_factura = $("#factura").val();

        registrarAbono(monto,id_factura,'completar');
      });
    });

    $("#factura").on('change',function(){
      element_factura = $("#factura option:selected");
      
      //TODO: cargar  los precios de venta
      total_factura = Number(element_factura.attr('total-factura'));
      total_abonado = Number(element_factura.attr('total-abonado'));
      restante = total_factura - total_abonado;
      $('#total_factura').val(parseFloat(total_factura.toFixed(2)));
      $('#total_abonado').val(parseFloat(total_abonado).toFixed(2)); 
      $('#total_restante').val(parseFloat(restante).toFixed(2)); 
      $('#monto').val(restante );
      if(restante <= 0){
        $('#monto').attr('readonly','readonly');
        alert('La factura seleccionada no tiene un monto restante. El monto abonado es igual al total de la factura.');
        $('#btn_registrar_restante').attr('disabled',true);
        $('#btn_registrar_abono').attr('disabled',true);
      }else{
        $('#monto').removeAttr('readonly');
        $('#btn_registrar_restante').removeAttr('disabled');
        $('#btn_registrar_abono').removeAttr('disabled');
      }

    });

      function registrarAbono(monto,id_factura,modo){

        if(modo == 'normal'){
          if(!(Number(monto) >= 0)){
            alert("Por favor ingrese un monto mayor a 0");
            monto.focus();
            return;
          }
        }  
          if(!(Number(id_factura) > 0)){
            alert("Numero de factura invalido");
            return;
          }
            $.ajax({
              url: "{{url('registrar-abono')}}",
              data: {
                _token : _token,
                monto : monto,
                facturaventa_id: id_factura,
                modo: modo
              },
              method: "POST",
              success: function(data){
                hideLoading();
                if(data.resp == "ok"){
                  $('#modal-abono-content').html(data.tiket);

                  alert(data.msg);
                  //TODO: actualizar el monto en la lista sin refrescar la pagina
                  $('#factura'+id_factura).attr('total-abonado',data.total_abonado);
                  abonado = Number(data.total_abonado);
                  $('#total_abonado').val(parseFloat(abonado).toFixed(2)); 
                  $('#total_restante').val(parseFloat(total_factura - abonado).toFixed(2)); 

                  $('#close_modal_abono').click();//cerramos la ventana
                  $('#modalAbonoTiket').modal();                  
                }else if(data.resp == "error"){
                  alert(data.msg);
                  return;
                }
                

              },
              beforeSend: function(){
                showLoading();
              },
              error: function(){
                hideLoading();
                alert("Ocurrio un error");
              }

              });
          }
</script>
@endsection
