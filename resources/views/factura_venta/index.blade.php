@extends('master')

@section('title', 'Ventas')

@section('active-venta', 'active')

@section('active-venta-factura', 'active')


@section('content')
<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Facturas de Ventas

        </h1>

        <ol class="breadcrumb">

            <li class="active">

                <a href="{{ url('/"') }}">

                    <i class="fa fa-dashboard">

                    </i>

                    Inicio

                </a>

            </li>

        </ol>

    </section>

    <section class="content">

       <div class="box">

            <div class="box-header">

              <h3 class="box-title">Listado de Facturas de Ventas</h3>             

                @if (session('status'))

                    <div class="alert alert-success">

                        {{session('status')}}

                    </div>

                @endif
              <!-- Split button -->
              @if(session('registrar'))
<a href="{{url('create_facturaventa')}}"><button type="button" class="btn btn-primary" style="float: right;">
    <i class="fa fa-plus"></i> Facturar
  </button></a>
    @endif

            </div>

            <!-- /.box-header -->
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
            <div class="box-body">
              <div class="row">
                <form method="GET">
              <div class="col-sm-12"><label>Consultar por fechas</label></div>
                        <div class="form-group col-sm-3">
                               <label >Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="from" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label >Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="to" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
                      </form>
              </div>
              <table id="example1" class="table table-bordered table-striped table-responsive informe responsive">

                <thead>

                  <tr>

                    <th>N #</th>

                    <!--th>N. Orden</th-->
                    <th >Cedula/RUC</th>
                    <th >Cliente</th>

                    <th>Subtotal</th>

                    <th>Impuesto</th>

                    <th>Total</th>

                    <th>Status</th>

                    <th>Fecha</th>

                    <th class="no-print">Opciones</th>

                  </tr>

                </thead>

                <tbody id="tabla_datos">



                @foreach($factura as $f)


                  <tr>

                    <td>{{$f->id}}</td>

                    <!--td>{{$f->n_orden}}</td-->

                    <td>{{$f->cliente->cedula_ruc}}</td>

                    <td>{{$f->cliente->razon_social}}</td>

                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f->subtotal)}} </td>

                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f->monto_iva)}}</td>

                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f->total)}}</td>

                    @if($f->status == 0)

                        <td style="color: red;"><b>{{"Cancelada"}}</b></td>

                    @elseif($f->status == 1)

                        <td style="color: gray;"><b>{{"Pendiente"}}</b></td>

                    @elseif($f->status == 2)

                        <td style="color: green;"><b>{{"Aceptada"}}</b></td>

                    @endif

                    <td>{{$f->fecha_facturacion}}</td>

                    <td>
                        @if(session('ver'))
                        <a href="{{url('show_facturaventa',$f->id)}}" data-toggle="tooltip" data-placement="top" title="Presione aqui para ver esta factura" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                        @if(strtolower($f->modo_pago->nombre) == "credito")
                        <a href="" data-toggle="modal" data-target="#modalAbonosFacturaventa" title="Presione aqui para ver los abonos hechos a esta factura" class="btn btn-success" onclick="cargarAbonos({{$f->id}})"><i class="fa fa-dollar"></i></a>
                        @endif
                        <a href="{{url('imprimir_facturaventa',$f->id)}}" target="_new" data-toggle="tooltip" data-placement="top" title="Presione aqui para imprimir esta factura" class="btn btn-warning"><i class="fa fa-print"></i></a>
                        @endif
                        @if(session('borrar'))
                        @if($f->status == 2)
                        <a onclick="anular('{{$f->id}}','{{url('anular_facturaventa',$f->id)}}')"  data-toggle="tooltip" data-placement="top" title="Presione aqui para anular esta factura" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                        @endif
                        @endif
                    </td>

                  </tr>

                @endforeach

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

    </section>

</div>
<?php echo view('modales/modal-abonos')->render(); ?>

    <script>
        $(document).ready(function(){

            $('.informe').DataTable({

                 dom: '<"html5buttons"B>lTfgitp',

                 select: true,

                 buttons: [

                     {extend: 'csv', title: 'Facturas de Ventas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

                     {extend: 'excel', title: 'Facturas de Ventas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

                     {extend: 'pdf', title: 'Facturas de Ventas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},



                     {extend: 'print',

                         customize: function (win){

                             $(win.document.body).addClass('white-bg');

                             $(win.document.body).css('font-size', '10px');

                             $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                         },
                         exportOptions: {
					        columns: ':not(.no-print)'
					    }

                     }

                 ],

             });
        });

        function cargarAbonos(id_factura){
          _token = $("#_token").val();
          $('#modal-body-abonos').html('');
          $.ajax({
              url: "{{url('cargar-abonos-facturaventa')}}",
              data: {
                _token : _token,
                factura_id: id_factura
              },
              dataType: "json",
              method: "POST",
              success: function(data){
                hideLoading();
                if(data.resp == "ok"){
                  $('#modal-body-abonos').html(data.html);
                }else if(data.resp == "error"){
                  alert(data.msg);
                  return;
                }
              },
              beforeSend: function(){
                showLoading();
              },
              error: function(){
                hideLoading();
                alert("Ocurrio un error");
              }

            });
      }

    </script>

@endsection

