<header class="main-header">
    <a class="logo" href="{{ url('/') }}">
        <span class="logo-mini">
            <b>
            A
            </b>
            A
        </span>
        <span class="logo-lg">
            <b>
            {{config('app.name')}}
            </b>
        </span>
    </a>
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a class="sidebar-toggle" data-toggle="offcanvas" href="#" role="button">
            <span class="sr-only">
                Toggle navigation
            </span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <img alt="User Image" class="user-image" src="{{ url('/img/default.png') }}"/>
                        <span class="hidden-xs">
                            {{Session::get('correo')}}
                        </span>
                        </img>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img alt="User Image" class="img-circle" src="{{ url('/img/default.png') }}"/>
                            <p>
                                {{session('nombre')}}
                            </p>
                            <p>
                                {{session('nombre_cargo')}}
                            </p>
                            </img>
                        </li>
                        
                        <li class="user-footer">
                            <div class="pull-center">
                                <a class="btn btn-default btn-flat" href="{{ url('/salir') }}">
                                    Cerrar Sesión
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">
                MENU
            </li>
            <li class="active">
                <a href="{{ url('/') }}">
                    <i class="fa fa-home">
                    </i>
                    <span>
                        Inicio
                    </span>
                </a>
            </li>
            {{-- VENTAS --}}
            <li class="treeview @yield('active-venta')">
                <a href="#">
                    <i class="fa fa-money">
                    </i>
                    <span>
                        Ventas
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right">
                        </i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!--li  class="@yield('active-venta-precio')">
                    <a href="{{ url('/precios') }}">
                        <i class="fa fa-circle-o">
                        </i>
                        Precios
                    </a>
                </li-->
                <li class="@yield('active-venta-factura')">
                    <a href="{{ url('/facturas_ventas') }}">
                        <i class="fa fa-circle-o">
                        </i>
                        Facturas de Ventas
                    </a>
                </li>
                <li class="@yield('active-ventas-por-cobrar')">
                    <a href="{{ url('/ventas_por_cobrar') }}">
                        <i class="fa fa-circle-o">
                        </i>
                        Ventas por Cobrar
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        
                        <li class="@yield('active-ventas-por-cobrar-facturas')"><a href="{{ url('/ventas_por_cobrar') }}">Facturas</a></li>
                        <li class="@yield('active-ventas-abonos')"><a href="{{ url('/ventas_abonos') }}">Abonos</a></li>
                    </ul>
                </li>
                <li  class="@yield('active-venta-estadistica')">
                    <a href="{{ url('/estadisticas_ventas') }}">
                        <i class="fa fa-circle-o">
                        </i>
                        Estadisticas de Ventas
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@yield('active-estadisticas-ventas')"><a href="{{ url('/estadisticas_ventas/') }}">Por productos</a></li>
                        <li class="@yield('active-estadisticas-ventas1')"><a href="{{ url('/estadistica_venta_por_productos1/') }}">10 productos + vendidos</a></li>
                        <li class="@yield('active-estadisticas-ventas2')"><a href="{{ url('/estadistica_venta_por_productos2') }}">10 productos - vendidos</a></li>
                        <li class="@yield('active-estadisticas-ventas3')"><a href="{{ url('/estadistica_venta_por_cat') }}">Por categoria</a></li>
                        <!--li class="@yield('active-estadisticas-ventas4')"><a href="{{ url('/estadistica_venta_por_cat_subcat') }}">Por cat. y subcat.</a></li-->
                        <li class="@yield('active-estadisticas-ventas5')"><a href="{{ url('/estadistica_venta_productos3') }}">Productos + y - vendidos</a></li>
                        <li class="@yield('active-estadisticas-ventas6')"><a href="{{ url('/estadistica_venta_subtotales') }}">Subtotales diarios</a></li>
                        <li class="@yield('active-estadisticas-ventas7')"><a href="{{ url('/estadistica_venta_plain') }}">Reporte texto</a></li>
                        <li class="@yield('active-estadisticas-ventas8')"><a href="{{ url('/estadistica_venta_global_cat') }}">Reporte global x cat.</a></li>
                        
                    </ul>
                </li>
            </ul>
        </li>
        {{-- COMPRAS --}}
        <li class="treeview @yield('active-compra')">
            <a href="#">
                <i class="fa fa-shopping-cart">
                </i>
                <span>
                    Compras
                </span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right">
                    </i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="@yield('active-compra-productos')">
                    <a href="{{ url('/facturas_compras/productos') }}">
                        <i class="fa fa-circle-o">
                        </i>
                        De Productos
                    </a>
                </li>
                <li class="@yield('active-compra-producciones')">
                    <a href="{{ url('/facturas_compras/producciones') }}">
                        <i class="fa fa-circle-o">
                        </i>
                        De Produccion
                    </a>
                </li>
                <li class="@yield('active-compra-estadisticas')">
                    <a href="{{ url('/estadistica_compra') }}">
                        <i class="fa fa-circle-o">
                        </i>
                        Estadistica de Compras
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right">
                            </i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@yield('active-estadisticas-compras7')"><a href="{{ url('/estadistica_compra_por_socios/') }}">Por socios</a></li>
                        <li class="@yield('active-estadisticas-compras1')"><a href="{{ url('/estadistica_compra_por_productos/') }}">Por productos</a></li>
                        <li class="@yield('active-estadisticas-compras2')"><a href="{{ url('/estadistica_compra_por_tipo_prod') }}">Por tipo y productos</a></li>
                        <li class="@yield('active-estadisticas-compras3')"><a href="{{ url('/estadistica_compra_por_cat_subcat') }}">Por cat. y subcat. 1</a></li>
                        <li class="@yield('active-estadisticas-compras4')"><a href="{{ url('/estadistica_compra_por_cat_subcat2') }}">Por cat. y subcat. 2</a></li>
                        <li class="@yield('active-estadisticas-compras5')"><a href="{{ url('/estadistica_compra_subtotales_diarios') }}">Subtotales diarios</a></li>
                        <li class="@yield('active-estadisticas-compras6')"><a href="{{ url('/estadistica_compra_plain') }}">Reporte texto</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        {{-- INVENTARIO --}}
        <li class="treeview @yield('active-inventario')">
            <a href="#">
                <i class="fa fa-cubes">
                </i>
                <span>
                    Inventario
                </span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right">
                    </i>
                </span>
            </a>
            <ul class="treeview-menu">
                <!--li class="@yield('active-inventario-inventario')">
                <a href="{{ url('/inventario') }}">
                    <i class="fa fa-circle-o">
                    </i>
                    Inventario
                </a>
            </li-->
            <li class="@yield('active-inventario-productos')">
                <a href="{{ url('/inventario/productos') }}">
                    <i class="fa fa-circle-o">
                    </i>
                    De Productos
                </a>
            </li>
            <li class="@yield('active-inventario-produccion')">
                <a href="{{ url('/inventario/producciones') }}">
                    <i class="fa fa-circle-o">
                    </i>
                    De Produccion
                </a>
            </li>
            <!--li class="@yield('active-inventario-kardex')">
            <a href="{{ url('/kardex') }}">
                <i class="fa fa-circle-o">
                </i>
                Kardex
            </a>
        </li-->
        <li class="@yield('active-inventario-categorias')">
            <a href="{{ url('/inventario/categorias') }}">
                <i class="fa fa-circle-o">
                </i>
                Por Categorías
            </a>
        </li>
    </ul>
</li>
{{-- PRODUCCION --}}
<li class="treeview @yield('active-revisiones')">
    <a href="{{ route('revisiones') }}">
        <i class="fa fa-stethoscope">
        </i>
        <span>
            Revisiones de Campo
        </span>
    </a>
</li>
{{-- PRODUCCION --}}
<li class="treeview @yield('active-produccion')">
    <a href="{{ url('/produccion') }}">
        <i class="fa fa-line-chart">
        </i>
        <span>
            Producción
        </span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right">
            </i>
        </span>
    </a>
    
    <ul class="treeview-menu">
        <li class="@yield('active-produccion-hectareas')">
            <a href="{{route('terrenos')}}">
                <i class="fa fa-circle-o"></i>
                <span>
                    Terrenos
                </span>
            </a>
        </li>
        <li class="@yield('active-produccion-insumos')">
            <a href="{{route('insumos')}}">
                <i class="fa fa-circle-o"></i>
                <span>
                    Insumos
                </span>
            </a>
        </li>
        <li class="@yield('active-produccion-tipos')">
            <a href="{{route('tipos_cosechas')}}">
                <i class="fa fa-circle-o"></i>
                <span>
                    Tipos de cosechas
                </span>
            </a>
        </li>
        <li class="@yield('active-produccion-cosechas')">
            <a href="">
                <i class="fa fa-circle-o"></i>
                <span>
                    Cosechas
                </span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="@yield('active-cosechas-cosechas')"><a href="/produccion/cosechas">Cosechas</a></li>
                <li class="@yield('active-cosechas-calificaciones')"><a href="/produccion/calificaciones">Calificaciones</a></li>
            </ul>
        </li>
    </ul>
</li>
{{-- ASOCIACION --}}
<li class="@yield('active-asociacion-socios')">
    <a href="{{ url('/socios') }}">
        <i class="fa fa-address-book">
        </i>
        Proveedores
    </a>
</li>
{{-- ESTADISTICAS --}}
<li class="treeview @yield('estadistica')">
<a href="#">
    <i class="fa fa-pie-chart">
    </i>
    <span>
        Estadisticas
    </span>
    <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right">
        </i>
    </span>
</a>
<ul class="treeview-menu">
    <li class="@yield('estadistica-ingreso')">
        <a href="{{ url('/estadisticas/ingreso') }}">
            <i class="fa fa-circle-o">
            </i>
            Ingresos
        </a>
    </li>
    <li class="@yield('estadistica-egreso')">
        <a href="{{ url('/estadisticas/egreso') }}">
            <i class="fa fa-circle-o">
            </i>
            Egresos
        </a>
    </li>
    <li class="@yield('estadistica-cosechas')">
        <a href="{{route('estadistica_cosechas')}}">
            <i class="fa fa-circle-o"></i>
            Cosechas
        </a>
    </li>
    <li class="@yield('estadistica-calificaciones')">
        <a href="{{route('estadistica_calificaciones')}}">
            <i class="fa fa-circle-o"></i>
            Calificaciones
        </a>
    </li>
</ul>
</li>
<?php
?>
{{-- CONFIGURACION --}}
@if(session('superuser') == true)
<li class="treeview @yield('active')">
<a href="#">
    <i class="fa fa-sliders">
    </i>
    <span>
        Configuración
    </span>
    <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right">
        </i>
    </span>
</a>

<ul class="treeview-menu">
    <li class="@yield('active-confproductos')">
        <a href="{{ url('/conf_productos') }}">
            <i class="fa fa-circle-o">
            </i>
            Productos
        </a>
    </li>
    <!--li class="@yield('active-configproduccion')">
    <a href="{{ url('/conf_produccion') }}">
        <i class="fa fa-circle-o">
        </i>
        Producción
    </a>
</li-->
<li class="@yield('active-iva')">
    <a href="{{ url('/configuracion/iva') }}">
        <i class="fa fa-circle-o">
        </i>
        Iva
    </a>
</li>
<li class="@yield('active-rol')">
    <a href="{{ url('/configuracion/roles') }}/">
        <i class="fa fa-circle-o">
        </i>
        Roles
    </a>
</li>
<li class="@yield('active-cargos')">
    <a href="{{ url('/configuracion/cargos') }}/">
        <i class="fa fa-circle-o">
        </i>
        Cargos
    </a>
</li>
<li class="@yield('active-usuario')">
    <a href="{{ url('/configuracion/usuario') }}">
        <i class="fa fa-circle-o">
        </i>
        Usuarios
    </a>
</li>

<li class="@yield('active-facturas')">
    <a href="{{ url('/configuracion/facturas') }}">
        <i class="fa fa-circle-o">
        </i>
        Facturas
    </a>
</li>
{{--
<li class="@yield('active-permiso')">
    <a href="#">
        <i class="fa fa-circle-o">
        </i>
        Permisos
    </a>
</li> --}}
<li class="@yield('active-categoria')">
    <a href="{{ url('/configuracion/categorias') }}">
        <i class="fa fa-circle-o">
        </i>
        Categorias
    </a>
</li>
<li class="@yield('active-subcategoria')">
    <a href="{{ url('/configuracion/subcategorias') }}">
        <i class="fa fa-circle-o">
        </i>
        SubCategorias
    </a>
</li>
<li class="@yield('active-mediciones')">
    <a href="{{ url('/configuracion/mediciones') }}">
        <i class="fa fa-circle-o">
        </i>
        Tipo de Medición
    </a>
</li>
<li class="@yield('active-clientes')">
    <a href="{{ url('/configuracion/clientes') }}">
        <i class="fa fa-circle-o">
        </i>
        Clientes
    </a>
</li>
<li class="@yield('active-sistema')">
    <a href="{{ url('/configuracion/sistema') }}">
        <i class="fa fa-circle-o">
        </i>
        Sistema
    </a>
</li>

</ul>
</li>
@endif

</ul>
</section>
</aside>