@extends('master')
@section('title', 'Socios')
@section('active-asociacion', 'active')
@section('active-asociacion-socios', 'active')
@section('content')

  <div class="content-wrapper">

      <section class="content-header">
          <h1>
            Registro de Socios
            <small>Secci&oacute;n para el registro de socios</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Asociaci&oacute;n</a></li>
            <li><a href="{{url('socios')}}">Socios</a></li>
            <li class="active">Nuevo Socio</li>
          </ol>
        </section>

      @foreach($errors->all() as $error)
      <p class="alert alert-danger">{{$error}}</p>
    @endforeach

    @if (session('status'))
      <div class="alert alert-success">
        {{session('status')}}
      </div>
    @endif

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
          <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">Formulario para el registro de socios</h3>
                  </div>
                   <form  method="POST">
                          <div class="box-body">
                            <div class="form-group col-sm-3">
                              <label><i class="fa fa-asterisk" style="color:red"></i>&nbsp;Nombre</label>
                              <div class="form-input">
                                <input type="text" name="nombre" class="form-control" required="" placeholder="Nombre del Socio">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label><i class="fa fa-asterisk" style="color:red"></i>&nbsp;Cedula/RUC <span class="error-msg-ruc" style="display: normal; color: red"></span></label>
                              <div class="form-input">
                                <input type="text" name="ruc" id="ruc" class="form-control numeros" required="" placeholder="RUC del Socio" maxlength="13" min="10">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label>Direccion</label>
                              <div class="form-input">
                                <input type="text" name="direccion" class="form-control" placeholder="Direccion del Socio">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label >Correo</label>
                              <div class="form-input">
                                <input type="email" name="email" class="form-control"  placeholder="Correo del Socio">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label>Tel&eacute;fono</label>
                              <div class="form-input">
                              <input minlength="9" maxlength="10" type="text" name="telefono" class="form-control numeros" placeholder="Tel&eacute;fono del Socio" maxlength="10">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label >Nombre del Representante</label>
                              <div class="form-input">
                                <input type="text" name="nombre_representante" class="form-control"  placeholder="Nombre del representante">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label >Cedula Representante <span class="error-msg-ruc2" style="display: normal; color: red"></span></label>
                              <div class="form-input">
                                <input type="text" name="cedula_representante" id="ruc2" class="form-control numeros"  placeholder="Cedula del representante" maxlength="10">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label>Telefono representante</label>
                              <div class="form-input">
                                <input type="text"  name="telefono_representante" class="form-control numeros" placeholder="Telefono del representante legal" maxlength="10">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label >Correo del representante</label>
                              <div class="form-input">
                                <input type="email" name="email_representante" class="form-control"  placeholder="Correo del representante">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label>Direccion representante</label>
                              <div class="form-input">
                                <input type="text" name="direccion_representante" class="form-control" placeholder="Direccion del representante legal">
                              </div>  
                            </div>
                            </div>
                        </div>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                          </div>
                          <!-- /.box-body -->
                        <div class="box-footer">
                          <a href="{{url('/socios')}}" class="btn btn-default"><< Volver</a>
                          <button type="submit" class="btn btn-info pull-right submit"><i class="fa fa-upload"></i> Registrar</button>
                        </div>
                        <!-- /.box-footer -->
                        </form>
                </div>
        </div>
      </div>
      </section>
<script type="text/javascript" src="{{ url('/js/ruc_jquery_validator.min.js') }}"></script>
<script type="text/javascript">
  var opciones = {
    strict: true,        // va a validar siempre, aunque la cantidad de caracteres no sea 10 ni 13
    events: "keyup",     // evento que va a disparar la validación
    the_classes: "error",// clase que se va a agregar al nodo en el que se realiza la validación
    onValid: function () {    
          console.log('cedula/ruc valido');
          $('.error-msg-ruc').hide();
          $('.submit').removeClass('disabled');
          $('.submit').removeAttr('disabled');
    },   // callback cuando la cédula es correcta.
    onInvalid: function () {
          console.log('cedula/ruc invalido');
          $('.error-msg-ruc').text('invalido').show();
          $('.submit').addClass('disabled');
          $('.submit').attr('disabled','disabled');
      }  // callback cuando la cédula es incorrecta.
  };

  var opciones2 = {
    strict: true,        // va a validar siempre, aunque la cantidad de caracteres no sea 10 ni 13
    events: "keyup",     // evento que va a disparar la validación
    the_classes: "error",// clase que se va a agregar al nodo en el que se realiza la validación
    onValid: function () {    
          console.log('cedula/ruc valido');
          $('.error-msg-ruc2').hide();
          $('.submit').removeClass('disabled');
          $('.submit').removeAttr('disabled');
    },   // callback cuando la cédula es correcta.
    onInvalid: function () {
          console.log('cedula/ruc invalido');
          $('.error-msg-ruc2').text('invalido').show();
          $('.submit').addClass('disabled');
          $('.submit').attr('disabled','disabled');
      }  // callback cuando la cédula es incorrecta.
  };
  
  $('#ruc').validarCedulaEC(opciones);
  
  $('#ruc2').validarCedulaEC(opciones2);
</script> 

@endsection