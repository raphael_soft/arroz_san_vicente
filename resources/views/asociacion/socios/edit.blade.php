@extends('master')
@section('title', 'Socios')
@section('active-asociacion', 'active')
@section('active-asociacion-socios', 'active')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edici&oacute;n de Socios
            <small>Secci&oacute;n para editar de socios</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Asociaci&oacute;n</a></li>
            <li><a href="{{url('socios')}}">Socios</a></li>
            <li class="active">Editar Socio</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario para editar socio</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                          <!-- Horizontal Form -->
                   @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                            <form  method="POST">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <input type="hidden" name="id" value="{{$socios->id}}">
                            <div class="box-body">
                              <div class="form-group col-sm-3">
                              <label><i class="fa fa-asterisk" style="color:red"></i>&nbsp;Codigo</label>
                              <div class="form-input">
                                <input type="text" required="" name="codigo" class="form-control" value="{{$socios->codigo}}" placeholder="Codigo del socio" readonly>
                              </div>
                            </div>
                                <div class="form-group col-sm-3">
                              <label><i class="fa fa-asterisk" style="color:red"></i>&nbsp;Nombre</label>
                              <div class="form-input">
                                <input type="text" name="nombre" value="{{$socios->nombre}}" class="form-control" required="" placeholder="Nombre del Socio">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label><i class="fa fa-asterisk" style="color:red"></i>&nbsp;Cedula/RUC <span class="error-msg-ruc" style="display: normal; color: red"></span></label>
                              <div class="form-input">
                                <input type="text" name="ruc" id="ruc" value="{{$socios->ruc}}" class="form-control numeros" required="" placeholder="RUC del Socio" maxlength="13" minlength="10">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label >Correo</label>
                              <div class="form-input">
                                <input type="email" name="email" class="form-control" value="{{strtolower($socios->email)}}"  placeholder="Correo del Socio">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label>Tel&eacute;fono</label>
                              <div class="form-input">
                              <input maxlength="10" type="text" value="{{$socios->telefono}}" name="telefono" class="form-control numeros" placeholder="Tel&eacute;fono del Socio">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label >Direccion</label>
                              <div class="form-input">
                                <input type="text" name="direccion" class="form-control" value="{{$socios->direccion}}" placeholder="Direccion del socio">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label >Nombre del Representante</label>
                              <div class="form-input">
                                <input type="text" name="nombre_representante" class="form-control" value="{{$socios->nombre_representante}}" placeholder="Nombre del representante">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label >Cedula del Representante <span class="error-msg-ruc2" style="display: normal; color: red"></span></label>
                              <div class="form-input">
                                <input type="text" name="cedula_representante" id="ruc2" class="form-control numeros" value="{{$socios->cedula_representante}}"  placeholder="Cedula del representante" maxlength="10">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label>Telefono del representante</label>
                              <div class="form-input">
                                <input type="text"  name="telefono_representante" class="form-control numeros" value="{{$socios->telefono_representante}}" placeholder="Telefono del representante legal" maxlength="10">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label>Email del representante</label>
                              <div class="form-input">
                                <input type="email"  name="email_representante" class="form-control" value="{{strtolower($socios->email_representante)}}" placeholder="Email del representante legal">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label>Direccion del representante</label>
                              <div class="form-input">
                                <input type="text"  name="direccion_representante" class="form-control" value="{{$socios->direccion_representante}}" placeholder="Direccion del representante legal">
                              </div>
                            </div>
                         </div>
                                  <!-- /.box-body -->
                          <div class="box-footer">
                            <a href="{{url('show_socio',$socios->id)}}" class="btn btn-default"><< Volver</a>
                            <button type="submit" class="btn btn-info pull-right submit"><i class="fa fa-save"></i> Guardar</button>
                          </div>
                  <!-- /.box-footer -->
                      </form>
                </div>
                <!-- /.box -->
              </div>
            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->

        </section>
        <!-- /.content -->
<script type="text/javascript" src="{{ url('/js/ruc_jquery_validator.min.js') }}"></script>
<script type="text/javascript">
  var opciones = {
    strict: true,        // va a validar siempre, aunque la cantidad de caracteres no sea 10 ni 13
    events: "keyup",     // evento que va a disparar la validación
    the_classes: "error",// clase que se va a agregar al nodo en el que se realiza la validación
    onValid: function () {    
          console.log('cedula/ruc valido');
          $('.error-msg-ruc').hide();
          $('.submit').removeClass('disabled');
          $('.submit').removeAttr('disabled');
    },   // callback cuando la cédula es correcta.
    onInvalid: function () {
          console.log('cedula/ruc invalido');
          $('.error-msg-ruc').text('invalido').show();
          $('.submit').addClass('disabled');
          $('.submit').attr('disabled','disabled');
      }  // callback cuando la cédula es incorrecta.
  };

  var opciones2 = {
    strict: true,        // va a validar siempre, aunque la cantidad de caracteres no sea 10 ni 13
    events: "keyup",     // evento que va a disparar la validación
    the_classes: "error",// clase que se va a agregar al nodo en el que se realiza la validación
    onValid: function () {    
          console.log('cedula/ruc valido');
          $('.error-msg-ruc2').hide();
          $('.submit').removeClass('disabled');
          $('.submit').removeAttr('disabled');
    },   // callback cuando la cédula es correcta.
    onInvalid: function () {
          console.log('cedula/ruc invalido');
          $('.error-msg-ruc2').text('invalido').show();
          $('.submit').addClass('disabled');
          $('.submit').attr('disabled','disabled');
      }  // callback cuando la cédula es incorrecta.
  };
  
  $('#ruc').validarCedulaEC(opciones);
  $('#ruc2').validarCedulaEC(opciones2);
</script> 
    
@endsection
