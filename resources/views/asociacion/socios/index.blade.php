@extends('master')
@section('title', 'Socios')
@section('active-asociacion', 'active')
@section('active-asociacion-socios', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Socios
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado de Socios</h3>
              
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                @if(session('registrar'))
              <div><a href="{{url('create_socio')}}" class="btn btn-primary" style="float: right;">Nuevo Socio</a></div>
              @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped informe responsive">
                <thead>
                  <tr>
                    <th class="visible">#</th>
                    <th class="visible">Nombre</th>
                    <th class="visible">RUC</th>
                    <th class="visible">Correo</th>
                    <th class="visible">Tel&eacute;fono</th>
                    <!--th>C&oacute;digo</th-->
                    <th class="no-print">Opciones</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($socios as $key => $s)
                  <tr>
                    <td>{{($key + 1)}}</td>
                    <td>{{$s->nombre}}</td>
                    <td>{{$s->ruc}}</td>
                    <td>{{strtolower($s->email)}}</td>
                    <td>{{$s->telefono}}</td>
                    <!--td>{{$s->codigo}}</td-->
                    <td>
                        @if(session('ver'))
                        <a href="{{url('show_socio',$s->id)}}" class="btn btn-primary"><i class="fa fa-eye" title="Ver"></i></a>
                        @endif
                        @if(session('editar'))
                        <a href="{{url('edit_socio',$s->id)}}" class="btn btn-warning"><i class="fa fa-edit" title="Editar"></i></a>
                        @endif
                        @if(session('borrar'))
                        <a href="{{url('destroy_socio',$s->id)}}" class="btn btn-danger"><i class="fa fa-trash" title="Eliminar"></i></a>
                        @endif
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
</div>
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista Socios', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
                    {extend: 'excel', title: 'Lista Socios', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
                    {extend: 'pdf', title: 'Lista Socios', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        },
                        exportOptions: {
					        columns: ':not(.no-print)'
					    }
                    }
                ],
            });
        });

    </script>
@endsection
