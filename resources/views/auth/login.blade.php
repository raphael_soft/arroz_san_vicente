
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ config('app.name') }} | Login</title>
        <meta name="description" content="Sistema de Gestion para la arrocera San Vicente">
            <!-- Favicons -->
            <link rel="icon" href="{{ url('/img/favicon-bar-chart.ico') }}" type="image/x-icon" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{ url('/css/Login/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/Login/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/Login/AdminLTE.css') }}">
        <link rel="stylesheet" href="{{ url('/css/Login/green.css') }}">
        
        <style>
            .login-box-body{
                border-radius: 20px;
            }
            .login-page{
                background: #ffff;
            }
        </style>
    </head>
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                 @if($sistema->logo1 != '')
          <?php $filename = str_replace("public/",'',$sistema->logo1); ?>
          <img src="{{url('/storage/'.$filename)}}" style="width: 100px; max-width: 100px;">
          @endif
                <a href="#" role="button"><b>
                    @if($sistema->membrete != '')
                    <?php echo($sistema->membrete) ?>
                    @else
                    {{$sistema->nombre}}
                    @endif
                </b></a>
            </div>

            <div class="login-box-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        <p class="login-box-msg">{{session('status')}}</p>
                    </div>
                @endif
                @if (session('error_datos'))
                    <div class="alert alert-warning">
                        <p class="login-box-msg">{{session('error_datos')}}</p>
                    </div>
                @endif

                <form method="POST" action="">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="form-group has-feedback">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> ¿ Recordar ?
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">Entrar</button>
                        </div>
                    </div>
                </form>
                {{-- <a href="{{ route('password.request') }}">¿Olvido su clave?</a><br> --}}
            </div>
        </div>

        <script src="{{ url('/js/Login/jquery-2.2.3.min.js') }}"></script>
        <script src="{{ url('/js/Login/bootstrap.js') }}"></script>
        <script src="{{ url('/js/Login/icheck.min.js') }}"></script>
        <script>
            $(function () {
                $('input').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
                increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>
