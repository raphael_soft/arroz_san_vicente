
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ config('app.name') }} | Permiso denegado</title>
        <meta name="description" content="Sistema de Gestion para la arrocera San Vicente">
            <!-- Favicons -->
            <link rel="icon" href="{{ url('/img/favicon-bar-chart.ico') }}" type="image/x-icon" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{ url('/css/Login/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/Login/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/Login/AdminLTE.css') }}">
        <link rel="stylesheet" href="{{ url('/css/Login/blue.css') }}">
        <script src="{{ url('/js/Template/fontawesome.js') }}"></script>
    </head>
    <body class="hold-transition login-page">
        <section class="content">

      <div class="error-page">
        <h2 class="headline text-red">Permiso Denegado</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Oops! Parece que no tienes permiso para acceder a esta area o ejecutar esta acción.</h3>

          <p>
            Si piensas que es un error, por favor ponte en contacto con un administrador de sistema.<br/>
            <a href="{{url('/')}}" class="btn btn-primary btn-lg">Ir a inicio</a> 
            <a href="{{url('/salir')}}" class="btn btn-success btn-lg">Salir e iniciar sesión con otra cuenta</a> 
          </p>

          
        </div>
      </div>
      <!-- /.error-page -->

    <div>

</div></section>
        <script src="{{ url('/js/Login/jquery-2.2.3.min.js') }}"></script>
        <script src="{{ url('/js/Login/bootstrap.js') }}"></script>
        
    </body>
</html>
