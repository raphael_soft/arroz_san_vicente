@extends('master')

@section('title', 'Inventario')

@section('active-inventario', 'active')

@if($tipo == "producciones")
@section('active-inventario-produccion', 'active')
@endif
@if($tipo == "productos")
@section('active-inventario-productos', 'active')
@endif

@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Inventario de {{$tipo}}
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Inventario de {{$tipo}}</h3>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped informe table-responsive responsive">
                <thead>
                  <tr>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Costo Unit</th>
                    <th>P. Venta Unit</th>
                    <th>Costo Total</th>
                    <th>Precio Total</th>
                    <th>Ganancia Total</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($inventario as $dato)
                        <tr 
                        @if($dato->cantidad <= 10)
                        style=" background-color: yellow;"
                        @endif
                        >
                            <?php
                                $cantidad = $dato->cantidad;
                                $simbolo_medicion = $dato->producto->medicion->simbolo;
                                $costo_unit = 0;
                                if($tipo == "producciones"){
                                    $costo_unit = $dato->producto->costo_produccion;
                                }
                                //obtenemos el ultimo precio usado para este producto de la tabla de detallefacturas
                                if($tipo == "productos"){
                                    $last = $dato->producto->detalle_factura_compra->filter(function($detalle) use ($tipo){
                                      if($detalle->factura != null)  
                                        return ($detalle->factura->tipo == 1);  //filtramos solo las facturas de tipo de que no es produccion, el 1
                                       else
                                        return false;
                                    })->last();
                                    if($last != null)
                                        $costo_unit = $last->precio;
                                }
                                
                                $precio_unit = $dato->producto->precio_venta;
                                $costo_total = $costo_unit * $cantidad;
                                $precio_total = $precio_unit * $cantidad;
                                $ganancia_total = $precio_total - $costo_total;
                             ?>
                            <td>{{ $dato->producto->nombre }}</td>
                            <td>{{ round($cantidad,2).' '.$simbolo_medicion }}</td>
                            <td>{{$sistema->currency->symbol}} {{ round($costo_unit,2) }}</td>
                            <td>{{$sistema->currency->symbol}} {{ round($precio_unit,2) }}</td>
                            <td>{{$sistema->currency->symbol}} {{ round($costo_total,2) }}</td>
                            <td>{{$sistema->currency->symbol}} {{ round($precio_total,2) }}</td>
                            <td>{{$sistema->currency->symbol}} {{ round($ganancia_total,2) }} {{ $costo_total > 0 ? ('= '.round((($ganancia_total * 100) / $costo_total),2).'%') : '' }}</td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
    </section>
</div>

    <script>

        $(document).ready(function(){

            $('.informe').DataTable({

                dom: '<"html5buttons"B>lTfgitp',

                select: true,

                buttons: [

                    {extend: 'csv', title: 'Inventario de {{$tipo}}'},

                    {extend: 'excel', title: 'Inventario de {{$tipo}}'},

                    {extend: 'pdf', title: 'Inventario de {{$tipo}}'},



                    {extend: 'print',

                        customize: function (win){

                            $(win.document.body).addClass('white-bg');

                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                        }

                    }

                ],

            });

        });



    </script>

@endsection

