@extends('master')
@section('title', 'Inventario')
@section('active-inventario', 'active')
@section('active-inventario-inventario', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Inventario
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Inventario</h3>
              
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped informe">
                <thead>
                  <tr>
                    <th>Producto</th>
                    <th>Stock</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>

                @foreach($inventario as $i)
                    <?php
                       $producto = DB::table('productos')->where('id', $i->producto_id)->get();
                       $datos = json_decode($producto,true);?>
                  <tr>
                    <td>{{$datos[0]['nombre']}}</td>
                    <td>{{$i->n_productos}}</td>
                    <td>
                        @if ($i->status == 1)
                            Activo
                        @else
                            Inactivo
                        @endif
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
</div>
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista Reses'},
                    {extend: 'excel', title: 'Lista Reses'},
                    {extend: 'pdf', title: 'Lista Reses'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>
@endsection
