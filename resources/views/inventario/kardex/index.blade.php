@extends('master')

@section('title', 'Kardex')

@section('active-inventario', 'active')

@section('active-inventario-kardex', 'active')

@section('content')

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Kardex

        </h1>

        <ol class="breadcrumb">

            <li class="active">

                <a href="{{ url('/"') }}">

                    <i class="fa fa-dashboard">

                    </i>

                    Inicio

                </a>

            </li>

        </ol>

    </section>

    <section class="content">

       <div class="box">

            <div class="box-header">

              <h3 class="box-title">Listado de Movimientos del Sistema</h3>

              

                @if (session('status'))

                    <div class="alert alert-success">

                        {{session('status')}}

                    </div>

                @endif

            </div>

            <!-- /.box-header -->

            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped informe">

                <thead>

                  <tr>

                    <th>Producto</th>
                    <th>Cantidad</th>

                    <th>Fecha</th>

                    <th>Tipo Movimiento</th>

                  </tr>

                </thead>

                <tbody>



                @foreach($datos as $m)
                    @if($m->tipo == "COMPRA")
                    <?php $nombre = DB::table('productos')->where('id', $m->producto_id)->value('nombre'); ?>
                      <tr>

                        <td>{{$nombre}}</td>

                        <td>{{$m->cantidad}}</td>

                        <td>{{$m->fecha}}</td>

                        <td>{{$m->tipo}}</td>

                      </tr>
                    @else

                        <?php $nombre = DB::table('tipo_producciones')->where('id', $m->producto_id)->value('nombre'); ?>
                      <tr>

                        <td>{{$nombre}}</td>

                        <td>{{$m->cantidad}}</td>

                        <td>{{$m->fecha}}</td>

                        <td>{{$m->tipo}}</td>

                      </tr>

                    @endif

                @endforeach

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->



    </section>

</div>

    <script>

        $(document).ready(function(){

            $('.informe').DataTable({

                dom: '<"html5buttons"B>lTfgitp',

                select: true,

                buttons: [

                    {extend: 'csv', title: 'Lista Reses'},

                    {extend: 'excel', title: 'Lista Reses'},

                    {extend: 'pdf', title: 'Lista Reses'},



                    {extend: 'print',

                        customize: function (win){

                            $(win.document.body).addClass('white-bg');

                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                        }

                    }

                ],

            });

        });



    </script>

@endsection

