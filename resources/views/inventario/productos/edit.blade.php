@extends('master')
@section('title', 'Editar Producto')
@section('active', 'active')
@section('active-confproductos', 'active')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edici&oacute;n de Productos
            <small>Secci&oacute;n para editar productos</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Inventario</a></li>
            <li><a href="{{url('productos')}}">Productos</a></li>
            <li class="active">Editar Producto</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario para editar Producto</h3>

            </div>
            <div class="box-body">
                
                          <!-- Horizontal Form -->
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                             <form  method="POST" id="edit-form">
                               <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                              <div class="form-group col-sm-3">
                              <label class="control-label"><strong style="color:red;">*</strong>&nbsp;<strong>Codigo</strong></label>
                              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                <input  type="text" id="codigo" required="" name="codigo" value="{{$producto->codigo}}" class="form-control" placeholder="">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong style="color:red;">*</strong>&nbsp;<strong>Nombre</strong></label>
                              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                <input  type="text" name="nombre" required="" value="{{$producto->nombre}}" class="form-control" placeholder="Nombre del Producto">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong style="color:red;">*</strong>&nbsp;<strong>Categoria</strong></label>
                              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-folder"></i></span>

                                <select name="categoria" id="categoria" class="form-control" required="">
                                    <option value="" selected="">Escoja una categoria</option>
                                    @if(isset($categorias))
                                    @foreach($categorias as $s)
                                    <option value="{{$s->id}}"
                                        @if($producto->categoria->id == $s->id)
                                        selected
                                        @endif
                                      >{{$s->nombre}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon" style="cursor: pointer;" title="Agregar una nueva categoria" data-toggle="modal" data-target="#modalCategoria"><i class="fa fa-plus"></i></span>
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong style="color:red;">*</strong>&nbsp;<strong>Subcategoria</strong></label>
                              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-folder-open"></i></span>
                                <select name="subcategoria" id="subcategoria" class="form-control" required="">
                                    <option value="">Escoja una subcategoria</option>
                                    @if(isset($producto->categoria->subcategorias))
                                    @foreach($producto->categoria->subcategorias as $s)
                                    <option value="{{$s->id}}"
                                        @if($producto->subcategoria->id == $s->id)
                                        selected = "selected"
                                        @endif
                                      >{{$s->nombre}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon" style="cursor: pointer;" title="Agregar una nueva subcategoria" data-toggle="modal" data-target="#modalSubcategoria"><i class="fa fa-plus"></i></span>
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong style="color:red;">*</strong>&nbsp;<strong>Medicion</strong></label>
                              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-balance-scale"></i></span>
                                <select name="medicion" id="medicion" class="form-control" required="">
                                    <option value="" selected="">Medicion</option>
                                    @if(isset($mediciones))
                                    @foreach($mediciones as $s)
                                    <option value="{{$s->id}}"
                                        @if($producto->medicion->id == $s->id)
                                        selected
                                        @endif
                                      >{{$s->nombre}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon" style="cursor: pointer;" title="Agregar un nuevo tipo de medicion" data-toggle="modal" data-target="#modalMedicion"><i class="fa fa-plus"></i></span>
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong style="color:red;">*</strong>&nbsp;<strong>Costo x Produccion</strong></label>
                              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-balance-scale"></i></span>
                                <input  type="number" name="costo_produccion" id="costo_produccion" required="" min="0" value="{{$producto->costo_produccion}}" class="form-control" placeholder="Precio de Compra por produccion">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong style="color:red;">*</strong>&nbsp;<strong>Precio de Venta</strong></label>
                              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-balance-scale"></i></span>
                                <input  type="number" name="precio_venta" id="precio_venta" required="" min="0" value="{{$producto->precio_venta}}" class="form-control" placeholder="Precio de Venta">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
      <label><strong style="color:red;">*</strong>&nbsp;<strong>IVA</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Seleccione aqui el IVA que paga el producto. Si el producto no paga IVA, seleccione la opcion No paga IVA" style="cursor: pointer;"></i></label>
      @php
        if($producto->iva != null){
          $iva_valor = $producto->iva->valor.'%';
          $iva_id = $producto->iva->id;
        }
        else{
          $iva_valor = "N/A";  
          $iva_id = null;
        }
        @endphp
       
        <select name="iva" id="iva" class="form-control" required="" >
                                    <option value="null">No paga IVA</option>
                                    @if(isset($ivas))
                                    @foreach($ivas as $s)
                                    <option value="{{$s->id}}"
                                      @if((old('iva') != null and old('iva') == $s->id) or $iva_id == $s->id)
                                        selected
                                      @endif
                                      >{{$s->nombre}} {{$s->valor}} %</option>
                                    @endforeach
                                    @endif
                                </select>
        
      
    </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong>Status</strong></label>
                              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-check-square"></i></span>
                                <select name="status" id="status" class="form-control" required="">
                                    <option value="1"
                                        @if($producto->status == 1)
                                        selected
                                        @endif
                                      >ACTIVO</option>
                                      <option value="0"
                                        @if($producto->status == 0)
                                        selected
                                        @endif
                                      >INACTIVO</option>
                                </select>
                              </div>
                            </div>
                        
                        
                     
                   <div class="box-footer col-sm-12">
                            
                            <button id="btn_agregar_producto" type="submit" class="btn btn-info pull-right btn_finish"><i class="fa fa-upload"></i> Guardar</button>
                            <a onclick="window.history.back()" class="btn btn-default"><< Volver</a>
                          </div>
                     </form>
                </div>

                <!-- /.box -->
              </div>
            

        </section>
        <!-- /.content -->
    </div>
 @include('modales.modal-categoria')   
 @include('modales.modal-subcategoria')
 @include('modales.modal-medicion')
 <script type="text/javascript">

   $(document).keypress(function(e) {
    if(e.which == 13) { // al presionar de enter, simulamos la presion de tabulacion para ir al siguiente focus
      //comprobamos si no estamos en la fila de productos, si estamos, verificamos si estamos en el boton agregar, si estamos en el boton agregar, se regresa el focus al imput producto
      e.preventDefault();
      element = $( document.activeElement );
      
      if(!element.hasClass('btn_finish')){
      
      $('input, select, textarea,button')
         [$('input,select,textarea,button').index(element)+1].focus();
      newFocused = $( document.activeElement );   
      newFocused.select();
      }
      else{
      
        element.click();
      }
    }
});
  
 $(document).ready(function(){
    $('#codigo').focus();
    $('#btn_agregar_producto').click(function(e){
        //dejar que haga submit
    });

    $('#categoria').on('change',function(){
          cargarSubcategorias1();
      });

    $('#edit-form').on('submit', function(e){
        /*
        esto se comenta para hacer posible el registro de productos que no se usaran para la venta, sino como insumos, donde estos tienen un costo mas no un precio de venta. Por lo que al registrarlos, el precio de venta sera cero y el costo mayor a cero
        if(Number($('#costo_produccion').val()) >= Number($('#precio_venta').val()))
        {
          alert('El costo de produccion no puede ser mayor ni igual al precio de venta. Por favor verifique.');
          e.preventDefault();
        }*/

    });

  });  
 function cargarSubcategorias1(){
    id_categoria = $('#categoria').val();
    if(id_categoria == 0){
      return;
    }
        $.ajax({
          headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url: "{{url('cargar-subcategorias')}}",
            data: {
              categoria : id_categoria
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok")
                $("#subcategoria").html(data.options);
            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
    }   
</script>
@endsection
