@extends('master')
@section('title', 'Productos')
@section('active', 'active')
@section('active-confproductos', 'active')
@section('content')

@php

use App\Sistema;

$sistema = Sistema::first();

@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Productos
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado de {{$titulo_lista}}</h3>
              
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                @endif
              <div><a href="{{url('create_producto')}}" class="btn btn-primary" style="float: right;">Nuevo Producto</a></div>
              <div class="btn-group">
                  <button type="button" class="btn btn-info">Mostrar por IVA</button>
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only"></span>
                </button>
                  <ul class="dropdown-menu" role="menu">
                      <li><a href="{{url('conf_productos','todos')}}">Mostrar todos</a></li>
                      <li><a href="{{url('conf_productos','con_iva')}}">Mostrar los que pagan IVA</a></li>
                      <li><a href="{{url('conf_productos','sin_iva')}}">Mostrar los que no pagan IVA</a></li>
                  </ul>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive informe responsive">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Categoria</th>
                    <th>Subcategoria</th>
                    <th style="max-width: 50px;">IVA</th>
                    <th style="max-width: 80px;">Costo x Produccion</th>
                    <th style="max-width: 80px;">Precio Venta</th>
                    <th style="max-width: 80px;">Status</th>
                    <th class="no-print" style="max-width: 100px;">Opciones</th>
                  </tr>
                </thead>
                <tbody>

                @foreach($productos as $key => $p)
                    
                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$p->codigo}}</td>
                    <td>{{$p->nombre}}</td>
                    <td>{{$p->categoria->nombre}}</td>
                    <td>{{$p->subcategoria->nombre}}</td>
                    <td>{{isset($p->iva) ? $p->iva->valor.' %' : "N/A"}}</td>
                    <td>{{$sistema->currency->symbol}} {{$p->costo_produccion}}</td>
                    <td>{{$sistema->currency->symbol}} {{$p->precio_venta}}</td>
                    <td>
                        @if ($p->status == 1)
                            Activo
                        @else
                            Inactivo
                        @endif
                    </td>
                    <td >
                        <a href="{{url('show_producto',$p->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>

                    <a href="{{url('/edit_producto',$p->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a href="javascript: eliminar({{$p->id}},'{{url('/destroys',$p->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
</div>

    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: "{{$titulo_lista}}", exportOptions: {
					        columns: ':not(.no-print)'
					    } },
                    {extend: 'excel', title: "{{$titulo_lista}}", exportOptions: {
					        columns: ':not(.no-print)'
					    } },
                    {extend: 'pdf', title: "{{$titulo_lista}}", exportOptions: {
					        columns: ':not(.no-print)'
					    } },

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        },
                        exportOptions: {
					        columns: ':not(.no-print)'
					    }
                    }
                ],
            });
        });
        function eliminar(id,url){
            resp = confirm("Al eliminar un producto este no podra ser recuperado y se eliminará de todos los registros asociados:\n"+
                "- Compras\n"+
                "- Ventas\n"+
                "- Inventario\n"+
                "¿Esta seguro que desea eliminar este registro?");
            if(resp){
                window.location.href = url;
            }
        }
    </script>
@endsection
