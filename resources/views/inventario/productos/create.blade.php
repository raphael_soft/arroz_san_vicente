@extends('master')
@section('title', 'Productos')
@section('active', 'active')
@section('active-confproductos', 'active')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Registro de Productos
            <small>Secci&oacute;n para el registro de producto</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Inventario</a></li>
            <li><a href="{{url('productos')}}">Productos</a></li>
            <li class="active">Nuevo Producto</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario para el registro de productos</h3>

            </div>
            <div class="box-body">
                
                          <!-- Horizontal Form -->
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                       <div class="alert alert-success">
                          {{session('status')}}
                       </div>
                   @endif
                   @if (session('error'))
                       <div class="alert alert-danger">
                          {{session('error')}}
                       </div>
                   @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="POST" id="form_producto">
                          <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">

    <div class="form-group col-sm-3">
      <label><strong style="color:red; font-size: 20px;">*</strong>&nbsp;<strong>Categoria</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Todo producto pertenece a una categoria general, como por ejemplo el queso pertenece a la categoria lacteos. Si no encuentra la categoria que busca, puede crear otra aqui mismo." style="cursor: pointer;"></i></label>
      <div class="input-group">
        
          <span class="input-group-addon" style="cursor: pointer;"  title="Agregar una nueva categoria" data-toggle="modal" data-target="#modalCategoria"><i class="fa fa-plus" ></i></span>
       
        <select name="categoria" id="categoria" class="form-control" required="" >
                                    <option value="">Escoja una categoria</option>
                                    @if(isset($categorias))
                                    @foreach($categorias as $s)
                                    <option value="{{$s->id}}"
                                      @if(old('categoria') != null and old('categoria') == $s->id)
                                        selected
                                      @endif
                                      >{{$s->nombre}}</option>
                                    @endforeach
                                    @endif
                                </select>
        
      </div>
    </div>
    <div class="fom-group col-sm-3">
      <label><strong style="color:red; font-size: 20px;">*</strong>&nbsp;<strong>Sub Categoria</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Toda categoria tiene subcategorias, como por ejemplo, los lacteos tienen Liquidos y solidos, o la carne tiene de primera o de segunda. Si no existe la subcategoria que desea puede crearla aqui mismo." style="cursor: pointer;"></i></label>
      <div class="input-group">
        
          <span class="input-group-addon" style="cursor: pointer;" title="Agregar una nueva subcategoria" data-toggle="modal" data-target="#modalSubcategoria"><i class="fa fa-plus"></i></span>
        
        <select name="subcategoria" id="subcategoria" class="form-control" required="" value="{{old('subcategoria')}}">
                                    <option value="" selected="">Escoja una subcategoria</option>
                                    
                                </select>
        
      </div>
    </div>
    <div class="form-group col-sm-3">
      <label><strong style="color:red; font-size: 20px;">*</strong>&nbsp;<strong>Codigo</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Este es el código que tendrá asignado este producto y el cual se usará al momento de comprar o vender productos. Puede dejar el asignado automaticamente o asignar uno propio" style="cursor: pointer;"></i></label>
      <div class="input-group">
          <span class="input-group-addon" ><i class="fa fa-hashtag"></i></span>
        
        <input type="text" class="form-control" id="validationTooltipCodigo" placeholder="Codigo" name="codigo" id="codigo" required="" value="{{old('codigo') != null ? old('codigo') : $codigo }}">
        
      </div>
    </div>
        <div class="form-group col-sm-3">
      <label><strong style="color:red; font-size: 20px;">*</strong>&nbsp;<strong>Nombre</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Este es el nombre del producto que desea agregar. Debe ser unico y descriptivo" style="cursor: pointer;"></i></label>
      <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-font"></i></span>
        <input type="text" class="form-control" placeholder="Nombre del producto" name="nombre" required="" value="{{old('nombre')}}">
      </div>
    </div>

    <div class="form-group col-sm-3">
      <label><strong style="color:red; font-size: 20px;">*</strong>&nbsp;<strong>Udad. Medición</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Todo producto tiene una unidad de medida para su peso, volumen, tamaño, etc. Puede tratarse de Kilogramos, gramos, onzas, toneladas, etc. Si no existe la unidad de medida que desea puede crearla aqui mismo." style="cursor: pointer;"></i></label>
      <div class="input-group">
          <span class="input-group-addon" style="cursor: pointer;"  data-placement="right" title="Agregar una nueva unidad de medicion" data-toggle="modal" data-target="#modalMedicion"><i class="fa fa-plus"></i></span>
        <select name="medicion" id="medicion" class="form-control" required="" value="{{old('medicion')}}">
                                   @if(isset($mediciones) or !empty($mediciones))
                                      @foreach($mediciones as $m)
                                          <option value="{{$m->id}}">{{$m->nombre.' '.$m->simbolo}}</option>
                                      @endforeach
                                   @endif
                                   @if(!isset($mediciones) or empty($mediciones))
                                          <option value="0">No hay registros</option>
                                   @endif
                                </select>
      </div>
    </div>
    <div class="form-group col-sm-3">
      <label><strong style="color:red; font-size: 20px;"></strong>&nbsp;<strong>Costo de producción</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Este es el costo del producto al comprarlo por producción." style="cursor: pointer;"></i></label>
      <div class="input-group">
          <span class="input-group-addon" ><i class="fa fa-money"></i></span>
        
        <input type="number" min="0" class="form-control" value="{{old('costo_produccion')}}" placeholder="Costo de produccion" name="costo_produccion" id="costo_produccion" required="">
        
      </div>
    </div>
    <div class="form-group col-sm-3">
      <label><strong style="color:red; font-size: 20px;"></strong>&nbsp;<strong>Precio de venta</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Este es el precio de venta al publico que tendra este producto y que se usara automaticamente al momento de hacer las ventas y sacar estadisticas." style="cursor: pointer;"></i></label>
      <div class="input-group">
          <span class="input-group-addon" ><i class="fa fa-money"></i></span>
        
        <input type="number" min="0" class="form-control" required="" placeholder="Precio venta" value="{{old('precio_venta')}}" name="precio_venta" id="precio_venta">
        
      </div>
    </div>
     <div class="form-group col-sm-3">
      <label><strong style="color:red; font-size: 20px;">*</strong>&nbsp;<strong>IVA</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Seleccione aqui el IVA que paga el producto. Si el producto no paga IVA, seleccione la opcion No paga IVA" style="cursor: pointer;"></i></label>
      
       
        <select name="iva" id="iva" class="form-control" required="">
                                    <option value="null">No paga IVA</option>
                                    @if(isset($ivas))
                                    @foreach($ivas as $s)
                                    <option value="{{$s->id}}"
                                      @if(old('iva') != null and old('iva') == $s->id)
                                        selected
                                      @endif
                                      >{{$s->nombre}} {{$s->valor}} %</option>
                                    @endforeach
                                    @endif
                                </select>
        
      
    </div>
    
                                
                             
                                  <!-- /.box-body -->
                          <div class="box-footer col-sm-12">
                            <button type="submit" id="btn_agregar_producto" class="btn btn-info pull-right btn_finish"><i class="fa fa-upload"></i> Registrar</button>
                            <a href="{{url('/conf_productos')}}" class="btn btn-default"><< Volver</a>
                            
                          </div>
                  <!-- /.box-footer -->
                        </form>
</div>
                        
                     
            <!-- /.box-body -->
           
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->

        </section>
        <!-- /.content -->
  </div>
  <!--      MODAL WINDOWS   -->
<?php echo view('modales.modal-categoria')->render(); ?>
<?php echo view('modales.modal-subcategoria')->render(); ?>
<?php echo view('modales.modal-medicion')->render(); ?>                  

           

                        <!-- /.MODAL WINDOWS -->
  <!-- /.content-wrapper -->
  <script type="text/javascript">
$(document).ready(function(){
    $('#categoria').focus();
    
      $('#categoria').on('change',function(){
          cargarSubcategorias2();
      });

      $(document).keypress(function(e) {
    if(e.which == 13) { // al presionar de enter, simulamos la presion de tabulacion para ir al siguiente focus
      //comprobamos si no estamos en la fila de productos, si estamos, verificamos si estamos en el boton agregar, si estamos en el boton agregar, se regresa el focus al imput producto
      e.preventDefault();
      element = $( document.activeElement );
      
      if(!element.hasClass('btn_finish')){
      
      $('input, select, textarea,button')
         [$('input,select,textarea,button').index(element)+1].focus();
      newFocused = $( document.activeElement );   
      newFocused.select();
      }
      else{
      
        element.click();
      }
    }
    });

      $('#form_producto').on('submit', function(e){
        //e.preventDefault();
        let costo = $('#costo_produccion').val();
        let precio = $('#precio_venta').val();
        console.log("costo: " + costo);
        console.log("precio: " + precio);
        /*
        esto se comenta para hacer posible el registro de productos que no se usaran para la venta, sino como insumos, donde estos tienen un costo mas no un precio de venta. Por lo que al registrarlos, el precio de venta sera cero y el costo mayor a cero
        if(Number(costo) >= Number(precio))
        {
          alert('El costo de produccion no puede ser mayor ni igual al precio de venta. Por favor verifique.');
          e.preventDefault();
        }
        */

      });
    });
 function cargarSubcategorias2(){
    id_categoria = $('#categoria').val();
    if(id_categoria == 0){
      return;
    }
        $.ajax({
          headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url: "{{url('cargar-subcategorias')}}",
            data: {
              categoria : id_categoria
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok")
                $("#subcategoria").html(data.options);
            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
    }

  </script>
@endsection
