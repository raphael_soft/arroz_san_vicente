@extends('master')
@section('title', 'Ver Producto')
@section('active', 'active')
@section('active-confproductos', 'active')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Datos del Producto
            <small>Secci&oacute;n para visualizar los datos del Producto</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Asociaci&oacute;n</a></li>
            <li><a href="{{url('productos')}}">Producto</a></li>
            <li class="active">Ver Producto</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box container">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario de datos del Producto</h3>
            </div>
            <div class="box-body col-sm-12">
                
                          <!-- Horizontal Form -->
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="POST">
                          <div class="box-body">
                             <div class="box-body">
                              <div class="form-row">
                              <div class="form-group col-sm-3">
                              <label class="control-label"><strong>Codigo</strong></label>
                              <div>
                                <input readonly type="text" name="codigo" value="{{$producto->codigo}}" class="form-control numeros" placeholder="">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong>Categoria</strong></label>
                              <div>
                                <input readonly type="text" name="categoria" value="{{$producto->categoria->nombre}}" class="form-control numeros" placeholder="">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong>Subcategoria</strong></label>
                              <div>
                                <input readonly type="text" name="subcategoria" value="{{$producto->subcategoria->nombre}}" class="form-control numeros" placeholder="">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong>Nombre</strong></label>
                              <div>
                                <input  readonly type="text" name="nombre" value="{{$producto->nombre}}" class="form-control" placeholder="Nombre del Producto">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong>Medicion</strong></label>
                              <div>
                                <input readonly type="text" name="medicion" value="{{$producto->medicion->nombre}}" class="form-control numeros" placeholder="Unidad de medicion">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong>Costo x Produccion</strong></label>
                              <div>
                                <input readonly type="text" name="costo_produccion" value="{{$producto->costo_produccion}}" class="form-control numeros" placeholder="Precio de compra por produccion">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong>Precio Venta</strong></label>
                              <div>
                                <input readonly type="text" name="precio_venta" value="{{$producto->precio_venta}}" class="form-control numeros" placeholder="Precio de venta">
                              </div>
                            </div>
                            <div class="form-group col-sm-3">
      <label><strong>IVA</strong></label>
      <div>
        @php
        if($producto->iva != null)
          $iva_valor = $producto->iva->valor.'%';
        else
          $iva_valor = "N/A";  
        @endphp
                                <input readonly type="text" name="iva" value="{{$iva_valor}}" class="form-control">
                              </div>
    </div>
                            <div class="form-group col-sm-3">
                              <label class="control-label"><strong>Status</strong></label>
                              <div>
                                @if($producto->status == 1)
                                <input readonly="readonly" value="Activo" type="text" name="status" class="form-control" placeholder="Status">
                                @else
                                <input readonly="readonly" value="Inactivo" type="text" name="status" class="form-control" placeholder="Status">
                                @endif
                              </div>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                     </div>
                   </div>
                     </form>
                          <!-- /.box-body -->
                  <div class="box-footer col-sm-12 text-center">
                    <a class="btn btn-default" onclick="window.history.back()"><< Volver</a>
                    <a href="{{url('/edit_producto',$producto->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                    <a href="javascript: eliminar({{$producto->id}},'{{url('/destroys',$producto->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                  </div>
                  <!-- /.box-footer -->
            
          
          <!-- /.box -->
        </div>



          </div>
          <!-- /.box -->

        </section>
        <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   
@endsection
