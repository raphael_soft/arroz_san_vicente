@extends('master')

@section('title', 'Inventario')

@section('active-inventario', 'active')

@section('active-inventario-categorias', 'active')

@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Inventario por categorias de productos
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Inventario por categorias agrupadas por nombre de categoria y tipo de medicion</h3>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped informe">
                <thead>
                  <tr>
                    <th>Categoria de Producto</th>
                    <th>Cantidad</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($inventario as $dato)
                        <tr 
                        @if($dato->cantidad <= 10)
                        style=" background-color: yellow;"
                        @endif
                        >
                            <td>{{ $dato->nombre }}</td>
                            <td>{{ round($dato->cantidad,2).' '.$dato->simbolo }}</td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
    </section>
</div>

    <script>

        $(document).ready(function(){

            $('.informe').DataTable({

                dom: '<"html5buttons"B>lTfgitp',

                select: true,

                buttons: [

                    {extend: 'csv', title: 'Inventario por categorias'},

                    {extend: 'excel', title: 'Inventario por categorias'},

                    {extend: 'pdf', title: 'Inventario por categorias'},



                    {extend: 'print',

                        customize: function (win){

                            $(win.document.body).addClass('white-bg');

                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                        }

                    }

                ],

            });

        });



    </script>

@endsection

