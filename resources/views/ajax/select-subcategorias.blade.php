
@if(isset($subcategorias) and !empty($subcategorias))
<option value="" selected>Escoja una subcategoria</option>
  @foreach($subcategorias as $key => $value)
    <option value="{{ $value->id }}"
    	@if(old('subcategoria') != null and old('subcategoria') == $value->id)
                                        selected
                                      @endif
    	>{{ $value->nombre }}</option>
  @endforeach
@endif