
@if(!empty($collection))
  @foreach($collection as $key => $value)
  <option value="{{$value->id}}" id="prod_{{$value->id}}" medicion="{{$value->medicion->simbolo}}" codigo="{{$value->codigo}}" title="{{$value->nombre}}" costo-prod="{{$value->costo_produccion}}" precioventa="{{$value->precio_venta}}" iva-val="{{isset($value->iva) ? $value->iva->valor : null}}" iva-id="{{isset($value->iva) ? $value->iva->id : null}}">{{$value->codigo}} {{strtoupper($value->nombre)}} {{$value->medicion->nombre}}</option>
  @endforeach
@endif