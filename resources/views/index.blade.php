@extends('master')
@section('title', 'Inicio')
{{-- @section('class_active_home', 'active') --}}
@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">

    <section class="content-header">
        <h1>
            Inicio
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="/">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
            @if(session('superuser'))
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>
                            {{$users}}
                        </h3>
                        <p>
                            Usuarios
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users">
                        </i>
                    </div>
                    <a class="small-box-footer" href="/configuracion/usuario">
                        Ver usuarios
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            @endif
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{$compras_produccion}} <b style="font-size: medium;">Compras</b></h3> 

                        <p>
                            Producción
                        </p>
                    </div>
                    <div class="icon">
                         <i class="fa fa-shopping-bag">
                        </i>
                    </div>
                    <a class="small-box-footer" href="/facturas_compras/producciones">
                        Ver compras
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                      
                            <h3>{{$compras_productos}} <b style="font-size: medium;">Compras</b></h3> 
                        <p>
                            Productos
                        </p>
                    </div>
                    <div class="icon">
                         <i class="fa fa-shopping-bag">
                        </i>
                    </div>
                    <a class="small-box-footer" href="/facturas_compras/productos">
                        Ver compras
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3>
                            {{$socios}}
                        </h3>
                        <p>
                            Proveedores
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user-plus">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{ url('/socios') }}">
                        Ver proveedores
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            {{$ventas}}
                        </h3>
                        <p>
                            Ventas
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-usd">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{ url('/facturas_ventas') }}">
                        Ver ventas
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <!-- nuevos -->
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3>{{$inventario_productos}} <b style="font-size: medium;">Productos</b></h3> 
                        <p>
                            Inv. de Productos
                        </p>                        
                    </div>
                    <div class="icon">
                         <i class="fa fa-cubes">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{route('inventario','productos')}}">
                        Ver inventario
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                      
                            <h3>{{$inventario_produccion}} <b style="font-size: medium;">Productos</b></h3> 
                        <p>
                            Inv. de Produccion
                        </p>
                    </div>
                    <div class="icon">
                         <i class="fa fa-shopping-bag">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{route('inventario','producciones')}}">
                        Ver inventario
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>
                            {{$revisiones}}
                        </h3>
                        <p>
                            Revisiones
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user-plus">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{ route('revisiones') }}">
                        Ver revisiones
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-olive">
                    <div class="inner">
                        <h3>
                            {{$terrenos}}
                        </h3>
                        <p>
                            Terrenos
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-usd">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{ route('terrenos') }}">
                        Ver terrenos
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-maroon">
                    <div class="inner">
                        <h3>
                            {{$insumos}}
                        </h3>
                        <p>
                            Insumos
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-usd">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{ route('insumos') }}">
                        Ver insumos
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                <!-- small box -->
                <div class="small-box bg-light-blue">
                    <div class="inner">
                        <h3>
                            {{$cosechas}}
                        </h3>
                        <p>
                            Cosechas
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-usd">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{ route('cosechas') }}">
                        Ver cosechas
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <!-- fin nuevos -->
            <div class="col-lg-12 col-xs-12">
                <!-- small box -->
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        
                <script type="text/javascript">
                    Highcharts.chart('container', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Estadisticas de Ingresos, Egresos y Utilidades: <b>{{$anio_actual}}</b>'
                        },
                        subtitle: {
                            text: 'Cantidad de Ingresos en {{$sistema->currency->name}} {{$sistema->currency->symbol}}'
                        },
                        xAxis: {
                            categories: [
                                'Ene',
                                'Feb',
                                'Mar',
                                'Abr',
                                'May',
                                'Jun',
                                'Jul',
                                'Ago',
                                'Sep',
                                'Oct',
                                'Nov',
                                'Dic'
                            ],
                            crosshair: true
                        },
                        yAxis: {
                            
                            title: {
                                text: 'Ingresos en {{$sistema->currency->symbol}}'
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{{$sistema->currency->symbol}} {point.y:.1f}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0,
                                
                            },
                            series: {
				                cursor: 'pointer',
				                point: {
				                    events: {
				                        click: function (e) {
				                        	let year = "{{ date('Y') }}";
				                        	let mes = this.x + 1;
				                        	let url = "{{ url('/facturas_ventas') }}";
				                        	let from = year + "-" + mes + "-01";
				                        	let to = year + "-" + mes + "-31";
				                        	window.location.href = url + "?from=" + from + "&to=" + to;
				                        }
				                    }
				                },
				                marker: {
				                    lineWidth: 1
				                }
				            }
                        },
                        series: [{
                            name: 'Ingresos',
                            data: @json($ingresos)
                        	},{
                                name: 'Egresos',
                                data: @json($egresos)
                            },{
                             name: 'Utilidades',
                             data: @json($utilidades)
                            },
                            ]
                    });
                </script>
            </div>

            

            <!--div class="col-lg-6 col-xs-6">
                
                <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        
                <script type="text/javascript">
                    Highcharts.chart('container2', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Estadisticas de Egresos: <b>{{$anio_actual}}</b>'
                        },
                        subtitle: {
                            text: 'Cantidad de Egresos'
                        },
                        xAxis: {
                            categories: [
                                'Ene',
                                'Feb',
                                'Mar',
                                'Abr',
                                'May',
                                'Jun',
                                'Jul',
                                'Ago',
                                'Sep',
                                'Oct',
                                'Nov',
                                'Dic'
                            ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Egresos'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} $</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0,
                                color: "orange",
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        series: [{
                            name: 'Egresos',
                            data: [
                            
                               @foreach($egresos as $c)
                                    {{$c}},
                                @endforeach

                            ]

                        }]
                    });
                </script>
            </div-->
        </div>
        
    </section>
</div>
@endsection
