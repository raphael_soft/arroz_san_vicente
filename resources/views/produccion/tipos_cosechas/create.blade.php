@extends('master')
@section('title', 'Tipos de Cosechas')
@section('active-produccion', 'active')
@section('active-produccion-tipos', 'active')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Registro de Tipos de Cosecha
    <small>Secci&oacute;n para el registro de Tipo de Cosecha</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    </ol>
  </section>
  @foreach($errors->all() as $error)
  <p class="alert alert-danger">{{$error}}</p>
  @endforeach
  @if (session('status'))
  <div class="alert alert-success">
    {{session('status')}}
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Formulario para el registro de tipo de cosecha</h3>
          </div>
          <form  method="POST" class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Nombre</label>
                <div class="col-sm-10">
                  <input type="text" name="nombre" class="form-control" placeholder="Nombre del tipo de cosecha" required="required">
                </div>
              </div>
            </div>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>          
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="{{route('tipos_cosechas')}}" type="submit" class="btn btn-default"><< Volver</a>
              <button type="submit" class="btn btn-info pull-right"><i class="fa fa-upload"></i> Registrar</button>
            </div>
            <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>

@endsection