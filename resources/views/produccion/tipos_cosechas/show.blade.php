@extends('master')
@section('title', 'Tipo De Cosecha')
@section('active-produccion', 'active')
@section('active-produccion-tipos', 'active')
@section('content')

  <div class="content-wrapper">

      <section class="content-header">
          <h1>
            Datos del Tipo de Cosecha
            <small>Secci&oacute;n para visualizar los datos del tipo de cosecha</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            
          </ol>
        </section>

      @foreach($errors->all() as $error)
      <p class="alert alert-danger">{{$error}}</p>
    @endforeach

    @if (session('status'))
      <div class="alert alert-success">
        {{session('status')}}
      </div>
    @endif

      <section class="content">
        <div class="row">
          <div class="col-md-3">
            
          </div>
          <div class="col-md-12 box-body">
                <div class="box box-widget widget-user-2">
                   <form  method="POST" class="form-horizontal">
                          <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Nombre</label>
                              <div class="col-sm-10">
                                <input value="{{$tipo->nombre}}" type="text" name="nombre" class="form-control" placeholder="Nombre del tipo de cosecha" @if($mode == 'show') readonly="readonly" @endif>
                              </div>
                            </div>                            
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                     </div>
                          <!-- /.box-body -->
                  <div class="box-footer col-sm-12 text-center">
                    @if($mode == 'show')
                        <a href="{{route('tipos_cosechas')}}"  class="btn btn-default"><< Volver</a>
                        <a href="{{route('edit_tipo_cosecha',$tipo->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                    @else($mode == 'edit')
                        <a href="{{route('show_tipo_cosecha', $tipo->id)}}"  class="btn btn-default"><< Volver</a>
                        <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Guardar</button>
                    @endif
                    <a href="{{route('destroy_tipo_cosecha',$tipo->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                  </div>
                  <!-- /.box-footer -->
            </form>
                 
                </div>
            </div>
          <div class="col-md-3"></div>
      </div>
      </section>

@endsection
