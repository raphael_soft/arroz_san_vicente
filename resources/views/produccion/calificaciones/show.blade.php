@extends('master')
@section('title', 'Calificaciones')
@section('active-produccion', 'active')
@section('active-produccion-cosechas', 'active')
@section('active-cosechas-calificaciones', 'active')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Calificacion # {{ $calificacion->id }}
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
    </ol>
  </section>
  @foreach($errors->all() as $error)
  <p class="alert alert-danger">{{$error}}</p>
  @endforeach
  @if (session('status'))
  <div class="alert alert-success">
    {{session('status')}}
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Calificacion #{{ $calificacion->id }}</h3>
          </div>
          <form  method="POST" class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Fecha</label>
                <div class="col-sm-10">
                  <input type="date" name="fecha" class="form-control" placeholder="Fecha" required="required" value="{{ date('Y-m-d', strtotime($calificacion->fecha)) }}" readonly="readonly">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Cosecha</label>
                <div class="col-sm-10">
                  <select name="cosecha" class="form-control" required="required" readonly="readonly">
                    <option value="">Seleccione la cosecha</option>
                    @foreach($cosechas as $h)
                      <option value="{{$h->id}}" @if($calificacion->cosecha_id == $h->id) selected @endif>{{$h->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>              
              <div class="form-group">
                <label class="col-sm-2 control-label">Peso en Kg</label>
                <div class="col-sm-10">
                  <input type="number" name="peso" min="1" max="10000" maxlength="4" minlength="1"  class="form-control" placeholder="Peso de la cosecha" required="required" value="{{ $calificacion->peso }}" readonly="readonly">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Calificacion</label>
                <div class="col-sm-10">
                  <input type="number" min="1" max="100" maxlength="3" minlength="1" name="calificacion" class="form-control" placeholder="calificacion" required="required" value="{{ $calificacion->calificacion }}" readonly="readonly">
                </div>
              </div>
            </div>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>          
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="{{route('calificaciones')}}"  class="btn btn-default"><< Volver</a>
              <a href="{{route('edit_calificacion',$calificacion->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
              <a href="{{route('destroy_calificacion',$calificacion->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
            </div>
            <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>
</div>
@endsection