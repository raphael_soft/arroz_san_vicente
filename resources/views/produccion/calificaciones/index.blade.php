@extends('master')
@section('title', 'Calificaciones')
@section('active-produccion', 'active')
@section('active-produccion-cosechas', 'active')
@section('active-cosechas-calificaciones', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Calificaciones
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado de Calificaciones</h3>
              
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                @endif
              <div><a href="{{route('create_calificacion')}}" class="btn btn-primary" style="float: right;">Nueva Calificacion</a></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped responsive informe">
                <thead>
                  <tr>
                    <th>C&oacute;digo</th>
                    <th>Cosecha</th>
                    <th>Tipo</th>
                    <th>Peso</th>
                    <th>Calificacion</th>
                    <th>Fecha</th>
                    <th class="no-print">Opciones</th>
                  </tr>
                </thead>
                <tbody>

                @foreach($calificaciones as $t)
                  <tr>
                    <td>{{$t->id}}</td>
                    <td>{{ $t->cosecha !== null ? $t->cosecha->nombre: 'No asignado' }}</td>
                    <td>{{ $t->cosecha !== null && $t->cosecha->tipo !== null ? $t->cosecha->tipo->nombre : 'No asignado' }}</td>
                    <td>{{ $t->peso }}</td>
                    <td>{{ $t->calificacion }}</td>
                    <td>{{ $t->fecha }}</td>
                    <td><a href="{{route('show_calificacion',$t->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i> Ver</a></td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
</div>
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Calificaciones', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
                    {extend: 'excel', title: 'Lista de Calificaciones', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
                    {extend: 'pdf', title: 'Lista de Calificaciones', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        },
                        exportOptions: {
					        columns: ':not(.no-print)'
					    }
                    }
                ],
            });
        });

    </script>
@endsection
