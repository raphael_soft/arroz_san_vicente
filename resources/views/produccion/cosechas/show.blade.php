@extends('master')
@section('title', 'Cosechas')
@section('active-produccion', 'active')
@section('active-produccion-cosechas', 'active')
@section('active-cosechas-cosechas', 'active')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Cosecha {{$cosecha->nombre}}
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
    </ol>
  </section>
  @foreach($errors->all() as $error)
  <p class="alert alert-danger">{{$error}}</p>
  @endforeach
  @if (session('status'))
  <div class="alert alert-success">
    {{session('status')}}
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Datos de cosecha</h3>
          </div>
          <form  method="POST" class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Fecha</label>
                <div class="col-sm-10">
                  <input type="date" name="fecha" class="form-control" placeholder="Fecha" required="required" value="{{ date('Y-m-d', strtotime($cosecha->fecha)) }}" readonly="readonly">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Nombre</label>
                <div class="col-sm-10">
                  <input type="text" name="nombre" class="form-control" placeholder="Nombre" required="required" value="{{$cosecha->nombre}}" readonly="readonly">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Terreno</label>
                <div class="col-sm-10">
                  <select name="terreno" class="form-control" required="required" readonly="readonly">
                    <option value="">Seleccione el terreno</option>
                    @foreach($terrenos as $h)
                      <option value="{{$h->id}}" @if($cosecha->terreno_id !== null && $cosecha->terreno_id == $h->id) selected @endif>{{$h->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Producto</label>
                <div class="col-sm-10">
                  <select name="producto" class="form-control" required="required" readonly="readonly">
                    <option value="">Seleccione el producto</option>
                    @foreach($productos as $h)
                      <option value="{{$h->id}}" @if($cosecha->producto_id !== null && $cosecha->producto_id == $h->id) selected @endif>{{$h->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Tipo de cosecha</label>
                <div class="col-sm-10">
                  <select name="tipo" class="form-control" readonly="readonly">
                    <option value="">Seleccione el tipo</option>
                    @foreach($tipos as $h)
                      <option value="{{$h->id}}" @if($cosecha->tipo_id !== null && $cosecha->tipo_id == $h->id) selected @endif>{{$h->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Peso en Kg</label>
                <div class="col-sm-10">
                  <input type="number" name="peso" class="form-control" placeholder="Peso de la cosecha" required="required" value="{{$cosecha->peso}}" readonly="readonly">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Precio de venta</label>
                <div class="col-sm-10">
                  <input type="number" name="precio_venta" class="form-control" placeholder="Precio de venta" required="required" value="{{$cosecha->precio_venta}}" readonly="readonly">
                </div>
              </div>
            </div>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>          
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="{{route('cosechas')}}"  class="btn btn-default"><< Volver</a>
              <a href="{{route('edit_cosecha',$cosecha->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
              <a href="{{route('destroy_cosecha',$cosecha->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
            </div>
            <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>
</div>
@endsection