@extends('master')
@section('title', 'Cosechas')
@section('active-produccion', 'active')
@section('active-produccion-cosechas', 'active')
@section('active-cosechas-cosechas', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Cosechas
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado de Cosechas</h3>
              
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                @endif
              <div><a href="{{route('create_cosecha')}}" class="btn btn-primary" style="float: right;">Nueva Cosecha</a></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="table-responsive">	
	              	<table id="example1" class="table table-bordered table-striped informe responsive">
		                <thead>
		                  <tr>
		                    <th>C&oacute;digo</th>
		                    <th>Terreno</th>
		                    <th>Nombre</th>
		                    <th>Producto</th>
		                    <th>Peso</th>
		                    <th>Tipo</th>
		                    <th>Precio</th>
		                    <th>Fecha</th>
		                    <th class="no-print">Opciones</th>
		                  </tr>
		                </thead>
		                <tbody>

		                @foreach($cosechas as $t)
		                  <tr>
		                    <td>{{$t->id}}</td>
		                    <td>{{ $t->terreno !== null ? $t->terreno->nombre: 'No asignado'}}</td>
		                    <td>{{$t->nombre}}</td>
		                    <td>{{$t->producto->nombre}}</td>
		                    <td>{{$t->peso}}</td>
		                    <td>{{ $t->tipo !== null ? $t->tipo->nombre: 'No asignado'}}</td>
		                    <td>{{$t->precio_venta}}</td>
		                    <td>{{$t->fecha}}</td>
		                    <td><a href="{{route('show_cosecha',$t->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i> Ver</a></td>
		                  </tr>
		                @endforeach
		                </tbody>
		            </table>
          		</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
</div>
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Cosechas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
                    {extend: 'excel', title: 'Lista de Cosechas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
                    {extend: 'pdf', title: 'Lista de Cosechas', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        },
                        exportOptions: {
					        columns: ':not(.no-print)'
					    }
                    }
                ],
            });
        });

    </script>
@endsection
