@extends('master')
@section('title', 'Terreno')
@section('active-produccion', 'active')
@section('active-produccion-hectareas', 'active')
@section('active-hectareas-terrenos', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Datos del Terreno
            <small>Secci&oacute;n para visualizar los datos del terreno</small>
        </h1>
        <ol class="breadcrumb">
        	<li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        </ol>
    </section>
    @foreach($errors->all() as $error)
      <p class="alert alert-danger">{{$error}}</p>
    @endforeach

    @if (session('status'))
      <div class="alert alert-success">
        {{session('status')}}
      </div>
    @endif
	<section class="content">
        <div class="row">
          	<div class="col-md-12 box-body">
                <div class="box box-widget widget-user-2">
                	<form  method="POST" class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                              	<label class="col-sm-2 control-label">Nombre</label>
                              	<div class="col-sm-10">
                                	<input value="{{$terreno->nombre}}" type="text" name="nombre" class="form-control" placeholder="Nombre del terreno" @if($mode == 'show') readonly="readonly" @endif>
                              	</div>
                            </div>
                            <div class="form-group">
                            	<label class="col-sm-2 control-label">Hectareas</label>
                              	<div class="col-sm-10">
                                	<input value="{{$terreno->hectareas}}" type="number" name="hectareas" class="form-control" placeholder="Hectareas" @if($mode == 'show') readonly="readonly" @endif >
                              </div>
                            </div>                            
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <!-- /.box-body -->
                  		<div class="box-footer col-sm-12 text-center">
	                    @if($mode == 'show')
	                        <a href="{{route('terrenos')}}"  class="btn btn-default"><< Volver</a>
	                        <a href="{{route('edit_terreno',$terreno->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
	                        <a href="{{route('destroy_terreno',$terreno->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
	                    @else($mode == 'edit')
	                        <a href="{{route('show_terreno', $terreno->id)}}"  class="btn btn-default"><< Volver</a>
	                        <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Guardar</button>
	                        <a href="{{route('destroy_terreno',$terreno->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
	                    @endif
	                  	</div>
	                  	<!-- /.box-footer -->
            		</form>                 
                </div>
            </div>
      	</div>
      	<div class="row">
      		<div class="col-md-12">
	      		<div class="box">
		            <div class="box-header">
		              <h3 class="box-title">Listado de Insumos</h3>
		              <div><a href="{{route('asignar_insumo')}}" class="btn btn-primary" style="float: right;">Asignar Insumo</a></div>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <table id="example1" class="table table-bordered table-striped informe table-responsive responsive">
		                <thead>
		                  <tr>
		                    <th>C&oacute;digo</th>
		                    <th>Terreno</th>
		                    <th>Producto</th>
		                    <th>Costo</th>
		                    <th>Cantidad</th>
		                    <th>Costo Total</th>
		                    <th>Fecha</th>
		                    <th>Opciones</th>
		                  </tr>
		                </thead>
		                <tbody>

		                @foreach($terreno->insumos as $t)
		                  <tr>
		                    <td>{{$t->id}}</td>
		                    <td>{{ $t->terreno !== null ? $t->terreno->nombre: 'No asignado'}}</td>
		                    <td>{{ $t->producto !== null ? $t->producto->nombre: 'No asignado'}}</td>
		                    <td>{{$t->costo_unitario}}</td>
		                    <td>{{$t->cantidad}}</td>
		                    <td>{{$t->costo_total}}</td>              
		                    <td>{{$t->fecha}}</td>
		                    <td><a href="{{route('show_insumo',$t->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i> Ver</a></td>
		                  </tr>
		                @endforeach
		                </tbody>
		              </table>
		            </div>
		            <!-- /.box-body -->
		        </div>
        	</div>
      	</div>
      	<div class="row">
      		<div class="col-lg-12">
      		{{-- COSTO Y PRODUCCION MENSUAL --}}
	       	<div class="box">
	            <!-- /.box-header -->
	            <div class="box-body">
	            	@if($mode == "show")
	                <div>
	                    <form method="POST">
	                        Consultar a&ntilde;o 
	                        <select name="anio_costo_prod">
	                            @php
	                            $anio = 2019;
	                            @endphp
	                            @while($anio <= $anio_actual)
	                            <option value="{{$anio}}"
	                            @if($anio == $anio_actual)
	                            selected
	                            @endif 
	                            >{{$anio++}}</option>
	                            @endwhile
	                        </select>
	                        <input type="submit" value="Ok">
	                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
	                    </form>
	                </div>
	                @endif
	    			<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	            </div>
	            <!-- /.box-body -->
	        </div>
	        <!-- /.box -->
	    </div>
      	</div>
      	<div class="row">
      		<div class="col-lg-12">
      		{{-- COSTO Y PRODUCCION MENSUAL --}}
	       	<div class="box">
	            <!-- /.box-header -->
	            <div class="box-body">
	            	@if($mode == "show")
	                <div>
	                    <form method="POST">
	                        Consultar a&ntilde;o 
	                        <select name="anio">
	                            @php
	                            $anio = 2019;
	                            @endphp
	                            @while($anio <= $anio_actual)
	                            <option value="{{$anio}}"
	                            @if($anio == $anio_actual)
	                            selected
	                            @endif 
	                            >{{$anio++}}</option>
	                            @endwhile
	                        </select>
	                        <input type="submit" value="Ok">
	                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
	                    </form>
	                </div>
	                @endif
	    			<div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	            </div>
	            <!-- /.box-body -->
	        </div>
	        <!-- /.box -->
	    </div>
      	</div>
    </section>
</div>
@endsection
@section('js')
	@parent
<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Insumos'},
                    {extend: 'excel', title: 'Lista de Insumos'},
                    {extend: 'pdf', title: 'Lista de Insumos'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });
</script>
@endsection
@section('js')
	@parent
<script type="text/javascript">
    Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    },
    chart: {
        events: {
            
            load: function () {
                /*this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);*/
                console.log("chartWidth: " + this.chartWidth);
                const calculedWidth = Math.floor(this.chartWidth / 8);
                console.log("chartHeight: " + this.chartHeight);
        <?php

        if($sistema->show_logo1_repo == true){            
            $filename = url('/storage/' . str_replace("public/",'',$sistema->logo1));
            $filename2 = url('/storage/' . $sistema->logo1);
            $filename3 = str_replace("public/",'',$sistema->logo1);
            $imageData = [];
            $fileHeight = $fileWidth = $width = $height = 0;
            
            if(Storage::disk('public')->exists($filename3)){
                $file = Storage::disk('public')->path( $filename3);
                
                list($width, $height) = getimagesize($file);

                if($width > 0 && $height > 0){
                    $fileWidth = intVal($width);
                    $fileHeight = intVal($height);
                }
            ?>  
            const calculedHeight = Math.floor(({{$fileHeight}} * calculedWidth) / {{$fileWidth}});
                
            console.log('calculedHeight: ' + calculedHeight);
                this.mylogo = this.renderer.image("{{$filename}}", 20, 20, calculedWidth, calculedHeight).add();
            <?php }else{ ?>
                console.log('{{$filename}} no es una ruta valida');
            <?php } ?>
        <?php } ?>
                
            }
        }
    }
});
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: '<center>'+
      @php
          if($sistema->show_membrete){
            echo('"'.$sistema->membrete.'<br><br>"+');
            }
      @endphp
      'Estadisticas de Costos Vs Produccion Mensuales del año: <b>{{$anio_actual}}</b>' +
      '</center>',
      useHTML: true
        },
        subtitle: {
            text: 'Costo Vs Produccion mensual por hectárea en ' + '{{$sistema->currency->symbol}}'
        },
        xAxis: {
            categories: [
                'Ene',
                'Feb',
                'Mar',
                'Abr',
                'May',
                'Jun',
                'Jul',
                'Ago',
                'Sep',
                'Oct',
                'Nov',
                'Dic'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Monto en ' + '{{$sistema->currency->symbol}}'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"> <b>{point.y:.2f} {{$sistema->currency->symbol}}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                color: "gray",
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [
        	{
	            name: 'Costo por hectárea',
	            color: "red",
	            data: [
	                @foreach($costos as $c)
	                    {{round($c,2)}},
	                @endforeach
	            ]
	        },
	        {
	            name: 'Produccion por hectárea',
	            color: "green",
	            data: [
	                @foreach($produccionesVenta as $c)
	                    {{round($c,2)}},
	                @endforeach
	            ]
	        }
	    ]
    });
</script>
@endsection
@section('js')
	@parent
<script type="text/javascript">    
    Highcharts.chart('container2', {
        chart: {
            type: 'column'
        },
        title: {
            text: '<center>'+
      @php
          if($sistema->show_membrete){
            echo('"'.$sistema->membrete.'<br><br>"+');
            }
      @endphp
      'Estadisticas de Producción Mensuales del año: <b>{{$anio_actual}}</b>' +
      '</center>',
      useHTML: true
        },
        subtitle: {
            text: 'Producción por hectárea mensuales en Kg'
        },
        xAxis: {
            categories: [
                'Ene',
                'Feb',
                'Mar',
                'Abr',
                'May',
                'Jun',
                'Jul',
                'Ago',
                'Sep',
                'Oct',
                'Nov',
                'Dic'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Producción por hectarea en kg'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f} Kg</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                color: "orange",
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Producción por hectárea',
            data: [
                @foreach($producciones as $c)
                    {{round($c,2)}},
                @endforeach

            ]

        }]
    });
</script>
@endsection
