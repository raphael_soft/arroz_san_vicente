@extends('master')
@section('title', 'Terrenos')
@section('active-produccion', 'active')
@section('active-hectareas-terrenos', 'active')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Registro de Terreno
    <small>Secci&oacute;n para el registro de terrenos</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li><a href="#">Produccion</a></li>
      <li><a href="{{url('terrenos')}}">Terrenos</a></li>
      <li class="active">Nuevo Terreno</li>
    </ol>
  </section>
  @foreach($errors->all() as $error)
  <p class="alert alert-danger">{{$error}}</p>
  @endforeach
  @if (session('status'))
  <div class="alert alert-success">
    {{session('status')}}
  </div>
  @endif
  @if (session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Formulario para el registro de terreno</h3>
          </div>
          <form  method="POST" class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Nombre de Terreno</label>
                <div class="col-sm-10">
                  <input type="text" name="nombre" class="form-control" placeholder="Nombre del terreno" required="required">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Numero de Hectareas</label>
                <div class="col-sm-10">
                  <input type="number" name="hectareas" class="form-control" placeholder="Numero de hectareas" required="required">
                </div>
              </div>
            </div>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>          
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="{{url('/reses')}}" type="submit" class="btn btn-default"><< Volver</a>
              <button type="submit" class="btn btn-info pull-right"><i class="fa fa-upload"></i> Registrar</button>
            </div>
            <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>
</div>
@endsection