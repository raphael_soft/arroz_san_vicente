
@extends('master')
@section('title', 'Produccio - '. $datos->fecha )
@section('active-produccion', 'active')
@section('content')

	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Produccion
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/"') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="">
	                <a href="{{ url('/produccion"') }}">
	                    Produccion
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    {{ $datos->fecha }}
	                </a>
	            </li>
	        </ol>
	    </section>

	    @foreach($errors->all() as $error)
			<p class="alert alert-danger">{{$error}}</p>
		@endforeach

		@if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif

	    <section class="content">
	    	<div class="row">
	    		<div class="col-md-3">
	    			
	    		</div>
	    		<div class="col-md-6">
		          	<div class="box box-widget widget-user-2">
			            <div class="widget-user-header bg-aqua">
			              	<div class="widget-user-image">
			              		<img class="img-circle" src="{{ url('/img/user_base.jpg') }}" alt="{{$datos->socio->nombre}}">
			              	</div>
			              	<h3 class="widget-user-username">{{$datos->socio->nombre}}</h3>
			              	<h5 class="widget-user-desc">
			              		Res: {{$datos->res->nombre}} 
			              		@if($datos->res->status == 0)  
			              			<span class="pull-right badge bg-red">De Baja</span>
			              		@endif
			              	</h5>
			              	<div class="margin">
			              		<div class="btn-group">
				                  	<button type="button" class="btn btn-success">Opciones</button>
				                  	<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
				                    	<span class="caret"></span>
				                    	<span class="sr-only">Toggle Dropdown</span>
				                  	</button>
				                  	<ul class="dropdown-menu" role="menu">
					                    <li><a href="{{ url()->previous() }}">Regresar</a></li>
					                    <li><a href="{{ action('ProduccionController@edit', $datos->id) }}">Actualizar</a></li>
					                    <li class="divider"></li>
					                    <li>
					                    	<a href="#">
				                    			<form class="" action="{{ action('ProduccionController@destroy', $datos->id) }}" method="POST">
								            		<input type="hidden" name="_token" value="{!! csrf_token() !!}">
								                	<button type="submit" class="btn btn-danger pull-right" style="margin-top: 10px">Borrar</button>
								            	</form>
					                    	</a>
					                	</li>	
				                  	</ul>
				                </div>
			              	</div>
			            </div>
			            <div class="box-footer no-padding">
			              	<ul class="nav nav-stacked">
			                	<li><a href="#">Fecha <span class="pull-right badge bg-blue">{{$datos->fecha}}</span></a></li>
			                	<li><a href="#">Cantidad <span class="pull-right badge bg-green">{{$datos->cantidad}}</span></a></li>
			                	<li><a href="#">Producto <span class="pull-right badge bg-red">{{$datos->tipo_produccion->nombre}}</span></a></li>
			                	<li><a href="#">Categoria <span class="pull-right badge bg-yellow">{{$datos->categoria->nombre}}</span></a></li>
			                	<li><a href="#">Medida <span class="pull-right badge bg-aqua">{{$datos->tipo_medicion->nombre}}</span></a></li>
			              	</ul>
			            </div>
		          	</div>
		        </div>
	    		<div class="col-md-3"></div>
			</div>
	    </section>
	</div>

@endsection
