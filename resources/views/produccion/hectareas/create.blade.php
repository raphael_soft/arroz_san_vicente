@extends('master')
@section('title', 'Hectareas')
@section('active-produccion', 'active')
@section('active-produccion-hectareas', 'active')
@section('active-hectareas-terrenos', 'active')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Registro de Hectarea
    <small>Secci&oacute;n para el registro de terrenos</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li><a href="#">Produccion</a></li>
      <li><a href="{{url('socios')}}">Hectareas</a></li>
      <li><a href="{{url('socios')}}">Hectareas</a></li>
      <li class="active">Nuevo Hectarea</li>
    </ol>
  </section>
  @foreach($errors->all() as $error)
  <p class="alert alert-danger">{{$error}}</p>
  @endforeach
  @if (session('status'))
  <div class="alert alert-success">
    {{session('status')}}
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Formulario para el registro de hectarea</h3>
          </div>
          <form  method="POST" class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Nombre de Hectarea</label>
                <div class="col-sm-10">
                  <input type="text" name="nombre" class="form-control" placeholder="Nombre de la hectarea">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Metros</label>
                <div class="col-sm-10">
                  <input type="text" name="metros" class="form-control" value="10000" placeholder="10.000">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Terreno</label>
                <div class="col-sm-10">
                  <select name="terreno" class="form-control">
                    <option value="">Seleccione el terreno</option>
                    @foreach($terrenos as $h)
                      <option value="{{$h->id}}">{{$h->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>          
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="{{route('hectareas')}}" class="btn btn-default"><< Volver</a>
              <button type="submit" class="btn btn-info pull-right"><i class="fa fa-upload"></i> Registrar</button>
            </div>
            <!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</section>

@endsection