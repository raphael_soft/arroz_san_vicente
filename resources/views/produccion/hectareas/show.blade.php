@extends('master')
@section('title', 'Hectarea')
@section('active-produccion', 'active')
@section('active-produccion-hectareas', 'active')
@section('active-hectareas-terrenos', 'active')
@section('content')

  <div class="content-wrapper">

      <section class="content-header">
          <h1>
            Datos de la Hectarea
            <small>Secci&oacute;n para visualizar los datos de la hectarea</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
          </ol>
        </section>

      @foreach($errors->all() as $error)
      <p class="alert alert-danger">{{$error}}</p>
    @endforeach

    @if (session('status'))
      <div class="alert alert-success">
        {{session('status')}}
      </div>
    @endif

      <section class="content">
        <div class="row">
          <div class="col-md-3">
            
          </div>
          <div class="col-md-12 box-body">
                <div class="box box-widget widget-user-2">
                   <form method="POST" class="form-horizontal">
                    <input type="hidden" name="id" value="{{$hectarea->id}}">
                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Nombre de Hectarea</label>
                        <div class="col-sm-10">
                          <input type="text" name="nombre" @if($mode == 'show') readonly="readonly" @endif value="{{$hectarea->nombre}}" class="form-control" placeholder="Nombre de la hectarea">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Metros</label>
                        <div class="col-sm-10">
                          <input type="text" name="metros" value="{{$hectarea->metros}}" class="form-control" value="10000" placeholder="10.000" @if($mode == 'show') readonly="readonly" @endif>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Terreno</label>
                        <div class="col-sm-10">
                          <select name="terreno" class="form-control" @if($mode == 'show') readonly="readonly" @endif>
                            <option value="">Seleccione el terreno</option>
                            @foreach($terrenos as $h)
                              <option value="{{$h->id}}"
                                @if($hectarea->terreno_id == $h->id)
                                selected
                                @endif
                                >{{$h->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>          
                    <!-- /.box-body -->
                    <div class="box-footer">
                      @if($mode == 'show')
                        <a href="{{route('hectareas')}}"  class="btn btn-default"><< Volver</a>
                        <a href="{{route('edit_hectarea',$hectarea->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                        <a href="{{route('destroy_hectarea',$hectarea->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                      @else($mode == 'edit')
                        <a href="{{route('show_hectarea', $hectarea->id)}}"  class="btn btn-default"><< Volver</a>
                        <button type="submit" href="{{route('edit_hectarea',$hectarea->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Guardar</button>
                        <a href="{{route('destroy_hectarea',$hectarea->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                      @endif
                    </div>
                    <!-- /.box-footer -->
                  </form>
                 
                </div>
            </div>
          <div class="col-md-3"></div>
      </div>
      </section>

@endsection
