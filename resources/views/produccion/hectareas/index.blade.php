@extends('master')
@section('title', 'Hectareas')
@section('active-produccion', 'active')
@section('active-produccion-hectareas', 'active')
@section('active-hectareas-hectareas', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Hectareas
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado de Hectareas</h3>
              
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
              <div><a href="{{route('create_hectarea')}}" class="btn btn-primary" style="float: right;">Nueva Hectarea</a></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped informe">
                <thead>
                  <tr>
                    <th>C&oacute;digo</th>
                    <th>Terreno</th>
                    <th>Nombre</th>
                    <th>Metros Cuadrados</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>

                @foreach($hectareas as $t)
                  <tr>
                    <td>{{$t->id}}</td>
                    <td>{{$t->terreno->nombre}}</td>
                    <td>{{$t->nombre}}</td>
                    <td>{{$t->metros}}</td>
                    <td><a href="{{route('show_hectarea',$t->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i> Ver</a></td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
</div>
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Hectareas'},
                    {extend: 'excel', title: 'Lista de Hectareas'},
                    {extend: 'pdf', title: 'Lista de Hectareas'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>
@endsection
