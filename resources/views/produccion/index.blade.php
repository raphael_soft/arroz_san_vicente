@extends('master')
@section('title', 'Producciones')
@section('active-produccion', 'active')
@section('content')


	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Producciones
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/"') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Producciones
	                </a>
	            </li>
	        </ol>
	    </section>

	    @if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif
		@if (session('status-error'))
			<div class="alert alert-danger">
				{{session('status')}}
			</div>
		@endif

	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
	    			<div class="box">
	    				<div class="box-header">
	    					<h3 class="box-title"><a href="{{url('produccion/nueva-produccion')}}" class="btn btn-primary">Crear Nueva</a></h3>
	    				</div>
	    				<div class="box-body">
	    					<div class="table-responsive">
	    						<table class="table table-bordered table-hover informacion">
	    							<thead>
	    								<th>Socio</th>
	    								<th>Res</th>
	    								<th>Código</th>
	    								<th>Produccion</th>
	    								<th>Categoria</th>
	    								<th>Medida</th>
	    								<th>Cantidad</th>
	    								<th>Fecha</th>
	    							</thead>
	    							<tbody>
	    								@foreach($datos as $dato)
		    								<tr>
		    									<td><a href="{!! action('ProduccionController@show', $dato->id) !!}">{{ $dato->socio->nombre }}</a></td>
		    									<td><a href="{!! action('ProduccionController@show', $dato->id) !!}">{{ $dato->res->nombre }}</a></td>
		    									<td><a href="{!! action('ProduccionController@show', $dato->id) !!}">{{ $dato->codigo }}</a></td>
		    									<td><a href="{!! action('ProduccionController@show', $dato->id) !!}">{{ $dato->tipo_produccion->nombre }}</a></td>
		    									<td><a href="{!! action('ProduccionController@show', $dato->id) !!}">{{ $dato->categoria->nombre }}</a></td>
		    									<td><a href="{!! action('ProduccionController@show', $dato->id) !!}">{{ $dato->tipo_medicion->nombre }}</a></td>
		    									<td><a href="{!! action('ProduccionController@show', $dato->id) !!}">{{ $dato->cantidad }}</a></td>
		    									<td><a href="{!! action('ProduccionController@show', $dato->id) !!}">{{ $dato->fecha }}</a></td>
		    								</tr>
		    							@endforeach
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </section>

	    <script>
	        $(document).ready(function(){
	            $('.informacion').DataTable({
	                dom: '<"html5buttons"B>lTfgitp',
	                select: true,
	                buttons: [
	                    // {extend: 'csv', title: 'Lista de Ivas'},
	                    // {extend: 'excel', title: 'Lista de Ivas'},
	                    // {extend: 'pdf', title: 'Lista de Ivas'},

	                    // {extend: 'print',
	                    //     customize: function (win){
	                    //         $(win.document.body).addClass('white-bg');
	                    //         $(win.document.body).css('font-size', '10px');
	                    //         $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
	                    //     }
	                    // }
	                ],
	            });
	        });

	    </script>
	</div>

@endsection