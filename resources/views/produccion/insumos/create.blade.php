@extends('master')
@section('title', 'Nuevo Insumo')
@section('active-produccion', 'active')
@section('active-produccion-insumos', 'active')
@section('css')
@parent
<style>
[v-cloak]{
display: none;
}
</style>
@endsection
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Registro de Insumos
    <small>Secci&oacute;n para el registro de insumos</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
    </ol>
  </section>
  @foreach($errors->all() as $error)
  <div class="alert alert-danger">{{$error}}</div>
  @endforeach
  @if (session('status'))
  <div class="alert alert-success">
    {{session('status')}}
  </div>
  @endif
  <section class="content">
    <div class="row" id="application" v-cloak>
        <div class="col-xs-12">
          <div class="box box-info" v-if="productos.length > 0 && terrenos.length > 0">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario para el registro de nuevos insumos</h3>
            </div>          
            <div class="box-body">
                <div class="box-body">
                  <div class="col-lg-3">
                    <label class="control-label">Terreno</label>
                    <select name="terreno" class="form-control" required="required" v-model="selectedTerrenoIndex">
                      <option v-for="(terreno, index) in terrenos" :value="index">@{{terreno.nombre}}</option>
                    </select>
                  </div>
                  <div class="col-lg-3">
                    <label class="control-label">Producto</label>                
                    <select name="producto" class="form-control" required="required" v-model="selectedProductoIndex">
                      <option v-for="(producto, index) in productos" :value="index">@{{producto.producto.nombre}}</option>
                    </select>
                  </div>
                  <div class="col-lg-1" v-if="nuevoInsumo !== null">
                    <label class="control-label">Disponible</label>
                    <input type="text" name="disponible" :value="productos[selectedProductoIndex].cantidad" readonly="readonly" class="form-control" >
                  </div>
                  <div class="col-lg-2" v-if="nuevoInsumo !== null">
                    <label class="control-label">Costo Unit</label>
                    <input type="text" name="costo_unit" :value="productos[selectedProductoIndex].producto.costo_produccion" readonly="readonly" class="form-control" >
                  </div>
                  <div class="col-lg-1" v-if="nuevoInsumo !== null">
                    <label class="control-label">Cantidad</label>
                    <input type="number" name="cantidad" v-model="nuevoInsumo.cantidad" v-if="productos[selectedProductoIndex].cantidad > 0" class="form-control">
                    <span v-if="productos[selectedProductoIndex].cantidad <= 0">No disponible</span>
                  </div>
                  <div class="col-lg-2" v-if="nuevoInsumo !== null">
                    <label class="control-label">Costo Total</label>
                    <input type="text" name="costo_total" :value="nuevoInsumoCostoTotal" readonly="readonly" class="form-control" >
                  </div>
                  <div class="col-lg-1 text-center">
                    <a class="btn btn-lg btn-info" v-on:click="addInsumo">Agregar</a>
                  </div>
                </div>
                <div class="form-group" v-if="insumos !== null && insumos.length > 0 && insumos[0] !== undefined && insumos[0] !== undefined">
                  <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Terreno</th>
                        <th>Producto</th>
                        <th>Costo Unit</th>
                        <th>Cantidad</th>
                        <th>Costo Total</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="insumo in insumos">
                        <td class="col-3">@{{terrenos[insumo.terrenoIndex].nombre}}</td>
                        <td class="col-3">@{{productos[insumo.productoIndex].producto.nombre}}</td>
                        <td class="col-1"><input type="number" v-model="insumo.costo_unitario" class="form-control" /></td>
                        <td class="col-1"><input type="number" v-model="insumo.cantidad" class="form-control" /></td>
                        <td class="col-1"><input type="number" :value="insumo.cantidad * insumo.costo_unitario" class="form-control" readonly /></td>
                        <td class="col-2">
                          <a class="btn btn-danger" v-on:click="removeInsumo(insumo)"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                </div>
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <!-- /.box-body -->
            </div>
            <div class="box-footer">
                <a href="{{route('insumos')}}" class="btn btn-default"><< Volver</a>
                <a class="btn btn-info pull-right" v-on:click="submit"><i class="fa fa-upload"></i> Registrar</a>
            </div>
            <div class="box" v-if="productos.length == 0">
              <div class="alert alert-info" >
                <i class="fa fa-info-circle"></i> No hay productos disponibles en inventario. Para agregar productos al inventario, realice compras.
              </div>
            </div>
          </div>
          <div class="alert alert-warning" v-if="productos.length == 0 || terrenos.length == 0">
            <h4>Información</h4>
            <p><i class="fa fa-info-circle"></i> Debe tener al menos un terreno registrado y al menos un producto registrado y disponible en el inventario de productos para poder asignar insumos a un terreno. Por favor diríjase al modulo de <strong><a href="{{route('terrenos')}}">Produccion/Terrenos</a></strong> para gestionar terrenos. Para gestionar productos diríjase al modulo <strong><a href="{{route('productos')}}" class="strong">Configuracion/Productos</a></strong>. Para agregar productos al inventario, debe realizar la compra de dichos productos. No se pueden agregar productos al inventario directamente. Para ver el inventario de productos dirijase al modulo <strong><a href="{{route('inventario','productos')}}" class="strong">Inventario/De Productos</a></strong>. Para realizar compras de productos dirijase al modulo <strong><a href="{{route('create_facturacompra', 'productos')}}" class="strong">Compras/De Productos</a></strong></p>
          </div>
        </div>
    </div>
  </section>
</div>
@endsection
@section('js')
@parent
<!-- production version, optimized for size and speed -->
<!--script type="text/javascript" src="{{asset('js/vue.min.js')}}"></script-->
<script type="text/javascript" src="{{asset('js/vue.js')}}"></script>
<script async src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript">
var productos = @json($productos);
var app = new Vue({
  el: '#application',
  data: {
  insumos: [],
  productos: @json($productos),
  terrenos: @json($terrenos),
  nuevoInsumo: {cantidad: 1},
  selectedProductoIndex: 0,
  selectedTerrenoIndex: 0
  },
  created: function(){
    this.createNuevoInsumo();
  },
  watch:{
    selectedProductoIndex: function(){
      this.createNuevoInsumo();
    },
    selectedTerrenoIndex: function(){
      this.createNuevoInsumo();
    }
  },
  computed:{
    nuevoInsumoCostoTotal: function(){
      return Number(this.productos[this.selectedProductoIndex].producto.costo_produccion) * Number(this.nuevoInsumo.cantidad);
    }
  },
  methods:{
    createNuevoInsumo: function(){
      if(this.terrenos.length == 0 || this.productos.length == 0){
        return;
      }
      console.log('selectedTerrenoIndex: ' + this.selectedTerrenoIndex);
      console.log('selectedProductoIndex: ' + this.selectedProductoIndex);
      this.nuevoInsumo = {
        terrenoId: this.terrenos[this.selectedTerrenoIndex].id,
        terrenoIndex: this.selectedTerrenoIndex,
        productoIndex: this.selectedProductoIndex,
        productoId: this.productos[this.selectedProductoIndex].producto.id,
        costo_unitario: this.productos[this.selectedProductoIndex].producto.costo_produccion,
        cantidad: 1
      };
    },
    addInsumo: function(){
      console.log('inside addInsumo');
      const indice = this.existeInsumo(this.nuevoInsumo);
      if(indice == -1){
        let aux = JSON.parse(JSON.stringify(this.nuevoInsumo));
        this.insumos.push(aux);
        return;
      }
      this.insumos[indice].cantidad = Number(this.insumos[indice].cantidad) + Number(this.nuevoInsumo.cantidad);
    },
    existeInsumo: function(insumo){
      console.log('inside existeInsumo');
            
      let indice = -1;
      this.insumos.forEach((element, index) => {
        console.log('terrenos: ' + element.terrenoIndex + ' == ' + insumo.terrenoIndex);
        console.log('productos: ' + element.productoIndex + ' == ' + insumo.productoIndex);
        if(element.terrenoIndex == insumo.terrenoIndex && element.productoIndex == insumo.productoIndex){
          console.log('si existe!');
          indice = index;
          return indice;
        }
      });
      if(indice == -1)
        console.log('no existe!');

      console.log('el indice a retornar es: ' + indice)
      return indice;
    },
    removeInsumo: function(insumo){
      console.log('inside removeInsumo');
      const indice = this.existeInsumo(insumo);
      if(indice >= 0)
        this.insumos.splice(indice, 1);
    },
    submit: function(){
      if(this.insumos.length == 0){
        alert('No hay insumos en la lista');
        return;
      }
      const data = {
        insumos: this.insumos
      };
      axios.post("{{route('store_insumo')}}", data)
      .then(
        function (response){
          console.log('exito');          
          console.log(response.data);
          if(response.data.cod !== undefined && response.data.msg !== undefined){
            alert(response.data.msg);
            if(response.data.cod == 'ok')
              window.location.href = "{{route('insumos')}}";
          }
      }).catch(
        function (error){
          console.log('error');
          console.log(error);
          alert("Error al enviar datos");
      });
    }
  }
})
</script>
@endsection