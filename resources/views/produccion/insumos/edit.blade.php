@extends('master')
@section('title', 'Editar Insumo')
@section('active-produccion', 'active')
@section('active-produccion-insumos', 'active')
@section('css')
@parent
<style>
[v-cloak]{
display: none;
}
</style>
@endsection
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Editar Insumo
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
    </ol>
  </section>
  @foreach($errors->all() as $error)
  <p class="alert alert-danger">{{$error}}</p>
  @endforeach
  @if (session('status'))
  <div class="alert alert-success">
    {{session('status')}}
  </div>
  @endif
  <section class="content">
    <div class="row" id="application" v-cloak>
        <div class="col-xs-12">
          <div class="box box-info" v-if="productos.length > 0">
          	<form method="post">
	            <div class="box-header with-border">
	              <h3 class="box-title">Datos del insumo</h3>
	            </div>
	            <div class="box-body">
	                <div class="col-lg-6">
	                	<input type="hidden" name="terreno" :value="terrenos[selectedTerrenoIndex].id">
	                	<div class="form-group">
		                    <label class="control-label">Terreno</label>
		                    <select name="terreno_" class="form-control" required="required" v-model="selectedTerrenoIndex">
		                      <option v-for="(terreno, index) in terrenos" :value="index">@{{terreno.nombre}}</option>
		                    </select>
		                </div>
	            	</div>
	            	<div class="col-lg-6">
	            		<input type="hidden" name="producto" :value="productos[selectedProductoIndex].producto.id">
	                	<div class="form-group">
		                    <label class="control-label">Producto</label>                
		                    <select name="producto_" class="form-control" required="required" v-model="selectedProductoIndex">
		                      <option v-for="(producto, index) in productos" :value="index">@{{producto.producto.nombre}}</option>
		                    </select>
	                  	</div>
	              	</div>
	              	<div class="col-lg-1" v-if="nuevoInsumo !== null">
	                    <label class="control-label">Disponible</label>
	                    <input type="text" name="disponible" :value="productos[selectedProductoIndex].cantidad" readonly="readonly" class="form-control" >
	                </div>
	              	<div class="col-lg-2">
	              		<div class="form-group">
		                    <label class="control-label">Costo Unit</label>
		                    <input type="number" min="0" name="costo_unit" v-model="nuevoInsumo.costo_unitario" class="form-control" required="required">
	                  </div>
	              	</div>
	              	<div class="col-lg-1">
	                  	<div class="form-group">
		                    <label class="control-label">Cantidad</label>
		                    <input type="number" min="0" name="cantidad" v-model="nuevoInsumo.cantidad" class="form-control" required="required" v-if="productos[selectedProductoIndex].cantidad > 0">
		                    <span v-if="productos[selectedProductoIndex].cantidad <= 0">No disponible</span>
	                  	</div>
	              	</div>
	                <div class="col-lg-2">
	                  	<div class="form-group">
		                    <label class="control-label">Costo Total</label>
		                    <input type="text" name="costo_total" :value="nuevoInsumoCostoTotal" class="form-control" readonly="readonly" >
	                  	</div>
	                </div>
	                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
	                <!-- /.box-body -->
	            </div>
	            <div class="box-footer">
	                <a href="{{route('insumos')}}"  class="btn btn-default"><< Volver</a>
                    <button type="submit"  class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                    <a href="{{route('destroy_insumo',$insumo->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
	            </div>
	        </form>
          </div>
        </div>
    </div>
  </section>
</div>
@endsection
@section('js')
@parent
<!-- production version, optimized for size and speed -->
<!--script type="text/javascript" src="{{asset('js/vue.min.js')}}"></script-->
<script type="text/javascript" src="{{asset('js/vue.js')}}"></script>

<script type="text/javascript">
var app = new Vue({
  el: '#application',
  data: {  
  insumo: @json($insumo),	
  productos: @json($productos),
  terrenos: @json($terrenos),
  nuevoInsumo: {cantidad: 1, costo_unitario: 0},
  selectedProductoIndex: 0,
  selectedTerrenoIndex: 0
  },
  created: function(){
    this.nuevoInsumo = {
        terrenoId: this.terrenos[this.selectedTerrenoIndex].id,
        terrenoIndex: this.selectedTerrenoIndex,
        productoIndex: this.selectedProductoIndex,
        productoId: this.productos[this.selectedProductoIndex].producto.id,
        cantidad: this.insumo.cantidad,
        costo_unitario: this.insumo.costo_unitario
      };
  },
  watch:{
    selectedProductoIndex: function(){
      this.createNuevoInsumo();
    },
    selectedTerrenoIndex: function(){
      this.createNuevoInsumo();
    }
  },
  computed:{
    nuevoInsumoCostoTotal: function(){
      return Number(this.nuevoInsumo.costo_unitario) * Number(this.nuevoInsumo.cantidad);
    }
  },
  methods:{
    createNuevoInsumo: function(){
      if(this.terrenos.length == 0 || this.productos.length == 0){
        return;
      }
      console.log('selectedTerrenoIndex: ' + this.selectedTerrenoIndex);
      console.log('selectedProductoIndex: ' + this.selectedProductoIndex);
      this.nuevoInsumo = {
        terrenoId: this.terrenos[this.selectedTerrenoIndex].id,
        terrenoIndex: this.selectedTerrenoIndex,
        productoIndex: this.selectedProductoIndex,
        productoId: this.productos[this.selectedProductoIndex].producto.id,
        cantidad: this.nuevoInsumo.cantidad,
        costo_unitario: this.nuevoInsumo.costo_unitario
      };
    }   
  } 
})
</script>
@endsection