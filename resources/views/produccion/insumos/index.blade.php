@extends('master')
@section('title', 'Insumos')
@section('active-produccion', 'active')
@section('active-produccion-insumos', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Insumos
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado de Insumos</h3>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
              <div><a href="{{route('asignar_insumo')}}" class="btn btn-primary" style="float: right;">Asignar Insumo</a></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped informe table-responsive responsive ">
                <thead>
                  <tr>
                    <th>C&oacute;digo</th>
                    <th>Terreno</th>
                    <th>Producto</th>
                    <th>Costo</th>
                    <th>Cantidad</th>
                    <th>Costo Total</th>
                    <th>Fecha</th>
                    <th class="no-print">Opciones</th>
                  </tr>
                </thead>
                <tbody>

                @foreach($insumos as $t)
                  <tr>
                    <td>{{$t->id}}</td>
                    <td>{{ $t->terreno !== null ? $t->terreno->nombre: 'No asignado'}}</td>
                    <td>{{ $t->producto !== null ? $t->producto->nombre: 'No asignado'}}</td>
                    <td>{{$t->costo_unitario}}</td>
                    <td>{{$t->cantidad}}</td>
                    <td>{{$t->costo_total}}</td>              
                    <td>{{$t->fecha}}</td>
                    <td><a href="{{route('show_insumo',$t->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i> Ver</a></td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
</div>
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Insumos', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
                    {extend: 'excel', title: 'Lista de Insumos', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
                    {extend: 'pdf', title: 'Lista de Insumos', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        },
                        exportOptions: {
					        columns: ':not(.no-print)'
					    }
                    }
                ],
            });
        });

    </script>
@endsection
