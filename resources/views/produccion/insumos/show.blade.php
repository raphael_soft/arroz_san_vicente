@extends('master')
@section('title', 'Ver Insumo')
@section('active-produccion', 'active')
@section('active-produccion-insumos', 'active')
@section('css')
@parent
<style>
[v-cloak]{
display: none;
}
</style>
@endsection
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Ver Insumo
    <small>Secci&oacute;n para ver un insumo</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
    </ol>
  </section>
  @foreach($errors->all() as $error)
  <p class="alert alert-danger">{{$error}}</p>
  @endforeach
  @if (session('status'))
  <div class="alert alert-success">
    {{session('status')}}
  </div>
  @endif
  <section class="content">
    <div class="row" >
        <div class="col-xs-12">
          <div class="box box-info" v-if="productos.length > 0">
          	<form method="post">
	            <div class="box-header with-border">
	              <h3 class="box-title">Datos del insumo</h3>
	            </div>
	            <div class="box-body">
	            	<div class="col-lg-6">
	            		<div class="form-group">            		
		                    <label class="control-label">Usuario</label>
		                    <input type="text" name="usuario" value="{{$insumo->usuario->nombre}}" readonly="readonly" class="form-control" >
	                  	</div>
	                </div>
	                <div class="col-lg-6">
	                	<div class="form-group">
		                    <label class="control-label">Terreno</label>
		                    <select name="terreno" class="form-control" required="required" readonly="readonly">
		                    	@foreach($terrenos as $terreno)
		                      		<option value="{{$terreno->id}}" @if($terreno->id == $insumo->terreno_id) selected @endif>{{$terreno->nombre}}</option>
		                      	@endforeach
		                    </select>
		                </div>
	            	</div>
	            	<div class="col-lg-6">
	                	<div class="form-group">
		                    <label class="control-label">Producto</label>                
		                    <select name="producto" class="form-control" required="required" readonly="readonly">
		                    	@foreach($productos as $producto)
		                      		<option value="{{$producto->producto->id}}" @if($producto->producto->id == $insumo->producto_id) selected @endif>{{$producto->producto->nombre}}</option>
		                      	@endforeach
		                    </select>
	                  	</div>
	              	</div>
	              	<div class="col-lg-2">
	              		<div class="form-group">
		                    <label class="control-label">Costo Unit</label>
		                    <input type="text" name="costo_unit" value="{{$insumo->costo_unitario}}" readonly="readonly" class="form-control" >
	                  </div>
	              	</div>
	              	<div class="col-lg-1">
	                  	<div class="form-group">
		                    <label class="control-label">Cantidad</label>
		                    <input type="number" name="cantidad" value="{{$insumo->cantidad}}" class="form-control" readonly="readonly">
	                  	</div>
	              	</div>
	                <div class="col-lg-2">
	                  	<div class="form-group">
		                    <label class="control-label">Costo Total</label>
		                    <input type="text" name="costo_total" value="{{$insumo->costo_total}}" readonly="readonly" class="form-control" >
	                  	</div>
	                </div>
	                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
	                <!-- /.box-body -->
	            </div>
	            <div class="box-footer">
	                <a href="{{route('insumos')}}"  class="btn btn-default"><< Volver</a>
                    <a href="{{route('edit_insumo',$insumo->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                    <a href="{{route('destroy_insumo',$insumo->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
	            </div>
	        </form>
          </div>
        </div>
    </div>
  </section>
</div>
@endsection
