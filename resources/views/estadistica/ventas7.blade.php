@extends('master')

@section('title', 'Ventas')

@section('active-venta', 'active')

@section('active-venta-estadistica', 'active')

@section('active-estadisticas-ventas7', 'active')

@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Ventas - Reporte solo texto
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            <form method="POST">
            <div class="box-body content">
                @if($totalVentas > 0)
                
                <div class="col-sm-12">
                    

                        <label>Consultar por fechas<label>
                        <div class="form-group col-sm-3">
                               <label >Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="fromDate" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label >Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="toDate" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    
                    <div id="informe8" class="col-sm-12" >
                        <div id="container8">

                        
            <a href="{{url('imprimir-ventas',['fein' => $fromDate,'fefi' => $toDate])}}" target="_blank" id="btn-imprimir" class="btn btn-success" style="float:right;"><i class="fa fa-print" style="padding-right: 10px;"></i>Imprimir</a>
<div class="row"></div>
<label></label>
                <table class="table table-bordered table-striped table-responsive">

                <thead>

                  <tr>
                    <th> # </th>

                    <th>Factura N #</th>

                    <!--th>N. Orden</th-->
                    
                    <th>Fecha</th>

                    <!--th>Subtotal</th>

                    <th>Impuesto</th-->

                    <th>Total</th>

                  </tr>

                </thead>

                <tbody id="tabla_datos">


<?php
 $c_counter = 0;
 $c_subtotal = 0;
 $c_impuestos = 0;
 $c_total = 0;
 $impuestos = [];
 $cont_no_iva = 0;
 $cont_si_iva = 0;
 $total_no_iva = 0;
 $total_si_iva = 0;
?>
                @foreach($facturas as $f)
                  <tr>
                    <td> {{ ++$c_counter }}</td>
                    <td>{{ $f->id }}</td>
                    <td>{{ $f->fecha_facturacion }}</td>
                    <td>{{ $sistema->currency->symbol }} {{sprintf("%.2f",$f->total)}} </td>

                    <?php 
                    
                    if(isset($f->detalles)){
                        //echo "dsfsd";
                        foreach ($f->detalles as $key => $d) {
                            # code...
                            //calculo de impuestos
                            if($d->impuesto_id !== null){
                                //dd($d->impuesto);   
                                $impuesto_id = $d->impuesto_id;
                                $cantidad = $d->cantidad;
                                $precio = $d->precio;

                                if(!($impuesto_id > 0)){
                                    //tomarlo como sin iva
                                    $cont_no_iva++;
                                    $total_no_iva += ($cantidad * $precio);
                                }else{
                                    $cont_si_iva++;
                                    $total_si_iva += ($cantidad * $precio);
                                    $valor_impuesto = $d->impuesto->valor;
                                    
                                    if(!isset($impuestos[$valor_impuesto])){
                                        $impuestos[$valor_impuesto] =  ($cantidad * $precio) * $valor_impuesto / 100;
                                    }else{
                                        $impuestos[$valor_impuesto] +=  ($cantidad * $precio) * $valor_impuesto / 100;

                                    }
                                   
                                }
                                
                            }


                        }
                        
                    }

                    $c_subtotal += $f->subtotal;
                    $c_impuestos += $f->monto_iva;
                    $c_total += $f->total;
                    ?>

                  </tr>

                @endforeach

                </tbody>
                <tfoot class="push-rigth" id="tfoot">
                    <tr>
                        <td colspan="2">Subtotal</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_subtotal)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Objeto IVA </td>
                        <td> $ {{sprintf("%.2f",$total_si_iva)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Objeto no IVA </td>
                        <td> $ {{sprintf("%.2f",$total_no_iva)}}</td>
                    </tr>
            @if(isset($impuestos) and !empty($impuestos)) 
                  
                @foreach($impuestos as $valor => $imp)
                    <tr>
                        <td colspan="2">Impuesto {{$valor}}%</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$impuestos[$valor])}}</td>
                    </tr>
                @endforeach
            @endif        
                    <tr>
                        <td colspan="2" style="font-size: 24px">TOTAL</td>
                        <td style="font-size: 24px">{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_total)}}</td>
                    </tr>
                </tfoot>
              </table>
                    </div>
                    </div>
                    
                
                    
                   
              
                </div>
                @else
                No hay resultados que cumplan con los parametros seleccionados &nbsp;<a href="javascript: window.history.back();" class="btn btn-info"><< Volver</a>
                @endif
                @if($contadorVentas == 0)
                <div class="callout callout-warning" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> IMPORTANTE:</h4> No existen ventas registradas, por tanto, no podrá visualizar ningun informe de ventas. <a href="{{url('/create_facturaventa')}}">Hacer primera venta</a>
      </div>
@endif
               </div> 
        </form>
          </div>
    </section>
</div>

<script type="text/javascript">
    Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    },
    chart: {
        events: {
            
            load: function () {
                /*this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);*/
                
                @if($sistema->show_logo1_repo == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1);  
          $width = 140; $height = 140;

                //list($width, $height) = getimagesize(url('/storage/'.$filename)); 
          ?>
          this.mylogo = this.renderer.image("{{url('/storage/'.$filename)}}", 20, 20, {{$width}}, {{$height}}).add();
          @endif
                
            }
        }
    }
});
             
        </script>
 


                <script>

        $(document).ready(function(){

            $('.table').DataTable({

                 

             });

        });

    </script>
@endsection
