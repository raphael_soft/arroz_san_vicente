
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>{{$title}}</title>
<link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
<link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
<style> 
.logo-reporte{
	padding: 0 0 0;
	vertical-align: top;
	width: 20%;
}
.membrete-repote{
	padding: 0 0 0;
}
.usuario-reporte{
	position: absolute;
	float: right;
	right: 10%;
	bottom: 10px;
}
tr,th,td,p{
	font-size: 10px;
	margin-bottom: 0;
}
@media print{
	tr,th,td,p{
		font-size: 10px;
	}
	h2{
		font-size: 12px;
	}
	h3{
		font-size: 10px;
	}
}
</style>
</head>
<body onload="window.print();window.close();" style="font-size: small;">
	<span class="usuario-reporte"> 
		<p>{{ session('nombre') }}</p>
		<p>{{ date('Y/m/d H:i:s') }}</p>
	</span>
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="row column text-center">
<table width="100%">
    <tr>
        <td class="logo-reporte">
        @if($sistema->show_logo1_repo == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1); ?>
          <img src="{{url('/storage/'.$filename)}}" style="width: 100px; max-width: 100px;">
        @endif
        </td>
        <td class="membrete-repote">    
            @if($sistema->show_membrete && strlen($sistema->membrete) > 5)
            	<span><?php echo($sistema->membrete); ?></span>
            @else
            	<h3><?php echo($sistema->razon_social); ?></h3>
            	<p><?php echo($sistema->direccion); ?></p>
            	<p><?php echo($sistema->telefono1); ?></p>
            	<p><?php echo($sistema->email1); ?></p>
        	@endif
            <h2>Reporte de ventas</h2>
            <p class="subheader">desde {{$fromDate}} hasta {{$toDate}}</p>
        </td>
        <td class="logo-reporte">
        	@if($sistema->show_logo2_repo == true)
          		<?php $filename = str_replace("public/",'',$sistema->logo2); ?>
          		<img src="{{url('/storage/'.$filename)}}" style="width: 100px; max-width: 100px;">
          	@endif
        </td>
</div>


<div style="width: 100%">
                          <table style="width: 100%" class="table compact">
                    <thead>
                        <tr align="left" style="border:2; border: solid; border-bottom: 2; border-color: gray;">
                            <th width="50px;">
                              #
                            </th>
                            <!--th width="100px;">
                                Factura #
                            </th>
                            <th width="120px;">
                                Orden #
                            </th-->
                            <th width="120px;">
                                Fecha
                            </th>
                            
                            <!--th width="100px;">
                                Subtotal
                            </th>
                            <th width="100px;">
                                Impuesto
                            </th-->
                            <th>
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                      
                        
<?php
 $c_counter = 0;
 $c_subtotal = 0;
 $c_impuestos = 0;
 $c_total = 0;

?>
                @foreach($facturas as $f)
                  <tr align="left">
                    <td>{{++$c_counter}}</td>
                    <td>{{$f->fecha_facturacion}}</td>
                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f->total)}} </td>

                    <?php 
                    $c_subtotal += $f->subtotal;
                    $c_impuestos += $f->monto_iva;
                    $c_total += $f->total;
                    ?>

                  </tr>

                @endforeach
</tbody><tfoot class="push-rigth" id="tfoot">
                    <tr>
                        <td colspan="2">Subtotal</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_subtotal)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Impuestos</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_impuestos)}}</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2">TOTAL</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_total)}}</td>
                    </tr>
                </tfoot>
              </table></div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>
