@extends('master')
@section('title', 'Estadistica de Compras')
@section('active-compra', 'active')
@section('active-compra-estadisticas', 'active')
@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Compras
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                
                
                
                @if($totalCompras > 0)
                <form method="POST">
                <div class="col-sm-12">

                    <div class="form-group col-sm-5">
                        <label>Tipo de Informe</label>
                        <select id="tipo_informe" name="tipo_informe" class="form-control">
                            <option value="dist_por_tipo"
                            @if(isset($tipo_informe) and $tipo_informe == "dist_por_tipo")
                                selected
                            @endif
                            >Distribucion de compras por productos</option>
                            <option value="dist_por_prod"
                            @if(isset($tipo_informe) and $tipo_informe == "dist_por_prod")
                                selected
                            @endif
                            >Distribucion de compras por tipo y productos</option>
                            <!--10 cat mas vendidas-->
                            <option value="dist_por_cat2"
                            @if(isset($tipo_informe) and $tipo_informe == "dist_por_cat2")

                                selected
                            @endif
                            >Distribucion de compras por categorias y subcategorias (10 mas compradas)</option>
                            <option value="dist_por_cat"
                            @if(isset($tipo_informe) and $tipo_informe == "dist_por_cat")
                                selected
                            @endif                            
                            >Distribucion de compras por categorias y subcategorias (seleccionadas por el usuario)</option>
                            <option value="subt_dia"
                            @if(isset($tipo_informe) and $tipo_informe == "subt_dia")
                                selected
                            @endif
                            >Subtotales diarios</option>
                            <option value="rep_comp"
                            @if(isset($tipo_informe) and $tipo_informe == "rep_comp")
                                selected
                            @endif
                            >Reporte de compras (Sin graficas)</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12" >
                    

                        <label>Parametros de busqueda<label>

                            <div class="form-group col-sm-3" id="select_tipo">
                               <label >Tipo de compra</label>
                            <select name="tipo" class="form-control">
    <option value="0" 
    @if(isset($tipo) and $tipo == 0)
    selected
    @endif
    >
        De produccion
    </option>
    <option value="1"
    @if(isset($tipo) and $tipo == 1)
    selected
    @endif
    >
        De productos
    </option>
    <option value="2"
    @if(isset($tipo) and $tipo == 2)
    selected
    @endif
    >
        Todos
    </option>
</select>
</div>

                        <div class="form-group col-sm-3">
                               <label >Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="fromDate" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label >Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="toDate" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
            
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    
                    <div id="subt_dia" class="col-sm-12" style="display: none;">
                    <div id="container1" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
                    </div>
                    <div id="dist_por_prod" class="col-sm-12" style="display: none;">
                    <div id="container2" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
                    </div>
                    <div id="dist_por_tipo" class="col-sm-12" style="display: none;">
                                    <div class="callout callout-info col-sm-12" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> Nota:</h4>
        Si en la grafica resultante no aparece alguno de los productos seleccionados, es debido a que no hay registros de compras de dicho producto en el intervalo de fechas seleccionadas.
      </div>
                        <label for="listaproductos" class="col-sm-12">Productos a elegir</label>
                        <select class="js-example-basic-multiple js-states form-control" name="listaproductos[]" multiple="multiple" id="listaproductos" >
                            @if(isset($listaproductos))
                                @foreach($listaproductos as $s)
                                <option value="{{$s->id}}" id="prod_{{$s->id}}" medicion="{{$s->medicion->simbolo}}" codigo="{{$s->codigo}}" title="{{$s->nombre}}" costo-prod="{{$s->costo_produccion}}" precio-v="{{$s->precio_venta}}"
                                    @if(isset($listaproductos2) and in_array($s->id,$listaproductos2))
                                    selected
                                    @endif
                                    >{{$s->codigo}} {{strtoupper($s->nombre)}} {{$s->medicion->nombre}}</option>
                              @endforeach
                              @endif
                        </select>
                        
                    <div id="container3" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
                    </div>
                    <div id="dist_por_cat2" class="col-sm-12" style="display: none;">                        
                    <div id="container5" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
                    </div>
                    <div id="dist_por_cat" class="col-sm-12" style="display: none;">
                           <div class="callout callout-info col-sm-12" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> Nota:</h4>
        Si en la grafica resultante no aparece alguna de las categorias seleccionadas, es debido a que no hay registros de compras de dicha categoria en el intervalo de fechas seleccionadas.
      </div>
                        <label for="listacategorias" class="col-sm-12">Categorias a elegir</label>
                        @if(isset($listacategorias))
                        <select class="js-example-basic-multiple js-states form-control" name="listacategorias[]" multiple="multiple" id="listacategorias" >
                            
                                @foreach($listacategorias as $s)
                                <option value="{{$s->id}}" id="cat_{{$s->id}}" title="{{$s->nombre}}"
                                    @if(isset($listacategorias2) and in_array($s->id,$listacategorias2))
                                    selected
                                    @endif
                                    >{{strtoupper($s->nombre)}}</option>
                              @endforeach
                              
                        </select>
                        <div id="container4" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
                        @else
                        No existen categorias registradas
                        @endif
                    </div>
                    <div id="rep_comp" class="col-sm-12" style="display: none;">                        
                    <div id="container6" style="min-width: 310px; height: 800px; margin: 0 auto;">
                        
            <a href="{{url('imprimir-compras',['fein' => $fromDate,'fefi' => $toDate,'tipo' => $tipo])}}" target="_blank" id="btn-imprimir" class="btn btn-success" style="float:right;"><i class="fa fa-print" style="padding-right: 10px;"></i>Imprimir</a>
<div class="row"></div>
<label></label>
                <table class="table table-bordered table-striped table-responsive">

                <thead>

                  <tr>
                    <th> # </th>

                    <!--th>Factura N #</th>

                    <th>N. Orden</th-->
                    
                    <th>Fecha</th>

                    <!--th>Subtotal</th>

                    <th>Impuesto</th-->

                    <th>Total</th>

                  </tr>

                </thead>

                <tbody id="tabla_datos">


<?php
 $c_counter = 0;
 $c_subtotal = 0;
 $c_impuestos = 0;
 $c_total = 0;

?>
                @foreach($facturas as $f)


                  <tr>
                    <td> {{++$c_counter}}</td>

                    <td>{{$f->fecha_facturacion}}</td>

                    
                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f->total)}} </td>

                    <?php 
                    $c_subtotal += $f->subtotal;
                    $c_impuestos += $f->monto_iva;
                    $c_total += $f->total;
                    ?>

                  </tr>

                @endforeach

                </tbody>
                <tfoot class="push-rigth" id="tfoot">
                    <tr>
                        <td colspan="2">Subtotal</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_subtotal)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Impuestos</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_impuestos)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">TOTAL</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_total)}}</td>
                    </tr>
                </tfoot>
              </table>
                    
                    </div>
                    </div>
                    
                    
                </div>
                </form> 
                @else
                No hay resultados que cumplan con los parametros seleccionados &nbsp;<a href="javascript: window.history.back();" class="btn btn-info"><< Volver</a>
                @endif
                @if($contadorCompras == 0)
                <div class="callout callout-warning" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> IMPORTANTE:</h4> No existen compras registradas, por tanto, no podrá visualizar ningun informe de compra. <a href="{{url('/create_facturacompra')}}">Hacer primera compra</a>
      </div>
                @endif
                     
            </div>
        
            <!-- /.box-body -->
          </div>
      
    </section>
</div>
<script type="text/javascript">
Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    },
    chart: {
        events: {
            
            load: function () {
                /*this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);*/
                
                @if($sistema->show_logo1_repo == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1);  
          $width = 140; $height = 140;

                //list($width, $height) = getimagesize(url('/storage/'.$filename)); 
          ?>
          this.mylogo = this.renderer.image("{{url('/storage/'.$filename)}}", 20, 20, {{$width}}, {{$height}}).add();
          @endif
                
            }
        }
    }
});
             Highcharts.chart('container1', {
            chart: {
                zoomType: 'x'
            },
            title: {

                text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Comparacion de volumen de compras diarias<br> (de produccion y no produccion)' +
                      '</center>'
                      ,
                useHTML: true,
                align: 'center'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Haga click y arrastre para hacer zoom' : 'Haga click para hacer zoom'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Subtotal diario'
                }
            },
            legend: {
                enabled: true
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'line',
                name: 'Subtotal Producciones',
                data: [
                    @foreach($compras_produccion as $key => $value)
                                    [{{$key*1000}},{{$value}}],
                                @endforeach 

                ]
            },{
                type: 'line',
                name: 'Subtotal Productos',
                data: [
                @foreach($compras_productos as $key => $value)
                                    [{{$key*1000}},{{$value}}],
                                @endforeach 
                ]
            },{
                type: 'line',
                name: 'Subtotal Todos',
                data: [
                @foreach($compras_todas as $key => $value)
                                    [{{$key*1000}},{{$value}}],
                                @endforeach 
                ]
            }]
        });
   
        </script>
    <script type="text/javascript">
        Highcharts.chart('container2', {
    chart: {
        type: 'pie'
    },
    title: {
        text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Distribucion de inversion en compras por tipo<br/>Total invertido en compras: {{$sistema->currency->symbol}} {{$totalCompras}}<br/>Cantidad de compras realizadas: {{$cantTotalCompras}}' + 
                      '</center>',
        useHTML: true
    },
    subtitle: {
        text: 'Desde {{$fromDate}} hasta {{$toDate}}<br>Haga click en una de las secciones para ver detalles'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.cantidad} {point.und}<br/>{{$sistema->currency->symbol}} {point.valor}<br/>{point.y:.1f}%'
            }
        },
        
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"><b>{point.name}<br/> {point.cantidad} {point.und}<br/>Total: {{$sistema->currency->symbol}} {point.valor} </b></span><br/>{point.y:.2f}%<br/></b>'
    },
    series: [{
        name: 'Tipo de compra',
        colorByPoint: true,
        data: [
            @foreach($tipos as $key => $value)
            {
                name: '{{$value['titulo']}}',
                und: 'compras',
                y: {{$value['porcentaje']}},
                drilldown: '{{$value['titulo']}}',
                cantidad: {{$value['cantidad']}},
                valor: {{$value['total']}},
                grantotal: {{$totalCompras}}
            },
            @endforeach
        ]
    }],
    drilldown: {
        series: [{
            name: 'producciones',
            id: 'producciones',
            data: [
                 @foreach($productosMasComprados1 as $key => $value)
            {
                name: '{{$value->nombre}}',
                und: '{{$value->medicion}}',
                y: {{$totalComprasProducciones > 0 ? ($value->total_compra * 100 / $totalComprasProducciones) : 0}},
                cantidad: {{$value->total_cantidad > 0 ? $value->total_cantidad : 0}},
                valor: {{$value->total_compra > 0 ? $value->total_compra : 0}},
                grantotal: {{$totalComprasProducciones}}
            },
            @endforeach
            ]
        }, {
            name: 'productos',
            id: 'productos',
            data: [
                 @foreach($productosMasComprados2 as $key => $value)
            {
                name: '{{$value->nombre}}',
                und: '{{$value->medicion}}',
                y: {{$totalComprasProductos > 0 ? ($value->total_compra * 100 / $totalComprasProductos) : 0}},
                cantidad: {{$value->total_cantidad > 0 ? $value->total_cantidad : 0}},
                valor: {{$value->total_compra > 0 ? $value->total_compra : 0}},
                grantotal: {{$totalComprasProductos}}
            },
            @endforeach
            ]
        }]
    }
});
    </script>
@(isset($categoriasMasCompradas) and !empty(($categoriasMasCompradas)))
 <script type="text/javascript">
        Highcharts.chart('container5', {
    chart: {
        type: 'pie'
    },
    title: {
        text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Distribucion de inversion en compras por categorias y subcategorias<br/>Total invertido en compras: {{$sistema->currency->symbol}} {{$totalCompras}}<br/>Cantidad de compras realizadas: {{$cantTotalCompras}}' + 
                      '</center>',
        useHTML: true
    },
    subtitle: {
        text: 'Desde {{$fromDate}} hasta {{$toDate}}<br>Haga click en una de las secciones para ver las subcategorias'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.cantidad} {point.und}<br/>{{$sistema->currency->symbol}} {point.valor}<br/>{point.y:.1f}%'
            }
        },
        
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"><b>{point.name}<br/> {point.cantidad} {point.und}<br/>Total: {{$sistema->currency->symbol}} {point.valor} </b></span><br/>{point.y:.2f}%<br/></b>'
    },
    series: [{
        name: 'Tipo de compra',
        colorByPoint: true,
        data: [
        <?php $sumaTotalSerie2 = 0; ?>
            @foreach($categoriasMasCompradas as $key => $value)
            {
                name: '{{$value->nombre}}',
                und: 'compras',
                y: {{$totalCompras > 0 ? ($value->total_compra * 100 / $totalCompras) : 0}},
                drilldown: '{{$value->nombre}}',
                cantidad: {{$value->total_cantidad}},
                valor: {{$value->total_compra}},
                grantotal: {{$totalCompras}}
                <?php $sumaTotalSerie2 += $value->total_compra; ?>
            },
            @endforeach
            @if($sumaTotalSerie2 < $totalCompras)
            {

                name: 'Otras',
                und: '',
                y: {{$totalCompras > 0 ? (($totalCompras - $sumaTotalSerie2) * 100 / $totalCompras) : 0}},
                cantidad: '',
                valor: {{($totalCompras - $sumaTotalSerie2) > 0 ? ($totalCompras - $sumaTotalSerie2) : 0}},
                grantotal: {{$totalCompras}}
            }
            @endif
        ]
    }],
    drilldown: {
        series: [
        @foreach($categoriasMasCompradas as $key => $value)
            {
            name: '{{$value->nombre}}',
            id: '{{$value->nombre}}',
            data: [
                 @foreach($subcategoriasMasCompradas[$value->nombre] as $key => $value2)
            {
                name: '{{$value2->nombre}}',
                y: {{$value->total_compra > 0 ? ($value2->total_compra * 100 / $value->total_compra) : 0}},
                cantidad: {{$value2->total_cantidad > 0 ? $value2->total_cantidad : 0}},
                valor: {{$value2->total_compra > 0 ? $value2->total_compra : 0}},
                grantotal: {{$value->total_compra}}
            },
            @endforeach
            ]},

            @endforeach
            ]
        }    
});
    </script>
@endif  
@if(isset($categoriasMasCompradas2) and !empty($categoriasMasCompradas2))    
<script type="text/javascript">
        Highcharts.chart('container4', {
    chart: {
        type: 'pie'
    },
    title: {
        text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Distribucion de inversion en compras por categoria y subcategorias<br/>Total invertido en compras: {{$sistema->currency->symbol}} {{$totalCompras}}<br/>Cantidad de compras realizadas: {{$cantTotalCompras}}' + 
                      '</center>',
        useHTML: true
    },
    subtitle: {
        text: 'Desde {{$fromDate}} hasta {{$toDate}}<br>Haga click en una de las secciones para ver las subcategorias'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.cantidad} {point.und}<br/>{{$sistema->currency->symbol}} {point.valor}<br/>{point.y:.1f}%'
            }
        },
        
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"><b>{point.name}<br/> {point.cantidad} {point.und}<br/>Total:{{$sistema->currency->symbol}} {point.valor} </b></span><br/>{point.y:.2f}%<br/></b>'
    },
    series: [{
        name: 'Tipo de compra',
        colorByPoint: true,
        data: [
        <?php $sumaTotalSerie2 = 0; ?>
            @foreach($categoriasMasCompradas2 as $key => $value)
            {
                name: '{{$value->nombre}}',
                und: 'compras',
                y: {{$totalCompras > 0 ? ($value->total_compra * 100 / $totalCompras) : 0}},
                drilldown: '{{$value->nombre}}',
                cantidad: {{$value->total_cantidad}},
                valor: {{$value->total_compra}},
                grantotal: {{$totalCompras}}
                <?php $sumaTotalSerie2 += $value->total_compra; ?>
            },
            @endforeach
            @if($sumaTotalSerie2 < $totalCompras)
            {

                name: 'Otras',
                und: '',
                y: {{$totalCompras > 0 ? (($totalCompras - $sumaTotalSerie2) * 100 / $totalCompras) : 0}},
                cantidad: '',
                valor: {{($totalCompras - $sumaTotalSerie2) > 0 ? ($totalCompras - $sumaTotalSerie2) : 0}},
                grantotal: {{$totalCompras}}
            }
            @endif
        ]
    }],
    drilldown: {
        series: [
        @foreach($categoriasMasCompradas2 as $key => $value)
            {
            name: '{{$value->nombre}}',
            id: '{{$value->nombre}}',
            data: [
                 @foreach($subcategoriasMasCompradas2[$value->nombre] as $key => $value2)
            {
                name: '{{$value2->nombre}}',
                y: {{$value->total_compra > 0 ? ($value2->total_compra * 100 / $value->total_compra) : 0}},
                cantidad: {{$value2->total_cantidad > 0 ? $value2->total_cantidad : 0}},
                valor: {{$value2->total_compra > 0 ? $value2->total_compra : 0}},
                grantotal: {{$value->total_compra}}
            },
            @endforeach
            ]},

            @endforeach
            ]
        }    
});
    </script>
@endif    
@if(isset($productosMasComprados) and !empty($productosMasComprados))    
<script type="text/javascript">
        Highcharts.chart('container3', {
    chart: {
        type: 'pie'
    },
    title: {
        text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Distribucion de inversion en compras por producto<br/>Total invertido en compras: {{$sistema->currency->symbol}} {{$totalCompras}}<br/>Cantidad de compras realizadas: {{$cantTotalCompras}}' + 
                      '</center>',
        useHTML: true
    },
    subtitle: {
        text: 'Desde {{$fromDate}} hasta {{$toDate}}'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.valor}$<br/>{point.y:.1f}%'
            }
        },
        
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"><b>{point.name}<br/> {point.cantidad} {point.und}<br/>Total: {{$sistema->currency->symbol}} {point.valor} </b></span><br/>{point.y:.2f}% del total {{$sistema->currency->symbol}} {point.grantotal}<br/></b>'
    },
    series: [{
        name: 'Tipo de compra',
        colorByPoint: true,
        data: [
        <?php $sumaTotalSerie = 0; ?>
            @foreach($productosMasComprados as $key => $value)
            {
                name: '{{$value->nombre}}',
                und: '{{$value->medicion}}',
                y: {{$totalCompras > 0 ? ($value->total_compra * 100 / $totalCompras) : 0}},
                cantidad: {{$value->total_cantidad > 0 ? $value->total_cantidad : 0}},
                valor: {{$value->total_compra > 0 ? $value->total_compra : 0}},
                grantotal: {{$totalCompras}}
                <?php $sumaTotalSerie += $value->total_compra; ?>
            },
            @endforeach

            @if($sumaTotalSerie < $totalCompras)
            {

                name: 'Otros',
                und: '',
                y: {{$totalCompras > 0 ? (($totalCompras - $sumaTotalSerie) * 100 / $totalCompras) : 0}},
                cantidad: '',
                valor: {{($totalCompras - $sumaTotalSerie) > 0 ? ($totalCompras - $sumaTotalSerie) : 0}},
                grantotal: {{$totalCompras}}
            }
            @endif
        ]
    }]
    
});
    </script>
@endif  
<script type="text/javascript">
    selectedForm = "";
    $(document).ready(function(){
        // In your Javascript (external .js resource or <script> tag)

    $('#listaproductos').select2({
        theme: 'classic',
        width: '100%',
        placeholder: 'Seleccione los productos'
        });
    $('#listacategorias').select2({
        theme: 'classic',
        width: '100%',
        placeholder: 'Seleccione las categorias'
    });

        $('#tipo_informe').change(function(e){
            selected = $('#tipo_informe').val();
            previousForm = null;

            if(selectedForm != "")
                previousForm = $("#"+selectedForm);
            else
                previousForm = $("#"+selected);
            
            currentForm = $("#"+selected);

            if(previousForm.hasClass('shown')){
                previousForm.fadeOut();    
                previousForm.removeClass('shown');
                currentForm.fadeIn();
                currentForm.addClass('shown');
            }else{
                
                currentForm.fadeIn();
                currentForm.addClass('shown');
            }
            selectedForm = selected;

            if(selected == "rep_comp"){
                $("#select_tipo").show();
            }else{
                $("#select_tipo").hide();
            }
        });

        $('#tipo_informe').change();
    });
</script>
<script>

        $(document).ready(function(){

            $('.table').DataTable({

                 

             });

        });

    </script>
@endsection
