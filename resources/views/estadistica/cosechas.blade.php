@extends('master')
@section('title', 'Estadistica de Cosechas')
@section('estadistica', 'active')
@section('estadistica-cosecha', 'active')
@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Cosechas
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div>
                    <form method="POST">
                        Consultar a&ntilde;o 
                        <select name="anio">
                            @php
                            $anio = 2017;
                            @endphp
                            @while($anio <= $anio_actual)
                            <option value="{{$anio}}"
                            @if($anio == $anio_seslected)
                            selected
                            @endif 
                            >{{$anio++}}</option>
                            @endwhile
                        </select>
                        <input type="submit" value="Ok">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    </form>
                </div>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        
                <script type="text/javascript">
                    Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    },
    chart: {
        events: {
            
            load: function () {
                /*this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);*/
                
                @if($sistema->show_logo1_repo == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1);  
          $width = 140; $height = 140;

                //list($width, $height) = getimagesize(url('/storage/'.$filename)); 
          ?>
          this.mylogo = this.renderer.image("{{url('/storage/'.$filename)}}", 20, 20, {{$width}}, {{$height}}).add();
          @endif
                
            }
        }
    }
});
                    Highcharts.chart('container', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Estadisticas de Producción Mensuales del año: <b>{{$anio_actual}}</b>' +
                      '</center>',
                      useHTML: true
                        },
                        subtitle: {
                            text: 'Producción por hectárea mensuales en {{$sistema->currency->name}} {{$sistema->currency->symbol}}'
                        },
                        xAxis: {
                            categories: [
                                'Ene',
                                'Feb',
                                'Mar',
                                'Abr',
                                'May',
                                'Jun',
                                'Jul',
                                'Ago',
                                'Sep',
                                'Oct',
                                'Nov',
                                'Dic'
                            ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Producción por hectarea en kg'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{{$sistema->currency->symbol}} {point.y:.2f}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0,
                                color: "orange",
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        series: [{
                            name: 'Producción por hectárea',
                            data: [
                                @foreach($producciones as $c)
                                    {{round($c,2)}},
                                @endforeach

                            ]

                        }]
                    });
                </script>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


        {{-- ANUALES --}}
       <!--div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            <!--div class="box-body">

                <div id="container2"></div>

                <script type="text/javascript">
                    Highcharts.chart('container2', {

                        title: {
                            text: '<center>' +
                            @php
                                  if($sistema->show_membrete){
                                        echo('"'.$sistema->membrete.'<br><br>"+');
                                   }
                            @endphp
                      'Estadisticas de Egresos Anuales' + '</center>',
                      useHTML: true
                        },

                        subtitle: {
                            text: 'Cantidad de Egresos anuales en {{$sistema->currency->name}} {{$sistema->currency->symbol}}'
                        },

                        yAxis: {
                            title: {
                                text: 'Cantidad de Egresos en {{$sistema->currency->symbol}}'
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },

                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                },
                                pointStart: {{$anio_actual-1}},
                                color: "orange",
                                dataLabels: {
                                    enabled: true
                                }
                                }
                                
                        },

                        series: [{
                            name: 'Rango de Egresos',
                            data: [

                                 @foreach($totales as $t)
                                    {{round($t,2)}},
                                @endforeach

                            ]
                        }],

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    });
                </script>

            </div>
            <!-- /.box-body -->
          <!--/div-->
          <!-- /.box -->
    </section>
</div>
@endsection
