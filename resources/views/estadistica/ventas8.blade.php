@extends('master')

@section('title', 'Ventas')

@section('active-venta', 'active')

@section('active-venta-estadistica', 'active')

@section('active-estadisticas-ventas8', 'active')

@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Ventas - Reporte global por categorias
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">
    	<!-- caja de busqueda -->
        <div class="box box-info"> 
        	<div class="box-header with-border">
	         	<h3 class="box-title">Filtros de busqueda</h3>

	          	<div class="box-tools pull-right">
	            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          	</div>
	        </div>
	        <div class="box-body"> 
	        	<!-- inline form -->
	        	<div class="row">
		        	<form method="POST" action="{{ route('filtrar_estadisticas_ventas_globales') }}">
		        		<input type="hidden" name="_token" value="{!! csrf_token() !!}">
		        		<div class="col-md-12">
			        		<div class="col-md-4">		        			
				        		<div class="form-group">
				        			<select name="categoria_id" class="form-control" >
				        				<option value="0">TODAS</option>
				        				@foreach($categorias as $categoria)
				        					<option value="{{ $categoria->id }}"
				        					 @if(isset($categoria_id) && $categoria_id == $categoria->id)
				        					  selected 
				        					  @endif>{{ $categoria->nombre }}</option>
				        				@endforeach
				        			</select>
				        		</div>				        	
				        	</div>
				        	<div class="col-md-2">
				        		<div class="form-group col-md-4">
				        			<button class="btn btn-info" type="submit">FILTRAR</button>
				        		</div>
				        	</div>			        	
				        </div>		        	
				    </form>
		        </div>
	        </div>
        </div>
        <!--/ caja de busqueda -->
        <!-- resultados -->
        <div class="row">
        	<div class="col-md-12">
        		<!-- BAR CHART -->
	          	<div class="box box-success">
	            	<div class="box-header with-border">
	              		<h3 class="box-title">Total ventas de hoy</h3>
	              		<div class="box-tools pull-right">
	                		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                		</button>
	              		</div>
	            	</div>
	            	<div class="box-body">
	              		<div class="chart">
	                		<div id="totales_diarios" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	              		</div>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
          		<!-- /.box -->
          	</div>
          	<div class="col-md-12">
          		<!-- AREA CHART -->
          		<div class="box box-primary">
            		<div class="box-header with-border">
              			<h3 class="box-title">Promedio de ventas de hoy</h3>

			            <div class="box-tools pull-right">
			                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
			                </button>
			            </div>
            		</div>
		            <div class="box-body">
		              	<div class="chart">
		                	<div id="promedios_diarios" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
		              	</div>
		            </div>
	            <!-- /.box-body -->
	          	</div>
          		<!-- /.box -->
			</div>
			<div class="col-md-12">
          		<!-- DONUT CHART -->
          		<div class="box box-danger">
            		<div class="box-header with-border">
              			<h3 class="box-title">Total de ventas semanales</h3>

              			<div class="box-tools pull-right">
                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                			</button>
              			</div>
            		</div>
            		<div class="box-body">
              			<div id="totales_semanales" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            		</div>
            		<!-- /.box-body -->
          		</div>
          	</div>
      		<!-- /.box -->
      		<div class="col-md-12">
          		<!-- LINE CHART -->
          		<div class="box box-info">
            		<div class="box-header with-border">
              			<h3 class="box-title">Promedio de ventas semanales</h3>

              			<div class="box-tools pull-right">
                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                			</button>
              			</div>
            		</div>
            		<div class="box-body">
              			<div class="chart">
                			<div id="promedios_semanales" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
              			</div>
            		</div>
            		<!-- /.box-body -->
          		</div>
          		<!-- /.box -->
          	</div>
          	<div class="col-md-12">
          		<!-- DONUT CHART -->
          		<div class="box box-danger">
            		<div class="box-header with-border">
              			<h3 class="box-title">Total de ventas mensuales</h3>

              			<div class="box-tools pull-right">
                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                			</button>
              			</div>
            		</div>
            		<div class="box-body">
              			<div id="totales_mensuales" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            		</div>
            		<!-- /.box-body -->
          		</div>
          	</div>
      		<!-- /.box -->
      		<div class="col-md-12">
          		<!-- LINE CHART -->
          		<div class="box box-info">
            		<div class="box-header with-border">
              			<h3 class="box-title">Promedio de ventas mensuales</h3>

              			<div class="box-tools pull-right">
                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                			</button>
              			</div>
            		</div>
            		<div class="box-body">
              			<div class="chart">
                			<div id="promedios_mensuales" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
              			</div>
            		</div>
            		<!-- /.box-body -->
          		</div>
          		<!-- /.box -->
          	</div>
    	</div>
	    <!-- /.col (RIGHT) -->
    <!--/ resultados -->
	</section>
</div>
<script src="https://www.highcharts.com/media/com_demo/js/highslide-full.min.js"></script>
<script src="https://www.highcharts.com/media/com_demo/js/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="https://www.highcharts.com/media/com_demo/css/highslide.css" />
<!-- page script -->
<script type="text/javascript">
	
	var totales_diarios = @json($totales_diarios);
	var promedios_diarios = @json($promedios_diarios);
	var totales_semanales = @json($totales_semanales);
	var promedios_semanales = @json($promedios_semanales);
	var totales_mensuales = @json($totales_mensuales);
	var promedios_mensuales = @json($promedios_mensuales);

	Highcharts.dateFormats = {
	    W: function (timestamp) {
	        var date = new Date(timestamp),
	            day = date.getUTCDay() === 0 ? 7 : date.getUTCDay(),
	            dayNumber;
	        date.setDate(date.getUTCDate() + 4 - day);
	        dayNumber = Math.floor((date.getTime() - new Date(date.getUTCFullYear(), 0, 1, -6)) / 86400000);
	        return 1 + Math.floor(dayNumber / 7);

	    }
	};
    Highcharts.chart('totales_diarios', {
        
        title: {
            text: 'Total de ventas del dia de hoy: {{ sprintf("$ %.2f", $total_diario) }}'
        },

        subtitle: {
            text: "{{ date('Y-m-d H:i:s') }}"
        },

        xAxis: {
        	type: 'datetime',
        	title: {
        		text: "Hora"
        	},
            tickInterval: 3600 * 1000, // por horas
            tickWidth: 5,
            gridLineWidth: 2,
            labels: {
                align: 'left',
                x: 3,
                y: -3
            },
            showFirstLabel: true
        },

        yAxis: [{ // left y axis
            title: {
                text: 'monto en USD'
            },
            labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value: .2f}'
            },
            showFirstLabel: false
        }, { // right y axis
            linkedTo: 0,
            gridLineWidth: 0,
            opposite: true,
            title: {
                text: null
            },
            labels: {
                align: 'right',
                x: -3,
                y: 16,
                format: '{value:.2f}'
            },
            showFirstLabel: false
        }],

        legend: {
            align: 'left',
            verticalAlign: 'top',
            y: 20,
            floating: true,
            borderWidth: 0,            
        },

        tooltip: {
            shared: true,
            crosshairs: true
        },

        plotOptions: {
            series: {
            	dataLabels: {
            		enabled: true,
            	},
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            hs.htmlExpand(null, {
                                pageOrigin: {
                                    x: e.pageX || e.clientX,
                                    y: e.pageY || e.clientY
                                },
                                headingText: this.series.name,
                                maincontentText: Highcharts.dateFormat('%A, %b %e, %Y %H:%M:%S', this.x) + '<br/> $' +
                                    this.y,
                                width: 200
                            });
                        }
                    }
                },
                marker: {
                    lineWidth: 1
                }
            }
        },

        series: [{
            name: 'Todas las ventas',
            lineWidth: 4,
            marker: {
                radius: 4
            },
            data: totales_diarios,
            pointStart: Date.parse('{{ date("Y-m-d") }}'),
            pointInterval: 3600 * 1000 // one hour
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 600
                },
                chartOptions: {
                    legend: {
                        verticalAlign: 'bottom',
                        y: 0,
                        floating: false
                    }
                }
            }]
        }
    });

    Highcharts.chart('promedios_diarios', {
        
        title: {
            text: 'Promedio de ventas del dia de hoy: {{ sprintf("$ %.2f", $promedio_diario) }}'
        },

        subtitle: {
            text: "{{ date('Y-m-d H:i:s') }}"
        },

        xAxis: {
        	type: 'datetime',
        	title: {
        		text: "Hora"
        	},
            tickInterval: 3600 * 1000, // por horas
            tickWidth: 5,
            gridLineWidth: 2,
            labels: {
                align: 'left',
                x: 3,
                y: -3
            },
            showFirstLabel: true
        },

        yAxis: [{ // left y axis
            title: {
                text: 'monto en USD'
            },
            labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value: .2f}'
            },
            showFirstLabel: false
        }, { // right y axis
            linkedTo: 0,
            gridLineWidth: 0,
            opposite: true,
            title: {
                text: null
            },
            labels: {
                align: 'right',
                x: -3,
                y: 16,
                format: '{value:.2f}'
            },
            showFirstLabel: false
        }],

        legend: {
            align: 'left',
            verticalAlign: 'top',
            y: 20,
            floating: true,
            borderWidth: 0
        },

        tooltip: {
            shared: true,
            crosshairs: true
        },

        plotOptions: {
            series: {
            	dataLabels: {
            		enabled: true
            	},
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            hs.htmlExpand(null, {
                                pageOrigin: {
                                    x: e.pageX || e.clientX,
                                    y: e.pageY || e.clientY
                                },
                                headingText: this.series.name,
                                maincontentText: Highcharts.dateFormat('%A, %b %e, %Y %H:%M:%S', this.x) + '<br/> $' +
                                    this.y,
                                width: 200
                            });
                        }
                    }
                },
                marker: {
                    lineWidth: 1
                }
            }
        },

        series: [{
            name: 'Todas las ventas',
            lineWidth: 4,
            marker: {
                radius: 4
            },
            data: promedios_diarios,
            pointStart: Date.parse('{{ date("Y-m-d") }}'),
            pointInterval: 3600 * 1000 // one hour
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 600
                },
                chartOptions: {
                    legend: {
                        verticalAlign: 'bottom',
                        y: 0,
                        floating: false
                    }
                }
            }]
        }
    });

    Highcharts.chart('totales_semanales', {
        
        title: {
            text: 'Total de ventas semanales: {{ sprintf("$ %.2f", $total_semanal) }}'
        },

        subtitle: {
            text: "Semanas del mes {{ date('m') }} del año {{ date('Y') }}"
        },

        xAxis: {
        	type: 'datetime',
        	title: {
        		text: "Semana"
        	},
            tickInterval: 7 * 24 * 36e5, // one week
            tickWidth: 5,
            gridLineWidth: 2,
            labels: {
                align: 'left',
                x: 3,
                y: -3,
                format: '{value: %W/%Y}',
            },
            showFirstLabel: true
        },

        yAxis: [{ // left y axis
            title: {
                text: 'monto en USD'
            },
            labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value: .2f}'
            },
            showFirstLabel: false
        }, { // right y axis
            linkedTo: 0,
            gridLineWidth: 0,
            opposite: true,
            title: {
                text: null
            },
            labels: {
                align: 'right',
                x: -3,
                y: 16,
                format: '{value:.2f}'
            },
            showFirstLabel: false
        }],

        legend: {
            align: 'left',
            verticalAlign: 'top',
            y: 20,
            floating: true,
            borderWidth: 0
        },

        tootltip:{
        	enabled: false,
        	format: "{value: sd}"
        },
        plotOptions: {

        tootltip:{
        	enabled: false,
        	format: "{value: sd}"
        },
            series: {
            	dataLabels: {
            		enabled: true
            	},
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            hs.htmlExpand(null, {
                                pageOrigin: {
                                    x: e.pageX || e.clientX,
                                    y: e.pageY || e.clientY
                                },
                                headingText: this.series.name,
                                maincontentText: Highcharts.dateFormat('Semana %W del año %Y', this.x) + '<br/> $' +
                                    this.y,
                                width: 200
                            });
                        }
                    }
                },
                marker: {
                    lineWidth: 1
                }
            }
        },

        series: [{
            name: 'Todas las ventas',
            lineWidth: 4,
            marker: {
                radius: 4
            },
            data: totales_semanales,
            pointStart: Date.parse('{{ date("Y") }}-01-07'),
            pointInterval: 24 * 7 * 36e5 // por semana
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 600
                },
                chartOptions: {
                    legend: {
                        verticalAlign: 'bottom',
                        y: 0,
                        floating: false
                    }
                }
            }]
        }
    });

 	Highcharts.chart('promedios_semanales', {
        
        title: {
            text: 'Promedio de ventas semanales: {{ sprintf("$ %.2f", $promedio_semanal) }}'
        },

        subtitle: {
            text: "Semanas del mes {{ date('m') }} del año {{ date('Y') }}"
        },

        xAxis: {
        	type: 'datetime',
        	title: {
        		text: "Semana"
        	},
            tickInterval: 24 * 7 * 36e5, // por semanas
            tickWidth: 5,
            gridLineWidth: 2,
            labels: {
                align: 'left',
                x: 3,
                y: -3
            },
            showFirstLabel: true,
            format: '{value: %W/%Y}',
        },

        yAxis: [{ // left y axis
            title: {
                text: 'monto en USD'
            },
            labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value: .2f}'
            },
            showFirstLabel: false
        }, { // right y axis
            linkedTo: 0,
            gridLineWidth: 0,
            opposite: true,
            title: {
                text: null
            },
            labels: {
                align: 'right',
                x: -3,
                y: 16,
                format: '{value:.2f}'
            },
            showFirstLabel: false
        }],

        legend: {
            align: 'left',
            verticalAlign: 'top',
            y: 20,
            floating: true,
            borderWidth: 0
        },

        tooltip: {
            shared: true,
            crosshairs: true
        },

        plotOptions: {
            series: {
            	dataLabels: {
            		enabled: true
            	},
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            hs.htmlExpand(null, {
                                pageOrigin: {
                                    x: e.pageX || e.clientX,
                                    y: e.pageY || e.clientY
                                },
                                headingText: this.series.name,
                                maincontentText: Highcharts.dateFormat('Semana %W del año %Y', this.x) + '<br/> $' +
                                    this.y,
                                width: 200
                            });
                        }
                    }
                },
                marker: {
                    lineWidth: 1
                }
            }
        },

        series: [{
            name: 'Todas las ventas',
            lineWidth: 4,
            marker: {
                radius: 4
            },
            data: promedios_semanales,
            pointStart: Date.parse('{{ date("Y") }}-01-07'),
            pointInterval: 24 * 7 * 36e5 // por semana
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 600
                },
                chartOptions: {
                    legend: {
                        verticalAlign: 'bottom',
                        y: 0,
                        floating: false
                    }
                }
            }]
        }
    });

    

	Highcharts.chart('totales_mensuales', {
	    chart: {
	        type: 'line'
	    },
	    title: {
	        text: 'Total de ventas mensuales'
	    },
	    subtitle: {
	        text: 'Meses del año {{ date('Y') }}'
	    },
	    xAxis: {
	        categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	    },
	    yAxis: {
	        title: {
	            text: 'Monto en USD'
	        },
	        labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value: .2f}'
            }
	    },
	    plotOptions: {
	        line: {
	            dataLabels: {
	                enabled: true
	            },
	            
	        }
	    },
	    series: [{
	        name: 'Total ventas',
	        data: totales_mensuales
	    }]
	});

 	Highcharts.chart('promedios_mensuales', {
	    chart: {
	        type: 'line'
	    },
	    title: {
	        text: 'Promedios de ventas mensuales'
	    },
	    subtitle: {
	        text: 'Meses del año {{ date('Y') }}'
	    },
	    xAxis: {
	        categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	    },
	    yAxis: {
	        title: {
	            text: 'Monto en USD'
	        },
	        labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value: .2f}'
            }
	    },
	    plotOptions: {
	        line: {
	            dataLabels: {
	                enabled: true
	            },
	            
	        }
	    },
	    series: [{
	        name: 'Promedio de ventas',
	        data: promedios_mensuales
	    }]
	});

</script>
</script>
@endsection
