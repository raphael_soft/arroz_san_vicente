@extends('master')

@section('title', 'Ventas')

@section('active-venta', 'active')

@section('active-venta-estadistica', 'active')

@section('active-estadisticas-ventas5', 'active')

@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Ventas - Mas vendido y menos vendido (Barras)
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            <form method="POST">
            <div class="box-body content">
                @if($totalVentas > 0)
                
                <div class="col-sm-12">
                    

                        <label>Consultar por fechas<label>
                        <div class="form-group col-sm-3">
                               <label >Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="fromDate" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label >Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="toDate" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    
                    <div id="informe7" class="col-sm-12">
                    <div id="container7" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
                    </div>
              
                </div>
                @else
                No hay resultados que cumplan con los parametros seleccionados &nbsp;<a href="javascript: window.history.back();" class="btn btn-info"><< Volver</a>
                @endif
                @if($contadorVentas == 0)
                <div class="callout callout-warning" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> IMPORTANTE:</h4> No existen ventas registradas, por tanto, no podrá visualizar ningun informe de ventas. <a href="{{url('/create_facturaventa')}}">Hacer primera venta</a>
      </div>
@endif
               </div> 
        </form>
          </div>
    </section>
</div>

<script type="text/javascript">
    Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    },
    chart: {
        events: {
            
            load: function () {
                /*this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);*/
                
                @if($sistema->show_logo1_repo == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1);  
          $width = 140; $height = 140;

                //list($width, $height) = getimagesize(url('/storage/'.$filename)); 
          ?>
          this.mylogo = this.renderer.image("{{url('/storage/'.$filename)}}", 20, 20, {{$width}}, {{$height}}).add();
          @endif
                
            }
        }
    }
});
             
        </script>
@if(isset($productosMasVendidos) and isset($productosMenosVendidos))
<script type="text/javascript">
                    Highcharts.chart('container7', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Producto más vendido y menos vendido<br>' + '<small>Desde {{$fromDate}} hasta {{$toDate}}</small>' + '</center>',
                      useHTML: true
                        },
                        subtitle: {
                            text: 'Cantidad de Ingresos mensuales en {{$sistema->currency->name}} {{$sistema->currency->symbol}}'
                        },
                        xAxis: {
                            categories: [
                                'Desde {{$fromDate}} hasta {{$toDate}}'                            ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Cantidad de Ingresos en {{$sistema->currency->symbol}}'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{{$sistema->currency->symbol}} {point.y:.2f}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0,
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        series: [
                        @if(isset($productosMenosVendidos[0]) and $productosMenosVendidos[0] != null)
                        {
                            name: '{{$productosMenosVendidos[0]->nombre}}',
                            data: [
                            
                                {{round($productosMenosVendidos[0]->total_venta,2)}}
                                
                            ]

                        }
                        @endif
                        ,
                        @if(isset($productosMasVendidos[0]) and $productosMasVendidos[0] != null)
                        {
                            name: '{{$productosMasVendidos[0]->nombre}}',
                            data: [
                            
                                {{round($productosMasVendidos[0]->total_venta,2)}}
                                
                            ]

                        }
                        @endif
                        ]
                    });
                </script>

@endif
                <script>

        $(document).ready(function(){

            $('.table').DataTable({

                 

             });

        });

    </script>
@endsection
