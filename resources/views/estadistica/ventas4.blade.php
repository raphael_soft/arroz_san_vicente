@extends('master')

@section('title', 'Ventas')

@section('active-venta', 'active')

@section('active-venta-estadistica', 'active')

@section('active-estadisticas-ventas4', 'active')

@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Ventas - Por categorias/subcategorias
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            <form method="POST">
            <div class="box-body content">
                @if($totalVentas > 0)
                
                <div class="col-sm-12">
                    

                        <label>Consultar por fechas<label>
                        <div class="form-group col-sm-3">
                               <label >Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="fromDate" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label >Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="toDate" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    
                    <div id="informe6" class="col-sm-12" >
                        <div class="callout callout-info col-sm-12" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> Nota:</h4>
        Si en la grafica resultante no aparece alguno de las categorias seleccionadas, es debido a que no hay registros de ventas de dicha categoria en el intervalo de fechas seleccionadas.
      </div>
                        <label for="listacategorias" class="col-sm-12">Categorias a elegir</label>
                        <select class="js-example-basic-multiple js-states form-control" name="listacategorias[]" multiple="multiple" id="listacategorias" >
                            @if(isset($listacategorias))
                                @foreach($listacategorias as $s)
                                <option value="{{$s->id}}" id="cat_{{$s->id}}"  title="{{$s->nombre}}"      
                                    @if(isset($listacategorias2) and in_array($s->id,$listacategorias2))
                                    selected
                                    @endif
                                    >{{strtoupper($s->nombre)}}</option>
                              @endforeach
                              @endif
                        </select>
                        <a id="selectAll" class="btn btn-info" >Seleccionar todas las categorias</a> 
                    <div id="container6" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
                    </div>
                    
                
                    
                   
              
                </div>
                @else
                No hay resultados que cumplan con los parametros seleccionados &nbsp;<a href="javascript: window.history.back();" class="btn btn-info"><< Volver</a>
                @endif
                @if($contadorVentas == 0)
                <div class="callout callout-warning" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> IMPORTANTE:</h4> No existen ventas registradas, por tanto, no podrá visualizar ningun informe de ventas. <a href="{{url('/create_facturaventa')}}">Hacer primera venta</a>
      </div>
@endif
               </div> 
        </form>
          </div>
    </section>
</div>

<script type="text/javascript">

    Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    },
    chart: {
        events: {
            
            load: function () {
                /*this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);*/
                
                @if($sistema->show_logo1_repo == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1);  
          $width = 140; $height = 140;

                //list($width, $height) = getimagesize(url('/storage/'.$filename)); 
          ?>
          this.mylogo = this.renderer.image("{{url('/storage/'.$filename)}}", 20, 20, {{$width}}, {{$height}}).add();
          @endif
                
            }
        }
    }
});
             
        </script>
 
@if(isset($categoriasMasVendidas2))    
<script type="text/javascript">
        Highcharts.chart('container6', {
    chart: {
        type: 'pie'
    },
    title: {
        text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Distribucion de ingresos en ventas por categoria y subcategorias<br/>Total ingresos en ventas: {{$sistema->currency->symbol}} {{$totalVentas}}<br/>Cantidad de vental realizadas: {{$cantTotalVentas}}' + '</center>',
        useHTML: true
    },
    subtitle: {
        text: 'Desde {{$fromDate}} hasta {{$toDate}}<br>Haga click en una de las secciones para ver las subcategorias'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.cantidad} {point.und}<br/>{{$sistema->currency->symbol}} {point.valor}<br/>{point.y:.1f}%'
            }
        },
        
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"><b>{point.name}<br/> {point.cantidad} {point.und}<br/>Total: {{$sistema->currency->symbol}} {point.valor} </b></span><br/>{point.y:.2f}%<br/></b>'
    },
    series: [{
        name: 'Tipo de venta',
        colorByPoint: true,
        data: [
        <?php $sumaTotalSerie2 = 0; ?>
            @foreach($categoriasMasVendidas2 as $key => $value)
            {
                name: '{{$value->nombre}}',
                und: 'venta',
                y: {{$totalVentas > 0 ? ($value->total_venta * 100 / $totalVentas) : 0}},
                drilldown: '{{$value->nombre}}',
                cantidad: {{$value->total_cantidad}},
                valor: {{$value->total_venta}},
                grantotal: {{$totalVentas}}
                <?php $sumaTotalSerie2 += $value->total_venta; ?>
            },
            @endforeach
            @if($sumaTotalSerie2 < $totalVentas)
            {

                name: 'Otras',
                und: '',
                y: {{$totalVentas > 0 ? (($totalVentas - $sumaTotalSerie2) * 100 / $totalVentas) : 0}},
                cantidad: '',
                valor: {{($totalVentas - $sumaTotalSerie2) > 0 ? ($totalVentas - $sumaTotalSerie2) : 0}},
                grantotal: {{$totalVentas}}
            }
            @endif
        ]
    }],
    drilldown: {
        series: [
        @foreach($categoriasMasVendidas2 as $key => $value)
            {
            name: '{{$value->nombre}}',
            id: '{{$value->nombre}}',
            data: [
                 @foreach($subcategoriasMasVendidas2[$value->nombre] as $key => $value2)
            {
                name: '{{$value2->nombre}}',
                y: {{$value->total_venta > 0 ? ($value2->total_venta * 100 / $value->total_venta) : 0}},
                cantidad: {{$value2->total_cantidad > 0 ? $value2->total_cantidad : 0}},
                valor: {{$value2->total_venta > 0 ? $value2->total_venta : 0}},
                grantotal: {{$value->total_venta}}
            },
            @endforeach
            ]},

            @endforeach
            ]}    
});
    </script>
@endif    


    <script>
        var allSelected = false;
        $(document).ready(function(){
            $('#listacategorias').select2({
                theme: 'classic',
                width: '100%'
            });

            $('#selectAll').click(function(){

                if(!allSelected){
                    $('#listacategorias').select2('destroy').find('option').prop('selected', 'selected').end().select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#selectAll').text('Deseleccionar todas las categorias');
                   // $('#listacategorias').trigger('change.select2');
                    allSelected = true;
                }else {
                    /* Stuff to do every *even* time the element is clicked */
                   /* $('#listacategorias > option').removeAttr('selected');
                    $('#listacategorias').trigger('change.select2');
                    */
                    $('#listacategorias').select2('destroy').find('option').prop('selected', false).end().select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#selectAll').text('Seleccionar todas las categorias');
                    allSelected = false;
                }
            });
            

            $('.table').DataTable({});

            });

    </script>
@endsection
