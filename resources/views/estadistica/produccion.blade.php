@extends('master')
@section('title', 'Estadistica de Venta')
@section('estadistica', 'active')
@section('estadistica-produccion', 'active')
@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Producci&oacute;n
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div>
                    <form method="POST">
                        Consulta Especifica
                        <input type="date" name="fecha" class="" max="{{ date('Y-m-d') }}" required />
                        <input type="submit" value="Ok">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    </form>
                </div>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                <script type="text/javascript">
                    Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    },
    chart: {
        events: {
            
            load: function () {
                /*this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);*/
                
                @if($sistema->show_logo1_repo == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1);  
          $width = 140; $height = 140;

                //list($width, $height) = getimagesize(url('/storage/'.$filename)); 
          ?>
          this.mylogo = this.renderer.image("{{url('/storage/'.$filename)}}", 20, 20, {{$width}}, {{$height}}).add();
          @endif
                
            }
        }
    }
});
                   
                    // obtener nombres de produccion
                    var nombres_produccion = @json($producciones);
                    var titulo_produccion = [];
                    for (var i = nombres_produccion.length - 1; i >= 0; i--)  {
                        titulo_produccion[i] = nombres_produccion[i].nombre;
                    }

                    // obtener valores de produccion
                    var valores_produccion = @json($valores);
                    var valor_produccion = [];
                    for (var i = valores_produccion.length - 1; i >= 0; i--)  {
                        if(valores_produccion[i].valor == null){
                            valor_produccion[i] = 0;
                        }else{
                            var aux = parseFloat(valores_produccion[i].valor);
                            valor_produccion[i] = aux;

                        }
                    }
                    console.log(valor_produccion);

                    Highcharts.chart('container', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Estadisticas de Produccion {{ ($fecha) ? $fecha : "Global" }}</b>' + '</center>',
                      useHTML: true
                        },
                        subtitle: {
                            text: 'Cantidad de Produccion {{ ($fecha) ? $fecha : "Global" }}'
                        },
                        xAxis: {
                            categories: titulo_produccion,
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Cantidad {{ ($fecha) ? $fecha : "Global" }}'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: 'Cantidad {{ ($fecha) ? $fecha : "Global" }}',
                            data: valor_produccion
                        }]
                    });
                </script>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    </section>
</div>
@endsection
