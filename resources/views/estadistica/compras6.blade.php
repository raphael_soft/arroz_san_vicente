@extends('master')
@section('title', 'Estadistica de Compras')
@section('active-compra', 'active')
@section('active-compra-estadisticas', 'active')
@section('active-estadisticas-compras6', 'active')
@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Compras - Reporte solo texto
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                
                
                
                @if($contTodasCompras > 0)
                
                <div class="col-sm-12" >
                    

                        <label>Parametros de busqueda</label>
                    </div>
      <div class="col-sm-12" >
        <form method="POST">
                        <div class="form-group col-sm-3">
                               <label>Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="fromDate" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label>Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="toDate" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
            
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        </form> 
            <a href="{{url('imprimir-compras',['fein' => $fromDate,'fefi' => $toDate,'tipo' => $tipo])}}" target="_blank" id="btn-imprimir" class="btn btn-success" style="float:right;"><i class="fa fa-print" style="padding-right: 10px;"></i>Imprimir</a>
<div class="row"></div>
<label></label>
                <table class="table table-bordered table-striped table-responsive">

                <thead>

                  <tr>
                    <th> # </th>

                    <th>Factura N #</th>

                    <!--th>N. Orden</th-->
                    
                    <th>Fecha</th>

                    <!--th>Subtotal</th-->

                    <th>Impuesto</th>

                    <th>Total</th>

                  </tr>

                </thead>

                <tbody id="tabla_datos">


<?php
 $c_counter = 0;
 $c_subtotal = 0;
 $c_impuestos = 0;
 $c_total = 0;

?>
                @foreach($facturas as $f)
                <?php 

                    $f_subtotal = 0;
                    $f_impuesto = 0;
                    $f_total = 0;
                if(isset($f->detalles)){    
                    foreach ($f->detalles as $key => $det) {
                                # code...
                            if(isset($det->precio) and isset($det->cantidad))
                                $f_subtotal += ($det->precio * $det->cantidad);
                            if(isset($det->impuesto->valor))
                                $f_impuesto += ($det->impuesto->valor * ($det->precio * $det->cantidad)) / 100;

                            }
                }
                    $f_total = $f_subtotal + $f_impuesto;        
                 ?>

                  <tr>
                    <td> {{++$c_counter}}
                    </td>

                    <td>{{$f->n_factura}}
                    </td>

                    <td>{{$f->fecha_facturacion}}
                    </td>

                   
                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f_impuesto)}}
                    </td>
                     
                    <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$f_total)}} 
                    </td>

                    <?php 
                    $c_subtotal += $f_subtotal;
                    $c_impuestos += $f_impuesto;
                    $c_total += $f_total;
                    ?>

                  </tr>

                @endforeach

                </tbody>
                <tfoot class="pull-rigth" id="tfoot">
                    <tr>
                        <td colspan="3 pull-right">Subtotal</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_subtotal)}}</td>
                    </tr>
                    <tr>
                        <td colspan="3 pull-right">Impuestos</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_impuestos)}}</td>
                    </tr>
                    <tr>
                        <td colspan="3 pull-right">TOTAL</td>
                        <td>{{$sistema->currency->symbol}} {{sprintf("%.2f",$c_total)}}</td>
                    </tr>
                </tfoot>
              </table>
                    
                    </div>
                    @else
                No hay resultados que cumplan con los parametros seleccionados &nbsp;<a href="javascript: window.history.back();" class="btn btn-info"><< Volver</a>
                @endif
                    </div>
            </div>
                
                
                @if($contTodasCompras == 0)
                <div class="callout callout-warning" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> IMPORTANTE:</h4> No existen compras registradas, por tanto, no podrá visualizar ningun informe de compra. <a href="{{url('/create_facturacompra')}}">Hacer primera compra</a>
      </div>
                @endif
                     
          
      
    </section>
</div>

<script type="text/javascript">
    selectedForm = "";
    $(document).ready(function(){
        // In your Javascript (external .js resource or <script> tag)

    
            $('.table').DataTable({
             });

        });

    </script>
@endsection
