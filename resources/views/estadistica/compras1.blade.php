@extends('master')
@section('title', 'Estadistica de Compras')
@section('active-compra', 'active')
@section('active-compra-estadisticas', 'active')
@section('active-estadisticas-compras1', 'active')
@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Compras - Distribucion de compras por productos
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                
                
                
                @if($totalCompras > 0)
                <form method="POST">
                <div class="col-sm-12" >
                    

                        <label>Parametros de busqueda</label>
                    </div>
      <div class="col-sm-12" >
                        <div class="form-group col-sm-3">
                               <label>Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="fromDate" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label>Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="toDate" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
            
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    
                    
                    <div id="dist_por_tipo" class="col-sm-12" >
                                    <div class="callout callout-info col-sm-12" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> Nota:</h4>
        Si en la grafica resultante no aparece alguno de los productos seleccionados, es debido a que no hay registros de compras de dicho producto en el intervalo de fechas seleccionadas.
      </div>
                        <label for="listaproductos" class="col-sm-12">Productos a elegir</label>
                        <select class="js-example-basic-multiple js-states form-control" name="listaproductos[]" multiple="multiple" id="listaproductos" >
                            @if(isset($listaproductos))
                                @foreach($listaproductos as $s)
                                <option value="{{$s->id}}" id="prod_{{$s->id}}" medicion="{{$s->medicion->simbolo}}" codigo="{{$s->codigo}}" title="{{$s->nombre}}" costo-prod="{{$s->costo_produccion}}" precio-v="{{$s->precio_venta}}"
                                    @if(isset($listaproductos2) and in_array($s->id,$listaproductos2))
                                    selected
                                    @endif
                                    >{{$s->codigo}} {{strtoupper($s->nombre)}} {{$s->medicion->nombre}}</option>
                              @endforeach
                              @endif
                        </select>
                        <a id="selectAll" class="btn btn-info" >Seleccionar todos los productos</a> 
                    <div id="container3" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
                </div>
            </div>
                </form> 
                @else
                No hay resultados que cumplan con los parametros seleccionados &nbsp;<a href="javascript: window.history.back();" class="btn btn-info"><< Volver</a>
                @endif
                @if($contadorCompras == 0)
                <div class="callout callout-warning" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> IMPORTANTE:</h4> No existen compras registradas, por tanto, no podrá visualizar ningun informe de compra. <a href="{{url('/create_facturacompra')}}">Hacer primera compra</a>
      </div>
                @endif
                     
            </div>
        
            <!-- /.box-body -->
          </div>
      
    </section>
</div>
<script type="text/javascript">
Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    },
    chart: {
        events: {
            
            load: function () {
                /*this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);*/
                
                @if($sistema->show_logo1_repo == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1);  
          $width = 140; $height = 140;

                //list($width, $height) = getimagesize(url('/storage/'.$filename)); 
          ?>
          this.mylogo = this.renderer.image("{{url('/storage/'.$filename)}}", 20, 20, {{$width}}, {{$height}}).add();
          @endif
                
            }
        }
    }
});
</script>

@if(isset($productosMasComprados))    
<script type="text/javascript">

        Highcharts.chart('container3', {
    chart: {
        type: 'pie'
    },
    title: {
        text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Distribucion de inversion en compras por producto<br/>Total invertido en compras: {{$sistema->currency->symbol}} {{$totalCompras}}<br/>Cantidad de compras realizadas: {{$cantTotalCompras}}' + 
                      '</center>',
        useHTML: true
    },
    subtitle: {
        text: 'Desde {{$fromDate}} hasta {{$toDate}}'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {{$sistema->currency->symbol}} {point.valor}<br/>{point.y:.1f}%'
            }
        },
        
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"><b>{point.name}<br/> {point.cantidad} {point.und}<br/>Total: {{$sistema->currency->symbol}} {point.valor} </b></span><br/>{point.y:.2f}% del total {{$sistema->currency->symbol}} {point.grantotal}<br/></b>'
    },
    series: [{
        name: 'Tipo de compra',
        colorByPoint: true,
        data: [
        <?php $sumaTotalSerie = 0; ?>
            @foreach($productosMasComprados as $key => $value)
            {
                name: '{{$value->nombre}}',
                und: '{{$value->medicion}}',
                y: {{$totalCompras > 0 ? ($value->total_compra * 100 / $totalCompras) : 0}},
                cantidad: {{$value->total_cantidad > 0 ? $value->total_cantidad : 0}},
                valor: {{$value->total_compra > 0 ? $value->total_compra : 0}},
                grantotal: {{$totalCompras}}
                <?php $sumaTotalSerie += $value->total_compra; ?>
            },
            @endforeach

            @if($sumaTotalSerie < $totalCompras)
            {

                name: 'Otros',
                und: '',
                y: {{$totalCompras > 0 ? (($totalCompras - $sumaTotalSerie) * 100 / $totalCompras) : 0}},
                cantidad: '',
                valor: {{($totalCompras - $sumaTotalSerie) > 0 ? ($totalCompras - $sumaTotalSerie) : 0}},
                grantotal: {{$totalCompras}}
            }
            @endif
        ]
    }]
    
});
    </script>
@endif  
<script type="text/javascript">
    selectedForm = "";
    var allSelected = false;
    $(document).ready(function(){

        $('#listaproductos').select2({
            theme: 'classic',
            width: '100%'
        });
        
        $('#selectAll').click(function(){

                if(!allSelected){
                    $('#listaproductos').select2('destroy').find('option').prop('selected', 'selected').end().select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#selectAll').text('Deseleccionar todos los productos');
                   // $('#listacategorias').trigger('change.select2');
                    allSelected = true;
                }else {
                    /* Stuff to do every *even* time the element is clicked */
                   /* $('#listacategorias > option').removeAttr('selected');
                    $('#listacategorias').trigger('change.select2');
                    */
                    $('#listaproductos').select2('destroy').find('option').prop('selected', false).end().select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#selectAll').text('Seleccionar todos los productos');
                    allSelected = false;
                }
        });        
        
        $('.table').DataTable({});

    });

    </script>
@endsection
