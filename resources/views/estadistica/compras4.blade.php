@extends('master')
@section('title', 'Estadistica de Compras')
@section('active-compra', 'active')
@section('active-compra-estadisticas', 'active')
@section('active-estadisticas-compras4', 'active')
@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Compras - Distribucion de compras por categorias y subcategorias
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                
                
                
                @if($totalCompras > 0)
                <form method="POST">
                <div class="col-sm-12" >
                    

                        <label>Parametros de busqueda</label>
                    </div>
      <div class="col-sm-12" >
                        <div class="form-group col-sm-3">
                               <label>Desde</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="fromDate" id="fromDate" value="{{$fromDate}}">
                               </div>
                        </div>
                        <div class="form-group col-sm-3">
                               <label>Hasta</label>
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" name="toDate" id="toDate" value="{{$toDate}}">
                               </div>
                        </div>    
                        <div class="form-group col-sm-1">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                    <input type="submit" value="Ok">
                            </div>
                        </div>
            
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    
                    
                    <div id="dist_por_cat" class="col-sm-12" >
                           <div class="callout callout-info col-sm-12" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> Nota:</h4>
        Si en la grafica resultante no aparece alguna de las categorias seleccionadas, es debido a que no hay registros de compras de dicha categoria en el intervalo de fechas seleccionadas.
      </div>
                        <label for="listacategorias" class="col-sm-12">Categorias a elegir</label>
                        @if(isset($listacategorias))
                        <select class="js-example-basic-multiple js-states form-control" name="listacategorias[]" multiple="multiple" id="listacategorias" >
                            
                                @foreach($listacategorias as $s)
                                <option value="{{$s->id}}" id="cat_{{$s->id}}" title="{{$s->nombre}}"
                                    @if(isset($listacategorias2) and in_array($s->id,$listacategorias2))
                                    selected
                                    @endif
                                    >{{strtoupper($s->nombre)}}</option>
                              @endforeach
                              
                        </select>
                        <a id="selectAll" class="btn btn-info" >Seleccionar todas las categorias</a> 
                        <div id="container4" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
                        @else
                        No existen categorias registradas
                        @endif
                    </div>
            </div>
                </form> 
                @else
                No hay resultados que cumplan con los parametros seleccionados &nbsp;<a href="javascript: window.history.back();" class="btn btn-info"><< Volver</a>
                @endif
                @if($contadorCompras == 0)
                <div class="callout callout-warning" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info-circle"></i> IMPORTANTE:</h4> No existen compras registradas, por tanto, no podrá visualizar ningun informe de compra. <a href="{{url('/create_facturacompra')}}">Hacer primera compra</a>
      </div>
                @endif
                     
            </div>
        
            <!-- /.box-body -->
          </div>
      
    </section>
</div>
<script type="text/javascript">
Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    },
    chart: {
        events: {
            
            load: function () {
                /*this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);*/
                
                @if($sistema->show_logo1_repo == true)
          <?php $filename = str_replace("public/",'',$sistema->logo1);  
          $width = 140; $height = 140;

                //list($width, $height) = getimagesize(url('/storage/'.$filename)); 
          ?>
          this.mylogo = this.renderer.image("{{url('/storage/'.$filename)}}", 20, 20, {{$width}}, {{$height}}).add();
          @endif
                
            }
        }
    }
});
</script>

@if(isset($categoriasMasCompradas2))    
<script type="text/javascript">
        Highcharts.chart('container4', {
    chart: {
        type: 'pie'
    },
    title: {
        text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Distribucion de inversion en compras por categoria y subcategorias<br/><smal>Desde {{$fromDate}} hasta {{$toDate}}</small><br>Total invertido en compras: {{$sistema->currency->symbol}} {{$totalCompras}}<br/>Cantidad de compras realizadas: {{$cantTotalCompras}}' + 
                      '</center>',
        useHTML: true
    },
    subtitle: {
        text: 'Haga click en una de las secciones para ver las subcategorias'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.cantidad} {point.und}<br/>{{$sistema->currency->symbol}} {point.valor}<br/>{point.y:.1f}%'
            }
        },
        
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"><b>{point.name}<br/> {point.cantidad} {point.und}<br/>Total: {{$sistema->currency->symbol}} {point.valor} </b></span><br/>{point.y:.2f}%<br/></b>'
    },
    series: [{
        name: 'Tipo de compra',
        colorByPoint: true,
        data: [
        <?php $sumaTotalSerie2 = 0; ?>
            @foreach($categoriasMasCompradas2 as $key => $value)
            {
                name: '{{$value->nombre}}',
                und: 'compras',
                y: {{$totalCompras > 0 ? ($value->total_compra * 100 / $totalCompras) : 0}},
                drilldown: '{{$value->nombre}}',
                cantidad: {{$value->total_cantidad}},
                valor: {{$value->total_compra}},
                grantotal: {{$totalCompras}}
                <?php $sumaTotalSerie2 += $value->total_compra; ?>
            },
            @endforeach
            @if($sumaTotalSerie2 < $totalCompras)
            {

                name: 'Otras',
                und: '',
                y: {{$totalCompras > 0 ? (($totalCompras - $sumaTotalSerie2) * 100 / $totalCompras) : 0}},
                cantidad: '',
                valor: {{($totalCompras - $sumaTotalSerie2) > 0 ? ($totalCompras - $sumaTotalSerie2) : 0}},
                grantotal: {{$totalCompras}}
            }
            @endif
        ]
    }],
    drilldown: {
        series: [
        @foreach($categoriasMasCompradas2 as $key => $value)
            {
            name: '{{$value->nombre}}',
            id: '{{$value->nombre}}',
            data: [
                 @foreach($subcategoriasMasCompradas2[$value->nombre] as $key => $value2)
            {
                name: '{{$value2->nombre}}',
                y: {{$value->total_compra > 0 ? ($value2->total_compra * 100 / $value->total_compra) : 0}},
                cantidad: {{$value2->total_cantidad > 0 ? $value2->total_cantidad : 0}},
                valor: {{$value2->total_compra > 0 ? $value2->total_compra : 0}},
                grantotal: {{$value->total_compra}}
            },
            @endforeach
            ]},

            @endforeach
            ]
        }    
});
    </script>
@endif    
<script type="text/javascript">
    selectedForm = "";
    var allSelected = false;

    $(document).ready(function(){
        // In your Javascript (external .js resource or <script> tag)
            $('#listacategorias').select2({
                theme: 'classic',
                width: '100%'
            });
            
             $('#selectAll').click(function(){

                if(!allSelected){
                    $('#listacategorias').select2('destroy').find('option').prop('selected', 'selected').end().select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#selectAll').text('Deseleccionar todas las categorias');
                   // $('#listacategorias').trigger('change.select2');
                    allSelected = true;
                }else {
                    /* Stuff to do every *even* time the element is clicked */
                   /* $('#listacategorias > option').removeAttr('selected');
                    $('#listacategorias').trigger('change.select2');
                    */
                    $('#listacategorias').select2('destroy').find('option').prop('selected', false).end().select2({
                        theme: 'classic',
                        width: '100%'
                    });
                    $('#selectAll').text('Seleccionar todas las categorias');
                    allSelected = false;
                }
            });

            $('.table').DataTable({
             });

        });

    </script>
@endsection
