@extends('master')

@section('title', 'Ventas')

@section('active-venta', 'active')

@section('active-venta-estadistica', 'active')

@section('active-estadisticas-ventas', 'active')

@section('content')
@php
use App\Sistema;
$sistema = Sistema::first();
@endphp
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Estadisticas de Ventas
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>

    <section class="content">
       {{-- MENSUAL --}}
       <div class="box">
            <div class="box-header">          
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
            <!-- /.box-header -->
	        <form method="POST">
	            <div class="box-body content">
	                @if($totalVentas > 0)
		                <div class="col-sm-12">
		                    <label>Consultar por fechas<label>
		                    <div class="form-group col-sm-3">
		                           <label >Desde</label>
		                           <div class="input-group">
		                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                                <input type="date" name="fromDate" id="fromDate" value="{{$fromDate}}">
		                           </div>
		                    </div>
		                    <div class="form-group col-sm-3">
		                           <label >Hasta</label>
		                           <div class="input-group">
		                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                                <input type="date" name="toDate" id="toDate" value="{{$toDate}}">
		                           </div>
		                    </div>    
		                    <div class="form-group col-sm-1">
		                        <label>&nbsp;</label>
		                        <div class="input-group">
		                                <input type="submit" value="Ok">
		                        </div>
		                    </div>
		                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
		                
		                    <div id="informe3" class="col-sm-12">
		                        <div class="callout callout-info col-sm-12" style="margin-bottom: 0!important;">
		        					<h4><i class="fa fa-info-circle"></i> Nota:</h4>
		        					Si en la grafica resultante no aparece alguno de los productos seleccionados, es debido a que no hay registros de ventas de dicho producto en el intervalo de fechas seleccionadas.
		      					</div>
		                        <label for="listaproductos" class="col-sm-12">Productos a elegir</label>
		                        <select class="js-example-basic-multiple js-states form-control" name="listaproductos[]" multiple="multiple" id="listaproductos" placeholder="Seleccione">
		                            @if(isset($listaproductos))
		                                @foreach($listaproductos as $s)
		                                <option value="{{$s->id}}" id="prod_{{$s->id}}" medicion="{{$s->medicion->simbolo}}" codigo="{{$s->codigo}}" title="{{$s->nombre}}" costo-prod="{{$s->costo_produccion}}" precio-v="{{$s->precio_venta}}"
		                                    @if(isset($listaproductos2) and in_array($s->id,$listaproductos2))
		                                    selected
		                                    @endif
		                                    >{{$s->codigo}} {{strtoupper($s->nombre)}} {{$s->medicion->nombre}}</option>
		                              @endforeach
		                              @endif
		                        </select>                        
		                    	<div id="container3" style="min-width: 310px; height: 800px; margin: 0 auto;"></div>
		                    </div>
		                </div>
	                @else
	                	No hay resultados que cumplan con los parametros seleccionados &nbsp;<a href="javascript: window.history.back();" class="btn btn-info"><< Volver</a>
	                @endif
	                @if($contadorVentas == 0)
	                	<div class="callout callout-warning" style="margin-bottom: 0!important;">
	        				<h4><i class="fa fa-info-circle"></i> IMPORTANTE:</h4> No existen ventas registradas, por tanto, no podrá visualizar ningun informe de ventas. <a href="{{url('/create_facturaventa')}}">Hacer primera venta</a>
	      				</div>
					@endif
	            </div> 
	        </form>
    	</div>
	</section>
</div>

<script type="text/javascript">
            
   Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ],
        shortWeekdays: [
            'Dom', 'Lun', 'Mar', 'Mier',
            'Jue', 'Vie', 'Sab'
        ],
        shortMonths: [
            'Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'
        ]
    }
});
        </script>
     
@if(isset($productosMasVendidosN))    
<script type="text/javascript">
        Highcharts.chart('container3', {
    chart: {
        type: 'pie'
    },
    title: {
        text: '<center>'+
                      @php
                          if($sistema->show_membrete){
                            echo('"'.$sistema->membrete.'<br><br>"+');
                            }
                      @endphp
                      'Distribucion de ingreso por productos<br/>Total ingreso en ventas: {{$sistema->currency->symbol}} {{$totalVentas}}' + '</center>',
        useHTML: true
    },
    subtitle: {
        text: 'Desde {{$fromDate}} hasta {{$toDate}}'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:.2f}% <br/>{{$sistema->currency->symbol}} {point.valor}',
                y: -20
            }
        },
        
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"><b>{point.name}<br/> {point.cantidad} {point.und}<br/>Total: {{$sistema->currency->symbol}} {point.valor} </b></span><br/>{point.y:.2f}% del total {{$sistema->currency->symbol}} {point.grantotal}<br/></b>'
    },
    series: [{
        name: 'Tipo de venta',
        colorByPoint: true,
        data: [
        <?php $sumaTotalSerie = 0;  ?>
            @foreach($productosMasVendidosN as $key => $value)
            {
                name: '{{$value->nombre}}',
                und: '{{$value->medicion}}',
                y: {{$totalVentas > 0 ? ($value->total_venta * 100 / $totalVentas) : 0}},
                cantidad: {{$value->total_cantidad > 0 ? $value->total_cantidad : 0}},
                valor: {{$value->total_venta > 0 ? $value->total_venta : 0}},
                grantotal: {{$totalVentas}}
                <?php $sumaTotalSerie += $value->total_venta; ?>
            },
            @endforeach

            @if($sumaTotalSerie < $totalVentas)
            {

                name: 'Otros',
                und: '',
                y: {{$totalVentas > 0 ? (($totalVentas - $sumaTotalSerie) * 100 / $totalVentas) : 0}},
                cantidad: '',
                valor: {{($totalVentas - $sumaTotalSerie) > 0 ? ($totalVentas - $sumaTotalSerie) : 0}},
                grantotal: {{$totalVentas}}
            }
            @endif
        ]
    }]
    
});
    </script>
@endif   
             
<script type="text/javascript">
    
$(document).ready(function(){
        // In your Javascript (external .js resource or <script> tag)

    $('#listaproductos').select2({
        theme: 'classic',
        width: '100%',
        placeholder: 'Seleccione los productos'
    });
});
</script>
@endsection
