@extends('master')

@section('title', 'Facturas')

@section('active', 'active')

@section('active-facturas', 'active')

@section('content')





	<div class="content-wrapper">

<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">

	    <section class="content-header">

	        <h1>

	            Configuración de Facturas

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>
	            	<li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Configuracion

	                </a>

	            </li>
	            <li class="active">

	                <a href="#">

	                    Facturas

	                </a>

	            </li>

	        </ol>

	    </section>



	    @if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

	    			<div class="box">

	    				<div class="box-header">

	    					 <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos de Factura</a></li>
    <!--li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Plantillas</a></li-->
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Conteo</a></li>
    
  </ul>

	    				</div>

	    				<div class="box-body">

	    			<div class="nav-tabs-custom">

 

  <!-- Tab panes -->
  <div class="tab-content container col-sm-12">
    <div role="tabpanel" class="tab-pane active" id="home">
    	<div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Informacion</h4>
                Este formulario incluye toda la informacion concerniente a la empresa. Cada campo, posee un check a su lado izquierdo. Si lo activa, este campo sera mostrado en la factura. Sino lo activa no se mostrara.
              </div>
    	<form method="POST" enctype="multipart/form-data">
    	<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
    	<div class="form-goup col-sm-3">
    		<label>Razon Social</label>
    		<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_razon_social"
                          @if($data->show_razon_social == true)
                          checked
                          @endif
                          >
                        </span>
    		<input type="text" name="razon_social" value="{{$data->razon_social}}" class="form-control" readonly="">
    	</div>
    	</div>
    	<div class="form-group col-sm-3">
    		<label>Telefono 1</label>
    		<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_telefono1" 
                          @if($data->show_telefono1 == true)
                          checked
                          @endif
                          >
                        </span>
    		<input type="tel" name="telefono1" value="{{$data->telefono1}}" class="form-control numeros" required="" maxlength="10">
    	</div>
    	</div>
        <div class="form-group col-sm-3">
            <label>Telefono 2</label>
            <div class="input-group">
                <span class="input-group-addon">
                          <input type="checkbox" name="show_telefono2" 
                          @if($data->show_telefono2 == true)
                          checked
                          @endif
                          >
                        </span>
            <input type="tel" name="telefono2" value="{{$data->telefono2}}" maxlength="10" class="form-control numeros" >
        </div>
        </div>
    	<div class="form-group col-sm-3">
    		<label>Email 1</label>
    		<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_email1"
                          @if($data->show_email1 == true)
                          checked
                          @endif
                          >
                        </span>
    		<input type="email" name="email1" value="{{$data->email1}}" class="form-control" required="">
    	</div>
    	</div>
    	<div class="form-group col-sm-3">
    		<label>Email 2</label>
    		<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_email2"
                          @if($data->show_email2 == true)
                          checked
                          @endif
                          >
                        </span>
    		<input type="email" name="email2" value="{{$data->email2}}" class="form-control">
    	</div>
    	</div>
    	
    	<div class="form-group col-sm-3">
    		<label>Facebook</label>
    		<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_facebook"
                          @if($data->show_facebook == true)
                          checked
                          @endif
                          >
                        </span>
    		<input type="text" name="facebook" value="{{$data->facebook}}" class="form-control">
    	</div>
    	</div>
    	<div class="form-group col-sm-3">
    		<label>Twiter</label>
    		<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_twiter"
                          @if($data->show_twiter == true)
                          checked
                          @endif
                          >
                        </span>
    		<input type="text" name="twiter" value="{{$data->twiter}}" class="form-control">
    	</div>
    	</div>
    	<div class="form-group col-sm-3">
    		<label>Direccion web</label>
    		<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_web"
                          @if($data->show_web == true)
                          checked
                          @endif
                          >
                        </span>
    		<input type="url" name="web" value="{{$data->web}}" class="form-control" >
    	</div>
    	</div>
    	<div class="form-group col-sm-6">
    	<div class="media">
    		<div class="media-left">
    			<img src=
                @if($data->logo1 == '')
                "{{url('img/empty1.jpg')}}"
                @else
                
                @php
                $filename = str_replace("public/",'',$data->logo1);
                @endphp
                "{{url('/storage/'.$filename)}}"
                @endif
                
                  title="Logo" alt="Logo" id="logo1-output" style="max-height:60px;max-width: 60px">
                
    		</div>
            
    		<div class="media-body">
    			<label>Logo izquierda</label>
    			<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_logo1" 
                          @if($data->show_logo1 == true)
                          checked
                          @endif
                          >
                        </span>
    			<input type="text" class="form-control file-image" name="logo1" title="Mostrar logo izquierda" readonly="" value="Mostrar Logo">
    		</div>
    	</div>
    	</div>
    	</div>
    	<!--div class="form-group col-sm-6">
    	<div class="media">
    		<div class="media-left">
    			<img src=
                @if($data->logo2 == '')
                "{{url('img/empty1.jpg')}}"
                @else
                @php
                $filename = str_replace('public/','',$data->logo2);
                @endphp
                "{{url('/storage/'.$filename)}}"
                @endif
                 title="Logo 2" alt="Logo 2" id="logo2-output" style="max-height:60px;max-width: 60px">
    		</div>
    		<div class="media-body">
    			<label>Logo 2</label>
    			<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_logo2"
                          @if($data->show_logo2 == true)
                          checked
                          @endif
                        >
                        </span>
    			<input type="file" accept="image/*" class="form-control file-image" name="logo2" input-id="logo2-container" alt="Archivo logo 2" title="Archivo logo 2" onchange="preview_image(event,'logo2-output');">
    		</div>
    	</div>
    	</div>
    	</div-->
    	<div class="form-group col-sm-6">
    		<label>Direccion Fiscal</label>
    		<div class="input-group">
    			<span class="input-group-addon">
                          <input type="checkbox" name="show_direccion_fiscal"
                          @if($data->show_direccion_fiscal == true)
                          checked
                          @endif
                          >
                        </span>
    		<textarea name="direccion_fiscal" class="form-control" required="">{{$data->direccion_fiscal}}</textarea>
    	</div>
    	</div>
    	
    		<div class="col-sm-12">
                
                <button type="submit" class="btn btn-info btn-lg pull-right">Guardar</button>
              </div>
    	</form>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">...</div>
    <div role="tabpanel" class="tab-pane" id="messages">
      <div class="box box-primary">
        <div class="box-header with-border">
        <h1>Contador de facturas de ventas</h1>
      </div>
      <div class="box-body">
    	<form method="POST">
            <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
            <div class="form-group col-sm-4">
            <label>Cantidad de facturas emitidas</label>
            <input type="text" name="cant_facturas" id="cant_facturas" style="font-size: 20px; font-style: bold;" value="{{$cant_facturas}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad de facturas canceladas</label>
            <input type="text" name="cant_facturas_canc" id="cant_facturas_canc" style="font-size: 20px; color: red; font-style: bold;" value="{{$cant_facturas_canc}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad de facturas aprobadas</label>
            <input type="text" name="cant_facturas_apro" id="cant_facturas_apro" style="font-size: 20px; color: green; font-style: bold;" value="{{$cant_facturas_apro}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad de facturas pendientes</label>
            <input type="text" name="cant_facturas_pen" id="cant_facturas_pen" style="font-size: 20px; color: orange; font-style: bold;" value="{{$cant_facturas_pen}}" readonly="" class="form-control input-lg" required="">
            </div>
    		<div class="form-group col-sm-4">
    		<label>Ultimo N° de factura</label>
    		<input type="text" name="ultima_factura" id="ultima_factura" style="font-size: 20px; color: red; font-style: bold;" value="{{$ultima}}" readonly="" class="form-control input-lg" required="">
    		</div>
    		<div class="form-group col-sm-4">
    		<label>Siguiente N° de factura</label>
    		<input type="text" name="siguiente_factura" id="siguiente_factura" style="font-size: 20px; color: red; font-style: bold;" value="{{$siguiente}}" readonly="" class="form-control input-lg" required="">
    	</div>
    	<div class="form-group col-sm-4">
    		<label>Establecer numero de factura siguiente</label>
    		<div class="input-group input-group-lg">
                <input type="number" class="form-control numero input-lg" id="num_factura" min="0" style="font-size: 20px; color: red; font-style: bold;">
                    <span class="input-group-btn">
                      <button type="button" id="conteo_submit"  class="btn btn-info btn-flat">Enviar</button>
                    </span>
              </div>
              <p class="help-block">Debe ser mayor al último numero de factura</p>
          </div>
              </form>
            </div><!-- fin box body -->
          </div><!-- fin box -->
          <div class="box box-warning">
        <div class="box-header with-border">
        <h1>Contador de todas las facturas de compras</h1>
      </div>
      <div class="box-body">
      
            
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas emitidas</label>
            <input type="text" name="cant_facturas_compras" id="cant_facturas_compras" style="font-size: 20px; font-style: bold;" value="{{$cant_facturas_compras}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas canceladas</label>
            <input type="text" name="cant_facturas_canc_compras" id="cant_facturas_canc_compras" style="font-size: 20px; color: red; font-style: bold;" value="{{$cant_facturas_canc_compras}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas aprobadas</label>
            <input type="text" name="cant_facturas_apro_compras" id="cant_facturas_apro_compras" style="font-size: 20px; color: green; font-style: bold;" value="{{$cant_facturas_apro_compras}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas pendientes</label>
            <input type="text" name="cant_facturas_pen_compras" id="cant_facturas_pen_compras" style="font-size: 20px; color: orange; font-style: bold;" value="{{$cant_facturas_pen_compras}}" readonly="" class="form-control input-lg" required="">
            </div>
        </div><!-- fin box-body -->
      </div>
        <div class="box box-danger">
          <div class="box-header">
            <h1>Contador de facturas de compra de produccion</h1>
          </div>
          <div class="box-body">
            <form method="POST">
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas emitidas</label>
            <input type="text" name="cant_facturas_compras_produccon" id="cant_facturas_compras_produccon" style="font-size: 20px; font-style: bold;" value="{{$cant_facturas_compras_produccion}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas canceladas</label>
            <input type="text" name="cant_facturas_canc_compras_produccion" id="cant_facturas_canc_compras_produccion" style="font-size: 20px; color: red; font-style: bold;" value="{{$cant_facturas_canc_compras_produccion}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas aprobadas</label>
            <input type="text" name="cant_facturas_apro_compras_produccion" id="cant_facturas_apro_compras_produccion" style="font-size: 20px; color: green; font-style: bold;" value="{{$cant_facturas_apro_compras_produccion}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas pendientes</label>
            <input type="text" name="cant_facturas_pen_compras_produccion" id="cant_facturas_pen_compras_produccion" style="font-size: 20px; color: orange; font-style: bold;" value="{{$cant_facturas_pen_compras_produccion}}" readonly="" class="form-control input-lg" required="">
            </div>
        <div class="form-group col-sm-4">
        <label>Ultimo N° de factura</label>
        <input type="text" name="ultima_factura_compra_produccion" id="ultima_factura_compra_produccion" style="font-size: 20px; color: red; font-style: bold;" value="{{$ultima_produccion}}" readonly="" class="form-control input-lg" required="">
        </div>
        <div class="form-group col-sm-4">
        <label>Siguiente N° de factura</label>
        <input type="text" name="siguiente_factura_produccion" id="siguiente_factura_produccion" style="font-size: 20px; color: red; font-style: bold;" value="{{$siguiente_produccion}}" readonly="" class="form-control input-lg" required="">
      </div>
      <div class="form-group col-sm-4">
        <label>Establecer numero de factura siguiente</label>
        <div class="input-group input-group-lg">
                <input type="number" class="form-control numeros input-lg" id="num_factura_produccion" min="0" style="font-size: 20px; color: red; font-style: bold;">
                    <span class="input-group-btn">
                      <button type="button" id="conteo_produccion_submit"  class="btn btn-info btn-flat">Enviar</button>
                    </span>
              </div>
              <p class="help-block">Debe ser mayor al último numero de factura</p>
          </div>
        </form>
        </div><!--fin box-body -->
      </div><!-- fin box -->
      <div class="box box-info">
          <div class="box-header">
            <h1>Contador de facturas de productos</h1>
          </div>
          <div class="box-body">
            <form method="POST">
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas emitidas</label>
            <input type="text" name="cant_facturas_compras_productos" id="cant_facturas_compras_productos" style="font-size: 20px; font-style: bold;" value="{{$cant_facturas_compras_productos}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas canceladas</label>
            <input type="text" name="cant_facturas_canc_compras_productos" id="cant_facturas_canc_compras_productos" style="font-size: 20px; color: red; font-style: bold;" value="{{$cant_facturas_canc_compras_productos}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas aprobadas</label>
            <input type="text" name="cant_facturas_apro_compras_productos" id="cant_facturas_apro_compras_productos" style="font-size: 20px; color: green; font-style: bold;" value="{{$cant_facturas_apro_compras_productos}}" readonly="" class="form-control input-lg" required="">
            </div>
            <div class="form-group col-sm-4">
            <label>Cantidad total de facturas pendientes</label>
            <input type="text" name="cant_facturas_pen_compras_productos" id="cant_facturas_pen_compras_productos" style="font-size: 20px; color: orange; font-style: bold;" value="{{$cant_facturas_pen_compras_productos}}" readonly="" class="form-control input-lg" required="">
            </div>
        <div class="form-group col-sm-4">
        <label>Ultimo N° de factura</label>
        <input type="text" name="ultima_factura_compra_productos" id="ultima_factura_compra_productos" style="font-size: 20px; color: red; font-style: bold;" value="{{$ultima_productos}}" readonly="" class="form-control input-lg" required="">
        </div>
      
        </form>
            </div><!-- fin box body -->
          </div><!-- fin box -->
    </div>
    
  </div>

</div>

	    				</div>

	    			</div>

	    		</div>

	    	</div>

	    </section>

	</div>
<script type="text/javascript">
    function preview_image(event,output_id){
        var reader = new FileReader();

        reader.onload = function(){
            var output = document.getElementById(output_id);
            output.src = reader.result;
        }

        reader.readAsDataURL(event.target.files[0]);
    }

    $(document).ready(function(){
            $('#conteo_submit').click(function(e){
            _token = $("#_token").val();    
            resp = confirm("Esta seguro de realizar esta accion? Esto alterara la secuencia de los numeros de facturas de ventas y este cambio, no podra deshacerse.");

            if(!resp)
                return;

                num_element = $('#num_factura');

                     if(num_element.val() < 0 || num_element.val() == ''){
                        alert('Error: Por favor verifique los datos ingresados.');
                        return;
                     }

                    $.ajax({
            url: "{{url('/reset-factura-counter')}}",
            data: {
              _token : _token,
               num : num_element.val() 
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Numero de factura modificado con exito");
                //agregamos el html
                
                //cerramos la ventana
                $('#siguiente_factura').val(parseInt(data.value));//cerramos la ventana
                
              }else if(data.resp == "error"){
                
                alert("Error: "+data.msg);
                
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ha ocurrio un error al hacer la petición");
            }

            });
                    
    });
            $('#conteo_produccion_submit').click(function(e){
            _token = $("#_token").val();    
            resp = confirm("Esta seguro de realizar esta accion? Esto alterara la secuencia de los numeros de facturas de produccion.");

            if(!resp)
                return;

                num_element = $('#num_factura_produccion');

                     if(num_element.val() < 0 || num_element.val() == ''){
                        alert('Error: Por favor verifique los datos ingresados.');
                        return;
                     }

                    $.ajax({
            url: "{{url('/reset-factura-produccion-counter')}}",
            data: {
              _token : _token,
               num : num_element.val() 
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Numero de factura modificado con exito");
                //agregamos el html
                
                //cerramos la ventana
                $('#siguiente_factura_produccion').val(parseInt(data.value));//cerramos la ventana
                
              }else if(data.resp == "error"){
                
                alert("Error: "+data.msg);
                
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ha ocurrio un error al hacer la petición");
            }

            });
            });
	});
</script>


@endsection