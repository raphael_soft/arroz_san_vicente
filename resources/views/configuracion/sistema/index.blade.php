@extends('master')

@section('title', 'Sistema')

@section('active', 'active')

@section('active-sistema', 'active')

@section('content')





	<div class="content-wrapper">

<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">

	    <section class="content-header">

	        <h1>

	            Configuración de Sistema

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>
	            	<li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Configuracion

	                </a>

	            </li>
	            <li class="active">

	                <a href="#">

	                    Sistema

	                </a>

	            </li>

	        </ol>

	    </section>



	    @if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

	    			<div class="box">

	    				<div class="box-header">

	    					

	    				</div>

	    				<div class="box-body">

	    			<div class="nav-tabs-custom">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Configuracion general</a></li>
    <!--li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Plantillas</a></li-->
    <!--li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Conteo</a></li-->
    
  </ul>

  <!-- Tab panes -->
  <div class="tab-content container col-sm-12">
    <div role="tabpanel" class="tab-pane active" id="home">
    	<div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Informacion</h4>
                Este formulario incluye toda la informacion basica del sistema y su propietario. Algunos de estos datos solo estarán disponibles internamente, y otros seran mostrados en facturas y/o reportes.
              </div>
    	<form method="POST" enctype="multipart/form-data" id="formulario-sistema">
    	<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
    	<div class="form-goup col-sm-4">
    		<label>Nombre Empresa</label>
    	
          <input type="text" name="nombre" value="{{$data->nombre}}" class="form-control" required="">
    	
    	</div>

      <div class="form-goup col-sm-4">
        <label>Razon Social</label>
      
          <input type="text" name="razon_social" value="{{$data->razon_social}}" class="form-control" required="">
      
      </div>
      
      <div class="form-group col-sm-4">
        <label>RUC/Cedula</label>
      
        <input type="text" name="ruc" id="ruc" class="form-control numeros" value="{{$data->ruc}}" maxlength="13" minlength="10">
        <span class="error-msg" style="display: normal; color: red"></span>
      </div>
      
    	<div class="form-group col-sm-4">
    		<label>Telefono 1</label>
    	
    		<input type="tel" name="telefono1" value="{{$data->telefono1}}" class="form-control numeros" maxlength="10" required="">
    	
    	</div>
      <div class="form-group col-sm-4">
        <label>Telefono 2</label>
      
        <input type="tel" name="telefono2" value="{{$data->telefono2}}" class="form-control numeros" maxlength="10" required="">
      
      </div>
    	<div class="form-group col-sm-4">
    		<label>Email 1</label>
    		
    	
    		<input type="email" name="email1" value="{{$data->email2}}" class="form-control" required="">
    	
    	</div>
      <div class="form-group col-sm-4">
        <label>Email 2</label>
        
      
        <input type="email" name="email2" value="{{$data->email2}}" class="form-control" required="">
      
      </div>
      <!--div class="form-group col-sm-4">
        <label>Tipo de moneda usada</label>
        
        <select name="currency" class="form-control">
          @foreach($currencies as $cu)
          <option value="{{$cu->id}}"
            @if($cu->id == $data->currencies_id)
            selected
            @endif
            > {{$cu->name}} {{$cu->symbol}}
          </option>
          @endforeach
        </select>
      
      </div-->
    	<div class="form-group col-sm-4">
    		<label>Nombre del representante legal</label>
    	
    		<input type="text" name="nombre_representante" class="form-control" value="{{$data->nombre_representante}}">
    	
    	</div>
    	<div class="form-group col-sm-4">
        <label>Cedula del representante legal</label>
      
        <input type="text" name="cedula_representante" id="cedula_representante" class="form-control numeros" value="{{$data->cedula_representante}}" maxlength="10">
        <span class="error-msg" style="display: normal; color: red"></span>
      </div>
      <div class="form-group col-sm-4">
        <label>Telefono del representante legal</label>
      
        <input type="tel" name="telefono_representante" class="form-control numeros" value="{{$data->telefono_representante}}" maxlength="10">
      
      </div>
      <div class="form-group col-sm-4">
        <label>Direccion</label>
      
          <textarea name="direccion" class="form-control" >{{$data->direccion}}</textarea>
      
      </div>
      <div class="form-group col-sm-4">
        <label>Direccion del representante legal</label>
      
        <textarea name="direccion_representante" class="form-control">{{$data->direccion_representante}}</textarea>
      
      </div>
      <div class="form-group col-sm-6">
        <label>Membrete (para reportes, NO FACTURAS) </label>
      
          <input type="text" name="membrete" class="form-control" value="{{$data->membrete}}"/>
        <p class="help-block">Puede usar HTML para dar formato al texto</p>
      </div>
      
      <div class="form-group col-sm-6">
      <div class="media">
        <div class="media-left">
          <img src=
                @if($data->logo1 == '')
                "{{url('img/empty1.jpg')}}"
                @else
                @php
                $filename = str_replace('public/','',$data->logo1);
                @endphp
                "{{asset('/storage/'.$filename)}}"
                @endif
                 title="Logo" alt="Logo" id="logo1-output" style="max-height:60px;max-width: 60px">
        </div>
        <div class="media-body">
          <label>Logo izquierdo</label>
          <div class="input-group">
          <input type="file" id="input-logo-izquierdo" accept=".png, .jpg, .jpeg, .bmp" class="form-control file-image" name="logo1" input-id="logo1-container" alt="Archivo logo 1" title="Archivo logo 1" onchange="preview_image(event,'logo1-output');">
           <p class="help-block">Formatos válidos: PNG, JPG, BMP. Use imagenes con una resolucion maxima de 200px x 200px y un peso inferior a 10MB</p>
        </div>
      </div>
      </div>
      </div>
      <div class="form-group col-sm-7">

          
                          <input type="checkbox" name="show_membrete" 
                          @if($data->show_membrete == true)
                          checked
                          @endif
                          >
          
          <label>Mostrar membrete en los reportes en lugar de los datos de la empresa</label><br>
          <small>Si activa esta opción no se mostrarán los datos de la empresa en los reportes. Solo se mostrará lo que ingrese en el campo Membrete.</small>
        </div>
      <div class="form-group col-sm-5">

          
                          <input type="checkbox" name="show_logo1_repo" 
                          @if($data->show_logo1_repo == true)
                          checked
                          @endif
                          >
          
          <label>Mostrar Logo en los reportes</label>
        </div>
      
        <div class="col-sm-12">
                
                <button type="submit" class="btn btn-info btn-lg pull-right submit">Guardar</button>
              </div>
    	</form>
    </div>
    
  </div>

</div>

	    				</div>

	    			</div>

	    		</div>

	    	</div>

	    </section>

	</div>
<script type="text/javascript" src="{{ url('/js/ruc_jquery_validator.min.js') }}"></script>
<script type="text/javascript">
  var opciones = {
    strict: true,        // va a validar siempre, aunque la cantidad de caracteres no sea 10 ni 13
    events: "keyup",     // evento que va a disparar la validación
    the_classes: "error",// clase que se va a agregar al nodo en el que se realiza la validación
    onValid: function () {    
          console.log('cedula/ruc valido');
          this.next('.error-msg').hide();
          $('.submit').removeClass('disabled');
          $('.submit').removeAttr('disabled');
    },   // callback cuando la cédula es correcta.
    onInvalid: function () {
          console.log('cedula/ruc invalido');
          this.next('.error-msg').text('RUC/Cedula invalido').show();
          $('.submit').addClass('disabled');
          $('.submit').attr('disabled','disabled');
      }  // callback cuando la cédula es incorrecta.
  };
  
  $('#ruc').validarCedulaEC(opciones);
  $('#cedula_representante').validarCedulaEC(opciones);

</script> 
<script type="text/javascript">
  function preview_image(event,output_id){
  		const file = document.getElementById('input-logo-izquierdo').files[0];
  		if(!validFile(file)){
  			event.preventDefault();
  			alert("El tamaño o el tipo de archivo son invalidos. Por favor verifique e intente de nuevo.")
  			return false;
  		}
        var reader = new FileReader();

        reader.onload = function(){
            var output = document.getElementById(output_id);
            output.src = reader.result;
        }

        reader.readAsDataURL(event.target.files[0]);
    }
    function validFileType(file) {
    	var fileTypes = [
    	  'image/bmp',
    	  'image/jpg',
		  'image/jpeg',
		  'image/pjpeg',
		  'image/png'
		];
	  for(var i = 0; i < fileTypes.length; i++) {
		    if(file.type === fileTypes[i]) {
		      return true;
		    }
		}
	}
	function validFileSize(file) {
	    return (file.size/1048576).toFixed(1) <= 1;
		
	}
	function validFile(file){
		//var file = getElementById('').files[0];

		return validFileSize(file) && validFileType(file);
	}
	const form = document.getElementById('formulario-sistema');
	form.onsubmit = validateSubmit;
	function validateSubmit(event){
		
		var file = document.getElementById('input-logo-izquierdo').files[0];
		if(file !== undefined && !validFile(file)){
  			event.preventDefault();	
  			alert("El tamaño o el tipo de archivo son invalidos. Por favor verifique e intente de nuevo.")
  			return false;
  		}
	}
</script>
@endsection