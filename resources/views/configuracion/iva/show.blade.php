@extends('master')

@section('title', 'Iva - '. $iva->nombre )

@section('active', 'active')

@section('active-iva', 'active')

@section('content')



	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Ivas

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="/">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="">

	                <a href="{{ url('/configuracion/iva') }}">

	                    Ivas

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    {{ $iva->nombre }}

	                </a>

	            </li>

	        </ol>

	    </section>



	    @foreach($errors->all() as $error)

			<p class="alert alert-danger">{{$error}}</p>

		@endforeach



		@if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

					<div class="box box-info">

			            <div class="box-header with-border">

			              	<h3 class="box-title">{{ $iva->nombre }}</h3>

			            </div>

			              	<div class="box-body">

				                <div class="form-group">

				                  	<label for="nombre" class="col-sm-2 control-label">Nombre: </label>

					                <div class="col-sm-10">

					                   	<h5>{{ $iva->nombre }}</h5>

					                </div>

				                </div>



				                <div class="form-group">

				                  	<label for="valor" class="col-sm-2 control-label">Valor %: </label>

				                  	<div class="col-sm-10">

				                    	<h5>{{ $iva->valor }}</h5>

				                  	</div>

				                </div>

			              	</div>

			              <div class="box-footer text-right">

			                <a onclick="window.history.back()" class="btn btn-warning">Regresar</a>

			                <a href="{{ action('IvaController@edit', $iva->id) }}" class="btn btn-info">Modificar</a>

			                <a href="javascript: eliminar({{$iva->id}},'{{url('configuracion/iva/destroy',$iva->id)}}');" class="btn btn-danger" >Borrar</a>

			              </div>

			          </div>

				</div>

			</div>

	    </section>

	</div>



@endsection