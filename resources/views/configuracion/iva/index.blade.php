@extends('master')
@section('title', 'Ivas')
@section('active', 'active')
@section('active-iva', 'active')
@section('content')


	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Ivas
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Ivas
	                </a>
	            </li>
	        </ol>
	    </section>

	    @if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif
		@if (session('error'))
			<div class="alert alert-danger">
				{{session('error')}}
			</div>
		@endif
	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
	    			<div class="box">
	    				<div class="box-header">
	    					<h3 class="box-title"><a href="iva/create/" class="btn btn-primary">Crear Nuevo</a></h3>
	    				</div>
	    				<div class="box-body">
	    					<div class="table-responsive">
	    						<table class="table table-bordered table-hover informacion">
	    							<thead>
	    								<th>Nombre</th>
	    								<th>Valor</th>
	    								<th class="no-print">Opciones</th>
	    							</thead>
	    							<tbody>
	    								@foreach($ivas as $iva)
		    								<tr>
		    									<td><a href="{!! action('IvaController@show', $iva->id) !!}">{{ $iva->nombre }}</a></td>
		    									<td><a href="{!! action('IvaController@show', $iva->id) !!}">{{ $iva->valor }}</a></td>
		    									<td >
                        <a href="{{url('configuracion/iva/show',$iva->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>

                    <a href="{{url('/configuracion/iva/edit',$iva->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a href="javascript: eliminar({{$iva->id}},'{{url('configuracion/iva/destroy',$iva->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
		    								</tr>
		    							@endforeach
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </section>

	    <script>
	        $(document).ready(function(){
	            $('.informacion').DataTable({
	                dom: '<"html5buttons"B>lTfgitp',
	                select: true,
	                buttons: [
	                     {extend: 'csv', title: 'Lista de Impuestos', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
	                     {extend: 'excel', title: 'Lista de Impuestos', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
	                     {extend: 'pdf', title: 'Lista de Impuestos', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

	                     {extend: 'print',
	                         customize: function (win){
	                             $(win.document.body).addClass('white-bg');
	                             $(win.document.body).css('font-size', '10px');
	                             $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
	                         },
	                         exportOptions: {
						        columns: ':not(.no-print)'
						    }
	                     }
	                ],
	            });
	        });

	    </script>
	</div>

@endsection