@extends('master')
@section('title', 'Roles')
@section('active', 'active')
@section('active-rol', 'active')
@section('content')


	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Roles
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Roles
	                </a>
	            </li>
	        </ol>
	    </section>

	    @if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif
		@if (session('error'))
			<div class="alert alert-danger">
				{{session('error')}}
			</div>
		@endif
	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
	    			<div class="box">
	    				<div class="box-header">
	    					<h3 class="box-title"><a href="/configuracion/roles/create" class="btn btn-primary">Crear Nuevo</a></h3>
	    				</div>
	    				<div class="box-body">
	    					<div class="table-responsive">
	    						<table class="table table-bordered table-hover informacion">
	    							<thead>
	    								<th>Nombre</th>
	    								<th>Responsabilidades</th>
	    								<th>Superuser</th>
	    								<th>Ver</th>
	    								<th>Registrar</th>
	    								<th>Editar</th>
	    								<th>Borrar</th>
	    								<th style="min-width: 150px;">Opciones</th>
	    							</thead>
	    							<tbody>
	    								@foreach($rols as $rol)
		    								<tr>
		    									<td><a href="{!! action('RolController@show', $rol->id) !!}">{{ $rol->nombre }}</a></td>
		    									<td><a href="{!! action('RolController@show', $rol->id) !!}">{{ $rol->responsabilidades }}</a></td>
		    											<td>{{$rol->superuser == 1? 'SI' : 'NO'}}</td>
		    											<td>{{$rol->ver == 1? 'SI' : 'NO'}}</td>
		    											<td>{{$rol->registrar == 1? 'SI' : 'NO'}}</td>
		    											<td>{{$rol->editar == 1? 'SI' : 'NO'}}</td>
		    											<td>{{$rol->borrar == 1? 'SI' : 'NO'}}</td>
		    											<td>
                        <a href="{{url('configuracion/roles/show',$rol->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>

                    <a href="{{url('/configuracion/roles/edit',$rol->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a href="javascript: eliminar({{$rol->id}},'{{url('configuracion/roles/destroy',$rol->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
		    								</tr>
		    							@endforeach
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </section>

	    <script>
	        $(document).ready(function(){
	            $('.informacion').DataTable({
	                dom: '<"html5buttons"B>lTfgitp',
	                select: true,
	                buttons: [
	                    // {extend: 'csv', title: 'Lista de Ivas'},
	                    // {extend: 'excel', title: 'Lista de Ivas'},
	                    // {extend: 'pdf', title: 'Lista de Ivas'},

	                    // {extend: 'print',
	                    //     customize: function (win){
	                    //         $(win.document.body).addClass('white-bg');
	                    //         $(win.document.body).css('font-size', '10px');
	                    //         $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
	                    //     }
	                    // }
	                ],
	            });
	        });

	    </script>
	</div>

@endsection