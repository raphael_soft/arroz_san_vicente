@extends('master')

@section('title', 'Rol - '. $rol->nombre )

@section('active', 'active')

@section('active-rol', 'active')

@section('content')



	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Roles

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="">

	                <a href="{{ url('/configuracion/roles') }}">

	                    Roles

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    {{ $rol->nombre }}

	                </a>

	            </li>

	        </ol>

	    </section>



	    @foreach($errors->all() as $error)

			<p class="alert alert-danger">{{$error}}</p>

		@endforeach



		@if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

					<div class="box box-info">

			            <div class="box-header with-border">

			              	<h3 class="box-title">{{ $rol->nombre }}</h3>

			            </div>

			              	<div class="box-body">

				                <div class="form-group">

				                  	<label for="nombre" class="col-sm-2 control-label">Nombre: </label>

					                <div class="col-sm-10">

					                   	<h5>{{ $rol->nombre }}</h5>

					                </div>

				                </div>
				                <div class="form-group">

				                  	<label for="responsabilidades" class="col-sm-2 control-label">Responsabilidades: </label>

					                <div class="col-sm-10">

					                   	<h5>{{ $rol->responsabilidades }}</h5>

					                </div>

				                </div>
				               <div class="form-group">

				                  	<label for="permisos" class="col-sm-2 control-label">Permisos</label>

					                <div class="col-sm-2">
										<input type="checkbox" value="1" readonly="" id="ver" name="ver" disabled="" title="Ver" 
										@if($rol->ver == 1)
					                	checked
					                	@endif
										>
										&nbsp;&nbsp;<label for="ver">Ver</label>
					                </div>
					                <div class="col-sm-2">
					                	<input type="checkbox" value="1" disabled="" readonly="" id="editar" name="editar" title="Editar" 
					                	@if($rol->editar == 1)
					                	checked
					                	@endif
					                	>
										&nbsp;&nbsp;<label for="editar">Editar</label>
					                </div>
					                <div class="col-sm-2">
					                	<input type="checkbox" value="1" readonly="" disabled="" id="registrar" name="registrar" title="Registrar"
					                	@if($rol->registrar == 1)
					                	checked
					                	@endif
					                	 >
										&nbsp;&nbsp;<label for="registrar">Registrar</label>
					                </div>
					                <div class="col-sm-2">
					                	<input type="checkbox" value="1" readonly="" disabled="" id="borrar"  name="borrar" title="Borrar" 
					                	@if($rol->borrar == 1)
					                	checked
					                	@endif
					                	>
										&nbsp;&nbsp;<label for="borrar">Borrar</label>
					                </div>
					                <div class="col-sm-2">
										<input type="checkbox" value="1" readonly="" id="superuser" disabled="" name="superuser" title="Super usuario" 
										@if($rol->superuser == 1)
					                	checked
					                	@endif
										>	
										&nbsp;&nbsp;<label for="superuser">Super usuario</label>
					                </div>

				                </div> 	
			              	</div>

			              <div class="box-footer text-right">

			                <a onclick="window.history.back()" class="btn btn-warning">Regresar</a>

			                <a href="{{ action('RolController@edit', $rol->id) }}" class="btn btn-info">Modificar</a>

			                <a href="javascript: eliminar({{$rol->id}},'{{url('configuracion/roles/destroy',$rol->id)}}');"  class="btn btn-danger">Eliminar</a>

			              </div>

			          </div>

				</div>

			</div>

	    </section>

	</div>



@endsection