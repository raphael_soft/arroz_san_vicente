@extends('master')
@section('title', 'Nuevo Rol')
@section('active', 'active')
@section('active-rol', 'active')
@section('content')

	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Roles
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="">
	                <a href="{{ url('/configuracion/roles/') }}">
	                    Roles
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Nuevo Rol
	                </a>
	            </li>
	        </ol>
	    </section>

	    @foreach($errors->all() as $error)
			<p class="alert alert-danger">{{$error}}</p>
		@endforeach

		@if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif

	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
					<div class="box box-info">
			            <div class="box-header with-border">
			              	<h3 class="box-title">Nuevo Rol</h3>
			            </div>
			            <form class="form-horizontal" method="POST">
			            	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			              	<div class="box-body">
				                <div class="form-group">
				                  	<label for="nombre" class="col-sm-2 control-label">Nombre</label>
					                <div class="col-sm-10">
					                   	<input type="text"  class="form-control" id="nombre" name="nombre" placeholder="Nombre" required />
					                </div>
				                </div>
				                <div class="form-group">

				                  	<label for="responsabilidades" class="col-sm-2 control-label">Responsabilidades</label>

					                <div class="col-sm-10">

					                   	<input type="text"  class="form-control" id="responsabilidades" name="responsabilidades" placeholder="Responsabilidades"x	 />

					                </div>

				                </div>
				                <div class="form-group">

				                  	<label for="permisos" class="col-sm-2 control-label">Permisos</label>

					                <div class="col-sm-2">
										<input type="checkbox" id="ver" name="ver" title="Ver" >
										&nbsp;&nbsp;<label for="ver">Ver</label>
					                </div>
					                <div class="col-sm-2">
					                	<input type="checkbox" id="editar" name="editar" title="Editar" >
										&nbsp;&nbsp;<label for="editar">Editar</label>
					                </div>
					                <div class="col-sm-2">
					                	<input type="checkbox" id="registrar" name="registrar" title="Registrar" >
										&nbsp;&nbsp;<label for="registrar">Registrar</label>
					                </div>
					                <div class="col-sm-2">
					                	<input type="checkbox" id="borrar"  name="borrar" title="Borrar">
										&nbsp;&nbsp;<label for="borrar">Borrar</label>
					                </div>
					                <div class="col-sm-2">
										<input type="checkbox" id="superuser" name="superuser" title="Super usuario">	
										&nbsp;&nbsp;<label for="superuser">Super usuario</label>
					                </div>

				                </div>
			              	</div>
			              <div class="box-footer">
			                <a onclick="window.history.back()"><button type="button" class="btn btn-warning">Regresar</button></a>
			                <button type="submit" class="btn btn-info pull-right">Registrar</button>
			              </div>
			            </form>
			          </div>
				</div>
			</div>
	    </section>
	</div>

@endsection