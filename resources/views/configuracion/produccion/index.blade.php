@extends('master')

@section('title', 'Produccion')

@section('active', 'active')

@section('active-configproduccion', 'active')

@section('content')






	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Producción

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    Producción

	                </a>

	            </li>

	        </ol>

	    </section>



	    @if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

	    			<div class="box">

	    				<div class="box-header">

	    					<h3 class="box-title"><a href="{{ url('/create_produccion') }}" class="btn btn-primary">Crear Nueva</a></h3>

	    				</div>

	    				<div class="box-body">

	    					<div class="table-responsive">

	    						<table class="table table-bordered table-hover informacion">

	    							<thead>

	    								<th>Nombre</th>
	    								<th>Tipo</th>
	    								<th>N&uacute;mero Producción</th>

	    							</thead>

	    							<tbody>

	    								@foreach($datos as $d)

		    								<tr>

		    									<td><a href="{!! action('ConfigproduccionController@show', $d->id) !!}">{{ $d->nombre }}</a></td>
		    									<td><a href="{!! action('ConfigproduccionController@show', $d->id) !!}">{{ $d->tipo }}</a></td>
		    									<td><a href="{!! action('ConfigproduccionController@show', $d->id) !!}">00{{ $d->contador }}</a></td>

		    								</tr>

		    							@endforeach

	    							</tbody>

	    						</table>

	    					</div>

	    				</div>

	    			</div>

	    		</div>

	    	</div>

	    </section>



	    <script>

	        $(document).ready(function(){

	            $('.informacion').DataTable({

	                dom: '<"html5buttons"B>lTfgitp',

	                select: true,

	                buttons: [

	                    // {extend: 'csv', title: 'Lista de Ivas'},

	                    // {extend: 'excel', title: 'Lista de Ivas'},

	                    // {extend: 'pdf', title: 'Lista de Ivas'},



	                    // {extend: 'print',

	                    //     customize: function (win){

	                    //         $(win.document.body).addClass('white-bg');

	                    //         $(win.document.body).css('font-size', '10px');

	                    //         $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

	                    //     }

	                    // }

	                ],

	            });

	        });



	    </script>

	</div>



@endsection