@extends('master')

@section('title', 'Produccion')

@section('active', 'active')

@section('active-configproduccion', 'active')

@section('content')



  <div class="content-wrapper">



      <section class="content-header">

          <h1>

            Datos de Produccion

            <small>Secci&oacute;n para visualizar los datos de Produccion</small>

          </h1>

          <ol class="breadcrumb">

            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>

            <li><a href="#">Configuracion</a></li>

            <li><a href="{{url('/config_produccion')}}">Produccion</a></li>

            <li class="active">Ver Produccion</li>

          </ol>

        </section>



      @foreach($errors->all() as $error)

      <p class="alert alert-danger">{{$error}}</p>

    @endforeach



    @if (session('status'))

      <div class="alert alert-success">

        {{session('status')}}

      </div>

    @endif



      <section class="content">

        <div class="row">

          <div class="col-md-3">

            

          </div>

          <div class="col-md-12 box-body">

                <div class="box box-widget widget-user-2">

                   <form  method="POST">

                          <div class="box-body">

                            <div class="form-group">

                              <label class="col-sm-2 control-label">Nombre</label>

                              <div class="col-sm-10">

                                <input readonly="readonly" value="{{$factura->nombre}}" type="text" name="nombre" class="form-control" placeholder="Nombre de la Produccion">

                              </div>

                              <label class="col-sm-2 control-label">T. de Produccion</label>

                              <div class="col-sm-10">

                                <input readonly="readonly" value="{{$factura->tipo}}" type="text" name="tipo" class="form-control" placeholder="Tipo de Produccion">

                              </div>

                              <label class="col-sm-2 control-label">N. de Produccion</label>

                              <div class="col-sm-10">

                                <input readonly="readonly" value="{{$factura->numero_factura}}" type="text" name="numero_factura" class="form-control" placeholder="Tipo de Produccion">

                              </div>


                            </div>

                         </div>

                              <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                             </div>

                                    <!-- /.box-body -->

                            <div class="box-footer col-sm-12 text-center">

                              <a onclick="window.history.back()"  class="btn btn-default"><< Volver</a>

                              <a href="{{url('/edit_produccion',$factura->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>

                              <a href="{{url('/destroy_produccion',$factura->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>



                            </div>

                            <!-- /.box-footer -->

                        </form>

                 

                </div>

            </div>

          <div class="col-md-3"></div>

      </div>

      </section>



@endsection

