@extends('master')

@section('title', 'Produccion')

@section('active', 'active')

@section('active-facturas', 'active')

@section('content')



	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Produccion

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="">

	                <a href="{{ url('/facturas') }}">

	                    Produccion

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    Editar Produccion

	                </a>

	            </li>

	        </ol>

	    </section>



	    @foreach($errors->all() as $error)

			<p class="alert alert-danger">{{$error}}</p>

		@endforeach



		@if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

					<div class="box box-info">

			            <div class="box-header with-border">

			              	<h3 class="box-title">Nueva Factura</h3>

			            </div>

			            <form class="form-horizontal" method="POST">

			            	<input type="hidden" name="_token" value="{!! csrf_token() !!}">

			              	<div class="box-body">

				                <div class="form-group">

				                  	<label for="nombre" class="col-sm-2 control-label">Nombre</label>

					                <div class="col-sm-10">

					                   	<input type="text"  class="form-control" value="{{$factura->nombre}}" id="nombre" name="nombre" placeholder="Nombre de la Produccion" required />

					                </div>

				                </div>

				                <div class="form-group">

				                  	<label for="nombre" class="col-sm-2 control-label">Tipo</label>

					                <div class="col-sm-10">

					                   	<select name="tipo" class="form-control" required="required">
					                   		<option style="background-color: green; color: white;" value="{{$factura->tipo}}">{{$factura->tipo}}</option>
					                   	</select>

					                </div>

				                </div>

				                <div class="form-group">

				                  	<label for="nombre" class="col-sm-2 control-label">N. Factura</label>

					                <div class="col-sm-10">

					                   	<input type="text"  class="form-control" value="{{$factura->numero_factura}}" id="numero_factura" name="numero_factura" placeholder="N&uacute;mero de la Produccion" required />

					                </div>

				                </div>


			              	</div>

			              <div class="box-footer">

                            <a onclick="window.history.back()" type="submit" class="btn btn-default"><< Volver</a>

			                <button type="submit" class="btn btn-info pull-right">Guardar</button>

			              </div>

			            </form>

			          </div>

				</div>

			</div>

	    </section>

	</div>



@endsection