@extends('master')
@section('title', 'Nuevo Cliente')
@section('active', 'active')
@section('active-clientes', 'active')
@section('content')

	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Clientes
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/"') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="">
	                <a href="{{ url('/configuracion/clientes') }}">
	                    Clientes
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Nuevo Cliente
	                </a>
	            </li>
	        </ol>
	    </section>

	    @foreach($errors->all() as $error)
			<p class="alert alert-danger">{{$error}}</p>
		@endforeach

		@if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif

	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
					<div class="box box-info">
			            <div class="box-header with-border">
			              	<h3 class="box-title">Nuevo Cliente</h3>
			            </div>
			            <form class="form-horizontal" method="POST">
			            	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			              	<div class="box-body">
				                <div class="form-group">
				                  	<label for="nombre" class="col-sm-2 control-label">Nombre o Razon Social</label>
					                <div class="col-sm-10">
					                   	<input type="text"  class="form-control" id="nombre" name="nombre" placeholder="Nombre" autofocus="Nombre" required />
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="cedula_ruc" class="col-sm-2 control-label">Cedula o RUC</label>
					                <div class="col-sm-10">
					                   	<input type="text"  class="form-control numeros" id="cedula_ruc" name="cedula_ruc" placeholder="Cedula o RUC" autofocus="cedula_ruc" maxlength="13" minlength="10" required />
					                   	<span class="error-msg" style="display: normal; color: red"></span>
					                </div>
				                </div>
				                
				                <div class="form-group">
				                  	<label for="email" class="col-sm-2 control-label">Email</label>
					                <div class="col-sm-10">
					                   	<input type="email"  class="form-control" id="email" name="email" placeholder="Email" required />
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="telefono" class="col-sm-2 control-label">Telefono</label>
					                <div class="col-sm-10">
					                   	<input type="tel"  class="form-control numeros" id="telefono" maxlength="10" name="telefono" placeholder="Telefono"/>
					                </div>
				                </div>
				                
				                <div class="form-group">
				                  	<label for="direccion" class="col-sm-2 control-label">Direccion</label>
					                <div class="col-sm-10">
					                   	<textarea  class="form-control" id="direccion" name="direccion" placeholder="Direccion"></textarea>
					                </div>
				                </div>

			              	</div>
			              <div class="box-footer">
			                <a href="/configuracion/clientes"><button type="button" class="btn btn-warning">Regresar</button></a>
			                <button type="submit" class="btn btn-info pull-right submit">Registrar</button>
			              </div>
			            </form>
			          </div>
				</div>
			</div>
	    </section>
	</div>
<script type="text/javascript" src="{{ url('/js/ruc_jquery_validator.min.js') }}"></script>
<script type="text/javascript">
  var opciones = {
    strict: true,        // va a validar siempre, aunque la cantidad de caracteres no sea 10 ni 13
    events: "keyup",     // evento que va a disparar la validación
    the_classes: "error",// clase que se va a agregar al nodo en el que se realiza la validación
    onValid: function () {    
          console.log('cedula/ruc valido');
          $('.error-msg').hide();
          $('.submit').removeClass('disabled');
          $('.submit').removeAttr('disabled');
    },   // callback cuando la cédula es correcta.
    onInvalid: function () {
          console.log('cedula/ruc invalido');
          $('.error-msg').text('RUC/Cedula invalido').show();
          $('.submit').addClass('disabled');
          $('.submit').attr('disabled','disabled');
      }  // callback cuando la cédula es incorrecta.
  };
  
  $('#cedula_ruc').validarCedulaEC(opciones);

</script> 
@endsection