

@extends('master')

@section('title', 'Clientes - '. $datos->razon_social )

@section('active', 'active')

@section('active-clientes', 'active')

@section('content')



	<div class="content-wrapper">

	    <section class="content-header">

	        <h1>

	            Clientes

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/"') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="">

	                <a href="{{ url('/configuracion/clientes') }}">

	                    Clientes

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    {{ $datos->razon_social }}

	                </a>

	            </li>

	        </ol>

	    </section>



	    @foreach($errors->all() as $error)

			<p class="alert alert-danger">{{$error}}</p>

		@endforeach



		@if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif

		@if (session('error'))

			<div class="alert alert-danger">

				{{session('error')}}

			</div>

		@endif

	    <section class="content">

	    	<div class="row">

	    		<div class="col-md-3">

	    			

	    		</div>

	    		<div class="col-md-6">

		          	<div class="box box-widget widget-user-2">

			            <div class="widget-user-header bg-aqua">

			              	<div class="widget-user-image">

			              		<img class="img-circle" src="{{ url('/img/user_base.jpg') }}" alt="{{$datos->razon_social}}">

			              	</div>

			              	<h3 class="widget-user-username">{{$datos->razon_social}}</h3>

			              	<div class="margin">

			              		<div class="btn-group">

				                  	<button type="button" class="btn btn-success">Opciones</button>

				                  	<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">

				                    	<span class="caret"></span>

				                    	<span class="sr-only">Toggle Dropdown</span>

				                  	</button>

				                  	<ul class="dropdown-menu" role="menu">

					                    <li><a onclick="window.history.back()">Regresar</a></li>

					                    <li><a href="{{ url('/configuracion/clientes/edit', $datos->id) }}">Modificar</a></li>

					                    <li class="divider"></li>

					                    <li><a href="javascript: eliminar({{$datos->id}},'{{url('configuracion/clientes/destroy',$datos->id)}}');" >Eliminar</a>
					                	</li>	

				                  	</ul>

				                </div>

			              	</div>

			            </div>

			            <div class="box-footer no-padding">

			              	<ul class="nav nav-stacked">
			              		<li><a href="#">Nombre o Razon Social <span class="pull-right">{{$datos->razon_social}}</span></a></li>
			                	<li><a href="#">RUC o Cedula <span class="pull-right">{{$datos->cedula_ruc}}</span></a></li>
			                	<li><a href="#">Email <span class="pull-right badge bg-blue">{{$datos->email}}</span></a></li>
			                	<li><a href="#">Telefono <span class="pull-right">{{$datos->telefono}}</span></a></li>
			                	<li><a href="#">Direccion <span class="pull-right">{{$datos->direccion}}</span></a></li>
			                	<li><a href="#">Fecha registro <span class="pull-right">{{$datos->created_at}}</span></a></li>
			              	</ul>

			            </div>

		          	</div>

		        </div>

	    		<div class="col-md-3"></div>

			</div>

	    </section>

	</div>



@endsection

