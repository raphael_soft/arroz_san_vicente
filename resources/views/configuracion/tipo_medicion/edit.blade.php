@extends('master')
@section('title', 'Tipo de Medicion - '. $datos->nombre )
@section('active', 'active')
@section('active-mediciones', 'active')
@section('content')

	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Tipo de Mediciones
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                 <a href="{{ url('/') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="">
	                <a href="{{ url()->previous() }}">
	                    Tipo de Mediciones
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Editar Medicion: {{ $datos->nombre }}
	                </a>
	            </li>
	        </ol>
	    </section>

	    @foreach($errors->all() as $error)
			<p class="alert alert-danger">{{$error}}</p>
		@endforeach

		@if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif

	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
					<div class="box box-info">
			            <div class="box-header with-border">
			              	<h3 class="box-title">Editar Medicion</h3>
			            </div>
			            <form class="form-horizontal" method="POST">
			            	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			              	<div class="box-body">
				                <div class="form-group">
				                  	<label for="nombre" class="col-sm-2 control-label">Nombre</label>
					                <div class="col-sm-10">
					                   	<input type="text"  class="form-control" value="{{ $datos->nombre }}" id="nombre" name="nombre" placeholder="Nombre" required />
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="simbolo" class="col-sm-2 control-label">Simbolo</label>
					                <div class="col-sm-10">
					                   	<input type="text"  class="form-control" value="{{ $datos->simbolo }}" id="simbolo" name="simbolo" placeholder="Simbolo" required />
					                </div>
				                </div>
			              	</div>
			              <div class="box-footer">
			                <a onclick="window.history.back()"><button type="button" class="btn btn-warning">Regresar</button></a>
			                <button type="submit" class="btn btn-info pull-right">Actualizar</button>
			              </div>
			            </form>
			          </div>
				</div>
			</div>
	    </section>
	</div>

@endsection