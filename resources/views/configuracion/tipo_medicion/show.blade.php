@extends('master')
@section('title', 'Tipo de Mediciones - '. $datos->nombre )
@section('active', 'active')
@section('active-mediciones', 'active')
@section('content')

	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Tipo de Mediciones
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="">
	                <a href="{{ url()->previous() }}">
	                    Tipo de Mediciones
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    {{ $datos->nombre }}
	                </a>
	            </li>
	        </ol>
	    </section>

	    @foreach($errors->all() as $error)
			<p class="alert alert-danger">{{$error}}</p>
		@endforeach

		@if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif

	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
					<div class="box box-info">
			            <div class="box-header with-border">
			              	<h3 class="box-title">{{ $datos->nombre }}</h3>
			            </div>
			              	<div class="box-body">
				                <div class="form-group">
				                  	<label for="nombre" class="col-sm-2 control-label">Nombre: </label>
					                <div class="col-sm-10">
					                   	<h5>{{ $datos->nombre }}</h5>
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="simbolo" class="col-sm-2 control-label">Simbolo: </label>
					                <div class="col-sm-10">
					                   	<h5>{{ $datos->simbolo }}</h5>
					                </div>
				                </div>
			              	</div>
			              <div class="box-footer text-right">
			                <a onclick="window.history.back()" class="btn btn-warning">Regresar</a>
			                <a href="{{ action('TipoMedicionController@edit', $datos->id) }}" class="btn btn-info">Modificar</a>
			                
			            	<a href="javascript: eliminar({{$datos->id}},'{{url('configuracion/mediciones/destroy',$datos->id)}}');"  class="btn btn-danger">Eliminar</a>
			              </div>
			          </div>
				</div>
			</div>
	    </section>
	</div>

@endsection