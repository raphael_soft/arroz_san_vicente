@extends('master')
@section('title', 'SubCategorias')
@section('active', 'active')
@section('active-subcategoria', 'active')
@section('content')


	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            SubCategorias
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    SubCategorias
	                </a>
	            </li>
	        </ol>
	    </section>

	    @if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif
		@if (session('error'))
			<div class="alert alert-danger">
				{{session('error')}}
			</div>
		@endif
	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
	    			<div class="box">
	    				<div class="box-header">
	    					<h3 class="box-title"><a href="{{url('/configuracion/subcategorias/create')}}" class="btn btn-primary">Crear Nuevo</a></h3>
	    				</div>
	    				<div class="box-body">
	    					<div class="table-responsive">
	    						<table class="table table-bordered table-hover informacion">
	    							<thead>
	    								<th>Categoria</th>
	    								<th>Nombre</th>
	    								<th>Descripcion</th>
	    								<th>Opciones</th>
	    							</thead>
	    							<tbody>
	    								@foreach($subcategorias as $subcategoria)
		    								<tr>
		    									<td><a href="{!! action('SubCategoriaController@show', $subcategoria->id) !!}">{{ $subcategoria->categoria->nombre }}</a></td>
		    									<td><a href="{!! action('SubCategoriaController@show', $subcategoria->id) !!}">{{ $subcategoria->nombre }}</a></td>
		    									<td><a href="{!! action('SubCategoriaController@show', $subcategoria->id) !!}">{{ $subcategoria->descripcion }}</a></td>
		    															<td >
                        <a href="{{url('configuracion/subcategorias/show',$subcategoria->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>

                    <a href="{{url('/configuracion/subcategorias/edit',$subcategoria->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a href="javascript: eliminar({{$subcategoria->id}},'{{url('configuracion/subcategorias/destroy',$subcategoria->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
		    								</tr>
		    							@endforeach
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </section>

	    <script>
	        $(document).ready(function(){
	            $('.informacion').DataTable({
	                dom: '<"html5buttons"B>lTfgitp',
	                select: true,
	                buttons: [
	                    // {extend: 'csv', title: 'Lista de Ivas'},
	                    // {extend: 'excel', title: 'Lista de Ivas'},
	                    // {extend: 'pdf', title: 'Lista de Ivas'},

	                    // {extend: 'print',
	                    //     customize: function (win){
	                    //         $(win.document.body).addClass('white-bg');
	                    //         $(win.document.body).css('font-size', '10px');
	                    //         $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
	                    //     }
	                    // }
	                ],
	            });
	        });

	    </script>
	</div>

@endsection