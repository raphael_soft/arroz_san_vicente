@extends('master')
@section('title', 'Nuevo Usuario')
@section('active', 'active')
@section('active-usuario', 'active')
@section('content')

	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Usuarios
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/"') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="">
	                <a href="{{ url('/configuracion/usuario"') }}">
	                    Usuarios
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Nuevo Usuario
	                </a>
	            </li>
	        </ol>
	    </section>

	    @foreach($errors->all() as $error)
			<p class="alert alert-danger">{{$error}}</p>
		@endforeach

		@if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif
		@if (session('error'))
			<div class="alert alert-danger">
				{{session('error')}}
			</div>
		@endif
	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
					<div class="box box-info">
			            <div class="box-header with-border">
			              	<h3 class="box-title">Nuevo Usuario</h3>
			            </div>
			            <form class="form-horizontal" method="POST">
			            	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			              	<div class="box-body">
			              		<div class="form-group">
				                  	<label for="rol" class="col-sm-2 control-label">Rol</label>
					                <div class="col-sm-10">
					                   	<select name="rol" id="rol" class="form-control" required>
					                   		<option value="">Seleccione una opcion</option>
					                   		@foreach ($roles as $rol)
					                   			<option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
					                   		@endforeach
					                   	</select>
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="nombres" class="col-sm-2 control-label">Nombres</label>
					                <div class="col-sm-10">
					                   	<input type="text"  class="form-control" id="nombres" name="nombres" placeholder="Nombres" autofocus="Nombres" required />
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="apellidos" class="col-sm-2 control-label">Apellidos</label>
					                <div class="col-sm-10">
					                   	<input type="text"  class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" autofocus="apellidos" required />
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="dni" class="col-sm-2 control-label">RUC/Cedula </label>
					                <div class="col-sm-10">
					                   	<input type="text"  class="form-control numeros" minlength="10" maxlength="13" id="dni" name="dni" placeholder="DNI o Cedula" autofocus="dni" required />
					                   	<span class="error-msg" style="display: normal; color: red"></span>
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="cargo" class="col-sm-2 control-label">Cargo</label>
					                <div class="col-sm-10">
					                   	<select name="cargo" id="cargo" class="form-control" required>
					                   		<option value="">Seleccione una opcion</option>
					                   		@foreach ($cargos as $cargo)
					                   			<option value="{{ $cargo->id }}">{{ $cargo->nombre }}</option>
					                   		@endforeach
					                   	</select>
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="fecha_nacimiento" class="col-sm-2 control-label">Fecha de nacimiento</label>
					                <div class="col-sm-10">
					                   	<input type="date"  class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Fecha de nacimiento" autofocus="fecha_nacimiento"  />
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="fecha_ingreso" class="col-sm-2 control-label">Fecha de ingreso</label>
					                <div class="col-sm-10">
					                   	<input type="date"  class="form-control" id="fecha_ingreso" name="fecha_ingreso" placeholder="Fecha de ingreso" autofocus="fecha_ingreso"  />
					                </div>
				                </div>	
				                <div class="form-group">
				                  	<label for="email" class="col-sm-2 control-label">Email</label>
					                <div class="col-sm-10">
					                   	<input type="email"  class="form-control" id="email" name="email" placeholder="Email" required />
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="telefono" class="col-sm-2 control-label">Telefono</label>
					                <div class="col-sm-10">
					                   	<input type="tel"  class="form-control numeros" id="telefono" name="telefono" placeholder="Telefono" maxlength="10"/>
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="ciudad" class="col-sm-2 control-label">Ciudad</label>
					                <div class="col-sm-10">
					                   	<input type="text"  class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" />
					                </div>
				                </div>
				                <div class="form-group">
				                  	<label for="direccion" class="col-sm-2 control-label">Direccion</label>
					                <div class="col-sm-10">
					                   	<textarea  class="form-control" id="direccion" name="direccion" placeholder="Direccion"></textarea>
					                </div>
				                </div>

				                <div class="form-group">
				                  	<label for="clave" class="col-sm-2 control-label">Contraseña</label>
					                <div class="col-sm-10">
					                   	<input type="password" maxlength=""  class="form-control" id="clave" name="clave" placeholder="Contraseña" required />
					                </div>
				                </div>
				                
			              	</div>
			              <div class="box-footer">
			                <a onclick="window.history.back()"><button type="button" class="btn btn-warning">Regresar</button></a>
			                <button type="submit" class="btn btn-info pull-right submit">Registrar</button>
			              </div>
			            </form>
			          </div>
				</div>
			</div>
	    </section>
	</div>
<script type="text/javascript" src="{{ url('/js/ruc_jquery_validator.min.js') }}"></script>
<script type="text/javascript">
  var opciones = {
    strict: true,        // va a validar siempre, aunque la cantidad de caracteres no sea 10 ni 13
    events: "keyup",     // evento que va a disparar la validación
    the_classes: "error",// clase que se va a agregar al nodo en el que se realiza la validación
    onValid: function () {    
          console.log('cedula/ruc valido');
          $('.error-msg').hide();
          $('.submit').removeClass('disabled');
          $('.submit').removeAttr('disabled');
    },   // callback cuando la cédula es correcta.
    onInvalid: function () {
          console.log('cedula/ruc invalido');
          $('.error-msg').text('RUC/Cedula invalido').show();
          $('.submit').addClass('disabled');
          $('.submit').attr('disabled','disabled');
      }  // callback cuando la cédula es incorrecta.
  };
  
  $('#dni').validarCedulaEC(opciones);

</script> 
@endsection