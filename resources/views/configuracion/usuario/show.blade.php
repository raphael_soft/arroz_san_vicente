

@extends('master')

@section('title', 'Usuario - '. $datos->nombre )

@section('active', 'active')

@section('active-usuario', 'active')

@section('content')



	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Usuarios

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/"') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="">

	                <a href="{{ url('/configuracion/usuario') }}">

	                    Usuarios

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    {{ $datos->nombre }}

	                </a>

	            </li>

	        </ol>

	    </section>



	    @foreach($errors->all() as $error)

			<p class="alert alert-danger">{{$error}}</p>

		@endforeach



		@if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif

		@if (session('error'))

			<div class="alert alert-danger">

				{{session('error')}}

			</div>

		@endif

	    <section class="content">

	    	<div class="row">

	    		<div class="col-md-3">

	    			

	    		</div>

	    		<div class="col-md-6">

		          	<div class="box box-widget widget-user-2">

			            <div class="widget-user-header bg-aqua">

			              	<div class="widget-user-image">

			              		<img class="img-circle" src="{{ url('/img/user_base.jpg') }}" alt="{{$datos->name}}">

			              	</div>

			              	<h3 class="widget-user-username">{{$datos->name}}</h3>

			              	<h5 class="widget-user-desc">{{ isset($datos->rol->nombre) ? $datos->rol->nombre : 'NO POSEE' }}</h5>

			              	<div class="margin">

			              		<div class="btn-group">

				                  	<button type="button" class="btn btn-success">Opciones</button>

				                  	<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">

				                    	<span class="caret"></span>

				                    	<span class="sr-only">Toggle Dropdown</span>

				                  	</button>

				                  	<ul class="dropdown-menu" role="menu">

					                    <li><a onclick="window.history.back()">Regresar</a></li>

					                    <li><a href="{{ action('UsuarioController@edit', $datos->id) }}">Modificar</a></li>

					                    <li class="divider"></li>

					                    <li>

				                    		<a href="javascript: eliminar({{$datos->id}},'{{url('configuracion/usuario/destroy',$datos->id)}}')" >Eliminar</a>

					                	</li>	

				                  	</ul>

				                </div>

			              	</div>

			            </div>

			            <div class="box-footer no-padding">

			              	<ul class="nav nav-stacked">
			              		<li><a href="#">Nombres <span class="pull-right">{{$datos->nombre}}</span></a></li>
			              		<li><a href="#">Apellidos <span class="pull-right">{{$datos->apellido}}</span></a></li>
			              		<li><a href="#">Fecha de nacimiento <span class="pull-right">{{$datos->fecha_nacimiento}}</span></a></li>
			                	<li><a href="#">Cargo <span class="pull-right">{{$datos->cargo->nombre}}</span></a></li>
			                	<li><a href="#">Rol <span class="pull-right">{{$datos->rol->nombre}}</span></a></li>
			                	<li><a href="#">Email <span class="pull-right badge bg-blue">{{$datos->email}}</span></a></li>
			                	<li><a href="#">Telefono <span class="pull-right">{{$datos->telefono}}</span></a></li>
			                	<li><a href="#">Direccion <span class="pull-right">{{$datos->direccion}}</span></a></li>
			                	<li><a href="#">Fecha ingreso <span class="pull-right">{{$datos->fecha_ingreso}}</span></a></li>
			                	<li><a href="#">Fecha registro <span class="pull-right">{{$datos->fecha_registro}}</span></a></li>
			              	</ul>

			            </div>

		          	</div>

		        </div>

	    		<div class="col-md-3"></div>

			</div>

	    </section>

	</div>



@endsection

