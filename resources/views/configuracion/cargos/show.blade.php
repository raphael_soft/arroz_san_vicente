@extends('master')

@section('title', 'Cargo - '. $cargo->nombre )

@section('active', 'active')

@section('active-cargos', 'active')

@section('content')



	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Cargos

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="">

	                <a href="{{ url('/configuracion/cargos') }}">

	                    Cargos

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    {{ $cargo->nombre }}

	                </a>

	            </li>

	        </ol>

	    </section>



	    @foreach($errors->all() as $error)

			<p class="alert alert-danger">{{$error}}</p>

		@endforeach



		@if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

					<div class="box box-info">

			            <div class="box-header with-border">

			              	<h3 class="box-title">{{ $cargo->nombre }}</h3>

			            </div>

			              	<div class="box-body">

				                <div class="form-group">

				                  	<label for="nombre" class="col-sm-2 control-label">Nombre: </label>

					                <div class="col-sm-10">

					                   	<h5>{{ $cargo->nombre }}</h5>

					                </div>

				                </div>
				                <div class="form-group">

				                  	<label for="responsabilidades" class="col-sm-2 control-label">Responsabilidades: </label>

					                <div class="col-sm-10">

					                   	<h5>{{ $cargo->responsabilidades }}</h5>

					                </div>

				                </div>
				               
			              	</div>

			              <div class="box-footer text-right">

			                <a onclick="window.history.back()" class="btn btn-warning">Regresar</a>

			                <a href="{{ action('CargosController@edit', $cargo->id) }}" class="btn btn-info">Modificar</a>

			                <a href="javascript: eliminar({{$cargo->id}},'{{url('configuracion/cargos/destroy',$cargo->id)}}');"  class="btn btn-danger">Eliminar</a>

			              </div>

			          </div>

				</div>

			</div>

	    </section>

	</div>



@endsection