@extends('master')
@section('title', 'Cargos')
@section('active', 'active')
@section('active-cargos', 'active')
@section('content')


	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Cargos
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Cargos
	                </a>
	            </li>
	        </ol>
	    </section>

	    @if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif
		@if (session('error'))
			<div class="alert alert-danger">
				{{session('error')}}
			</div>
		@endif
	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
	    			<div class="box">
	    				<div class="box-header">
	    					<h3 class="box-title"><a href="/configuracion/cargos/create" class="btn btn-primary">Crear Nuevo</a></h3>
	    				</div>
	    				<div class="box-body">
	    					<div class="table-responsive">
	    						<table class="table table-bordered table-hover informacion">
	    							<thead>
	    								<th>Nombre</th>
	    								<th>Responsabilidades</th>
	    								
	    								<th style="min-width: 150px;" class="no-print">Opciones</th>
	    							</thead>
	    							<tbody>
	    								@foreach($cargos as $cargo)
		    								<tr>
		    									<td><a href="{!! action('CargosController@show', $cargo->id) !!}">{{ $cargo->nombre }}</a></td>
		    									<td><a href="{!! action('CargosController@show', $cargo->id) !!}">{{ $cargo->responsabilidades }}
		    											<td>
                        <a href="{{url('configuracion/cargos/show',$cargo->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>

                    <a href="{{url('/configuracion/cargos/edit',$cargo->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a href="javascript: eliminar({{$cargo->id}},'{{url('configuracion/cargos/destroy',$cargo->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
		    								</tr>
		    							@endforeach
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </section>

	    <script>
	        $(document).ready(function(){
	            $('.informacion').DataTable({
	                dom: '<"html5buttons"B>lTfgitp',
	                select: true,
	                buttons: [
	                     {extend: 'csv', title: 'Lista de Cargos', exportOptions: {
					        columns: ':not(.no-print)'
					    } },
	                     {extend: 'excel', title: 'Lista de Cargos', exportOptions: {
					        columns: ':not(.no-print)'
					    }},
	                     {extend: 'pdf', title: 'Lista de Cargos', exportOptions: {
					        columns: ':not(.no-print)'
					    }},

	                     {extend: 'print',
	                         customize: function (win){
	                             $(win.document.body).addClass('white-bg');
	                             $(win.document.body).css('font-size', '10px');
	                             $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
	                         },
	                         exportOptions: {
						        columns: ':not(.no-print)'
						    }
	                     }
	                ],
	            });
	        });

	    </script>
	</div>

@endsection