<div class="modal fade" id="modalAbonosFacturaventa" tabindex="-1" role="dialog" aria-labelledby="modalAbonosFacturaventa" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title">Abonos a Factura</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body-abonos">
        <i class="fa fa-sync-spin"></i> Cargando... Por favor, espere.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="close_modal_abonos" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>