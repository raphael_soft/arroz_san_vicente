<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
<div class="modal fade" id="modalAbono" tabindex="-1" role="dialog" aria-labelledby="modalAbono" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalAbono">Agregar Abono</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <input type="hidden" name="facturaventa_id" id="modal_facturaventa_id" value="">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Monto:</label>
            <input type="text" class="form-control precio" id="monto_abono">
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="registrar_abono_completo">Completar restante</button>
        <button type="button" class="btn btn-primary" id="registrar_abono">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_abono" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalAbonoTiket" tabindex="-1" role="dialog" aria-labelledby="modalAbonoTiket" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div id="modal-abono-content"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="close_modal_abono_invoice" data-dismiss="modal">Cerrar</button>
      </div>
</div>
</div>
</div>
<script type="text/javascript">
      $(document).ready(function(){
  $('#modalAbono').on('shown.bs.modal', function () {
  $('#monto_abono').trigger('focus')
});
  _token = $("#_token").val();
      $("#registrar_abono").click(function(e){

        monto = $("#monto_abono").val();
        id_factura = $("#modal_facturaventa_id").val();

        registrarAbono(monto,id_factura,'normal');
      });

      $("#registrar_abono_completo").click(function(e){

        monto = $("#monto_abono").val();
        id_factura = $("#modal_facturaventa_id").val();

        registrarAbono(monto,id_factura,'completar');
      });
    });

      function registrarAbono(monto,id_factura,modo){

        if(modo == 'normal'){
          if(!(Number(monto) >= 0)){
            alert("Por favor ingrese un monto mayor a 0");
            monto.focus();
            return;
          }
        }  
          if(!(Number(id_factura) > 0)){
            alert("No se recibio el identificador de factura");
            return;
          }
            $.ajax({
              url: "{{url('registrar-abono')}}",
              data: {
                _token : _token,
                monto : monto,
                facturaventa_id: id_factura,
                modo: modo
              },
              method: "POST",
              success: function(data){
                hideLoading();
                if(data.resp == "ok"){
                  $('#modal-abono-content').html(data.tiket);

                  alert(data.msg);
                  //TODO: actualizar el monto en la lista sin refrescar la pagina
                  location.reload();
                }else if(data.resp == "error"){
                  alert(data.msg);
                  return;
                }
                

              },
              beforeSend: function(){
                showLoading();
              },
              error: function(){
                hideLoading();
                alert("Ocurrio un error");
              }

              });
          }
</script>