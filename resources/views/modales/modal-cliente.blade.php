<div class="modal fade" id="modalCliente" tabindex="-1" role="dialog" aria-labelledby="modalCliente" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalCliente">Agregar Nuevo Cliente</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group">
      <label><strong style="color:red; font-size: 20px;">*</strong>&nbsp;<strong>Socio</strong>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Si el cliente a agregar es socio, seleccionelo de esta lista" style="cursor: pointer;"></i></label>
      <div class="input-group">
        
          <span class="btn btn-success input-group-addon" style="cursor: pointer;"  title="Agregar un nuevo socio" data-toggle="modal" data-target="#modalSocio"><i class="fa fa-plus" ></i></span>
       
        <select name="socio" id="socio" class="form-control campo selectpicker" data-live-search="true" required="">
                                    
                                    <option value="0">Selecciones un socio</option>
                                    @foreach($socios as $s)
                                    <option value="{{$s->id}}">{{$s->ruc}}-{{$s->nombre}}</option>
                                    @endforeach
                                    
                                </select>
        
      </div>

</div>
        <div class="row">
          <div class="form-group col-sm-6">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Cedula/RUC:</label>
            <input type="text" class="form-control numeros" id="dni-cliente" required="" maxlength="13" minlength="10">
            <span class="error-msg" style="display: none; color: red"></span>
          </div>
          <div class="form-group col-sm-6">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Nombre:</label>
            <input type="text" class="form-control" id="nombre-cliente" required="">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-6">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;"></strong>&nbsp;Direccion:</label>
            <input type="text" class="form-control" id="direccion-cliente" required="">
          </div>
          <div class="form-group col-sm-6">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;"></strong>&nbsp;Telefono:</label>
            <input type="text" class="form-control numeros" id="telefono-cliente" required="" maxlength="10">
          </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn_finish" id="registrar_cliente">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_cliente" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="{{ url('/js/ruc_jquery_validator.min.js') }}"></script>
<script type="text/javascript">

$(document).ready(function(){

  var opciones = {
    strict: true,        // va a validar siempre, aunque la cantidad de caracteres no sea 10 ni 13
    events: "keyup",     // evento que va a disparar la validación
    the_classes: "error",// clase que se va a agregar al nodo en el que se realiza la validación
    onValid: function () {    
          console.log('cedula valida');
          this.next('.error-msg').hide();
          $('#registrar_cliente').removeClass('disabled');
          $('#registrar_cliente').removeAttr('disabled');
    },   // callback cuando la cédula es correcta.
    onInvalid: function () {
          console.log('cedula invalida');
          this.next('.error-msg').text('Cedula invalida').show();
          $('#registrar_cliente').addClass('disabled');
          $('#registrar_cliente').attr('disabled','disabled');
      }  // callback cuando la cédula es incorrecta.
  };
  
  $('#dni-cliente').validarCedulaEC(opciones);

  $('#modalCliente').on('shown.bs.modal', function () {
    $('#dni-cliente').trigger('focus')
  });

  $('#socio').on('change',function(){
    //TODO: cambiar el nombre en el modal de precios
    element_socio = $("#socio option[value="+$('#socio').val()+"]");
    if($('#socio').val() == '0'){
      
      $('#dni-cliente').val(''); 
      $('#nombre-cliente').val(''); 
      $('#telefono-cliente').val(''); 
      $('#direccion-cliente').val(''); 
      $('#dni-cliente').removeProp('readonly');
      $('#nombre-cliente').removeProp('readonly');
      $('#telefono-cliente').removeProp('readonly');
      $('#direccion-cliente').removeProp('readonly');
      $('#dni-cliente').removeAttr('readonly');
      $('#nombre-cliente').removeAttr('readonly');
      $('#telefono-cliente').removeAttr('readonly');
      $('#direccion-cliente').removeAttr('readonly');
      $('#dni-cliente').focus(); 
      return;
    }
    _token = $("#_token").val();
      $.ajax({
            url: "{{url('cargar-datos-socio')}}",
            data: {
              _token : _token,
              socio : $('#socio').val()
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                
                //agregamos el html
                $('#dni-cliente').val(data.ruc); 
                $('#nombre-cliente').val(data.nombre); 
                $('#telefono-cliente').val(data.telefono); 
                $('#direccion-cliente').val(data.direccion); 
                $('#dni-cliente').attr('readonly','readonly');
                $('#nombre-cliente').attr('readonly','readonly');
                $('#telefono-cliente').attr('readonly','readonly');
                $('#direccion-cliente').attr('readonly','readonly');
                //$('#precio_producto').html(data.options);
                
                //cerramos la ventana
                //$('#close_modal_cliente').click();//cerramos la ventana
                $('#cliente').focus();
              }else if(data.resp == "no-existe"){
                
                alert("Informacion: Este socio NO EXISTE en la Base de datos. Por favor, ingrese un nuevo regstro valido.");
                //$('#close_modal_cliente').click();//cerramos la ventana
                return;
              }else if(data.resp == "no"){
                
                alert("Informacion: Este socio NO tiene datos de identificacion asociados. Por favor, modifique este registro o ingrese un nuevo regstro valido.");
                //$('#close_modal_cliente').click();//cerramos la ventana
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
      
  });
  $("#registrar_cliente").click(function(e){
            
        nombre = $("#nombre-cliente");
        ruc = $("#dni-cliente");
        direccion = $("#direccion-cliente");
        telefono = $("#telefono-cliente");
        if(nombre.val() == ''){
          alert("Por favor ingrese un nombre para este cliente");
          nombre.focus();
          return;
        }
        if(ruc.val() == ''){
          alert("Por favor ingrese un numero de cedula o ruc para este cliente");
          ruc.focus();
          return;
        }
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url: "{{url('registrar-cliente')}}",
            data: {
              nombre : nombre.val(),
              ruc : ruc.val(),
              telefono : telefono.val(),
              direccion : direccion.val()
            },
            dataType: 'JSON',
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Exito!");
                //agregamos el html
                $('#cliente').html(data.options);
                $('#cliente').selectpicker('refresh');
                //cerramos la ventana
                //$('#close_modal_cliente').click();//cerramos la ventana
                $('#modalCliente').modal('hide');//cerramos la ventana
                $('#cliente').focus();
              }else if(data.resp == "existe"){
                alert("Error: El cliente "+nombre+" ya se encuentra registrado en la Base de datos!");
                return;
              }else if(data.resp == "error"){
                alert("Error: Por favor verifique los datos suministrados e intente nuevamente");
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
  });
});
</script>