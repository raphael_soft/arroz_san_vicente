<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
<div class="modal fade" id="modalAnularVenta" tabindex="-1" role="dialog" aria-labelledby="modalAnularVenta" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalAnular">Anular Venta</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Por favor seleccione a dónde deben devolverse los productos de esta fatura:</p>
        <form>
          <div class="form-group">
            <label for="select-dest1" class="col-form-label">Al inventario de produccion primero que al inventario de productos</label>
            <input type="radio" name="destino" value="productos" class="form-control" id="select-dest1" required="">
          </div>
          <div class="form-group">
            <label for="select-dest2" class="col-form-label">Al inventario de productos primero que al inventario de produccion</label>
            <input type="radio" name="destino" value="produccion" class="form-control" id="select-dest2" required="">
          </div>
          <div class="form-group">
            <label for="select-dest3" class="col-form-label">No regresar a inventario</label>
            <input type="radio" name="destino" value="ninguno" class="form-control" id="select-dest3" required="">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <a href="{{url('/anular_facturaventa',$factura->id)}}" type="button" class="btn btn-primary btn_finish" id="anular_btn">Anular</a>
        <button  type="button" class="btn btn-secondary" id="close_modal_categoria" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
