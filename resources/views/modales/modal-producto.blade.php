<div class="modal fade" id="modalProducto" tabindex="-1" role="dialog" aria-labelledby="modalProducto" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalProducto">Agregar Nuevo Producto</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="content">
        <form>
          <div class="form-group col-sm-3">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Codigo:</label>
            <input type="text" class="form-control" id="codigo-producto" required="" value="{{uniqid('P')}}">
          </div>
          <div class="form-group col-sm-5">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Nombre:</label>
            <input type="text" class="form-control" id="nombre-producto" required="">
          </div>
          <div class="form-group col-sm-4">
            <label for="message-text" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Medicion:</label>
            <div class="input-group">
              <span class="input-group-addon" style="cursor: pointer;" title="Agregar un nuevo tipo de medicion" data-toggle="modal" data-target="#modalMedicion"><i class="fa fa-plus"></i></span>
            <select class="form-control" id="medicion-producto" required="">
              <option value="" selected="">Escoja una medicion</option>
                                    @if(isset($mediciones))
                                    @foreach($mediciones as $s)
                                    <option value="{{$s->id}}">{{$s->nombre}}</option>
                                    @endforeach
                                    @endif
                                  </select>
          </div>
        </div>
        <div class="form-group col-sm-4">
            <label for="message-text" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;IVA:</label>
            <select class="form-control" id="iva-producto" required="">
              <option value="" selected="">No paga IVA</option>
                                    @if(isset($impuestos))
                                    @foreach($impuestos as $s)
                                    <option value="{{$s->id}}">{{$s->nombre}} {{$s->valor}}%</option>
                                    @endforeach
                                    @endif
                                  </select>
        </div>
          <div class="form-group col-sm-6">
            <label for="message-text" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Categoria:</label>
            <div class="input-group">
              <span class="input-group-addon" style="cursor: pointer;" title="Agregar una nueva categoria" data-toggle="modal" data-target="#modalCategoria"><i class="fa fa-plus"></i></span>
            <select class="form-control" id="categoria-producto" required="">
              <option value="" selected="">Escoja una categoria</option>
                                    @if(isset($categorias))
                                    @foreach($categorias as $s)
                                    <option value="{{$s->id}}">{{$s->nombre}}</option>
                                    @endforeach
                                    @endif
                                  </select>
          </div>
        </div>
          <div class="form-group col-sm-6">
            <label for="message-text" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Subcategoria:</label>
            <div class="input-group">
              <span class="input-group-addon" style="cursor: pointer;" title="Agregar una nueva subcategoria" data-toggle="modal" data-target="#modalSubcategoria"><i class="fa fa-plus"></i></span>
            <select class="form-control" id="subcategoria-producto" required="">
              <option value="" selected>Escoja una subcategoria</option>                        
            </select>
          </div>
        </div>
        	<div class="form-group col-sm-6">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Cantidad en inventario:</label>
            <input type="number" min="1" class="form-control" name="inventario-producto" id="inventario-producto" required="">
          </div>
          <div class="form-group col-sm-6">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Costo x Produccion:</label>
            <input type="number" min="0" class="form-control" name="costo-producto" id="costo-producto" required="">
          </div>
          <div class="form-group col-sm-6">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Precio de Venta:</label>
            <input type="number" min="0" class="form-control" name="precio-producto" id="precio-producto" required="">
          </div>
        </form>
      </div>
      <
      <div class="modal-footer">
        <div class="col-sm-12">
        <button type="button" class="btn btn-primary btn_finish" id="registrar_producto">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_producto" data-dismiss="modal">Cerrar</button>
      </div>
      </div>
    </div>
  </div>
</div>
<script>
        $(document).ready(function(){
  $('#modalProducto').on('shown.bs.modal', function () {
  $('#codigo-producto').trigger('focus')
});
     $("#registrar_producto").click(function(e){
        nombre_producto = $("#nombre-producto");
        codigo_producto = $("#codigo-producto");
        categoria_producto = $("#categoria-producto");
        subcategoria_producto = $("#subcategoria-producto");
        medicion_producto = $("#medicion-producto");
        costo_producto = $("#costo-producto");
        precio_producto = $("#precio-producto");
        iva_producto = $("#iva-producto");
        inventario_producto = $("#inventario-producto");
        if(codigo_producto.val() == ''){
          alert("Por favor ingrese un codigo para este producto");
          codigo_producto.focus();
          return;
        }
        if(nombre_producto.val() == ''){
          alert("Por favor ingrese un nombre para este producto");
          nombre_producto.focus();
          return;
        }
        if(medicion_producto.val() == ''){
          alert("Por favor seleccione un tipo de medicion valido");
          medicion_producto.focus();
          return;
        }
        if(iva_producto.val() == ''){
          alert("Por favor seleccione un iva para este producto");
          iva_producto.focus();
          return;
        }
        if(categoria_producto.val() == ''){
          alert("Por favor seleccione una categoria valida para este producto");
          categoria_producto.focus();
          return;
        }
        if(subcategoria_producto.val() == ''){
          alert("Por favor selecciones una subcategoria valida para este producto");
          subcategoria_producto.focus();
          return;
        }
        if(costo_producto.val() == ''){
          alert("Por favor ingrese un costo valido para este producto");
          costo_producto.focus();
          return;
        }
        if(precio_producto.val() == ''){
          alert("Por favor ingrese un precio valido para este producto");
          precio_producto.focus();
          return;
        }
        if(inventario_producto.val() == ''){
          alert("Por favor ingrese una cantidad de inventario para este producto");
          inventario_producto.focus();
          return;
        }
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url: "{{url('registrar-producto')}}",
            data: {
            	inventario_producto : inventario_producto.val(),		
              iva_producto : iva_producto.val(),		
              nombre_producto : nombre_producto.val(),
              codigo_producto : codigo_producto.val(),
              categoria_producto : categoria_producto.val(),
              subcategoria_producto : subcategoria_producto.val(),
              medicion_producto : medicion_producto.val(),
              costo_producto : costo_producto.val(),
              precio_producto : precio_producto.val()
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Exito!");
                //agregamos los resultados al html
                $('#productos').html(data.options);
                $("#productos").selectpicker("refresh");
                $("#productos").trigger('change');
                //cerramos la ventana
                //$('#close_modal_producto').click();//cerramos la ventana
                $('#modalProducto').modal('hide');//cerramos la ventana
                $('#productos').focus();
              }else if(data.resp == "existe"){
                alert("Error: El codigo o el nombre del producto ya se encuentra registrado en la Base de datos!");
                return;
              }else if(data.resp == "error"){
                alert("Error: " + data.msg);
                return;
              }

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ops! Ocurrio un error! Contacte a un administrador del sistema.");
            }

            });
      });

             $('#categoria-producto').on('change',function(){
          cargarSubcategorias1();
      });


        });

  function cargarSubcategorias1(){
    id_categoria = $('#categoria-producto').val();
    if(id_categoria == 0){
      return;
    }
        $.ajax({
          headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url: "{{url('cargar-subcategorias')}}",
            data: {
              categoria : id_categoria
            },
            method: "POST",
            success: function(data){
              if(data.resp == "ok")
                $("#subcategoria-producto").html(data.options);
            },
            beforeSend: function(){

            },
            error: function(){
              alert("Ocurrio un error");
            }

            });
    }
    </script>
