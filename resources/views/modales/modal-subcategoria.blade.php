<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
<div class="modal fade" id="modalSubcategoria" tabindex="-1" role="dialog" aria-labelledby="modalSubcategoria" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalSubcategoria">Agregar Subcategoria</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Nombre:</label>
            <input type="text" class="form-control" id="nombre-subcategoria" required="">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Categoria padre:</label>
            <select name="categoria-padre" id="categoria-padre" class="form-control" required="">
                                    
                                    @if(isset($categorias))
                                    @foreach($categorias as $s)
                                    <option value="{{$s->id}}">{{$s->nombre}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                 
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary  btn_finish" id="registrar_subcategoria">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_subcategoria" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
      $(document).ready(function(){
  $('#modalSubcategoria').on('shown.bs.modal', function () {
  $('#nombre-subcategoria').trigger('focus')
});
  _token = $("#_token").val();
  $("#registrar_subcategoria").click(function(e){
        nombre = $("#nombre-subcategoria").val();
        categoria = $("#categoria-padre").val();
        if(nombre == '' || nombre == null || categoria == null || categoria == ''){
          alert('Error: Faltan datos. Verifique los datos ingresados');
          nombre.focus();
          return;
        }

          $.ajax({
            url: "{{url('registrar-subcategoria')}}",
            data: {
              _token : _token,
              nombre : nombre,
              categoria : categoria
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Exito!");
                //agregamos el html
                $('#subcategoria').html(data.options);
                $('#subcategoria-producto').html(data.options);
                //cerramos la ventana
                $('#modalSubcategoria').modal('hide');//cerramos la ventana
                $('#subcategoria').focus();
              }else if(data.resp == "existe"){
                alert("Error: La subcategoria "+nombre+" ya se encuentra registrado para la categoria "+categoria+" en la Base de datos!");
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
      });
});
</script>