<div class="modal fade" id="modalImpuesto" tabindex="-1" role="dialog" aria-labelledby="modalImpuesto" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalImpuesto">Agregar Nuevo Impuesto</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Nombre:</label>
            <input type="text" class="form-control" id="nombre-impuesto" required="">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Valor:</label>
            <input type="text" class="form-control" id="valor-impuesto" required="">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn_finish" id="registrar_impuesto">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_impuesto" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

  $(document).ready(function(){
    $('#modalImpuesto').on('shown.bs.modal', function () {
  $('#nombre-impuesto').trigger('focus')
});

  $("#registrar_impuesto").click(function(e){
        nombre = $("#nombre-impuesto");
        valor = $("#valor-impuesto");
        if(nombre.val() == ''){
          alert("Por favor ingrese un nombre para este impuesto");
          nombre.focus();
          return;
        }
        if(valor.val() == '' || valor.val() > 100){
          alert("Por favor ingrese un valor valido para este impuesto");
          valor.focus();
          return;
        }
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url: "{{url('registrar-impuesto')}}",
            data: {
              nombre : nombre.val().toUpperCase(),
              valor : valor.val()
            },
            dataType: 'JSON',
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Exito!");
                //agregamos el html
                
                  $('#impuesto_producto').html(data.options);
                
                //cerramos la ventana
                //$('#close_modal_impuesto').click();//cerramos la ventana
                $('#modalImpuesto').modal('hide');//cerramos la ventana
                $('#impuesto_producto').focus();
              }else if(data.resp == "existe"){
                alert("Error: El impuesto "+nombre.val()+" ya se encuentra registrado en la Base de datos!");
                return;
              }else if(data.resp == "error"){
                alert("Error: Por favor verifique los datos suministrados e intente nuevamente");
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
      });
});
</script>