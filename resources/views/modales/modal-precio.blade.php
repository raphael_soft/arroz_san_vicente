<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
<div class="modal fade" id="modalPrecio" tabindex="-1" role="dialog" aria-labelledby="modalPrecio" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title">Agregar Precio</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
          <label for="message-text" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Para el producto:</label>
            <span id="nombre_producto_precio"></span>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Tipo de precio:</label>
            <select name="tipo-precio" id="tipo-precio" class="form-control" required="">
                                    
                                    <option value="{{$tipo}}" selected="">
                                    @if($tipo == "producciones")    
                                    De produccion
                                    @endif  
                                    
                                    @if($tipo == "productos")
                                    De Producto
                                    @endif
                                    </option>
                                    
            </select>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Precio:</label>
            <input type="number" class="form-control" id="valor-precio" required="">
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary  btn_finish" id="registrar_precio">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_precio" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
      $(document).ready(function(){
  

  $('#modalPrecio').on('shown.bs.modal', function () {
  $('#valor-precio').trigger('focus')
});
  _token = $("#_token").val();
  $("#registrar_precio").click(function(e){
        valor = $("#valor-precio").val();
        tipo = $("#tipo-precio").val();
        producto = $("#productos").val();

        if(producto == 0 || producto == ''){

          alert("Seleccione un producto, primero. Luego puede agregar un precio relacionado a este.");
          return;
        }

        if(valor == '' || valor == 0){
          alert("Por favor ingrese un valor valido");
          valor.focus();
          return;
        }

        if(tipo == ''){
          alert("Por favor selecciones un tipo de producto");
          tipo.focus();
          return;
        }

          $.ajax({
            url: "{{url('registrar-precio')}}",
            data: {
              _token : _token,
              valor : valor,
              tipo : tipo,
              producto : producto
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Exito!");
                //agregamos el html
                $('#precio_producto').val(parseFloat(data.value)); //just testing
                //$('#precio_producto').html(data.options);
                
                //cerramos la ventana
                //$('#close_modal_precio').click();//cerramos la ventana
                $('#modalprecio').modal('hide');//cerramos la ventana
                $('#precio_producto').focus();
              }else if(data.resp == "existe"){
                alert("Error: Este producto ya tiene precios asociados!");
                //$('#close_modal_precio').click();//cerramos la ventana
                $('#modalPrecio').modal('hide');//cerramos la ventana
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
      });
});
</script>