<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
<div class="modal fade" id="modalMedicion" tabindex="-1" role="dialog" aria-labelledby="modalMedicion" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalMedicion">Agregar Unidad de Medición</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Nombre (p.ej: Kilogramos, Gramos, Litros):</label>
            <input type="text" class="form-control" id="nombre-medicion">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Nombre abreviado o simbolo (p.ej: Kg, Gr, Lt):</label>
            <input type="text" class="form-control" id="simbolo-medicion" required="">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn_finish" id="registrar_medicion">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_medicion" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
      $(document).ready(function(){
  $('#modalMedicion').on('shown.bs.modal', function () {
  $('#nombre-medicion').trigger('focus')
});
  _token = $("#_token").val();
      $("#registrar_medicion").click(function(e){
        nombre = $("#nombre-medicion").val();
        simbolo = $("#simbolo-medicion").val();
        if(nombre == ''){
          alert("Por favor ingrese un nombre para este tipo de medicion");
          nombre.focus();
          return;
        }
        if(simbolo == ''){
          alert("Por favor ingrese un simbolo para este tipo de medicion");
          simbolo.focus();
          return;
        }
          $.ajax({
            url: "{{url('registrar-medicion')}}",
            data: {
              _token : _token,
              nombre : nombre,
              simbolo : simbolo
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Exito!");
                //agregamos el html
                $('#medicion').html(data.options);
                $('#medicion-producto').html(data.options);
                //cerramos la ventana
                //$('#close_modal_medicion').click();//cerramos la ventana
                $('#modalMedicion').modal('hide');//cerramos la ventana
                $('#medicion').focus(); 
              }else if(data.resp == "existe"){
                alert("Error: El tipo de medicion "+nombre+" ya se encuentra registrado en la Base de datos!");
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
      });
    });
</script>