<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
<div class="modal fade" id="modalModoPago" tabindex="-1" role="dialog" aria-labelledby="modalModoPago" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalModoPago">Agregar Modo de Pago</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Nombre:</label>
            <input type="text" class="form-control" id="modal_nombre_modo_pago" name="nombre_modo_pago">
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn_finish" id="registrar_modo_pago">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_modopago" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
      $(document).ready(function(){
  $('#modalModoPago').on('shown.bs.modal', function () {
  $('#modal_nombre_modo_pago').trigger('focus')
});
  _token = $("#_token").val();
      $("#registrar_modo_pago").click(function(e){
        nombreModoPago = $("#modal_nombre_modo_pago").val();
        
        if(nombreModoPago == ''){
          alert("Por favor ingrese un nombre");
          nombreModoPago.focus();
          return;
        }
          $.ajax({
            url: "{{url('registrar-modo-pago')}}",
            data: {
              _token : _token,
              nombre : nombreModoPago              
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert(data.msg);
                //TODO: actualizar el monto en la lista sin refrescar la pagina
                $('#modo_pago').append(data.option);
                $("#modo_pago").selectpicker("refresh");
                $("#modo_pago").trigger('change');
                //cerramos la ventana
                $('#modo_pago').focus();
                //$('#close_modal_modopago').click();//cerramos la ventana
                $('#modalModoPago').modal('hide');//cerramos la ventana
              }else if(data.resp == "error"){
                alert(data.msg);
                return;
              }
              
            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
              
            }

            });
      });
    });
</script>