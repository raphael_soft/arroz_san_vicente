<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}">
<div class="modal fade" id="modalCategoria" tabindex="-1" role="dialog" aria-labelledby="modalCategoria" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalCategoria">Agregar Categoria</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Nombre:</label>
            <input type="text" class="form-control" id="nombre-categoria" required="">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Descripcion:</label>
            <textarea class="form-control" id="descripcion-categoria"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn_finish" id="registrar_categoria">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_categoria" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
 
      $(document).ready(function(){
$('#modalCategoria').on('shown.bs.modal', function () {
  $('#nombre-categoria').trigger('focus')
});
_token = $("#_token").val();
      $("#registrar_categoria").click(function(e){
        nombre_categoria = $("#nombre-categoria").val();
        descripcion_categoria = $("#descripcion-categoria").val();
        if(nombre_categoria == ''){
          alert("Por favor ingrese un nombre para esta categoria");
          nombre_categoria.focus();
          return;
        }

          $.ajax({
            url: "{{url('registrar-categoria')}}",
            data: {
              _token : _token,
              nombre_categoria : nombre_categoria,
              descripcion_categoria : descripcion_categoria
            },
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Exito!");
                //agregamos los resultados al html
                $('#categoria').html(data.options);
                $('#categoria-padre').html(data.options); 
                $('#categoria-producto').html(data.options); 
                $("#categoria").trigger('change');
                $("#categoria-padre").trigger('change');
                $('#categoria-producto').trigger('change');
                //cerramos la ventana
                $('#modalCategoria').modal('hide');//cerramos la ventana
                $('#categoria').focus();
              }else if(data.resp == "existe"){
                alert("Error: La categoria "+nombre_categoria+" ya se encuentra registrada en la Base de datos!");
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(){
              hideLoading();
              alert("Ocurrio un error");
            }

            });
      });
    });
</script>