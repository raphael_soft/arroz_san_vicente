<div class="modal fade" id="modalSocio" tabindex="-1" role="dialog" aria-labelledby="modalSocio" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modalSocio">Agregar Nuevo Socio</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;Nombre:</label>
            <input type="text" class="form-control" id="nombre-socio" required="">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label"><strong style="color:red; font-size: 20px;">*</strong>&nbsp;RUC/Cedula:</label>
            <input type="text" class="form-control numeros" id="ruc-socio" required="" maxlength="13" minlength="10">
            <span class="error-msg" style="display: normal; color: red"></span>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn_finish submit" id="registrar_socio">Registrar</button>
        <button type="button" class="btn btn-secondary" id="close_modal_socio" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="{{ url('/js/ruc_jquery_validator.min.js') }}"></script>
<script type="text/javascript">
  var opciones = {
    strict: true,        // va a validar siempre, aunque la cantidad de caracteres no sea 10 ni 13
    events: "keyup",     // evento que va a disparar la validación
    the_classes: "error",// clase que se va a agregar al nodo en el que se realiza la validación
    onValid: function () {    
          console.log('cedula/ruc valido');
          $('.error-msg').hide();
          $('.submit').removeClass('disabled');
          $('.submit').removeAttr('disabled');
    },   // callback cuando la cédula es correcta.
    onInvalid: function () {
          console.log('cedula/ruc invalido');
          $('.error-msg').text('RUC/Cedula invalido').show();
          $('.submit').addClass('disabled');
          $('.submit').attr('disabled','disabled');
      }  // callback cuando la cédula es incorrecta.
  };
  
  $('#ruc-socio').validarCedulaEC(opciones);

</script> 

<script type="text/javascript">
  
  $(document).ready(function(){

  $('#modalSocio').on('shown.bs.modal', function () {
    $('#nombre-socio').trigger('focus')
  });
  $("#registrar_socio").click(function(e){
            
        nombre = $("#nombre-socio");
        ruc = $("#ruc-socio");

        if(nombre.val() == ''){
            alert('Error: Ingrese un nombre para este vendedor');
            nombre.focus();
            return;
        }
        if(ruc.val() == '' || ruc.val() <= 0){
            alert('Error: RUC invalido. verifiquelo e intente nuevamente');
            ruc.focus();
            return;
        } 
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url: "{{url('registrar-proveedor')}}",
            data: {
              nombre : nombre.val(),
              ruc: ruc.val()
            },
            dataType: 'JSON',
            method: "POST",
            success: function(data){
              hideLoading();
              if(data.resp == "ok"){
                alert("Exito!");
                //agregamos el html
                $('#socio').html(data.options);
                $('#socio').selectpicker('refresh');
                //cerramos la ventana
                //$('#close_modal_socio').click();//cerramos la ventana
                $('#modalSocio').modal('hide');//cerramos la ventana
                $('#socio').focus();
              }else if(data.resp == "existe"){
                alert("Error: El vendedor "+nombre+" ya se encuentra registrado en la Base de datos!");
                return;
              }else if(data.resp == "error"){
                alert("Error: Por favor verifique los datos suministrados e intente nuevamente");
                return;
              }
              

            },
            beforeSend: function(){
              showLoading();
            },
            error: function(e){
              hideLoading();
              alert("Ha ocurrido un error al hacer la peticion: ");
            }

            });

      });
});
</script>