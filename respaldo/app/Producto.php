<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = "productos";

    protected $fillable = ['id', 'nombre', 'precio', 'codigo', 'socio_id', 'status'];

    
    public function socio() {
        return $this->belongsTo('App\Socio');
    }

    public function facturacompra() {
        return $this->belongsTo('App\Facturacompra');
    }

    public function inventario() {
        return $this->belongsTo('App\Inventario');
    }
    
}
