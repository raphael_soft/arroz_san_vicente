<?php

namespace App\Http\Controllers;

use App\Http\Requests\TipoMedicionFormRequest;
use App\TipoMedicion;
use Illuminate\Http\Request;

class TipoMedicionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $datos = TipoMedicion::all();
        return view('configuracion.tipo_medicion.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.tipo_medicion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoMedicionFormRequest $request) {
        //
        // dd($request;
        // dd($request->all());

        $nombre = strtoupper($request->get('nombre'));

        $datos = new TipoMedicion(array(
            'nombre' => $nombre,
        ));

        $datos->save();

        return redirect('/configuracion/tipo-de-mediciones/nueva-medicion')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $datos = TipoMedicion::whereId($id)->firstOrFail();
        return view('configuracion.tipo_medicion.show', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $datos = TipoMedicion::whereId($id)->firstOrFail();
        return view('configuracion.tipo_medicion.edit', compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TipoMedicionFormRequest $request, $id) {
        //

        $datos = TipoMedicion::whereId($id)->firstOrFail();
        $nombre    = strtoupper($request->get('nombre'));

        $datos->nombre = $nombre;
        $datos->save();

        return redirect('/configuracion/tipo-de-mediciones')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $datos = TipoMedicion::whereId($id)->firstOrFail();
        $datos->delete();
        return redirect('/configuracion/tipo-de-mediciones')->with('status', 'Borrado Exitoso ');
    }
}
