<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests\ProduccionFormRequest;
use App\Produccion;
use App\Res;
use App\Socio;
use App\TipoMedicion;
use App\TipoProduccion;
use Illuminate\Http\Request;

class ProduccionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $datos = Produccion::all();
        return view('produccion.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        $socios            = Socio::all();
        $categorias        = Categoria::all();
        $tipo_mediciones   = TipoMedicion::all();
        $tipo_producciones = TipoProduccion::all();
        return view('produccion.create', compact('socios', 'categorias', 'tipo_mediciones', 'tipo_producciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProduccionFormRequest $request) {
        //
        // dd($request;
        // dd($request->all());

        if ($request->get('baja')) {

            $res         = Res::whereId($request->get('res'))->firstOrFail();
            $res->status = "0";
            $res->save();

            $produccion = new Produccion(array(
                'socio_id'           => $request->get('socio'),
                'res_id'             => $request->get('res'),
                'categoria_id'       => $request->get('categoria'),
                'tipo_medicion_id'   => $request->get('tipo_medicion'),
                'tipo_produccion_id' => $request->get('tipo_produccion'),
                'cantidad'           => $request->get('cantidad'),
                'fecha'              => date("Y-m-d"),
            ));

            $produccion->save();
            return redirect('/produccion/nueva-produccion')->with('status', 'Registro Exitoso.');
        } else {
            $produccion = new Produccion(array(
                'socio_id'           => $request->get('socio'),
                'res_id'             => $request->get('res'),
                'categoria_id'       => $request->get('categoria'),
                'tipo_medicion_id'   => $request->get('tipo_medicion'),
                'tipo_produccion_id' => $request->get('tipo_produccion'),
                'cantidad'           => $request->get('cantidad'),
                'fecha'              => date("Y-m-d"),
            ));

            $produccion->save();
            return redirect('/produccion/nueva-produccion')->with('status', 'Registro Exitoso.');

        }

    }

    public function GetReses(Request $request) {

        $id  = $request->get('socio');
        $res = Res::whereSocioId($id)->whereStatus(1)->get();

        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $datos = Produccion::whereId($id)->firstOrFail();
        return view('produccion.show', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $datos             = Produccion::whereId($id)->firstOrFail();
        $socios            = Socio::all();
        $categorias        = Categoria::all();
        $tipo_mediciones   = TipoMedicion::all();
        $tipo_producciones = TipoProduccion::all();
        return view('produccion.edit', compact('datos', 'socios', 'categorias', 'tipo_mediciones', 'tipo_producciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProduccionFormRequest $request, $id) {
        //
        if ($request->get('baja')) {

            $res         = Res::whereId($request->get('res'))->firstOrFail();
            $res->status = "0";
            $res->save();

            $produccion                     = Produccion::whereId($id)->firstOrFail();
            
            $produccion->socio_id           = $request->get('socio');
            $produccion->res_id             = $request->get('res');
            $produccion->categoria_id       = $request->get('categoria');
            $produccion->tipo_medicion_id   = $request->get('tipo_medicion');
            $produccion->tipo_produccion_id = $request->get('tipo_produccion');
            $produccion->cantidad           = $request->get('cantidad');

            $produccion->save();
            return redirect('/produccion')->with('status', 'Registro Exitoso.');
        } else {

            $res         = Res::whereId($request->get('res'))->firstOrFail();
            $res->status = "1";
            $res->save();

            $produccion                     = Produccion::whereId($id)->firstOrFail();

            $produccion->socio_id           = $request->get('socio');
            $produccion->res_id             = $request->get('res');
            $produccion->categoria_id       = $request->get('categoria');
            $produccion->tipo_medicion_id   = $request->get('tipo_medicion');
            $produccion->tipo_produccion_id = $request->get('tipo_produccion');
            $produccion->cantidad           = $request->get('cantidad');

            $produccion->save();
            return redirect('/produccion')->with('status', 'Registro Exitoso.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $datos = Produccion::whereId($id)->firstOrFail();
        $datos->delete();
        return redirect('/produccion')->with('status', 'Borrado Exitoso');
    }
}
