<?php

namespace App\Http\Controllers;

use App\Http\Requests\IvaFormRequest;
use App\Iva;
use Illuminate\Http\Request;

class IvaController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $ivas = Iva::all();
        return view('configuracion.iva.index', compact('ivas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.iva.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IvaFormRequest $request) {
        //
        // dd($request;
        // dd($request->all());

        $nombre = strtoupper($request->get('nombre'));

        $iva = new Iva(array(
            'nombre'    => $nombre,
            'valor'     => $request->get('valor'),
        ));

        $iva->save();

        return redirect('/configuracion/iva/nuevo-iva')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $iva = Iva::whereId($id)->firstOrFail();
        return view('configuracion.iva.show', compact('iva'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $iva = Iva::whereId($id)->firstOrFail();
        return view('configuracion.iva.edit', compact('iva'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IvaFormRequest $request, $id) {
        //

        $iva = Iva::whereId($id)->firstOrFail();
        $nombre = strtoupper($request->get('nombre'));

        $iva->nombre = $nombre;
        $iva->valor = $request->get('valor');
        $iva->save();

        return redirect('/configuracion/iva/')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $iva = Iva::whereId($id)->firstOrFail();
        $iva->delete();
        return redirect('/configuracion/iva')->with('status', 'Borrado Exitoso ');
    }
}
