<?php

namespace App\Http\Controllers;

use App\Http\Requests\RolFormRequest;
use App\Rol;
use Illuminate\Http\Request;

class RolController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $rols = Rol::all();
        return view('configuracion.rol.index', compact('rols'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.rol.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolFormRequest $request) {
        //
        // dd($request;
        // dd($request->all());

        $nombre = strtoupper($request->get('nombre'));

        $rol = new Rol(array(
            'nombre' => $nombre,
        ));

        $rol->save();

        return redirect('/configuracion/roles/nuevo-rol')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $rol = Rol::whereId($id)->firstOrFail();
        return view('configuracion.rol.show', compact('rol'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $rol = Rol::whereId($id)->firstOrFail();
        return view('configuracion.rol.edit', compact('rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RolFormRequest $request, $id) {
        //

        $rol    = Rol::whereId($id)->firstOrFail();
        $nombre = strtoupper($request->get('nombre'));

        $rol->nombre = $nombre;
        $rol->save();

        return redirect('/configuracion/roles/')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $rol = Rol::whereId($id)->firstOrFail();
        $rol->delete();
        return redirect('/configuracion/roles')->with('status', 'Borrado Exitoso ');
    }
}
