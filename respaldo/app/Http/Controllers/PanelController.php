<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Res;
use App\User;
use App\Socio;
use App\Produccion;
use App\Venta;
use App\Facturacompra;

class PanelController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    // INICIO
    public function Home() {



        // INGRESO ESTADISTICAS

        //A#O ACTUAL DE LA PRIMERA VISTA
        $anio_actual = date("Y");

        //VARIABLES

        //CONSULTA ENERO
        $enero_inicial = $anio_actual . "-" . "01" . "-" . "01";
        $enero_final   = $anio_actual . "-" . "01" . "-" . "31";
        $ventas_enero1  = Venta::where('fecha', '>=', $enero_inicial)->where('fecha', '<=', $enero_final)->where('status', '=', '2')->get();
        $ventas_enero = 0;
        foreach ($ventas_enero1 as $dato ) {
            $ventas_enero = $ventas_enero + $dato->total;
        }

        //CONSULTA FEBRERO
        $febrero_inicial = $anio_actual . "-" . "02" . "-" . "01";
        $febrero_final   = $anio_actual . "-" . "02" . "-" . "28";
        $ventas_febrero1  = Venta::where('fecha', '>=', $febrero_inicial)->where('fecha', '<=', $febrero_final)->where('status', '=', '2')->get();
        $ventas_febrero = 0;
        foreach ($ventas_febrero1 as $dato ) {
            $ventas_febrero = $ventas_febrero + $dato->total;
        }

        //CONSULTA MARZO
        $marzo_inicial = $anio_actual . "-" . "03" . "-" . "01";
        $marzo_final   = $anio_actual . "-" . "03" . "-" . "31";
        $ventas_marzo1  = Venta::where('fecha', '>=', $marzo_inicial)->where('fecha', '<=', $marzo_final)->where('status', '=', '2')->get();
        $ventas_marzo = 0;
        foreach ($ventas_marzo1 as $dato ) {
            $ventas_marzo = $ventas_marzo + $dato->total;
        }

        //CONSULTA ABRIL
        $abril_inicial = $anio_actual . "-" . "04" . "-" . "01";
        $abril_final   = $anio_actual . "-" . "04" . "-" . "30";
        $ventas_abril1  = Venta::where('fecha', '>=', $abril_inicial)->where('fecha', '<=', $abril_final)->where('status', '=', '2')->get();
        $ventas_abril = 0;
        foreach ($ventas_abril1 as $dato ) {
            $ventas_abril = $ventas_abril + $dato->total;
        }

        //CONSULTA MAYO
        $mayo_inicial = $anio_actual . "-" . "05" . "-" . "01";
        $mayo_final   = $anio_actual . "-" . "05" . "-" . "31";
        $ventas_mayo1  = Venta::where('fecha', '>=', $mayo_inicial)->where('fecha', '<=', $mayo_final)->where('status', '=', '2')->get();
        $ventas_mayo = 0;
        foreach ($ventas_mayo1 as $dato ) {
            $ventas_mayo = $ventas_mayo + $dato->total;
        }

        //CONSULTA JUNIO
        $junio_inicial = $anio_actual . "-" . "06" . "-" . "01";
        $junio_final   = $anio_actual . "-" . "06" . "-" . "30";
        $ventas_junio1  = Venta::where('fecha', '>=', $junio_inicial)->where('fecha', '<=', $junio_final)->where('status', '=', '2')->get();
        $ventas_junio = 0;
        foreach ($ventas_junio1 as $dato ) {
            $ventas_junio = $ventas_junio + $dato->total;
        }

        //CONSULTA JULIO
        $julio_inicial = $anio_actual . "-" . "07" . "-" . "01";
        $julio_final   = $anio_actual . "-" . "07" . "-" . "31";
        $ventas_julio1  = Venta::where('fecha', '>=', $julio_inicial)->where('fecha', '<=', $julio_final)->where('status', '=', '2')->get();
        $ventas_julio = 0;
        foreach ($ventas_julio1 as $dato ) {
            $ventas_julio = $ventas_julio + $dato->total;
        }

        //CONSULTA AGOSTO
        $agosto_inicial = $anio_actual . "-" . "08" . "-" . "01";
        $agosto_final   = $anio_actual . "-" . "08" . "-" . "31";
        $ventas_agosto1  = Venta::where('fecha', '>=', $agosto_inicial)->where('fecha', '<=', $agosto_final)->where('status', '=', '2')->get();
        $ventas_agosto = 0;
        foreach ($ventas_agosto1 as $dato ) {
            $ventas_agosto = $ventas_agosto + $dato->total;
        }

        //CONSULTA SEPTIEMBRE
        $septiembre_inicial = $anio_actual . "-" . "09" . "-" . "01";
        $septiembre_final   = $anio_actual . "-" . "09" . "-" . "30";
        $ventas_septiembre1  = Venta::where('fecha', '>=', $septiembre_inicial)->where('fecha', '<=', $septiembre_final)->where('status', '=', '2')->get();
        $ventas_septiembre = 0;
        foreach ($ventas_septiembre1 as $dato ) {
            $ventas_septiembre = $ventas_septiembre + $dato->total;
        }

        //CONSULTA OCTUBRE
        $octubre_inicial = $anio_actual . "-" . "10" . "-" . "01";
        $octubre_final   = $anio_actual . "-" . "10" . "-" . "30";
        $ventas_octubre1  = Venta::where('fecha', '>=', $octubre_inicial)->where('fecha', '<=', $octubre_final)->where('status', '=', '2')->get();
        $ventas_octubre = 0;
        foreach ($ventas_octubre1 as $dato ) {
            $ventas_octubre = $ventas_octubre + $dato->total;
        }

        //CONSULTA NOVIEMBRE
        $noviembre_inicial = $anio_actual . "-" . "11" . "-" . "01";
        $noviembre_final   = $anio_actual . "-" . "11" . "-" . "30";
        $ventas_noviembre1  = Venta::where('fecha', '>=', $noviembre_inicial)->where('fecha', '<=', $noviembre_final)->where('status', '=', '2')->get();
        $ventas_noviembre = 0;
        foreach ($ventas_noviembre1 as $dato ) {
            $ventas_noviembre = $ventas_noviembre + $dato->total;
        }

        //CONSULTA DICIEMBRE
        $diciembre_inicial = $anio_actual . "-" . "12" . "-" . "01";
        $diciembre_final   = $anio_actual . "-" . "12" . "-" . "30";
        $ventas_diciembre1  = Venta::where('fecha', '>=', $diciembre_inicial)->where('fecha', '<=', $diciembre_final)->where('status', '=', '2')->get();
        $ventas_diciembre = 0;
        foreach ($ventas_diciembre1 as $dato ) {
            $ventas_diciembre = $ventas_diciembre + $dato->total;
        }

        //CONSULTA POS A#OS

        //CONSULTA 2017
        $anio_inicio_2017 = "2017" . "-" . "01" . "-" . "01";
        $anio_final_2017  = "2017" . "-" . "12" . "-" . "31";
        $total_20171       = Venta::where('fecha', '>=', $anio_inicio_2017)->where('fecha', '<=', $anio_final_2017)->where('status', '=', '2')->get();
        $total_2017 = 0;
        foreach ($total_20171 as $dato ) {
            $total_2017 = $total_2017 + $dato->total;
        }

        //CONSULTA 2018
        $anio_inicio_2018 = "2018" . "-" . "01" . "-" . "01";
        $anio_final_2018  = "2018" . "-" . "12" . "-" . "31";
        $total_20181       = Venta::where('fecha', '>=', $anio_inicio_2018)->where('fecha', '<=', $anio_final_2018)->where('status', '=', '2')->get();
        $total_2018 = 0;
        foreach ($total_20181 as $dato ) {
            $total_2018 = $total_2018 + $dato->total;
        }

        //CONSULTA 2019
        $anio_inicio_2019 = "2019" . "-" . "01" . "-" . "01";
        $anio_final_2019  = "2019" . "-" . "12" . "-" . "31";
        $total_20191       = Venta::where('fecha', '>=', $anio_inicio_2019)->where('fecha', '<=', $anio_final_2019)->where('status', '=', '2')->get();
        $total_2019 = 0;
        foreach ($total_20191 as $dato ) {
            $total_2019 = $total_2019 + $dato->total;
        }

        //CONSULTA 2020
        $anio_inicio_2020 = "2020" . "-" . "01" . "-" . "01";
        $anio_final_2020  = "2020" . "-" . "12" . "-" . "31";
        $total_20201       = Venta::where('fecha', '>=', $anio_inicio_2020)->where('fecha', '<=', $anio_final_2020)->where('status', '=', '2')->get();
        $total_2020 = 0;
        foreach ($total_20201 as $dato ) {
            $total_2020 = $total_2020 + $dato->total;
        }

        //CONSULTA 2021
        $anio_inicio_2021 = "2020" . "-" . "01" . "-" . "01";
        $anio_final_2021  = "2020" . "-" . "12" . "-" . "31";
        $total_20211      = Venta::where('fecha', '>=', $anio_inicio_2021)->where('fecha', '<=', $anio_final_2021)->where('status', '=', '2')->get();
        $total_2021 = 0;
        foreach ($total_20211 as $dato ) {
            $total_2021 = $total_2021 + $dato->total;
        }




        // ESTADISTICAS EGRESO

        //CONSULTA ENERO
        $enero_inicial = $anio_actual . "-" . "01" . "-" . "01";
        $enero_final   = $anio_actual . "-" . "01" . "-" . "31";
        $ventas_enero1  = Facturacompra::where('fecha_facturacion', '>=', $enero_inicial)->where('fecha_facturacion', '<=', $enero_final)->where('status', '=', '2')->get();
        $ventas_mes_enero = 0;
        foreach ($ventas_enero1 as $dato ) {
            $ventas_mes_enero = $ventas_mes_enero + $dato->total;
        }

        //CONSULTA FEBRERO
        $febrero_inicial = $anio_actual . "-" . "02" . "-" . "01";
        $febrero_final   = $anio_actual . "-" . "02" . "-" . "28";
        $ventas_febrero1  = Facturacompra::where('fecha_facturacion', '>=', $febrero_inicial)->where('fecha_facturacion', '<=', $febrero_final)->where('status', '=', '2')->get();
        $ventas_mes_febrero = 0;
        foreach ($ventas_febrero1 as $dato ) {
            $ventas_mes_febrero = $ventas_mes_febrero + $dato->total;
        }

        //CONSULTA MARZO
        $marzo_inicial = $anio_actual . "-" . "03" . "-" . "01";
        $marzo_final   = $anio_actual . "-" . "03" . "-" . "31";
        $ventas_marzo1  = Facturacompra::where('fecha_facturacion', '>=', $marzo_inicial)->where('fecha_facturacion', '<=', $marzo_final)->where('status', '=', '2')->get();
        $ventas_mes_marzo = 0;
        foreach ($ventas_marzo1 as $dato ) {
            $ventas_mes_marzo = $ventas_mes_marzo + $dato->total;
        }

        //CONSULTA ABRIL
        $abril_inicial = $anio_actual . "-" . "04" . "-" . "01";
        $abril_final   = $anio_actual . "-" . "04" . "-" . "30";
        $ventas_abril1  = Facturacompra::where('fecha_facturacion', '>=', $abril_inicial)->where('fecha_facturacion', '<=', $abril_final)->where('status', '=', '2')->get();
        $ventas_mes_abril = 0;
        foreach ($ventas_abril1 as $dato ) {
            $ventas_mes_abril = $ventas_mes_abril + $dato->total;
        }

        //CONSULTA MAYO
        $mayo_inicial = $anio_actual . "-" . "05" . "-" . "01";
        $mayo_final   = $anio_actual . "-" . "05" . "-" . "31";
        $ventas_mayo1  = Facturacompra::where('fecha_facturacion', '>=', $mayo_inicial)->where('fecha_facturacion', '<=', $mayo_final)->where('status', '=', '2')->get();
        $ventas_mes_mayo = 0;
        foreach ($ventas_mayo1 as $dato ) {
            $ventas_mes_mayo = $ventas_mes_mayo + $dato->total;
        }

        //CONSULTA JUNIO
        $junio_inicial = $anio_actual . "-" . "06" . "-" . "01";
        $junio_final   = $anio_actual . "-" . "06" . "-" . "30";
        $ventas_junio1  = Facturacompra::where('fecha_facturacion', '>=', $junio_inicial)->where('fecha_facturacion', '<=', $junio_final)->where('status', '=', '2')->get();
        $ventas_mes_junio = 0;
        foreach ($ventas_junio1 as $dato ) {
            $ventas_mes_junio = $ventas_mes_junio + $dato->total;
        }

        //CONSULTA JULIO
        $julio_inicial = $anio_actual . "-" . "07" . "-" . "01";
        $julio_final   = $anio_actual . "-" . "07" . "-" . "31";
        $ventas_julio1  = Facturacompra::where('fecha_facturacion', '>=', $julio_inicial)->where('fecha_facturacion', '<=', $julio_final)->where('status', '=', '2')->get();
        $ventas_mes_julio = 0;
        foreach ($ventas_julio1 as $dato ) {
            $ventas_mes_julio = $ventas_mes_julio + $dato->total;
        }

        //CONSULTA AGOSTO
        $agosto_inicial = $anio_actual . "-" . "08" . "-" . "01";
        $agosto_final   = $anio_actual . "-" . "08" . "-" . "31";
        $ventas_agosto1  = Facturacompra::where('fecha_facturacion', '>=', $agosto_inicial)->where('fecha_facturacion', '<=', $agosto_final)->where('status', '=', '2')->get();
        $ventas_mes_agosto = 0;
        foreach ($ventas_agosto1 as $dato ) {
            $ventas_mes_agosto = $ventas_mes_agosto + $dato->total;
        }

        //CONSULTA SEPTIEMBRE
        $septiembre_inicial = $anio_actual . "-" . "09" . "-" . "01";
        $septiembre_final   = $anio_actual . "-" . "09" . "-" . "30";
        $ventas_septiembre1  = Facturacompra::where('fecha_facturacion', '>=', $septiembre_inicial)->where('fecha_facturacion', '<=', $septiembre_final)->where('status', '=', '2')->get();
        $ventas_mes_septiembre = 0;
        foreach ($ventas_septiembre1 as $dato ) {
            $ventas_mes_septiembre = $ventas_mes_septiembre + $dato->total;
        }

        //CONSULTA OCTUBRE
        $octubre_inicial = $anio_actual . "-" . "10" . "-" . "01";
        $octubre_final   = $anio_actual . "-" . "10" . "-" . "30";
        $ventas_octubre1  = Facturacompra::where('fecha_facturacion', '>=', $octubre_inicial)->where('fecha_facturacion', '<=', $octubre_final)->where('status', '=', '2')->get();
        $ventas_mes_octubre = 0;
        foreach ($ventas_octubre1 as $dato ) {
            $ventas_mes_octubre = $ventas_mes_octubre + $dato->total;
        }

        //CONSULTA NOVIEMBRE
        $noviembre_inicial = $anio_actual . "-" . "11" . "-" . "01";
        $noviembre_final   = $anio_actual . "-" . "11" . "-" . "30";
        $ventas_noviembre1  = Facturacompra::where('fecha_facturacion', '>=', $noviembre_inicial)->where('fecha_facturacion', '<=', $noviembre_final)->where('status', '=', '2')->get();
        $ventas_mes_noviembre = 0;
        foreach ($ventas_noviembre1 as $dato ) {
            $ventas_mes_noviembre = $ventas_mes_noviembre + $dato->total;
        }

        //CONSULTA DICIEMBRE
        $diciembre_inicial = $anio_actual . "-" . "12" . "-" . "01";
        $diciembre_final   = $anio_actual . "-" . "12" . "-" . "30";
        $ventas_diciembre1  = Facturacompra::where('fecha_facturacion', '>=', $diciembre_inicial)->where('fecha_facturacion', '<=', $diciembre_final)->where('status', '=', '2')->get();
        $ventas_mes_diciembre = 0;
        foreach ($ventas_diciembre1 as $dato ) {
            $ventas_mes_diciembre = $ventas_mes_diciembre + $dato->total;
        }




    	$res = Res::all()->count();
    	$user = User::all()->count();
    	$socio = Socio::all()->count();
    	$produccion = Produccion::all()->count();
    	return view('index', compact('res', 'user', 'socio', 'produccion','anio_actual', 'ventas_enero', 'ventas_febrero', 'ventas_marzo', 'ventas_abril', 'ventas_mayo', 'ventas_junio', 'ventas_julio', 'ventas_agosto', 'ventas_septiembre', 'ventas_octubre', 'ventas_noviembre', 'ventas_diciembre', 'ventas_mes_enero', 'ventas_mes_febrero', 'ventas_mes_marzo', 'ventas_mes_abril', 'ventas_mes_mayo', 'ventas_mes_junio', 'ventas_mes_julio', 'ventas_mes_agosto', 'ventas_mes_septiembre', 'ventas_mes_octubre', 'ventas_mes_noviembre', 'ventas_mes_diciembre'));
    }

}
