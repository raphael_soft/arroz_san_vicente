<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Socio;
use App\Producto;
use App\Inventario;
use App\Http\Requests\ProductoFormRequests;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::all();
        return view('inventario.productos.index', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {	
    	$socio = Socio::all();
        return view('inventario.productos.create', compact('socio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductoFormRequests $request)
    {
        $slug = uniqid();
        $registro            = new Producto;
        $registro->nombre    = $request['nombre'];
        $registro->precio    = $request['precio'];
        $registro->socio_id  = $request['proveedor'];
        $registro->codigo    = $slug;
        $registro->save();

        $produc = Producto::all();
        $produc = $produc->last();
        

        $inventario                 = new Inventario;
        $inventario->n_productos    = "0";
        $inventario->producto_id    = $produc->id;
        $inventario->save();


        //LISTADO DE PRODUCTOS
        return redirect('productos')->with('status', 'Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto   = Producto::find($id);
        $socio      = Socio::find($producto->socio_id);
    	return view('inventario.productos.show', compact('producto','socio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::find($id);
        $socio    = Socio::find($producto->socio_id);
        $socios = Socio::all();
    	return view('inventario.productos.edit', compact('producto','socio','socios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductoFormRequests $request, $id)
    {
        $id  			   = $_POST['id'];
        $update            = Producto::find($id);
       	$update->nombre    = $request['nombre'];
        $update->precio    = $request['precio'];
        $update->socio_id  = $request['proveedor'];
        $update->status    = $request['status'];
        $update->save();

        $socios = Socio::find($id);
		return redirect("edit_producto/$id")->with('status', 'Actualizaci&oacute;n Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroys($id)
    {
        $producto = Producto::whereId($id)->firstOrFail();
		$producto->delete();
		
        return redirect('productos')->with('status', 'Borrado Exitoso');
    }
}
