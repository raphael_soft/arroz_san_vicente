<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Socio;
use App\Iva;
use App\Facturacompra;
use App\Detallefactura;
use App\Inventario;
use App\Producto;
use App\Http\Requests\FacturacompraFormRequests;

class FacturacompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $factura = Facturacompra::all();
        return view('factura_compra.index', compact('factura'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$socio = Socio::all();
    	$iva   = Iva::all();
    	$consulta_facturas = Facturacompra::all()->count();


    	//CONDICION PARA LLEVAR A N FACTURAS A 1 SI NO HAY REGISTROS Y NO COMIENCE DE CERO
    	if($consulta_facturas == 0){

    		$n_facturas = 1;

    	}else{

    		$n_facturas = $consulta_facturas +1;
    	}

    	//CODIGO DE FACTURA PARA EL SIGUIENTE REGISTRO
    	$codigo_factura = date("Ymd")."-".$n_facturas;

    	//ORDEN ID
    	$order_id = uniqid();

    	//FECHA ACTUAL
    	$fecha =date("Y-M-d");

    	return view('factura_compra.create', compact('codigo_factura','order_id','fecha','socio','iva'));

    }

    public function ImprimirPdf($id){

        $factura   = Facturacompra::find($id);
        $detalles  = Detallefactura::where('n_factura', '=', $factura->n_factura)->get();
        $socio     = Socio::where('id', '=', $factura->socio_id)->get();
        $iva 	   = Iva::where('id', '=', $factura->iva_id)->get();

    	return view('factura_compra.imprimir', compact('factura','detalles','iva','socio'));
    }

    public function GetProductos(Request $request){

        // $id = $post['socio'];
        $id = $request->get('socio');
        $producto = Producto::where('socio_id', '=', $id)->get();

        return response()->json($producto);
    }



    public function StatusAceptar($id){
        $factura   = Facturacompra::find($id);
       
        $detalle    = Detallefactura::where('n_factura', '=', $factura->n_factura)->get();

        $datos = json_decode($detalle);
        

        foreach ($datos as $key => $value) {

            $stock  =  Inventario::where('producto_id', '=', $value->id_producto)->value('n_productos');

            $nueva_cantidad = $stock + $value->cantidad;

            $update               =  Inventario::find($value->id_producto);
            $update->n_productos  =  $nueva_cantidad;
            $update->save();


        }

        //CAMBIAR STATUS
        $update2             = Facturacompra::find($id);
        $update2->status     = 2;
        $update2->save();

        return redirect("facturascompras")->with('status', 'Status Actualizado: Aceptado');
        

    }

    public function StatusCalcelar($id){

        $update             = Facturacompra::find($id);
        $update->status   	= 0;
        $update->save();

        return redirect("facturascompras")->with('status', 'Status Actualizado: Cancelado');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	//TABLA FACTURA
        $registro                     = new Facturacompra;
        $registro->socio_id  		  = $request['socio'];
        $registro->iva_id    		  = $request['iva'];
        $registro->n_factura 		  = $request['n_factura'];
        $registro->n_orden   		  = $request['orden_id'];
        $registro->fecha_facturacion   = date("Y-m-d");
        $registro->save();

        //MONTO DE IVA
        $iva             = Iva::find($request['iva']);

        $calculo_subtotal = 0;
        foreach ($request['id_producto'] as $key => $value) {


            //COSTOS

                $calculo_subtotal += $request['precio'][$key] * $request['cantidad'][$key];//SUBTOTAL
                $monto_iva         = ($calculo_subtotal*$iva->valor/100);
                $total             = $monto_iva + $calculo_subtotal;

           	$registro2                    = new Detallefactura;
        	$registro2->id_producto  	  = $value;
        	$registro2->n_factura  	      = $registro->n_factura;
        	$registro2->cantidad  	 	  = $request['cantidad'][$key];
        	$registro2->save();

            $factura             =  Facturacompra::all();
            $ultimoid            =  $factura->last();
            $id                  =  $ultimoid->id;
            $update              =  Facturacompra::find($id);
            
            $update->subtotal    =  $calculo_subtotal;
            $update->monto_iva   =  $monto_iva;
            $update->total       =  $total;
            $update->save();

        };


        return redirect("facturascompras")->with('status', 'Facturaci&oacute;n Exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $factura   = Facturacompra::find($id);
        $subtotal  = $factura->subtotal;
        $total     = $factura->total;
        $detalles  = Detallefactura::where('n_factura', '=', $factura->n_factura)->get();
        $socio     = Socio::where('id', '=', $factura->socio_id)->get();
        $iva 	   = Iva::where('id', '=', $factura->iva_id)->get();

    	return view('factura_compra/show', compact('factura','detalles','iva','socio','subtotal','total'));
    }

}
