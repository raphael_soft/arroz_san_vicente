<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Res;
use App\Socio;
use App\Http\Requests\ResFormRequests;


class ResController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $res = Res::all();
        return view('asociacion.reses.index', compact('res'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$socio = Socio::all();
        return view('asociacion.reses.create', compact('socio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResFormRequests $request)
    {
        $registro           = new Res;

        $nombre = strtoupper($request['nombre']);

        $registro->nombre   = $nombre;
        $registro->socio_id = $request['socio'];
        $registro->save();

        //LISTADO DE SOCIOS
        return redirect('reses')->with('status', 'Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $res   = Res::find($id);
        $socio = Socio::where('id', '=', $res->socio_id)->firstOrFail();
        return view('asociacion.reses.show', compact('res', 'socio'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $res    		 = Res::find($id);
       $sociotodos	 = Socio::all();
       $socios 		 = Socio::where('id', '=', $res->socio_id)->firstOrFail();
       return view('asociacion.reses.edit', compact('socios','res','sociotodos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ResFormRequests $request)
    {

    	$id  				= $_POST['id'];

        $update             = Res::find($id);

        $nombre = strtoupper($_POST['nombre']);
        
        $update->nombre  	= $nombre;
        $update->socio_id 	= $_POST['socio'];
        $update->status     = $_POST['status'];
        $update->save();

		return redirect("edit_re/$id")->with('status', 'Actualizaci&oacute;n Exitosa');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $user = Res::whereId($id)->firstOrFail();
	   $user->delete();
       return redirect("reses")->with('status', 'Borrado exitoso!');

    }
}
