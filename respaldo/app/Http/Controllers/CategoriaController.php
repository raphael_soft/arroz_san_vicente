<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests\CategoriaFormRequest;
use Illuminate\Http\Request;

class CategoriaController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $categorias = Categoria::all();
        return view('configuracion.categoria.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('configuracion.categoria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaFormRequest $request) {
        //
        // dd($request;
        // dd($request->all());

        $nombre = strtoupper($request->get('nombre'));

        $categoria = new Categoria(array(
            'nombre' => $nombre,
        ));

        $categoria->save();

        return redirect('/configuracion/categorias/nueva-categoria')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $categoria = Categoria::whereId($id)->firstOrFail();
        return view('configuracion.categoria.show', compact('categoria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $categoria = Categoria::whereId($id)->firstOrFail();
        return view('configuracion.categoria.edit', compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriaFormRequest $request, $id) {
        //

        $categoria    = Categoria::whereId($id)->firstOrFail();
        $nombre = strtoupper($request->get('nombre'));

        $categoria->nombre = $nombre;
        $categoria->save();

        return redirect('/configuracion/categorias/')->with('status', 'Actualizacion Exitosa.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $categoria = Categoria::whereId($id)->firstOrFail();
        $categoria->delete();
        return redirect('/configuracion/categorias')->with('status', 'Borrado Exitoso ');
    }
}
