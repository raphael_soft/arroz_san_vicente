<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Iva;
use App\Mail\OrdenVenta;
use App\Socio;
use App\TipoMedicion;
use App\TipoProduccion;
use App\Venta;
use App\Venta_Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos = Venta::all();
        return view('venta.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $socios            = Socio::all();
        $ivas              = Iva::all();
        $categorias        = Categoria::all();
        $tipo_mediciones   = TipoMedicion::all();
        $tipo_producciones = TipoProduccion::all();
        return view('venta.create', compact('socios', 'ivas', 'categorias', 'tipo_mediciones', 'tipo_producciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());

        function getToken()
        {
            $an = "0123456789";
            $su = strlen($an) - 1;
            return substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1);
        }

        $socio_id       = $request->get('socio');
        $codigo_factura = getToken();
        $orden_factura  = uniqid();
        $fecha          = date("Y-m-d");
        $iva_id         = $request->get('iva');
        $datos_iva      = Iva::whereId($iva_id)->firstOrFail();

        $venta = new Venta(array(
            'codigo_factura' => $codigo_factura,
        ));

        $venta->save();

        $tem_sub_total = 0;
        $tem_total_iva = 0;
        $tem_total     = 0;

        for ($x = 0; $x <= count($request->all); $x++) {
            if ($request->get('cantidad' . $x) > 1) {

                $produccion = $request->get('produccion' . $x);
                $medida     = $request->get('medida' . $x);
                $categoria  = $request->get('categoria' . $x);
                $precio     = $request->get('precio' . $x);
                $cantidad   = $request->get('cantidad' . $x);

                $temporal      = $precio * $cantidad;
                $tem_sub_total = $tem_sub_total + $temporal;

                $venta_item = new Venta_Item(array(
                    'codigo_factura'     => $codigo_factura,
                    'categoria_id'       => $categoria,
                    'tipo_medicion_id'   => $medida,
                    'tipo_produccion_id' => $produccion,
                    'cantidad'           => $cantidad,
                    'precio'             => $precio,
                    'fecha'              => $fecha,
                ));

                $venta_item->save();
            } else {
                return redirect('/facturas-ventas/nueva-factura-venta')->with('status-danger', 'Hubo un problema, asegurese de ingresar una cantidad y precio.');
            }
        }

        $tem_total_iva = ($tem_sub_total * $datos_iva->valor) / 100;

        $sub_total = $tem_sub_total;
        $total_iva = $tem_total_iva;
        $total     = $sub_total + $total_iva;

        $dato = Venta::whereCodigoFactura($codigo_factura)->firstOrFail();

        $dato->socio_id      = $socio_id;
        $dato->orden_factura = $orden_factura;
        $dato->fecha         = $fecha;
        $dato->iva_id        = $iva_id;
        $dato->sub_total     = $sub_total;
        $dato->total_iva     = $total_iva;
        $dato->total         = $total;

        $dato->save();

        return redirect('/facturas-ventas/nueva-factura-venta')->with('status', 'Registro Exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo_factura)
    {
        //

        $datos = Venta::whereCodigoFactura($codigo_factura)->firstOrFail();
        $items = Venta_Item::whereCodigoFactura($codigo_factura)->get();
        return view('venta.show', compact('datos', 'items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $id    = $request->get('codigo');
        $venta = Venta::whereId($id)->firstOrFail();
        if ($venta) {
            $venta->status = "2";

            $venta->save();

            Mail::to($venta->socio->email)->send(new OrdenVenta($venta->codigo_factura));

            return response()->json("1");
        } else {
            return response()->json("0");
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo)
    {
        //
        $venta = Venta::whereId($codigo)->firstOrFail();
        if ($venta) {
            $venta->status = "0";

            $venta->save();
            
            Mail::to($venta->socio->email)->send(new OrdenVenta($venta->codigo_factura));

            return response()->json("1");
        } else {
            return response()->json("0");
        }
    }

    // ESTADISTICA DE VENTA
    public function estadistica()
    {

        //A#O ACTUAL DE LA PRIMERA VISTA
        $anio_actual = date("Y");

        //VARIABLES

        //CONSULTA ENERO
        $enero_inicial = $anio_actual . "-" . "01" . "-" . "01";
        $enero_final   = $anio_actual . "-" . "01" . "-" . "31";
        $ventas_enero  = Venta::where('fecha', '>=', $enero_inicial)->where('fecha', '<=', $enero_final)->where('status', '=', '2')->count();

        //CONSULTA FEBRERO
        $febrero_inicial = $anio_actual . "-" . "02" . "-" . "01";
        $febrero_final   = $anio_actual . "-" . "02" . "-" . "28";
        $ventas_febrero  = Venta::where('fecha', '>=', $febrero_inicial)->where('fecha', '<=', $febrero_final)->where('status', '=', '2')->count();

        //CONSULTA MARZO
        $marzo_inicial = $anio_actual . "-" . "03" . "-" . "01";
        $marzo_final   = $anio_actual . "-" . "03" . "-" . "31";
        $ventas_marzo  = Venta::where('fecha', '>=', $marzo_inicial)->where('fecha', '<=', $marzo_final)->where('status', '=', '2')->count();

        //CONSULTA ABRIL
        $abril_inicial = $anio_actual . "-" . "04" . "-" . "01";
        $abril_final   = $anio_actual . "-" . "04" . "-" . "30";
        $ventas_abril  = Venta::where('fecha', '>=', $abril_inicial)->where('fecha', '<=', $abril_final)->where('status', '=', '2')->count();

        //CONSULTA MAYO
        $mayo_inicial = $anio_actual . "-" . "05" . "-" . "01";
        $mayo_final   = $anio_actual . "-" . "05" . "-" . "31";
        $ventas_mayo  = Venta::where('fecha', '>=', $mayo_inicial)->where('fecha', '<=', $mayo_final)->where('status', '=', '2')->count();

        //CONSULTA JUNIO
        $junio_inicial = $anio_actual . "-" . "06" . "-" . "01";
        $junio_final   = $anio_actual . "-" . "06" . "-" . "30";
        $ventas_junio  = Venta::where('fecha', '>=', $junio_inicial)->where('fecha', '<=', $junio_final)->where('status', '=', '2')->count();

        //CONSULTA JULIO
        $julio_inicial = $anio_actual . "-" . "07" . "-" . "01";
        $julio_final   = $anio_actual . "-" . "07" . "-" . "31";
        $ventas_julio  = Venta::where('fecha', '>=', $julio_inicial)->where('fecha', '<=', $julio_final)->where('status', '=', '2')->count();

        //CONSULTA AGOSTO
        $agosto_inicial = $anio_actual . "-" . "08" . "-" . "01";
        $agosto_final   = $anio_actual . "-" . "08" . "-" . "31";
        $ventas_agosto  = Venta::where('fecha', '>=', $agosto_inicial)->where('fecha', '<=', $agosto_final)->where('status', '=', '2')->count();

        //CONSULTA SEPTIEMBRE
        $septiembre_inicial = $anio_actual . "-" . "09" . "-" . "01";
        $septiembre_final   = $anio_actual . "-" . "09" . "-" . "30";
        $ventas_septiembre  = Venta::where('fecha', '>=', $septiembre_inicial)->where('fecha', '<=', $septiembre_final)->where('status', '=', '2')->count();

        //CONSULTA OCTUBRE
        $octubre_inicial = $anio_actual . "-" . "10" . "-" . "01";
        $octubre_final   = $anio_actual . "-" . "10" . "-" . "30";
        $ventas_octubre  = Venta::where('fecha', '>=', $octubre_inicial)->where('fecha', '<=', $octubre_final)->where('status', '=', '2')->count();

        //CONSULTA NOVIEMBRE
        $noviembre_inicial = $anio_actual . "-" . "11" . "-" . "01";
        $noviembre_final   = $anio_actual . "-" . "11" . "-" . "30";
        $ventas_noviembre  = Venta::where('fecha', '>=', $noviembre_inicial)->where('fecha', '<=', $noviembre_final)->where('status', '=', '2')->count();

        //CONSULTA DICIEMBRE
        $diciembre_inicial = $anio_actual . "-" . "12" . "-" . "01";
        $diciembre_final   = $anio_actual . "-" . "12" . "-" . "30";
        $ventas_diciembre  = Venta::where('fecha', '>=', $diciembre_inicial)->where('fecha', '<=', $diciembre_final)->where('status', '=', '2')->count();

        //CONSULTA POS A#OS

        //CONSULTA 2017
        $anio_inicio_2017 = "2017" . "-" . "01" . "-" . "01";
        $anio_final_2017  = "2017" . "-" . "12" . "-" . "31";
        $total_2017       = Venta::where('fecha', '>=', $anio_inicio_2017)->where('fecha', '<=', $anio_final_2017)->where('status', '=', '2')->count();

        //CONSULTA 2018
        $anio_inicio_2018 = "2018" . "-" . "01" . "-" . "01";
        $anio_final_2018  = "2018" . "-" . "12" . "-" . "31";
        $total_2018       = Venta::where('fecha', '>=', $anio_inicio_2018)->where('fecha', '<=', $anio_final_2018)->where('status', '=', '2')->count();

        //CONSULTA 2019
        $anio_inicio_2019 = "2019" . "-" . "01" . "-" . "01";
        $anio_final_2019  = "2019" . "-" . "12" . "-" . "31";
        $total_2019       = Venta::where('fecha', '>=', $anio_inicio_2019)->where('fecha', '<=', $anio_final_2019)->where('status', '=', '2')->count();

        //CONSULTA 2020
        $anio_inicio_2020 = "2020" . "-" . "01" . "-" . "01";
        $anio_final_2020  = "2020" . "-" . "12" . "-" . "31";
        $total_2020       = Venta::where('fecha', '>=', $anio_inicio_2020)->where('fecha', '<=', $anio_final_2020)->where('status', '=', '2')->count();

        //CONSULTA 2021
        $anio_inicio_2021 = "2020" . "-" . "01" . "-" . "01";
        $anio_final_2021  = "2020" . "-" . "12" . "-" . "31";
        $total_2021       = Venta::where('fecha', '>=', $anio_inicio_2021)->where('fecha', '<=', $anio_final_2021)->where('status', '=', '2')->count();

        return view('venta.estadistica', compact('anio_actual', 'ventas_enero', 'ventas_febrero', 'ventas_marzo', 'ventas_abril', 'ventas_mayo', 'ventas_junio', 'ventas_julio', 'ventas_agosto', 'ventas_septiembre', 'ventas_octubre', 'ventas_noviembre', 'ventas_diciembre', 'total_2017', 'total_2018', 'total_2019', 'total_2020', 'total_2021'));
    }

    public function consultaanio()
    {

        //A#O ACTUAL DE LA PRIMERA VISTA
        $anio_actual = $_POST['anio'];

        //VARIABLES

        //CONSULTA ENERO
        $enero_inicial = $anio_actual . "-" . "01" . "-" . "01";
        $enero_final   = $anio_actual . "-" . "01" . "-" . "31";
        $ventas_enero  = Venta::where('fecha', '>=', $enero_inicial)->where('fecha', '<=', $enero_final)->where('status', '=', '2')->count();

        //CONSULTA FEBRERO
        $febrero_inicial = $anio_actual . "-" . "02" . "-" . "01";
        $febrero_final   = $anio_actual . "-" . "02" . "-" . "28";
        $ventas_febrero  = Venta::where('fecha', '>=', $febrero_inicial)->where('fecha', '<=', $febrero_final)->where('status', '=', '2')->count();

        //CONSULTA MARZO
        $marzo_inicial = $anio_actual . "-" . "03" . "-" . "01";
        $marzo_final   = $anio_actual . "-" . "03" . "-" . "31";
        $ventas_marzo  = Venta::where('fecha', '>=', $marzo_inicial)->where('fecha', '<=', $marzo_final)->where('status', '=', '2')->count();

        //CONSULTA ABRIL
        $abril_inicial = $anio_actual . "-" . "04" . "-" . "01";
        $abril_final   = $anio_actual . "-" . "04" . "-" . "30";
        $ventas_abril  = Venta::where('fecha', '>=', $abril_inicial)->where('fecha', '<=', $abril_final)->where('status', '=', '2')->count();

        //CONSULTA MAYO
        $mayo_inicial = $anio_actual . "-" . "05" . "-" . "01";
        $mayo_final   = $anio_actual . "-" . "05" . "-" . "31";
        $ventas_mayo  = Venta::where('fecha', '>=', $mayo_inicial)->where('fecha', '<=', $mayo_final)->where('status', '=', '2')->count();

        //CONSULTA JUNIO
        $junio_inicial = $anio_actual . "-" . "06" . "-" . "01";
        $junio_final   = $anio_actual . "-" . "06" . "-" . "30";
        $ventas_junio  = Venta::where('fecha', '>=', $junio_inicial)->where('fecha', '<=', $junio_final)->where('status', '=', '2')->count();

        //CONSULTA JULIO
        $julio_inicial = $anio_actual . "-" . "07" . "-" . "01";
        $julio_final   = $anio_actual . "-" . "07" . "-" . "31";
        $ventas_julio  = Venta::where('fecha', '>=', $julio_inicial)->where('fecha', '<=', $julio_final)->where('status', '=', '2')->count();

        //CONSULTA AGOSTO
        $agosto_inicial = $anio_actual . "-" . "08" . "-" . "01";
        $agosto_final   = $anio_actual . "-" . "08" . "-" . "31";
        $ventas_agosto  = Venta::where('fecha', '>=', $agosto_inicial)->where('fecha', '<=', $agosto_final)->where('status', '=', '2')->count();

        //CONSULTA SEPTIEMBRE
        $septiembre_inicial = $anio_actual . "-" . "09" . "-" . "01";
        $septiembre_final   = $anio_actual . "-" . "09" . "-" . "30";
        $ventas_septiembre  = Venta::where('fecha', '>=', $septiembre_inicial)->where('fecha', '<=', $septiembre_final)->where('status', '=', '2')->count();

        //CONSULTA OCTUBRE
        $octubre_inicial = $anio_actual . "-" . "10" . "-" . "01";
        $octubre_final   = $anio_actual . "-" . "10" . "-" . "30";
        $ventas_octubre  = Venta::where('fecha', '>=', $octubre_inicial)->where('fecha', '<=', $octubre_final)->where('status', '=', '2')->count();

        //CONSULTA NOVIEMBRE
        $noviembre_inicial = $anio_actual . "-" . "11" . "-" . "01";
        $noviembre_final   = $anio_actual . "-" . "11" . "-" . "30";
        $ventas_noviembre  = Venta::where('fecha', '>=', $noviembre_inicial)->where('fecha', '<=', $noviembre_final)->where('status', '=', '2')->count();

        //CONSULTA DICIEMBRE
        $diciembre_inicial = $anio_actual . "-" . "12" . "-" . "01";
        $diciembre_final   = $anio_actual . "-" . "12" . "-" . "30";
        $ventas_diciembre  = Venta::where('fecha', '>=', $diciembre_inicial)->where('fecha', '<=', $diciembre_final)->where('status', '=', '2')->count();

        //CONSULTA POS A#OS

        //CONSULTA 2017
        $anio_inicio_2017 = "2017" . "-" . "01" . "-" . "01";
        $anio_final_2017  = "2017" . "-" . "12" . "-" . "31";
        $total_2017       = Venta::where('fecha', '>=', $anio_inicio_2017)->where('fecha', '<=', $anio_final_2017)->where('status', '=', '2')->count();

        //CONSULTA 2018
        $anio_inicio_2018 = "2018" . "-" . "01" . "-" . "01";
        $anio_final_2018  = "2018" . "-" . "12" . "-" . "31";
        $total_2018       = Venta::where('fecha', '>=', $anio_inicio_2018)->where('fecha', '<=', $anio_final_2018)->where('status', '=', '2')->count();

        //CONSULTA 2019
        $anio_inicio_2019 = "2019" . "-" . "01" . "-" . "01";
        $anio_final_2019  = "2019" . "-" . "12" . "-" . "31";
        $total_2019       = Venta::where('fecha', '>=', $anio_inicio_2019)->where('fecha', '<=', $anio_final_2019)->where('status', '=', '2')->count();

        //CONSULTA 2020
        $anio_inicio_2020 = "2020" . "-" . "01" . "-" . "01";
        $anio_final_2020  = "2020" . "-" . "12" . "-" . "31";
        $total_2020       = Venta::where('fecha', '>=', $anio_inicio_2020)->where('fecha', '<=', $anio_final_2020)->where('status', '=', '2')->count();

        //CONSULTA 2021
        $anio_inicio_2021 = "2020" . "-" . "01" . "-" . "01";
        $anio_final_2021  = "2020" . "-" . "12" . "-" . "31";
        $total_2021       = Venta::where('fecha', '>=', $anio_inicio_2021)->where('fecha', '<=', $anio_final_2021)->where('status', '=', '2')->count();

        return view('venta.estadistica', compact('anio_actual', 'ventas_enero', 'ventas_febrero', 'ventas_marzo', 'ventas_abril', 'ventas_mayo', 'ventas_junio', 'ventas_julio', 'ventas_agosto', 'ventas_septiembre', 'ventas_octubre', 'ventas_noviembre', 'ventas_diciembre', 'total_2017', 'total_2018', 'total_2019', 'total_2020', 'total_2021'));
    }
}
