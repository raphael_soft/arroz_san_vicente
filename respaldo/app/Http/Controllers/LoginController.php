<?php

namespace App\Http\Controllers;

use App\Http\Requests\InicioFormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller {
    //

    // inicio de sesion
    public function inicio() {
        return view('auth.login');
    }

    public function authenticate(Request $request) {
        // dd($request->all());

        $email = $request->get('email');
        $password = $request->get('password');
        $remember = ($request->get('remember')) ? true : false ;

        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            return redirect()->intended('/');
        }else{
        	return redirect()->intended('/inicio')->with('error_datos', 'Error con los datos.');
        	
        }
    }

    public function logout() {
        Auth::logout();
        return redirect()->intended('/inicio')->with('status', 'Gracias por visitarnos.');
    }


}
