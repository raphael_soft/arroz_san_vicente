<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Res extends Model
{

    protected $table = "reses";

    protected $fillable = ['nombre', 'id', 'socio_id', 'status'];

    
    public function socio() {
        return $this->belongsTo('App\Socio');
    }

    public function producciones() {
        return $this->hasMany('App\Produccion');
    }

}
