<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produccion extends Model
{
    //

    protected $table = "producciones";

    protected $fillable = ['id', 'socio_id', 'res_id', 'categoria_id', 'tipo_medicion_id', 'tipo_produccion_id', 'cantidad', 'fecha', 'status'];

    
    public function socio() {
        return $this->belongsTo('App\Socio');
    }

    public function res() {
        return $this->belongsTo('App\Res');
    }

    public function categoria() {
        return $this->belongsTo('App\Categoria');
    }

    public function tipo_medicion() {
        return $this->belongsTo('App\TipoMedicion');
    }

    public function tipo_produccion() {
        return $this->belongsTo('App\TipoProduccion');
    }

}
