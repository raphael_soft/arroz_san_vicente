<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detallefactura extends Model
{
    protected $table = "detallefacturas";

    protected $fillable = ['id', 'n_factura', 'id_producto', 'cantidad'];

}
