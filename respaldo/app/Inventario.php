<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{

	protected $table = "inventarios";

    protected $fillable = ['id','n_productos','producto_id','status'];

     public function producto() {
        return $this->hasOne('App\Producto');
    }
}
