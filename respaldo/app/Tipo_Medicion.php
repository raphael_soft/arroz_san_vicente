<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoMedicion extends Model {
    //
    protected $table = "tipo_mediciones";

    protected $fillable = ['id', 'nombre', 'status'];

    public function producciones() {
        return $this->hasMany('App\Produccion');
    }
}
