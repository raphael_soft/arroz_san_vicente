<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturacompra extends Model
{
    protected $table = "facturacompras";

    protected $fillable = ['id', 'socio_id', 'iva_id', 'n_orden', 'n_factura', 'fecha_facturacion','subtotal','monto_iva','total'];

    public function iva() {
        return $this->hasOne('App\iva');
    }

     public function socio() {
        return $this->hasOne('App\socio');
    }
}
