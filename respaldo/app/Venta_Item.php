<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta_Item extends Model
{
    //

    protected $table = "ventas_items";

    protected $fillable = ['id', 'codigo_factura', 'categoria_id', 'tipo_medicion_id', 'tipo_produccion_id', 'precio', 'cantidad', 'fecha', 'satus'];

    public function categoria() {
        return $this->belongsTo('App\Categoria');
    }

    public function tipo_medicion() {
        return $this->belongsTo('App\TipoMedicion');
    }

    public function tipo_produccion() {
        return $this->belongsTo('App\TipoProduccion');
    }

    public function ventas() {
        return $this->hasMany('App\Venta');
    }
}
