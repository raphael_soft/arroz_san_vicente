<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProduccion extends Model {
    //
    protected $table = "tipo_producciones";

    protected $fillable = ['id', 'nombre', 'status'];

    public function producciones() {
        return $this->hasMany('App\Produccion');
    }
}
