<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iva extends Model {
    //
    protected $table = "ivas";

    protected $fillable = ['id', 'nombre', 'valor' ,'status'];
}
