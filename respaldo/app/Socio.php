<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Socio extends Model
{
    protected $table = "socios";

    protected $fillable = ['id','nombre','email','telefono','codigo_socio'];

    public function res() {
        return $this->hasOne('App\Res');
    }

    public function producto() {
        return $this->hasMany('App\Producto');
    }

    public function producciones() {
        return $this->hasMany('App\Produccion');
    }
}
