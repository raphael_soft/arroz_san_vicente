<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    protected $table = "categorias";

    protected $fillable = ['id', 'nombre', 'status'];

    public function producciones() {
        return $this->hasMany('App\Produccion');
    }
}
