<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        DB::table('users')->insert([
            'name'     => "ADMIN ADMIN",
            'email'    => 'admin@admin.com',
            'phone'    => '1234567890',
            'password' => bcrypt('123456'),
        ]);
    }
}
