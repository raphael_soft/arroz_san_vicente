<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturacomprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturacompras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('socio_id')->unsigned();
            $table->integer('iva_id')->unsigned();
            $table->string('n_orden',50);
            $table->string('n_factura',50);
            $table->double('subtotal')->nullable();
            $table->double('monto_iva')->nullable();
            $table->double('total')->nullable();
            $table->char('status', 1)->default('1');
            $table->date('fecha_facturacion');
            $table->timestamps();

            $table->foreign('socio_id')->references('id')->on('socios')->onDelete('cascade');
            $table->foreign('iva_id')->references('id')->on('ivas');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturacompra');
    }
}
