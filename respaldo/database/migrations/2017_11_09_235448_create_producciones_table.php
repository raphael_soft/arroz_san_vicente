<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('socio_id')->unsigned()->nullable();
            $table->integer('res_id')->unsigned()->nullable();
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->integer('tipo_medicion_id')->unsigned()->nullable();
            $table->integer('tipo_produccion_id')->unsigned()->nullable();
            $table->double('cantidad');
            $table->date('fecha');
            $table->char('status', 1)->default('1');
            $table->timestamps();

            $table->foreign('socio_id')->references('id')->on('socios')->onDelete('set null');
            $table->foreign('res_id')->references('id')->on('reses')->onDelete('set null');
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('set null');
            $table->foreign('tipo_medicion_id')->references('id')->on('tipo_mediciones')->onDelete('set null');
            $table->foreign('tipo_produccion_id')->references('id')->on('tipo_producciones')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producciones');
    }
}
