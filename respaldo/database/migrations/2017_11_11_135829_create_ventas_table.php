<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('socio_id')->unsigned()->nullable();
            $table->integer('codigo_factura')->unsigned()->index();
            $table->string('orden_factura')->nullable();
            $table->date('fecha')->nullable();
            $table->integer('iva_id')->unsigned()->nullable();
            $table->double('sub_total')->nullable();
            $table->double('total_iva')->nullable();
            $table->double('total')->nullable();
            $table->char('status', 1)->default('1');

            $table->foreign('socio_id')->references('id')->on('socios')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('iva_id')->references('id')->on('ivas')->onDelete('set null')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
