<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codigo_factura')->unsigned()->nullable();
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->integer('tipo_medicion_id')->unsigned()->nullable();
            $table->integer('tipo_produccion_id')->unsigned()->nullable();
            $table->double('cantidad');
            $table->double('precio');
            $table->date('fecha');
            $table->char('status', 1)->default('1');

            $table->foreign('codigo_factura')->references('codigo_factura')->on('ventas')->onDelete('cascade');
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('tipo_medicion_id')->references('id')->on('tipo_mediciones')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('tipo_produccion_id')->references('id')->on('tipo_producciones')->onDelete('set null')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas_items');
    }
}
