@extends('master')

@section('title', 'Categoria - '. $categoria->nombre )

@section('active', 'active')

@section('active-categoria', 'active')

@section('content')



	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Categorias

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="/">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="">

	                <a href="{{ url('/configuracion/categorias') }}">

	                    Categorias

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    {{ $categoria->nombre }}

	                </a>

	            </li>

	        </ol>

	    </section>



	    @foreach($errors->all() as $error)

			<p class="alert alert-danger">{{$error}}</p>

		@endforeach



		@if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

					<div class="box box-info">

			            <div class="box-header with-border">

			              	<h3 class="box-title">{{ $categoria->nombre }}</h3>

			            </div>

			              	<div class="box-body">

				                <div class="form-group">

				                  	<label for="nombre" class="col-sm-2 control-label">Nombre: </label>

					                <div class="col-sm-10">

					                   	<h5>{{ $categoria->nombre }}</h5>

					                </div>

				                </div>

			              	</div>

			              <div class="box-footer">

			                <a href="{{ url('/configuracion/categorias')) }}"><button type="button" class="btn btn-warning">Regresar</button></a>

			                <a href="{{ action('CategoriaController@edit', $categoria->id) }}"><button type="submit" class="btn btn-info pull-right">Actualizar</button></a>

			                

			            	<form class="form-horizontal" action="{{ action('CategoriaController@destroy', $categoria->id) }}" method="POST">

			            		<input type="hidden" name="_token" value="{!! csrf_token() !!}">

			                	<button type="submit" class="btn btn-danger pull-right" style="margin-top: 10px">Borrar</button>

			            	</form>

			              </div>

			          </div>

				</div>

			</div>

	    </section>

	</div>



@endsection