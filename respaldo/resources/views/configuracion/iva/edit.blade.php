@extends('master')

@section('title', 'Iva - '. $iva->nombre )

@section('active', 'active')

@section('active-iva', 'active')

@section('content')



	<div class="content-wrapper">



	    <section class="content-header">

	        <h1>

	            Ivas

	        </h1>

	        <ol class="breadcrumb">

	            <li class="">

	                <a href="{{ url('/') }}">

	                    <i class="fa fa-dashboard">

	                    </i>

	                    Inicio

	                </a>

	            </li>

	            <li class="">

	                <a href="{{ url('/configuracion/iva') }}">

	                    Ivas

	                </a>

	            </li>

	            <li class="active">

	                <a href="#">

	                    Editar Iva: {{ $iva->nombre }}

	                </a>

	            </li>

	        </ol>

	    </section>



	    @foreach($errors->all() as $error)

			<p class="alert alert-danger">{{$error}}</p>

		@endforeach



		@if (session('status'))

			<div class="alert alert-success">

				{{session('status')}}

			</div>

		@endif



	    <section class="content">

	    	<div class="row">

	    		<div class="col-xs-12">

					<div class="box box-info">

			            <div class="box-header with-border">

			              	<h3 class="box-title">Editar Iva</h3>

			            </div>

			            <form class="form-horizontal" method="POST">

			            	<input type="hidden" name="_token" value="{!! csrf_token() !!}">

			              	<div class="box-body">

				                <div class="form-group">

				                  	<label for="nombre" class="col-sm-2 control-label">Nombre</label>

					                <div class="col-sm-10">

					                   	<input type="text"  class="form-control" value="{{ $iva->nombre }}" id="nombre" name="nombre" placeholder="Nombre" required />

					                </div>

				                </div>



				                <div class="form-group">

				                  	<label for="valor" class="col-sm-2 control-label">Valor %</label>

				                  	<div class="col-sm-10">

				                    	<input type="text" class="form-control numeros" value="{{ $iva->valor }}" id="valor" name="valor" placeholder="Valor %" required />

				                  	</div>

				                </div>

			              	</div>

			              <div class="box-footer">

			                <a href="{{ url('/configuracion/iva') }}"><button type="button" class="btn btn-warning">Regresar</button></a>

			                <button type="submit" class="btn btn-info pull-right">Actualizar</button>

			              </div>

			            </form>

			          </div>

				</div>

			</div>

	    </section>

	</div>



@endsection