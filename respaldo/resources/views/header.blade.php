<header class="main-header">

    <a class="logo" href="{{ url('/') }}">

        <span class="logo-mini">

            <b>

                A

            </b>

            G

        </span>

        <span class="logo-lg">

            <b>

                Asoc. Gana.

            </b>

        </span>

    </a>

    <nav class="navbar navbar-static-top">

        <!-- Sidebar toggle button-->

        <a class="sidebar-toggle" data-toggle="offcanvas" href="#" role="button">

            <span class="sr-only">

                Toggle navigation

            </span>

        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">



                <li class="dropdown user user-menu">

                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">

                        <img alt="User Image" class="user-image" src="{{ url('/img/default.png') }}"/>

                            <span class="hidden-xs">

                                Alexander Pierce

                            </span>

                        </img>

                    </a>

                    <ul class="dropdown-menu">

                        <!-- User image -->

                        <li class="user-header">

                            <img alt="User Image" class="img-circle" src="{{ url('/img/default.png') }}"/>

                                <p>

                                    nombre - rol

                                    <small>

                                        email

                                    </small>

                                </p>

                            </img>

                        </li>

                    

                        <li class="user-footer">

                            <div class="pull-left">

                                <a class="btn btn-default btn-flat" href="#">

                                    Profile

                                </a>

                            </div>

                            <div class="pull-right">

                                <a class="btn btn-default btn-flat" href="{{ url('/salir') }}">

                                    Cerrar Sesión

                                </a>

                            </div>

                        </li>

                    </ul>

                </li>

            </ul>

        </div>

    </nav>

</header>



<aside class="main-sidebar">

    <section class="sidebar">

        <div class="user-panel">

            <div class="pull-left image">

                <img alt="User Image" class="img-circle" src="{{ url('/img/default.png') }}"/>

            </div>

            <div class="pull-left info">

                <p>

                    {{-- {{user()->name }} --}}

                </p>

                <a href="#">

                    <i class="fa fa-circle text-success">

                    </i>

                    Online

                </a>

            </div>

        </div>

        <form action="#" class="sidebar-form" method="get">

            <div class="input-group">

                <input class="form-control" name="q" placeholder="Buscar..." type="text"/>

                    <span class="input-group-btn">

                        <button class="btn btn-flat" id="search-btn" name="search" type="submit">

                            <i class="fa fa-search"></i>

                        </button>

                    </span>

                </input>

            </div>

        </form>



        <ul class="sidebar-menu">

            <li class="header">

                MENU

            </li>

            <li class="active">

                <a href="{{ url('/') }}">

                    <i class="fa fa-dashboard">

                    </i>

                    <span>

                        Inicio

                    </span>

                </a>

            </li>



            {{-- VENTAS --}}

            <li class="treeview @yield('active-venta')">

                <a href="#">

                    <i class="fa fa-usd">

                    </i>

                    <span>

                        Ventas

                    </span>

                    <span class="pull-right-container">

                        <i class="fa fa-angle-left pull-right">

                        </i>

                    </span>

                </a>

                <ul class="treeview-menu">

                    <li class="@yield('active-venta-factura')">

                        <a href="{{ url('/facturas-ventas') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Facturas de Ventas

                        </a>

                    </li>

                    <li  class="@yield('active-estadistica-venta')">

                        <a href="{{ url('/estadistica-de-ventas') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Estadisticas de Ventas

                        </a>

                    </li>

                </ul>

            </li>



            {{-- COMPRAS --}}

            <li class="treeview @yield('active-compra')">

                <a href="#">

                    <i class="fa fa-shopping-bag">

                    </i>

                    <span>

                        Compras

                    </span>

                    <span class="pull-right-container">

                        <i class="fa fa-angle-left pull-right">

                        </i>

                    </span>

                </a>

                <ul class="treeview-menu">

                    <li class="@yield('active-compra-factura')">

                        <a href="{{ url('/facturascompras') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Facturas de Compras

                        </a>

                    </li>

                    <li class="@yield('active-compra-estadisticas')">

                        <a href="{{ url('/estadistica_compra') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Estadistica de Compras

                        </a>

                    </li>

                </ul>

            </li>



            {{-- INVENTARIO --}}

            <li class="treeview @yield('active-inventario')">

                <a href="#">

                    <i class="fa fa-archive">

                    </i>

                    <span>

                        Inventario

                    </span>

                    <span class="pull-right-container">

                        <i class="fa fa-angle-left pull-right">

                        </i>

                    </span>

                </a>

                <ul class="treeview-menu">

                    <li class="@yield('active-inventario-inventario')">

                        <a href="{{ url('/inventario') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Inventario

                        </a>

                    </li>

                    <li class="@yield('active-inventario-productos')">

                        <a href="{{ url('/productos') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Productos

                        </a>

                    </li>

                    {{-- <li class="@yield('active-inventario-inventario-produccion')">

                        <a href="#">

                            <i class="fa fa-circle-o">

                            </i>

                            Inventario de Produccion

                        </a>

                    </li> --}}

                </ul>

            </li>



            {{-- PRODUCCION --}}

            <li class="treeview @yield('active-produccion')">

                <a href="{{ url('/produccion') }}">

                    <i class="fa fa-podcast">

                    </i>

                    <span>

                        Producción

                    </span>

                </a>

            </li>



            {{-- ASOCIACION --}}

            <li class="treeview @yield('active-asociacion')">

                <a href="#">

                    <i class="fa fa-user-plus">

                    </i>

                    <span>

                        Asociación

                    </span>

                    <span class="pull-right-container">

                        <i class="fa fa-angle-left pull-right">

                        </i>

                    </span>

                </a>

                <ul class="treeview-menu">

                    <li class="@yield('active-asociacion-reses')">

                        <a href="{{ url('/reses') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Reses

                        </a>

                    </li>

                    <li class="@yield('active-asociacion-socios')">

                        <a href="{{ url('/socios') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Socios

                        </a>

                    </li>

                </ul>

            </li>



            {{-- ESTADISTICAS --}}

            <li class="treeview @yield('estadistica')">

                <a href="#">

                    <i class="fa fa-line-chart">

                    </i>

                    <span>

                        Estadisticas

                    </span>

                    <span class="pull-right-container">

                        <i class="fa fa-angle-left pull-right">

                        </i>

                    </span>

                </a>

                <ul class="treeview-menu">

                    <li class="@yield('estadistica-ingreso')">

                        <a href="{{ url('/estadisticas/ingreso') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Ingresos

                        </a>

                    </li>

                    <li class="@yield('estadistica-egreso')">

                        <a href="{{ url('/estadisticas/egreso') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Egresos

                        </a>

                    </li>

                    <li class="@yield('estadistica-produccion')">

                        <a href="{{ url('/estadisticas/produccion') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Produccion

                        </a>

                    </li>

                </ul>

            </li>



            {{-- CONFIGURACION --}}

            <li class="treeview @yield('active')">

                <a href="#">

                    <i class="fa fa-cog">

                    </i>

                    <span>

                        Configuración

                    </span>

                    <span class="pull-right-container">

                        <i class="fa fa-angle-left pull-right">

                        </i>

                    </span>

                </a>

                <ul class="treeview-menu">

                    <li class="@yield('active-iva')">

                        <a href="{{ url('/configuracion/iva') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Iva

                        </a>

                    </li>

                    <li class="@yield('active-rol')">

                        <a href="{{ url('/configuracion/roles') }}/">

                            <i class="fa fa-circle-o">

                            </i>

                            Roles

                        </a>

                    </li>

                    <li class="@yield('active-usuario')">

                        <a href="{{ url('/configuracion/usuario') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Usuarios

                        </a>

                    </li>
{{-- 
                    <li class="@yield('active-permiso')">

                        <a href="#">

                            <i class="fa fa-circle-o">

                            </i>

                            Permisos

                        </a>

                    </li> --}}

                    <li class="@yield('active-categoria')">

                        <a href="{{ url('/configuracion/categorias') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Categorias

                        </a>

                    </li>

                    <li class="@yield('active-tipo-medicion')">

                        <a href="{{ url('/configuracion/tipo-de-mediciones') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Tipo de Medición

                        </a>

                    </li>

                    <li class="@yield('active-tipo-produccion')">

                        <a href="{{ url('/configuracion/tipo-de-produccion') }}">

                            <i class="fa fa-circle-o">

                            </i>

                            Tipo de Produccion

                        </a>

                    </li>

                </ul>

            </li>

            

        </ul>

    </section>

</aside>

