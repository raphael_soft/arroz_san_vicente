@extends('master')
@section('title', 'Nueva Produccion')
@section('active-produccion', 'active')
@section('content')

	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Produccion
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="">
	                <a href="{{ url('/produccion') }}">
	                    Producciones
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Nueva Produccion
	                </a>
	            </li>
	        </ol>
	    </section>

	    @foreach($errors->all() as $error)
			<p class="alert alert-danger">{{$error}}</p>
		@endforeach

		@if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif

	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
					<div class="box box-info">
			            <div class="box-header with-border">
			              	<h3 class="box-title">Nueva Produccion</h3>
			            </div>
			            <form class="form-horizontal" method="POST">
			            	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			              	<div class="box-body">

			              		<div class="form-group">
				                  	<label for="socio" class="col-sm-2 control-label">Socios</label>
					                <div class="col-sm-10">
					                   	<select name="socio" id="socio" class="form-control" required>
					                   		<option value="">Seleccione una opcion</option>
					                   		@foreach ($socios as $socio)
					                   			<option value="{{ $socio->id }}">{{ $socio->nombre }}</option>
					                   		@endforeach
					                   	</select>
					                </div>
				                </div>

				                <div class="form-group">
				                  	<label for="res" class="col-sm-2 control-label">Res</label>
					                <div class="col-sm-10">
					                   	<select name="res" id="res" class="form-control" required>
					                   	</select>
					                </div>
				                </div>

				                <div class="form-group">
				                  	<label for="tipo_produccion" class="col-sm-2 control-label">Tipo de Produccion</label>
					                <div class="col-sm-10">
					                   	<select name="tipo_produccion" id="tipo_produccion" class="form-control" required>
					                   		<option value="">Seleccione una opcion</option>
					                   		@foreach ($tipo_producciones as $tipo_produccion)
					                   			<option value="{{ $tipo_produccion->id }}">{{ $tipo_produccion->nombre }}</option>
					                   		@endforeach
					                   	</select>
					                </div>
				                </div>

				                <div class="form-group">
				                  	<label for="categoria" class="col-sm-2 control-label">Categoria</label>
					                <div class="col-sm-10">
					                   	<select name="categoria" id="categoria" class="form-control" required>
					                   		<option value="">Seleccione una opcion</option>
					                   		@foreach ($categorias as $categoria)
					                   			<option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
					                   		@endforeach
					                   	</select>
					                </div>
				                </div>

				                <div class="form-group">
				                  	<label for="tipo_medicion" class="col-sm-2 control-label">Tipo de Medicion</label>
					                <div class="col-sm-10">
					                   	<select name="tipo_medicion" id="tipo_medicion" class="form-control" required>
					                   		<option value="">Seleccione una opcion</option>
					                   		@foreach ($tipo_mediciones as $tipo_medicion)
					                   			<option value="{{ $tipo_medicion->id }}">{{ $tipo_medicion->nombre }}</option>
					                   		@endforeach
					                   	</select>
					                </div>
				                </div>
				                
				                <div class="form-group">
				                  	<label for="cantidad" class="col-sm-2 control-label">Cantidad</label>
					                <div class="col-sm-10">
					                   	<input type="tel"  class="form-control precio" id="cantidad" name="cantidad" placeholder="Cantidad" value="{{ old('cantidad') }}" required />
					                </div>
				                </div>

								<div class="form-group">
					                <div class="checkbox icheck">
		                                <label for="baja" class="col-sm-12 control-label">
		                                    <input type="checkbox" name="baja" id="baja" value="1"> <strong>¿ Dar de baja a este res ?</strong>
		                                </label>
		                            </div>
								</div>
				               
			              	</div>
			              <div class="box-footer">
			                <a href="{{ url('/produccion') }}"><button type="button" class="btn btn-warning">Regresar</button></a>
			                <button type="submit" class="btn btn-info pull-right">Registrar</button>
			              </div>
			            </form>
			          </div>
				</div>
			</div>
	    </section>
	</div>


	<script>
		$(document).ready(function() {
			$('select#socio').on('change',function(){
	            var socio = $('#socio').val();
	            var parametros = {'socio' : socio};
	            $.ajax({
	            	headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
	                url: '/produccion/get_reses',
	                data: parametros,
	                type: 'POST',
	                dataType: 'JSON',
	                beforeSend: function(){
	                    $("#cargando").html("<i class='fa fa-refresh fa-spin fa-3x fa-fw'></i>");
	                },
	                success: function (data){
	                    // $("#cargando").html("<i></i>");
	                    console.log(data);
                        var html = '<option>Seleccione una opcion</option>';
                        for(var o =0; o < data.length; o++){
                            html += '<option value="'+data[o]['id']+'">'+data[o]['nombre']+'</option>';
                        }
                        $('#res').html(html);
	                },
	            });

	        });
		});
	</script>

@endsection