@extends('master')
@section('title', 'Socios')
@section('active-asociacion', 'active')
@section('active-asociacion-socios', 'active')
@section('content')

  <div class="content-wrapper">

      <section class="content-header">
          <h1>
            Datos del socio
            <small>Secci&oacute;n para visualizar los datos del socio</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Asociaci&oacute;n</a></li>
            <li><a href="{{url('socios')}}">Socios</a></li>
            <li class="active">Ver Socio</li>
          </ol>
        </section>

      @foreach($errors->all() as $error)
      <p class="alert alert-danger">{{$error}}</p>
    @endforeach

    @if (session('status'))
      <div class="alert alert-success">
        {{session('status')}}
      </div>
    @endif

      <section class="content">
        <div class="row">
          <div class="col-md-3">
            
          </div>
          <div class="col-md-12 box-body">
                <div class="box box-widget widget-user-2">
                   <form  method="POST">
                          <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Nombre</label>
                              <div class="col-sm-10">
                                <input readonly="readonly" value="{{$socios->nombre}}" type="text" name="nombre" class="form-control" placeholder="Nombre del Socio">
                              </div>
                              <label class="col-sm-2 control-label">Correo</label>
                              <div class="col-sm-10">
                                <input readonly="readonly" value="{{$socios->email}}" type="text" name="correo" class="form-control" placeholder="Correo del Socio">
                              </div>
                              <label class="col-sm-2 control-label">Tel&eacute;fono</label>
                              <div class="col-sm-10">
                                <input readonly="readonly" value="{{$socios->telefono}}" type="text" name="telefono" class="form-control" placeholder="Tel&eacute;fono del Socio">
                              </div>
                              <label class="col-sm-2 control-label">C&oacute;digo</label>
                              <div class="col-sm-10">
                                <input readonly="readonly" value="{{$socios->codigo}}" type="text" name="codigo" class="form-control" placeholder="C&oacute;digo del Socio">
                              </div>
                            </div>
                         </div>
                              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                             </div>
                                    <!-- /.box-body -->
                            <div class="box-footer col-sm-12 text-center">
                              <a href="{{url('socios')}}"  class="btn btn-default"><< Volver</a>
                              <a href="{{url('edit_socio',$socios->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                              <a href="{{url('destroy_socio',$socios->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>

                            </div>
                            <!-- /.box-footer -->
                        </form>
                 
                </div>
            </div>
          <div class="col-md-3"></div>
      </div>
      </section>

@endsection
