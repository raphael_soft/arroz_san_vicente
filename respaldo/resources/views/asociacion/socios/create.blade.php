@extends('master')
@section('title', 'Socios')
@section('active-asociacion', 'active')
@section('active-asociacion-socios', 'active')
@section('content')

  <div class="content-wrapper">

      <section class="content-header">
          <h1>
            Registro de Socios
            <small>Secci&oacute;n para el registro de socios</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Asociaci&oacute;n</a></li>
            <li><a href="{{url('socios')}}">Socios</a></li>
            <li class="active">Nuevo Socio</li>
          </ol>
        </section>

      @foreach($errors->all() as $error)
      <p class="alert alert-danger">{{$error}}</p>
    @endforeach

    @if (session('status'))
      <div class="alert alert-success">
        {{session('status')}}
      </div>
    @endif

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
          <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">Formulario para el registro de socios</h3>
                  </div>
                   <form  method="POST">
                          <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Nombre</label>
                              <div class="col-sm-10">
                                <input type="text" name="nombre" class="form-control" placeholder="Nombre del Socio">
                              </div>
                              <label class="col-sm-2 control-label">Correo</label>
                              <div class="col-sm-10">
                                <input type="text" name="correo" class="form-control" placeholder="Correo del Socio">
                              </div>
                              <label class="col-sm-2 control-label">Tel&eacute;fono</label>
                              <div class="col-sm-10">
                                <input minlength="9" maxlength="10" type="text" name="telefono" class="form-control numeros" placeholder="Tel&eacute;fono del Socio">
                              </div>
                            </div>
                        </div>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                          </div>
                          <!-- /.box-body -->
                        <div class="box-footer">
                          <a href="{{url('/socios')}}" type="submit" class="btn btn-default"><< Volver</a>
                          <button type="submit" class="btn btn-info pull-right"><i class="fa fa-upload"></i> Registrar</button>
                        </div>
                        <!-- /.box-footer -->
                        </form>
                </div>
        </div>
      </div>
      </section>

 

@endsection