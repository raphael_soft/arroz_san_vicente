@extends('master')
@section('title', 'Socios')
@section('active-asociacion', 'active')
@section('active-asociacion-socios', 'active')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edici&oacute;n de Socios
            <small>Secci&oacute;n para editar de socios</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Asociaci&oacute;n</a></li>
            <li><a href="{{url('socios')}}">Socios</a></li>
            <li class="active">Editar Socio</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario para editar socio</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                          <!-- Horizontal Form -->
                   @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                            <form  method="POST">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <input type="hidden" name="id" value="{{$socios->id}}">
                            <div class="box-body">
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Nombre</label>
                              <div class="col-sm-10">
                                <input value="{{$socios->nombre}}" type="text" name="nombre" class="form-control" placeholder="Nombre del Socio">
                              </div>
                              <label class="col-sm-2 control-label">Correo</label>
                              <div class="col-sm-10">
                                <input value="{{$socios->email}}" type="text" name="correo" class="form-control" placeholder="Correo del Socio">
                              </div>
                              <label class="col-sm-2 control-label">Tel&eacute;fono</label>
                              <div class="col-sm-10">
                                <input value="{{$socios->telefono}}" type="text" name="telefono" class="form-control" placeholder="Tel&eacute;fono del Socio">
                              </div>
                              <label class="col-sm-2 control-label">C&oacute;digo</label>
                              <div class="col-sm-10">
                                <input readonly="readonly" value="{{$socios->codigo}}" type="text" name="codigo" class="form-control" placeholder="C&oacute;digo del Socio">
                              </div>
                            </div>
                            
                         </div>
                                  <!-- /.box-body -->
                          <div class="box-footer">
                            <a href="{{url('show_socio',$socios->id)}}" type="submit" class="btn btn-default"><< Volver</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Guardar</button>
                          </div>
                  <!-- /.box-footer -->
                      </form>
                </div>
                <!-- /.box -->
              </div>
            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->

        </section>
        <!-- /.content -->
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista negra de guardias'},
                    {extend: 'excel', title: 'Lista negra de guardias'},
                    {extend: 'pdf', title: 'Lista negra de guardias'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>
@endsection
