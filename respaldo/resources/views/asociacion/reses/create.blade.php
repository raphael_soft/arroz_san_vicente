@extends('master')
@section('title', 'Reses')
@section('active-asociacion', 'active')
@section('active-asociacion-reses', 'active')
@section('content')

  <div class="content-wrapper">

      <section class="content-header">
          <h1>
            Registro de Reses
            <small>Secci&oacute;n para el registro de reses</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Asociaci&oacute;n</a></li>
            <li><a href="{{url('socios')}}">Socios</a></li>
            <li class="active">Nuevo Socio</li>
          </ol>
        </section>

      @foreach($errors->all() as $error)
      <p class="alert alert-danger">{{$error}}</p>
    @endforeach

    @if (session('status'))
      <div class="alert alert-success">
        {{session('status')}}
      </div>
    @endif

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
          <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">Formulario para el registro de reses</h3>
                  </div>
                  <form  method="POST">
                          <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Nombre Res</label>
                              <div class="col-sm-10">
                                <input type="text" name="nombre" class="form-control" placeholder="Datos de la res">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Socio</label>
                              <div class="col-sm-10">
                                <select name="socio" class="form-control">
                                    <option value="">Escoja un Socio</option>
                                    @foreach($socio as $s)
                                    <option value="{{$s->id}}">{{$s->nombre}}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                          </div>
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                             </div>
                                  <!-- /.box-body -->
                          <div class="box-footer">
                            <a href="{{url('/reses')}}" type="submit" class="btn btn-default"><< Volver</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-upload"></i> Registrar</button>
                          </div>
                  <!-- /.box-footer -->
                        </form>
                </div>
        </div>
      </div>
      </section>

 

@endsection