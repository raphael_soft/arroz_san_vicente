@extends('master')
@section('title', 'Socios')
@section('active-asociacion', 'active')
@section('active-asociacion-socios', 'active')
@section('content')

  <div class="content-wrapper">

      <section class="content-header">
          <h1>
            Datos de la res
            <small>Secci&oacute;n para visualizar los datos de la res</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Asociaci&oacute;n</a></li>
            <li><a href="{{url('/res')}}">Res</a></li>
            <li class="active">Ver Socio</li>
          </ol>
        </section>

      @foreach($errors->all() as $error)
      <p class="alert alert-danger">{{$error}}</p>
    @endforeach

    @if (session('status'))
      <div class="alert alert-success">
        {{session('status')}}
      </div>
    @endif

      <section class="content">
        <div class="row">
          <div class="col-md-3">
            
          </div>
          <div class="col-md-12 box-body">
                <div class="box box-widget widget-user-2">
                   <form  method="POST">
                          <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Nombre</label>
                              <div class="col-sm-10">
                                <input readonly="readonly" value="{{$res->nombre}}" type="text" name="nombre" class="form-control" placeholder="Nombre de la res">
                              </div>
                              <label class="col-sm-2 control-label">Socio</label>
                              <div class="col-sm-10">
                                <input readonly="readonly" value="{{$socio->nombre}}" type="text" name="socio" class="form-control" placeholder="Nombre socio">
                              </div>
                              <label class="col-sm-2 control-label">Status</label>
                              <div class="col-sm-10">
                                @if($res->status == 1)
                                   <input readonly="readonly" value="Activo" type="text" name="socio" class="form-control">
                                @else
                                   <input readonly="readonly" value="Inactivo" type="text" name="socio" class="form-control">
                                @endif
                              </div>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                     </div>
                          <!-- /.box-body -->
                  <div class="box-footer col-sm-12 text-center">
                    <a href="{{url('reses')}}"  class="btn btn-default"><< Volver</a>
                    <a href="{{url('edit_re',$res->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                    <a href="{{url('destroy_res',$res->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>

                  </div>
                  <!-- /.box-footer -->
            </form>
                 
                </div>
            </div>
          <div class="col-md-3"></div>
      </div>
      </section>

@endsection
