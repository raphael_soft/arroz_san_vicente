@extends('master')
@section('title', 'Nueva Factura de Venta')
@section('active-venta', 'active')
@section('active-venta-factura', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Factura de Venta
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
            <li class="">
                <a href="{{ url('/facturas-ventas') }}">
                    Facturas de Ventas
                </a>
            </li>
            <li class="active">
                <a href="#">
                    Nueva Factura
                </a>
            </li>
        </ol>
    </section>

    @foreach($errors->all() as $error)
	    <p class="alert alert-danger">
	        {{$error}}
	    </p>
    @endforeach

	@if (session('status'))
    	<div class="alert alert-success">
        	{{session('status')}}
    	</div>
    @endif

    @if (session('status-danger'))
    	<div class="alert alert-warning">
        	{{session('status-danger')}}
    	</div>
    @endif

    <section class="invoice">
    	<form action="" method="POST">
    		<input type="hidden" name="_token" value="{!! csrf_token() !!}">
	        <div class="row">
	            <div class="col-xs-12">
	                <h2 class="page-header">
	                    <i class="fa fa-globe">
	                    </i>
	                    Asociación de Ganaderos
	                    <small class="pull-right">
	                        Fecha Actual: {{date('d-m-Y')}}
	                    </small>
	                </h2>
	            </div>
	        </div>

	        <div class="row invoice-info">
	            <div class="col-sm-4 invoice-col">
	                <div class="form-group">
	                  	<label for="socio" class="control-label">Cliente</label>
	                   	<select name="socio" id="socio" class="form-control" required>
	                   		<option value="">Seleccione una opcion</option>
	                   		@foreach ($socios as $socio)
	                   			<option value="{{ $socio->id }}">{{ $socio->nombre }}</option>
	                   		@endforeach
	                   	</select>
	                </div>
	            </div>
	            <div class="col-sm-4 invoice-col">
	                <div class="form-group">
	                  	<label for="iva" class="control-label">Ivas</label>
	                   	<select name="iva" id="iva" class="form-control" required>
	                   		<option value="">Seleccione una opcion</option>
	                   		@foreach ($ivas as $iva)
	                   			<option value="{{ $iva->id }}">{{ $iva->nombre }}</option>
	                   		@endforeach
	                   	</select>
	                </div>
	            </div>
	            <div class="col-sm-4 invoice-col">
	                <button class="btn btn-primary pull-right" id="AddItem" type="button">Añadir Item <i class="fa fa-plus" aria-hidden="true"></i></button>
	            </div>
	        </div>

	        <br><br>

	        <div class="row">
	            <div class="col-xs-12 table-responsive">
	                <table class="table table-striped">
	                    <thead>
	                        <tr>
	                            <th>Tipo de Producto</th>
	                            <th>Tipo de Medida</th>
	                            <th>Tipo de Categoria</th>
	                            <th>Precio</th>
	                            <th>Cantidad</th>
	                            <th>Opcion</th>
	                        </tr>
	                    </thead>
	                    <tbody id="seccion_items"></tbody>
	                </table>
	            </div>
	        </div>

	        <br><br>

	        <div class="row no-print">
	            <div class="col-xs-12">
	                <button class="btn btn-success pull-right" type="submit">
	                    <i class="fa fa-credit-card">
	                    </i>
	                    Enviar Presupuesto
	                </button>
	            </div>
	        </div>
        </form>
    </section>
</div>
<script>
	$(document).ready(function() {
		var i = 0;
		$('#AddItem').click(function(event) {

			var html = '<tr id="item'+i+'">';

            	html += '<td>';
            		html += '<select name="produccion'+i+'" class="form-control" required>';
                   		html += '<option value="">Seleccione una opcion</option>';
                   		@foreach ($tipo_producciones as $tipo_produccion)
	                   		html += '<option value="{{ $tipo_produccion->id }}">{{ $tipo_produccion->nombre }}</option>';
	                   	@endforeach
                   	html += '</select>';
            	html += '</td>';

            	html += '<td>';
            		html += '<select name="medida'+i+'" class="form-control" required>';
                   		html += '<option value="">Seleccione una opcion</option>';
                   		@foreach ($tipo_mediciones as $tipo_medicion)
	                   		html += '<option value="{{ $tipo_medicion->id }}">{{ $tipo_medicion->nombre }}</option>';
	                   	@endforeach
                   	html += '</select>';
            	html += '</td>';

            	html += '<td>';
            		html += '<select name="categoria'+i+'" class="form-control" required>';
                   		html += '<option value="">Seleccione una opcion</option>';
                   		@foreach ($categorias as $categoria)
	                   		html += '<option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>';
	                   	@endforeach
                   	html += '</select>';
            	html += '</td>';

            	html += '<td>';
            		html += '<input type="text" class="form-control precio" name="precio'+i+'" required />';
            	html += '</td>';

            	html += '<td>';
            		html += '<input type="text" class="form-control precio" name="cantidad'+i+'" required />';
            	html += '</td>';

            	html += '<td>';
            		html += '<button type="button" class="btn btn-danger btn-sm quitar"><i class="fa fa-times" aria-hidden="true"></i></button>';
            	html += '</td>';

            html += '</tr>';

            $('#seccion_items').append(html);

            i++;
		});

        $(document).on('click', '.quitar', function(event) {
            let id = this.id;
            $(this).closest('tr').remove();
        });
	});
</script>
@endsection
