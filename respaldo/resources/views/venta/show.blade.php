@extends('master')
@section('title', 'Factura de Venta - '. $datos->fecha )
@section('active-venta', 'active')
@section('active-venta-factura', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Factura de Venta
        </h1>
        <ol class="breadcrumb">
            <li class="">
                <a href="{{ url('/') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
            <li class="">
                <a href="{{ url()->previous() }}">
                    Ventas
                </a>
            </li>
            <li class="active">
                <a href="#">
                    {{ $datos->fecha }}
                </a>
            </li>
        </ol>
    </section>
    @foreach($errors->all() as $error)
    <p class="alert alert-danger">
        {{$error}}
    </p>
    @endforeach

		@if (session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
    @endif


    <section class="invoice" id="factura">

        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe">
                    </i>
                    Asociación de Ganaderos
                    <small class="pull-right">
                        Fecha Actual: {{date('d-m-Y')}}
                    </small>
                </h2>
            </div>
        </div>

        <div class="row invoice-info">

            <div class="col-sm-4 invoice-col">
                Cliente
                <address>
                    <strong>
                        {{ ($datos->socio_id == null) ? 'NO HAY SOCIO' : $datos->socio->nombre }}
                    </strong>
                    <br>
                        Telefono: {{ ($datos->socio_id == null) ? 'NO HAY TELEFONO' : $datos->socio->telefono }}

                        <br>
                            Email: {{ ($datos->socio_id == null) ? 'NO HAY EMAIL' : $datos->socio->email }}
                        </br>
                    </br>
                </address>
            </div>

            <div class="col-sm-4 invoice-col"></div>

            <div class="col-sm-4 invoice-col">
                <b>
                    factura #00{{$datos->codigo_factura}}
                </b>
                <br>
                    <br>
                    <b>Orden:</b>
                    {{$datos->codigo_orden}}
                   	<br>
                    <b>Fecha de la Factura</b>
                    {{$datos->fecha}}
                    </br>
                    <b>Status:</b> 
                    @if($datos->status == 1)
						<small class="label text-center bg-yellow">PENDIENTE</small>
					@endif

					@if($datos->status == 0)
						<small class="label text-center bg-red">CANCELADA</small>
					@endif

					@if($datos->status == 2)
						<small class="label text-center bg-green">ACEPTADA</small>
					@endif
                    </br>
                </br>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Tipo de Producto</th>
                            <th>Tipo de Medida</th>
                            <th>Tipo de Categoria</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Sub-total</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($items as $item)
                        {{-- {{dd($item)}} --}}
	                        <tr>
                                <td>
                                    @if ($item->tipo_produccion_id == null)
                                        <small class="label text-center bg-red">NO HAY PRODUCTO</small>
                                    @else
                                        {{$item->tipo_produccion->nombre}}
                                    @endif
                                </td>
                                <td>
                                    @if ($item->tipo_medicion_id == null)
                                        <small class="label text-center bg-red">NO HAY MEDICION</small>
                                    @else
                                        {{$item->tipo_medicion->nombre}}
                                    @endif
                                </td>
	                        	<td>
                                    @if ($item->categoria_id == null)
                                        <small class="label text-center bg-red">NO HAY CATEGORIA</small>
                                    @else
                                        {{$item->categoria->nombre}}
                                    @endif
                                </td>
	                        	<td>$ {{ $item->precio }}</td>
	                        	<td>{{ $item->cantidad }}</td>
	                        	<td>$ {{ $item->precio * $item->cantidad }}</td>
	                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6 pull-right">
                <p class="lead">
                    Fecha: {{$datos->fecha}}
                </p>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">
                                Subtotal:
                            </th>
                            <td>
                                ${{$datos->sub_total}}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Iva ({{$datos->iva->valor}}%)
                            </th>
                            <td>
                                ${{$datos->total_iva}}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Total:
                            </th>
                            <td>
                                ${{$datos->total}}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>

        <div class="row no-print">
            <div class="col-xs-12">
                <a class="btn btn-default" href="#" role="button" onclick="window.print(); return false;">
                    <i class="fa fa-print">
                    </i>
                    Imprimir
                </a>

				@if($datos->status == 1)
	                <button class="btn btn-danger pull-right" type="button" id="cancelar">
	                    <i class="fa fa-ban">
	                    </i>
	                    Cancelar
	                </button>
	                <button class="btn btn-primary pull-right" style="margin-right: 5px;" type="button" id="aceptar">
	                    <i class="fa fa-check">
	                    </i>
	                    Aceptar
	                </button>
				@endif

            </div>
        </div>
    </section>

	<script>
		$(document).ready(function() {
			$('#aceptar').click(function(event) {
	            var parametros = {'codigo' : {{$datos->id}} };
	            $.ajax({
	            	headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
	                url: '/facturas-ventas/update/{{$datos->id}}',
	                data: parametros,
	                type: 'POST',
	                dataType: 'JSON',
	                beforeSend: function(){
	                    $("#cargando").html("<i class='fa fa-refresh fa-spin fa-3x fa-fw'></i>");
	                },
	                success: function (data){
                        setTimeout("location.href='/facturas-ventas' ", 500);
	                },
	            });
	        });

	        $('#cancelar').click(function(event) {
	            var parametros = {'codigo' : {{$datos->id}} };
	            $.ajax({
	            	headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
	                url: '/facturas-ventas/destroy/{{$datos->id}}',
	                data: parametros,
	                type: 'POST',
	                dataType: 'JSON',
	                beforeSend: function(){
	                    $("#cargando").html("<i class='fa fa-refresh fa-spin fa-3x fa-fw'></i>");
	                },
	                success: function (data){
	                	setTimeout("location.href='/facturas-ventas' ", 500);
	                },
	            });

	        });
		});
	</script>

</div>
@endsection
