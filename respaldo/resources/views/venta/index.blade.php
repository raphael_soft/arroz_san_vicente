@extends('master')
@section('title', 'Facturas de Ventas')
@section('active-venta', 'active')
@section('active-venta-factura', 'active')
@section('content')


	<div class="content-wrapper">

	    <section class="content-header">
	        <h1>
	            Facturas de Ventas
	        </h1>
	        <ol class="breadcrumb">
	            <li class="">
	                <a href="{{ url('/"') }}">
	                    <i class="fa fa-dashboard">
	                    </i>
	                    Inicio
	                </a>
	            </li>
	            <li class="active">
	                <a href="#">
	                    Facturas de Ventas
	                </a>
	            </li>
	        </ol>
	    </section>

	    @if (session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		@endif
		@if (session('status-error'))
			<div class="alert alert-danger">
				{{session('status')}}
			</div>
		@endif

	    <section class="content">
	    	<div class="row">
	    		<div class="col-xs-12">
	    			<div class="box">
	    				<div class="box-header">
	    					<h3 class="box-title"><a href="/facturas-ventas/nueva-factura-venta" class="btn btn-primary">Crear Nueva</a></h3>
	    				</div>
	    				<div class="box-body">
	    					<div class="table-responsive">
	    						<table class="table table-bordered table-hover informacion">
	    							<thead>
	    								<th>Cliente</th>
	    								<th>Codigo de Factura</th>
	    								<th>Orden de Factura</th>
	    								<th>Fecha</th>
	    								<th>Total</th>
	    								<th>Status</th>
	    							</thead>
	    							<tbody>
	    								@foreach($datos as $dato)
		    								<tr>
		    									<td><a href="{!! action('VentaController@show', $dato->codigo_factura) !!}">@if ($dato->socio_id== null) <small class="label text-center bg-red">NO HAY SOCIO</small> @else {{$dato->socio->nombre}} @endif </a></td>
		    									<td><a href="{!! action('VentaController@show', $dato->codigo_factura) !!}">#00{{ $dato->codigo_factura }}</a></td>
		    									<td><a href="{!! action('VentaController@show', $dato->codigo_factura) !!}">{{ $dato->orden_factura }}</a></td>
		    									<td><a href="{!! action('VentaController@show', $dato->codigo_factura) !!}">{{ $dato->fecha }}</a></td>
		    									<td><a href="{!! action('VentaController@show', $dato->codigo_factura) !!}">$ {{ $dato->total }}</a></td>
		    									@if($dato->status == 1)
		    										<td><a href="{!! action('VentaController@show', $dato->codigo_factura) !!}"><small class="label text-center bg-yellow">PENDIENTE</small></a></td>
		    									@endif

		    									@if($dato->status == 0)
		    										<td><a href="{!! action('VentaController@show', $dato->codigo_factura) !!}"><small class="label text-center bg-red">CANCELADA</small></a></td>
		    									@endif

		    									@if($dato->status == 2)
		    										<td><a href="{!! action('VentaController@show', $dato->codigo_factura) !!}"><small class="label text-center bg-green">ACEPTADA</small></a></td>
		    									@endif
		    								</tr>
		    							@endforeach
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </section>

	    <script>
	        $(document).ready(function(){
	            $('.informacion').DataTable({
	                dom: '<"html5buttons"B>lTfgitp',
	                select: true,
	                buttons: [
	                    // {extend: 'csv', title: 'Lista de Ivas'},
	                    // {extend: 'excel', title: 'Lista de Ivas'},
	                    // {extend: 'pdf', title: 'Lista de Ivas'},

	                    // {extend: 'print',
	                    //     customize: function (win){
	                    //         $(win.document.body).addClass('white-bg');
	                    //         $(win.document.body).css('font-size', '10px');
	                    //         $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
	                    //     }
	                    // }
	                ],
	            });
	        });

	    </script>
	</div>

@endsection