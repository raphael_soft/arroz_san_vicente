@extends('master')
@section('title', 'Editar Producto')
@section('active-inventario', 'active')
@section('active-inventario-productos', 'active')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edici&oacute;n de Productos
            <small>Secci&oacute;n para editar productos</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Inventario</a></li>
            <li><a href="{{url('productos')}}">Productos</a></li>
            <li class="active">Editar Producto</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario para editar Producto</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                          <!-- Horizontal Form -->
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                            <form  method="POST">
                              <input type="hidden" name="id" value="{{$producto->id}}" class="form-control">
                              <div class="box-body">
                                 <div class="box-body">
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Nombre</label>
                                  <div class="col-sm-10">
                                    <input type="text" name="nombre" value="{{$producto->nombre}}" class="form-control" placeholder="Nombre del Producto">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Precio</label>
                                  <div class="col-sm-10">
                                    <input type="text" name="precio" value="{{$producto->precio}}" class="form-control numeros" placeholder="Precio del Producto">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Proveedor</label>
                                  <div class="col-sm-10">
                                    <select name="proveedor" class="form-control">
                                        <option style="background-color: green; color: white;" value="{{$producto->socio_id}}">{{$socio->nombre}}</option>
                                        <option disabled>&nbsp;</option>
                                        @foreach($socios as $s)
                                        $key++;
                                        <option value="{{$s->id}}">{{$s->nombre}}</option>
                                        @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Status</label>
                                  <div class="col-sm-10">
                                    <select name="status" class="form-control">
                                        @if($producto->status == 1)
                                          <option value="1">Activo</option>
                                          <option value="2">Inactivo</option>
                                        @else
                                          <option value="2">Inactivo</option>
                                          <option value="1">Activo</option>
                                        @endif
                                    </select>
                                   
                                  </div>
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                         </div>
                                  <!-- /.box-body -->
                          <div class="box-footer">
                            <a href="{{url('show_producto',$producto->id)}}" type="submit" class="btn btn-default"><< Volver</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-upload"></i> Guardar</button>
                          </div>
                  <!-- /.box-footer -->
                        </form>
                </div>
                <!-- /.box -->
              </div>
            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->

        </section>
        <!-- /.content -->
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista negra de guardias'},
                    {extend: 'excel', title: 'Lista negra de guardias'},
                    {extend: 'pdf', title: 'Lista negra de guardias'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>
@endsection
