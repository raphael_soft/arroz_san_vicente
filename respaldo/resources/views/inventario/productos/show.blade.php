@extends('master')
@section('title', 'Ver Producto')
@section('active-inventario', 'active')
@section('active-inventario-productos', 'active')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Datos del Producto
            <small>Secci&oacute;n para visualizar los datos del Producto</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Asociaci&oacute;n</a></li>
            <li><a href="{{url('productos')}}">Producto</a></li>
            <li class="active">Ver Producto</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario de datos del Producto</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                          <!-- Horizontal Form -->
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="POST">
                          <div class="box-body">
                             <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Nombre</label>
                              <div class="col-sm-10">
                                <input  readonly type="text" name="nombre" value="{{$producto->nombre}}" class="form-control" placeholder="Nombre del Producto">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Precio</label>
                              <div class="col-sm-10">
                                <input readonly type="text" name="precio" value="{{$producto->precio}}" class="form-control numeros" placeholder="Precio del Producto">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Proveedor</label>
                              <div class="col-sm-10">
                                <input readonly type="text" name="proveedor" value="{{$socio->nombre}}" class="form-control numeros" placeholder="Precio del Producto">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Status</label>
                              <div class="col-sm-10">
                                @if($producto->status == 1)
                                <input readonly="readonly" value="Activo" type="text" name="status" class="form-control" placeholder="Status">
                                @else
                                <input readonly="readonly" value="Inactivo" type="text" name="status" class="form-control" placeholder="Status">
                                @endif
                              </div>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                     </div>
                          <!-- /.box-body -->
                  <div class="box-footer col-sm-12 text-center">
                    <a href="{{url('/productos')}}"  class="btn btn-default"><< Volver</a>
                    <a href="{{url('/edit_producto',$producto->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                    <a href="{{url('/destroys',$producto->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>

                  </div>
                  <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>



          </div>
          <!-- /.box -->

        </section>
        <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista negra de guardias'},
                    {extend: 'excel', title: 'Lista negra de guardias'},
                    {extend: 'pdf', title: 'Lista negra de guardias'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>
@endsection
