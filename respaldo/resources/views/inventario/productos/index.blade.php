@extends('master')
@section('title', 'Productos')
@section('active-inventario', 'active')
@section('active-inventario-productos', 'active')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Productos
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="{{ url('/"') }}">
                    <i class="fa fa-dashboard">
                    </i>
                    Inicio
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado de Productos</h3>
              
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
              <div><a href="{{url('create_producto')}}" class="btn btn-primary" style="float: right;">Nuevo Producto</a></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped informe">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Proveedor</th>
                    <th>Status</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>

                @foreach($productos as $p)
                    <?php
                       $proveedor = DB::table('socios')->where('id', $p->socio_id)->get();
                       $datos = json_decode($proveedor,true);?>
                  <tr>
                    <td>{{$p->nombre}}</td>
                    <td>{{$p->precio}}</td>
                    <td>{{$datos[0]['nombre']}}</td>
                    <td>
                        @if ($p->status == 1)
                            Activo
                        @else
                            Inactivo
                        @endif
                    </td>
                    <td><a href="{{url('show_producto',$p->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i> Ver</a></td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
</div>
    <script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista Reses'},
                    {extend: 'excel', title: 'Lista Reses'},
                    {extend: 'pdf', title: 'Lista Reses'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>
@endsection
