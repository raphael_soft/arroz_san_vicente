@extends('master')
@section('title', 'Registrar Producto')
@section('active-inventario', 'active')
@section('active-inventario-productos', 'active')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Registro de Productos
            <small>Secci&oacute;n para el registro de producto</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/"') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#">Inventario</a></li>
            <li><a href="{{url('productos')}}">Productos</a></li>
            <li class="active">Nuevo Producto</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario para el registro de productos</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                          <!-- Horizontal Form -->
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                       <div class="alert alert-success">
                          {{session('status')}}
                       </div>
                   @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="POST">
                          <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Nombre</label>
                              <div class="col-sm-10">
                                <input type="text" name="nombre" value="{{ old('nombre') }}" class="form-control" placeholder="Nombre del Producto">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Precio</label>
                              <div class="col-sm-10">
                                <input type="text" name="precio" value="{{ old('precio') }}" class="form-control numeros" placeholder="Precio del Producto">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Proveedor</label>
                              <div class="col-sm-10">
                                <select name="proveedor" class="form-control">
                                    <option value="">Escoja un Proveedor</option>
                                    @foreach($socio as $s)
                                    <option value="{{$s->id}}">{{$s->nombre}}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                          </div>
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                             </div>
                                  <!-- /.box-body -->
                          <div class="box-footer">
                            <a href="{{url('/productos')}}" type="submit" class="btn btn-default"><< Volver</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-upload"></i> Registrar</button>
                          </div>
                  <!-- /.box-footer -->
                        </form>
                      </div>
                      <!-- /.box -->
                    </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              Footer
            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->

        </section>
        <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
