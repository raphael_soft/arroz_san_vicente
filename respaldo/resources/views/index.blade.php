@extends('master')
@section('title', 'Inicio')
{{-- @section('class_active_home', 'active') --}}
@section('content')
<div class="content-wrapper">

    <section class="content-header">
        <h1>
            Inicio
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="/">
                    <i class="fa fa-dashboard">
                    </i>
                    Home
                </a>
            </li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            {{$user}}
                        </h3>
                        <p>
                            Usuarios
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users">
                        </i>
                    </div>
                    <a class="small-box-footer" href="/configuracion/usuario">
                        Mas Informacion
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            {{$produccion}}
                        </h3>
                        <p>
                            Produccion
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-network">
                        </i>
                    </div>
                    <a class="small-box-footer" href="/produccion">
                        Mas Informacion
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            {{$socio}}
                        </h3>
                        <p>
                            Socios
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{ url('/socios') }}">
                        Mas Informacion
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>
                            {{$res}}
                        </h3>
                        <p>
                            reses
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-paw">
                        </i>
                    </div>
                    <a class="small-box-footer" href="{{ url('/reses') }}">
                        Mas Informacion
                        <i class="fa fa-arrow-circle-right">
                        </i>
                    </a>
                </div>
            </div>

            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        
                <script type="text/javascript">
                    Highcharts.chart('container', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Estadisticas de Ingresos: <b>{{$anio_actual}}</b>'
                        },
                        subtitle: {
                            text: 'Cantidad de Ingresos'
                        },
                        xAxis: {
                            categories: [
                                'Ene',
                                'Feb',
                                'Mar',
                                'Abr',
                                'May',
                                'Jun',
                                'Jul',
                                'Ago',
                                'Sep',
                                'Oct',
                                'Nov',
                                'Dic'
                            ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Ingresos'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} $</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: 'Ingresos',
                            data: [
                            
                                //ENERO
                                {{$ventas_enero}}, 

                                //FEBRERO
                                {{$ventas_febrero}}, 

                                //MARZO
                                {{$ventas_marzo}}, 

                                //ABRIL
                                {{$ventas_abril}}, 

                                //MAYO
                                {{$ventas_mayo}},

                                //JUNIO 
                                {{$ventas_junio}}, 

                                //JULIO
                                {{$ventas_julio}}, 

                                //AGOSTO
                                {{$ventas_agosto}}, 

                                //SEPTIEMBRE
                                {{$ventas_septiembre}}, 

                                //OCTUBRE
                                {{$ventas_octubre}}, 

                                //NOVIEMBRE
                                {{$ventas_noviembre}}, 

                                //DICIEMBRE
                                {{$ventas_diciembre}}

                            ]

                        }]
                    });
                </script>
            </div>

            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        
                <script type="text/javascript">
                    Highcharts.chart('container2', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Estadisticas de Egresos: <b>{{$anio_actual}}</b>'
                        },
                        subtitle: {
                            text: 'Cantidad de Egresos'
                        },
                        xAxis: {
                            categories: [
                                'Ene',
                                'Feb',
                                'Mar',
                                'Abr',
                                'May',
                                'Jun',
                                'Jul',
                                'Ago',
                                'Sep',
                                'Oct',
                                'Nov',
                                'Dic'
                            ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Egresos'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} $</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: 'Egresos',
                            data: [
                            
                                //ENERO
                                {{$ventas_mes_enero}}, 

                                //FEBRERO
                                {{$ventas_mes_febrero}}, 

                                //MARZO
                                {{$ventas_mes_marzo}}, 

                                //ABRIL
                                {{$ventas_mes_abril}}, 

                                //MAYO
                                {{$ventas_mes_mayo}},

                                //JUNIO 
                                {{$ventas_mes_junio}}, 

                                //JULIO
                                {{$ventas_mes_julio}}, 

                                //AGOSTO
                                {{$ventas_mes_agosto}}, 

                                //SEPTIEMBRE
                                {{$ventas_mes_septiembre}}, 

                                //OCTUBRE
                                {{$ventas_mes_octubre}}, 

                                //NOVIEMBRE
                                {{$ventas_mes_noviembre}}, 

                                //DICIEMBRE
                                {{$ventas_mes_diciembre}}

                            ]

                        }]
                    });
                </script>
            </div>
        </div>
        
    </section>
</div>
@endsection
