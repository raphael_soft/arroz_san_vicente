<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', function () {
    return view('welcome');
});


Route::get('/email_test', function () {
    return view('emails.test');
});

// home
Route::get('/', 'PanelController@Home')->middleware('auth');

// inicio de sesion
Route::get('/inicio', 'LoginController@inicio');
Route::post('/inicio', 'LoginController@authenticate');
Route::get('/salir', 'LoginController@logout');


// CONFIGURACION
Route::group(['middleware' => 'auth'], function() {
	Route::prefix('configuracion')->group(function () {

		// IVA
	    Route::get('/iva', 'IvaController@index');
			Route::prefix('iva')->group(function () {
				// crear
				Route::get('/nuevo-iva', 'IvaController@create');
				// guardar
				Route::post('/nuevo-iva', 'IvaController@store');
				// mostrar
				Route::get('{id?}', 'IvaController@show');
				// edit
				Route::get('/editar-iva/{id?}', 'IvaController@edit');
				// update
				Route::post('/editar-iva/{id?}', 'IvaController@update');
				// destroy
				Route::post('/destroy/{id?}', 'IvaController@destroy');
			});

	    // ROLES
	    Route::get('/roles', 'RolController@index');
	    	Route::prefix('roles')->group(function () {
				// crear
				Route::get('/nuevo-rol', 'RolController@create');
				// guardar
				Route::post('/nuevo-rol', 'RolController@store');
				// mostrar
				Route::get('{id?}', 'RolController@show');
				// edit
				Route::get('/editar-rol/{id?}', 'RolController@edit');
				// update
				Route::post('/editar-rol/{id?}', 'RolController@update');
				// destroy
				Route::post('/destroy/{id?}', 'RolController@destroy');
			});

	    // USUARIOS
	    Route::get('/usuario', 'UsuarioController@index');
	    	Route::prefix('usuario')->group(function () {
				// crear
				Route::get('/nuevo-usuario', 'UsuarioController@create');
				// guardar
				Route::post('/nuevo-usuario', 'UsuarioController@store');
				// mostrar
				Route::get('{id?}', 'UsuarioController@show');
				// edit
				Route::get('/editar-usuario/{id?}', 'UsuarioController@edit');
				// update
				Route::post('/editar-usuario/{id?}', 'UsuarioController@update');
				// destroy
				Route::post('/destroy/{id?}', 'UsuarioController@destroy');
			});

	    // PERMISOS
	    Route::get('/permisos', 'PermisoController@index');

	    // CATEGORIAS
	    Route::get('/categorias', 'CategoriaController@index');
	    	Route::prefix('categorias')->group(function () {
				// crear
				Route::get('/nueva-categoria', 'CategoriaController@create');
				// guardar
				Route::post('/nueva-categoria', 'CategoriaController@store');
				// mostrar
				Route::get('{id?}', 'CategoriaController@show');
				// edit
				Route::get('/editar-categoria/{id?}', 'CategoriaController@edit');
				// update
				Route::post('/editar-categoria/{id?}', 'CategoriaController@update');
				// destroy
				Route::post('/destroy/{id?}', 'CategoriaController@destroy');
			});

	    // TIPO DE MEDICION
	    Route::get('/tipo-de-mediciones', 'TipoMedicionController@index');
	    	Route::prefix('tipo-de-mediciones')->group(function () {
				// crear
				Route::get('/nueva-medicion', 'TipoMedicionController@create');
				// guardar
				Route::post('/nueva-medicion', 'TipoMedicionController@store');
				// mostrar
				Route::get('{id?}', 'TipoMedicionController@show');
				// edit
				Route::get('/editar-medicion/{id?}', 'TipoMedicionController@edit');
				// update
				Route::post('/editar-medicion/{id?}', 'TipoMedicionController@update');
				// destroy
				Route::post('/destroy/{id?}', 'TipoMedicionController@destroy');
			});

	    // TIPO DE PRODUCCION
	    Route::get('/tipo-de-produccion', 'TipoProduccionController@index');
	    	Route::prefix('tipo-de-produccion')->group(function () {
				// crear
				Route::get('/nueva-produccion', 'TipoProduccionController@create');
				// guardar
				Route::post('/nueva-produccion', 'TipoProduccionController@store');
				// mostrar
				Route::get('{id?}', 'TipoProduccionController@show');
				// edit
				Route::get('/editar-produccion/{id?}', 'TipoProduccionController@edit');
				// update
				Route::post('/editar-produccion/{id?}', 'TipoProduccionController@update');
				// destroy
				Route::post('/destroy/{id?}', 'TipoProduccionController@destroy');
			});
	});
});

// PRODUCCION
Route::get('/produccion', 'ProduccionController@index');
Route::group(['middleware' => 'auth'], function() {
	Route::prefix('produccion')->group(function () {
		// crear
		Route::get('/nueva-produccion', 'ProduccionController@create');
		// guardar
		Route::post('/nueva-produccion', 'ProduccionController@store');
		Route::post('/get_reses', 'ProduccionController@GetReses');
		// mostrar
		Route::get('{id?}', 'ProduccionController@show');
		// edit
		Route::get('/editar-produccion/{id?}', 'ProduccionController@edit');
		// update
		Route::post('/editar-produccion/{id?}', 'ProduccionController@update');
		// destroy
		Route::post('/destroy/{id?}', 'ProduccionController@destroy');
	});
});

// FACTURAS DE VENTAS
Route::get('/facturas-ventas', 'VentaController@index');
Route::group(['middleware' => 'auth'], function() {
	Route::prefix('facturas-ventas')->group(function () {
		Route::get('/nueva-factura-venta', 'VentaController@create');
		Route::post('/nueva-factura-venta', 'VentaController@store');
		Route::get('{id?}', 'VentaController@show');
		Route::post('/update/{id}', 'VentaController@update');
		Route::post('/destroy/{id}', 'VentaController@destroy');
	});
});

// ESTADISTICA DE VENTA
Route::get('/estadistica-de-ventas', 'VentaController@estadistica')->middleware('auth');
Route::post('/estadistica-de-ventas', 'VentaController@consultaanio')->middleware('auth');

Route::group(['middleware' => 'auth'], function() {
	Route::prefix('estadisticas')->group(function () {
		Route::get('/ingreso', 'EstadisticaController@ingreso');
		Route::post('/ingreso', 'EstadisticaController@get_ingreso');
		// 
		Route::get('/egreso', 'EstadisticaController@egreso');
		Route::post('/egreso', 'EstadisticaController@get_egreso');
		// 
		Route::get('/produccion', 'EstadisticaController@produccion');
		Route::post('/produccion', 'EstadisticaController@get_produccion');
	});
});

//MODULO SOCIO

	//INDEX
	Route::get('/socios', 'SocioController@index')->middleware('auth');

	//CREATE
	Route::get('/create_socio', 'SocioController@create')->middleware('auth');
	Route::post('/create_socio', 'SocioController@store')->middleware('auth');
		
	//EDIT
	Route::get('/edit_socio/{id}', 'SocioController@edit')->middleware('auth');
	Route::post('/edit_socio/{id}', 'SocioController@update')->middleware('auth');
		
	// SHOW
	Route::get('/show_socio/{id}', 'SocioController@show')->middleware('auth');

	// DELETE
	Route::get('/destroy_socio/{id}', 'SocioController@destroy')->middleware('auth');


	//ROUTES MODULO RESES

	//INDEX
	Route::get('/reses', 'ResController@index')->middleware('auth');

	//CREATE
	Route::get('/create_res', 'ResController@create')->middleware('auth');
	Route::post('/create_res', 'ResController@store')->middleware('auth');
		
	//EDIT
	Route::get('/edit_re/{id}', 'ResController@edit')->middleware('auth');
	Route::post('/edit_re/{id}', 'ResController@update')->middleware('auth');
		
	//SHOW
	Route::get('/show_res/{id}', 'ResController@show')->middleware('auth');
		
	//DELETE
	Route::get('/destroy_res/{id}', 'ResController@destroy')->middleware('auth');


//MODULO PRODUCTOS

	//ROUTES PRODUCTOS
	
	//INDEX
	Route::get('/productos', 'ProductoController@index')->middleware('auth');

	//SHOW
	Route::get('/show_producto/{id}', 'ProductoController@show')->middleware('auth');

	//EDIT
	Route::get('/edit_producto/{id}', 'ProductoController@edit')->middleware('auth');
	Route::post('/edit_producto/{id}', 'ProductoController@update')->middleware('auth');

	//CREATE GET
	Route::get('/create_producto', 'ProductoController@create')->middleware('auth');
	Route::post('/create_producto', 'ProductoController@store')->middleware('auth');

	//DESTROY
	Route::get('/destroys/{id}', 'ProductoController@destroys')->middleware('auth');


//MODULO FACTURACION COMPRAS

	//INDEX
	Route::get('/facturascompras', 'FacturacompraController@index')->middleware('auth');

	//SHOW
	Route::get('/show_facturacompra/{id}', 'FacturacompraController@show')->middleware('auth');

	//EDIT
	Route::get('/edit_facturacompra/{id}', 'FacturacompraController@index@edit')->middleware('auth');
	Route::post('/edit_facturacompra/{id}', 'FacturacompraController@update')->middleware('auth');

	//CREATE GET
	Route::get('/create_facturacompra', 'FacturacompraController@create')->middleware('auth');
	Route::post('/create_facturacompra', 'FacturacompraController@store')->middleware('auth');

	//DESTROY
	Route::get('/destroy/{id}', 'FacturacompraController@destroy')->middleware('auth');

	//GET PRODUCTO
	Route::post('/get_producto', 'FacturacompraController@GetProductos')->middleware('auth');

	//STATUS ACEPTADO
	Route::get('/status_aceptar/{id}', 'FacturacompraController@StatusAceptar')->middleware('auth');

	//STATUS RECHAZADO
	Route::get('/status_cancelar/{id}', 'FacturacompraController@StatusCalcelar')->middleware('auth');

	//IMPRIMIR PDF FACTURA
	Route::get('/imprimir/{id}', 'FacturacompraController@ImprimirPdf')->middleware('auth');


//MODULO ESTADISTICAS COMPRAS
		
	Route::get('/estadistica_compra', 'EstadisticaCompraController@index')->middleware('auth');
	Route::post('/estadistica_compra', 'EstadisticaCompraController@consultaanio')->middleware('auth');


//MODULO INVENTARIO 
		
	Route::get('/inventario', 'InventarioController@index')->middleware('auth');